#!/bin/sh
SCRIPT_DIR="$(cd "$(dirname "$0" )" && pwd )" 

echo $SCRIPT_DIR
#pinout

PINS="-y l 0"
PINS="$PINS -y f 1"
PINS="$PINS -y r 4"
PINS="$PINS -y - 17"
PINS="$PINS -y # 21"
PINS="$PINS -y p 22"
PINS="$PINS -y b 10"
PINS="$PINS -y c 9"
PINS="$PINS -y s 11"
PINS="$PINS -y d 14"
PINS="$PINS -y 0 15"
PINS="$PINS -y 1 18"
PINS="$PINS -y t 23"
PINS="$PINS -y v 24"
PINS="$PINS -y m 25"
PINS="$PINS -y e 7"
PINS="$PINS -y z 8"

DEVICES="-r 1 44100 1 16 -p 1001 44100 2 16 -a 0.03"
MEGAHAL="-m $SCRIPT_DIR/lmnc_lyrics.txt"
DEBUG="-I -G"

CMD="$DEBUG $MEGAHAL $PINS $DEVICES -c -v"

echo $CMD

$SCRIPT_DIR/pibaudot $CMD
