#include <malloc.h>
#include <stdint.h>
#include <string.h>

#include "internal.h"



MemoryBuffer * membuf_init()
{
    MemoryBuffer * membuf = (MemoryBuffer *)malloc(sizeof(MemoryBuffer));
    membuf->contentsz = 0;
    membuf->maxsize = 0;
    membuf->ptr = NULL;
    membuf->blksize = 256;
    return membuf;
}

MemoryBuffer * membuf_init_blk(size_t blksize)
{
    MemoryBuffer * membuf = (MemoryBuffer *)malloc(sizeof(MemoryBuffer));
    membuf->contentsz = 0;
    membuf->maxsize = blksize;
    membuf->ptr = malloc(blksize);
    membuf->blksize = blksize;
    return membuf;
}

void membuf_free(MemoryBuffer * buf)
{
    if(buf->ptr != NULL)
        free(buf->ptr);

    buf->ptr=NULL;
    free(buf);
}

void membuf_append(MemoryBuffer * buf, const void * data, size_t amount)
{
     uint8_t * dataptr ;

    if(buf->ptr == NULL)
    {
        buf->ptr = malloc(amount +  buf->blksize);
        buf->maxsize = amount +  buf->blksize;
    }
    else if((buf->contentsz + amount) > buf->maxsize)
    {
        buf->maxsize = buf->contentsz + amount + buf->blksize;
        buf->ptr = realloc(buf->ptr, buf->maxsize);
    }

    dataptr = (uint8_t *)buf->ptr;
    memcpy(dataptr + buf->contentsz, data, amount);
    buf->contentsz += amount;
}


void membuf_append_value(MemoryBuffer * buf, const void * valueptr, size_t valuesize, size_t repeatCount)
{
    size_t x;
    for(x=0; x < repeatCount; ++x)
    {
        membuf_append(buf, valueptr, valuesize);
    }    
}

void membuf_erase(MemoryBuffer * buf)
{
    if(buf->ptr != NULL)
    {
        memset(buf->ptr,0, buf->maxsize);
    }

    buf->contentsz = 0;
}

void membuf_reset(MemoryBuffer * buf)
{
    membuf_erase(buf);
    if(buf->ptr != NULL)
    {
        free(buf->ptr);
        buf->ptr=NULL;
    }

    buf->maxsize = 0;
}



size_t membuf_fread_append(MemoryBuffer * buf, FILE * file, size_t maxAmountToRead)
{
    uint8_t * dataptr;
    size_t amountRead;

    if(buf->ptr == NULL)
    {
        buf->ptr = malloc(maxAmountToRead +  buf->blksize);
        buf->maxsize = maxAmountToRead +   buf->blksize;
    }
    else if((buf->contentsz + maxAmountToRead) > buf->maxsize)
    {
        buf->maxsize = buf->contentsz + maxAmountToRead + buf->blksize;
        buf->ptr = realloc(buf->ptr, buf->maxsize);
    }

    dataptr = (uint8_t *)buf->ptr;
    amountRead = fread( dataptr + buf->contentsz, 1, maxAmountToRead, file);
    buf->contentsz += amountRead;

    return amountRead;
}

size_t membuf_copyToExternalBuffer(const MemoryBuffer * buf, void * dest, size_t offset, size_t amount)
{
    uint8_t * dataptr;
    size_t amountAvailable, copysz;

    if((offset >= buf->contentsz) || (buf->ptr == NULL))
        return 0;

    amountAvailable = buf->contentsz - offset;
    copysz = (amount > amountAvailable) ? amountAvailable : amount;
    dataptr = (uint8_t *)buf->ptr;
    memcpy(dest, dataptr + offset, copysz);
    return copysz;
}

void membuf_removeFromFront(MemoryBuffer * buf, size_t amount)
{
    uint8_t * dataptr;
    size_t oldContentSz;

    if(buf->ptr == NULL)
        return;

    if(amount >= buf->contentsz)
    {
        memset(buf->ptr,0, buf->contentsz);
        buf->contentsz = 0;
        return;
    }

    dataptr = (uint8_t *)buf->ptr;
    memmove(buf->ptr, dataptr + amount, buf->contentsz - amount);
    oldContentSz = buf->contentsz;
    buf->contentsz -= amount;
    memset(dataptr + buf->contentsz, 0, oldContentSz - buf->contentsz);
}

void membuf_removeFromBack(MemoryBuffer * buf, size_t amount)
{
    uint8_t * dataptr;
    
    if(buf->ptr == NULL)
        return;

    if(amount >= buf->contentsz)
    {
        memset(buf->ptr,0, buf->contentsz);
        buf->contentsz = 0;
        return;
    }

    dataptr = (uint8_t *)buf->ptr;
    memset(dataptr + (buf->contentsz - amount), 0, amount);
    buf->contentsz -= amount;
}

void membuf_append_copy(MemoryBuffer * dest, const MemoryBuffer * src)
{
    if((src->ptr == NULL) || (src->contentsz == 0))
        return;

    membuf_append(dest, src->ptr, src->contentsz);
}


uint8_t * membuf_reserveSpaceForNewData(MemoryBuffer * buf, size_t amount)
{
    uint8_t * dataptr;

    if(buf->ptr == NULL)
    {
        buf->ptr = malloc(amount +  buf->blksize);
        buf->maxsize = amount +  buf->blksize;
    }
    else if((buf->contentsz + amount) > buf->maxsize)
    {
        buf->maxsize = buf->contentsz + amount + buf->blksize;
        buf->ptr = realloc(buf->ptr, buf->maxsize);
    }

    dataptr = (uint8_t *)buf->ptr;
    dataptr += buf->contentsz;
    buf->contentsz += amount;
    memset(dataptr,0,amount);
    return dataptr;
}
