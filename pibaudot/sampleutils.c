#include <stdint.h>
#include <stddef.h>
#include <string.h>

#define _USE_MATH_DEFINES
#include <math.h>


#include <stdio.h>
#include "internal.h"


size_t smputil_seekBackToSilenceEnd(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset, size_t searchEnd, double silenceThreshold)
{
    size_t x;

    for(x=currentOffset; x > searchEnd; --x)
    {
        if(fabs(sampleData[x]) <= silenceThreshold)
        {
            return x;
        }
    }

    return currentOffset;
}

enum BOOLEAN_VALUE smputil_findNextNonSilence(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset,  double silenceThreshold, size_t * retNonSilencePos)
{
    size_t x;

    for(x=currentOffset; x < numSampleValuesInBuffer; ++x)
    {
        if(fabs(sampleData[x]) > silenceThreshold)
        {
            *retNonSilencePos = x;
            return BOOL_TRUE;
        }
    }

    return BOOL_FALSE;
}


size_t smputil_findWindowEndEdge(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset,  size_t numValuesInWindow,  double zeroThreshold )
{
    //Search the last half of the previous window, and the first half of the current window
    const size_t overlapSize = (numValuesInWindow / 2);
    const size_t searchEnd = currentOffset + overlapSize;
    size_t searchPos;
    double lowestValue = 0;
    double val;

    for(searchPos = currentOffset; (searchPos < numSampleValuesInBuffer) && (searchPos < searchEnd); ++searchPos)
    {
        val = fabs(sampleData[searchPos]);
        if(val <= zeroThreshold)
        {
            return searchPos;
        }
        
    }

    return currentOffset;
}

size_t smputil_findWindowStartEdge(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset,  size_t numValuesInWindow,  double zeroThreshold )
{
    //Search the last half of the previous window, and the first half of the current window
    const size_t overlapSize = (numValuesInWindow / 2);
    const size_t searchEnd = (currentOffset >= overlapSize) ? (currentOffset - overlapSize) : 0;
    size_t searchPos;
    double val;

    for(searchPos = currentOffset; searchPos > 0; --searchPos)
    {
        val = fabs(sampleData[searchPos]);
        if(val <= zeroThreshold)
        {
            return searchPos;
        }
        
    }

    return currentOffset;
}


size_t smputil_convertSampleBytesToDoubles(const uint8_t * sampleBuffer, size_t dataSizeBytes, enum WAV_FORMAT format, unsigned int channels, unsigned int sampleRate, unsigned int bitsPerSample, 
                                           double silenceThreshold, MemoryBuffer * destBuffer, enum BOOLEAN_VALUE * retClipping, enum BOOLEAN_VALUE * retSilence)
{
    static const short ulaw_decode[256] = {
    -32124, -31100, -30076, -29052, -28028, -27004, -25980, -24956,
    -23932, -22908, -21884, -20860, -19836, -18812, -17788, -16764,
    -15996, -15484, -14972, -14460, -13948, -13436, -12924, -12412,
    -11900, -11388, -10876, -10364,  -9852,  -9340,  -8828,  -8316,
     -7932,  -7676,  -7420,  -7164,  -6908,  -6652,  -6396,  -6140,
     -5884,  -5628,  -5372,  -5116,  -4860,  -4604,  -4348,  -4092,
     -3900,  -3772,  -3644,  -3516,  -3388,  -3260,  -3132,  -3004,
     -2876,  -2748,  -2620,  -2492,  -2364,  -2236,  -2108,  -1980,
     -1884,  -1820,  -1756,  -1692,  -1628,  -1564,  -1500,  -1436,
     -1372,  -1308,  -1244,  -1180,  -1116,  -1052,   -988,   -924,
      -876,   -844,   -812,   -780,   -748,   -716,   -684,   -652,
      -620,   -588,   -556,   -524,   -492,   -460,   -428,   -396,
      -372,   -356,   -340,   -324,   -308,   -292,   -276,   -260,
      -244,   -228,   -212,   -196,   -180,   -164,   -148,   -132,
      -120,   -112,   -104,    -96,    -88,    -80,    -72,    -64,
       -56,    -48,    -40,    -32,    -24,    -16,     -8,      0,
     32124,  31100,  30076,  29052,  28028,  27004,  25980,  24956,
     23932,  22908,  21884,  20860,  19836,  18812,  17788,  16764,
     15996,  15484,  14972,  14460,  13948,  13436,  12924,  12412,
     11900,  11388,  10876,  10364,   9852,   9340,   8828,   8316,
      7932,   7676,   7420,   7164,   6908,   6652,   6396,   6140,
      5884,   5628,   5372,   5116,   4860,   4604,   4348,   4092,
      3900,   3772,   3644,   3516,   3388,   3260,   3132,   3004,
      2876,   2748,   2620,   2492,   2364,   2236,   2108,   1980,
      1884,   1820,   1756,   1692,   1628,   1564,   1500,   1436,
      1372,   1308,   1244,   1180,   1116,   1052,    988,    924,
       876,    844,    812,    780,    748,    716,    684,    652,
       620,    588,    556,    524,    492,    460,    428,    396,
       372,    356,    340,    324,    308,    292,    276,    260,
       244,    228,    212,    196,    180,    164,    148,    132,
       120,    112,    104,     96,     88,     80,     72,     64,
	    56,     48,     40,     32,     24,     16,      8,      0 };

    const size_t bytes_per_wide_sample = channels * (bitsPerSample / 8);
    const uint64_t signval = (format == WAV_FORMAT_PCM) ? (((uint64_t)1) << (bitsPerSample - 1)) : 32768;
    size_t x;
    double smpval;
    uint64_t v, v2;
    unsigned int c;
    unsigned int shift;
    double averagesmpval;
    size_t samplePos = 0;
    
    *retClipping = BOOL_FALSE;
    *retSilence = BOOL_TRUE;
    
    //Iterate over sample data, converting it into mono doubles
    for(x=0; x < dataSizeBytes; x += bytes_per_wide_sample)
    {
        //Take average of channel values
        averagesmpval = 0;
        for(c=0;c < channels; ++c)
        {
            //Convert from mulaw, using the 
            smpval =0;
            if(format == WAV_FORMAT_ULAW)
            {
                v = sampleBuffer[samplePos];
                smpval = ulaw_decode[v  & 0xFF];
                ++samplePos;
            }
            else
            {
                //PCM
                //Read each value in LSB order
                v = 0;
                for(shift=0; shift < bitsPerSample; shift += 8)
                {
                    v2 = sampleBuffer[samplePos];
                    v |= v2 << shift;
                    ++samplePos;
                }

                //Check for negative, and convert negative values to signed
                if(v & signval)
                {
                    //uint64_t vx = v;
                    
                    v &= (signval - 1);

                    smpval = (double)(signval - v);
                    smpval *= -1;

                    //printf("sample:%llu sig:%llu x:%llu = %f \r\n", vx, signval,  v, smpval);
                }
                else
                {
                    smpval = (double)v;
                }
            }

            averagesmpval += smpval;
        }

        //Generate average of channels and make it a decimal percentage (0 - 1)
        smpval = averagesmpval / (signval * channels);

         //If the sample value is equal to 1 (or very close to it), then count it as a clipped sample
        if(smpval >= 0.95)
            *retClipping = BOOL_TRUE;

        if((smpval >= silenceThreshold) && (*retSilence))
            *retSilence = BOOL_FALSE;
        
        //Write it out
        membuf_append(destBuffer, &smpval, sizeof(smpval));
    }

    //Return number of bytes processed
   return samplePos;
}

size_t smputil_convertDoublesToSampleBytes(const double * sampleData, size_t numSamplesToWrite, unsigned int channels, unsigned int numBitsPerSample, double scaleFactor, uint8_t * outputBuffer, size_t maxOutputSizeBytes)
{
    size_t x;
    double s;
    const int32_t maxSampleValue = (1 << (numBitsPerSample - 1));
    uint64_t outputValue;
    size_t outputpos = 0;
    uint64_t v;
    unsigned int c;
    unsigned int b;

    for(x=0;x < numSamplesToWrite; ++x)
    {
        //Prevent buffer overrun
        if(outputpos >= maxOutputSizeBytes)
            return x;

        //Convert doubles into 16-bit signed values
        s = sampleData[x];
        s *= (maxSampleValue - 1);
        s *= scaleFactor;

        if(s > (maxSampleValue - 1))
            s = (maxSampleValue - 1);
        else if((s < 0) && ((s * -1)  > (maxSampleValue - 1)))
        {
            s = (maxSampleValue - 1);
            s *= -1;
        }

        //Work out the value
        if(s < 0)
        {
            s *= -1;
            outputValue = (uint64_t)s;
            --outputValue;
            outputValue = ~outputValue;
        }
        else
        {
            outputValue = (uint64_t)s;
        }

        //Write out
        for(c=0; c < channels; ++c)
        {
            v = outputValue;
            for(b=0; b < numBitsPerSample; b += 8)
            {
                outputBuffer[outputpos] = (uint8_t)(v & 0xFF);
                v >>= 8;
                ++outputpos;
            }
        }
    }

    return numSamplesToWrite;
}

void smputil_generateToneDoubles(unsigned int freq, unsigned int durationMs,  unsigned int samplesPerSec, double scale, MemoryBuffer * output)
{
    static const double twopi = M_PI * 2;

    //Generate wakeup data...
    const size_t numWakeSamples = (((size_t)durationMs) * ((size_t)samplesPerSec)) / 1000;
    size_t wakepos = 0;
    double s;
    double phaseIncrement = (double)freq / (double)samplesPerSec;
    double phase = 0;
    

    while(wakepos < numWakeSamples)
    {
        s = sin( phase * twopi ) * scale;
        phase += phaseIncrement;
          
        membuf_append( output, &s, sizeof(s));
        ++wakepos;
    }
}
