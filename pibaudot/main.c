#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <malloc.h>

#include "internal.h"
#include "megahal_lib.h"

#ifdef _WIN32
    #include <direct.h>
    #define getcwd _getcwd // stupid MSFT "deprecation" warning
    #include <Windows.h>
#else
    #include <unistd.h>
    #include <sys/sysctl.h>
    #include <time.h>
#endif


//=======================================================================================================================================


static void print_help(BauDot_Decoder_Options * defaults, MegaHALOptions * megahalopts, int pollTimeMs)
{
    printf("\r\npibaudot - Utility to process baudot audio\r\n");
    printf("Version : 0.01\r\n");
    printf("Author  : Mark Collier - 2021  http://www.marcnetsystem.co.uk/\r\n");
    printf("Goertzel: https://exstrom.com/journal/sigproc/goertzel.c\r\n\t\t(C)2006 Exstrom Laboratories LLC\r\n");
    printf("MegaHAL : https://github.com/z0rc/megahal.mod\r\n\t\t\2009 Zev Toledano & (C) 1999 Jason Hutchens\r\n");
    printf("muLaw conversion : Dan Ellis - https://github.com/dpwe/dpwelib\r\n");
    printf("Thanks  : Felix Rieger :)\r\n");
    printf("\r\n");
    printf("\r\n");    
    printf("  -h                : print this help and exit.\r\n");
    printf("  -d                : print list of audio playback & capture devices.\r\n");

    printf("\r\n");    
    printf("Input Modes:\r\n");
    printf("-------------------\r\n");
    printf("  -i <wav filename> : Process audio data from a .wav file.\r\n");
    printf("  -r <device> <sample rate> <channels> <bits per sec>  : \r\n\t\t\t\t\t\trecord baudot audio from device\r\n");
    printf("  -c                : Enable text entry from console.\r\n");
    printf("  -x <string>       : Process input from this text string.\r\n");
        
    printf("\r\n");
    printf("Output Modes:\r\n");
    printf("-------------------\r\n");
    printf("  -p <device> <sample rate> <channels> <bits per sec>  : \r\n\t\t\t\t\tplayback output as baudot audio.\r\n");
    printf("  -o <filename> <samplerate> : Write output as baudot audio \r\n\t\t\t\t\tto this file and samplerate.\r\n");
    printf("  -v                         : Write output text to console.\r\n");
    

    printf("\r\n");
    printf("Input / Baudot options:\r\n");
    printf("-------------------\r\n");
    printf("  -u <size ms>      : Set the read buffer size in milliseconds (default: %u).\r\n", defaults->bufferSizeMs); 
    printf("  -s <frequency hz> : Start scanning at this frequency (default:%u).\r\n", defaults->startFreq);
    printf("  -e <frequency hz> : end scanning at this frequency (default:%u).\r\n", defaults->endFreq);
    printf("  -f <block size hz>: Frequency block size (default:%u).\r\n", defaults->freqBlockSize);
    printf("  -z <threshold>    : Set zero (silence) threshold (default:%u.)\r\n",defaults->silenceThreshold);
    printf("  -w <size ms>      : Set tone detection window size in ms (default: %d).\r\n", defaults->windowSizeMs);
    printf("  -L <frequency hz> : Set frequency of low tone (default:%u).\r\n",defaults->lowFreq);
    printf("  -H <frequency hz> : Set frequency of high tone (default:%u).\r\n",defaults->highFreq);
    printf("  -E <frequency hz> : Set allowed error size when detecting tones (default:%u).\r\n",defaults->freqErrorHz);
    printf("  -n <size ms>      : Set the length, in ms, of a bit tone (default:%f).\r\n",defaults->bitLengthMs);
    printf("  -b <silence ms>   : Set the max amount of silence before detecting \r\n\t\t\t\ta break of input (default:%u)\r\n", 0);
    printf("\r\n");

    printf("Megahal options:\r\n");
    printf("-------------------\r\n");
    printf("  -m  <training file> : process input (wav / capture) in to megahal with \r\n\t\t\ttraining file (training file ignored if brain present).\r\n");
    printf("  -M <size in K>      : Set maximum brain size in kilobytes (approx) (default:%u).\r\n", megahalopts->maxBrainSize / 1024);
    printf("  -t  <seconds>       : MegaHAL response time (seconds) (default:%u).\r\n", megahalopts->megaHALResponseTime);
    printf("  -l                  : Enable MegaHAL learning mode (default off).\r\n");
    
   /* printf("\r\n");
    printf("Input options:\r\n");
    printf("-------------------\r\n");
    printf("  -W <time ms>  : Maximum wait time when using audio capture (default: %u)\r\n", pollTimeMs);*/

    printf("\r\n");
    printf("Output options:\r\n");
    printf("-------------------\r\n");
    printf("  -k <freq> <milliseconds>   : Generate tone to wake sound device up \r\n\t\t\t\t\tbefore output (defaults: %u, %u).\r\n", defaults->playWakeFreq, defaults->playWakeDurationMs);
    printf("  -g <freq> <milliseconds>   : Play silence (freq=0) or tone between \r\n\t\t\t\t\teach character (defaults: %u, %u).\r\n", defaults->charGapFreq, defaults->charGapDurationMs);
    printf("  -a <amp scale factor>      : Scale generated output sound\r\n\t\t\t\t\tby this much (default:%f).\r\n", defaults->outputScale);


    printf("\r\n");
    printf("GPIO options:\r\n");
    printf("-------------------\r\n");
    printf(" -y <function> <pin> : Assign function to pin number\r\n");
    printf("\t functions:\r\n");
    printf("\t\t l  : Listening.  High=Audio is being captured.\r\n");
    printf("\t\t f  : Finding.    High=Trying to find data part of signal.\r\n");
    printf("\t\t r  : Data Rx.    High=Receiving baudot audio data\r\n");
    printf("\t\t -  : Data Low Rx.  High=Current bit being captured is a 0.\r\n");
    printf("\t\t #  : Data High Rx. High=Current bit being captured is a 1.\r\n");
    printf("\t\t p  : Padding     High=Currently receiving padding.\r\n");
    printf("\t\t b  : End of block.  High=End of block detected.\r\n");
    printf("\t\t q  : Quiet.      High=Audio received too quiet.\r\n");
    printf("\t\t c  : Clipping.   High=Volume of input audio is too high\r\n");
    printf("\r\n");
    printf("\t\t s  : Sending.    High=audio is being sent\r\n");
    printf("\t\t w  : Wake up Tx. High=Wakeup audio is being played\r\n");
    printf("\t\t d  : Data Tx.    High=Data audio is being played\r\n");
    printf("\t\t g  : Gap Tx.     High=Audio played is gap between bits\r\n");
    printf("\t\t 0  : Data Low Tx.   High=Current bit being played is a 0.\r\n");
    printf("\t\t 1  : Data High Tx.  High=Current bit being played is a 1.\r\n");
    printf("\r\n");
    printf("\t\t t  : Thinking.   High=Megahal is thinking about response\r\n");
    printf("\t\t v  : saVing.     High=Saving (comitting) megahal brain to disk\r\n");
    printf("\t\t m  : TriMming.   High=Trimming brain down to size.\r\n");
    printf("\r\n");
    printf("\t\t z  : Sleep (zzz) Input pin. LOW=sleep. High=running.\r\n");
    printf("\t\t e  : lEarn.      Input pin. when high, enables megahal learning.\r\n");
    

    printf("\r\n");
    printf("Debug options:\r\n");
    printf("-------------------\r\n");
    printf("  -T            : Enable multi-thread diagnostics\r\n");
    printf("  -B            : Enable baudot diagnostics\r\n");
    printf("  -V            : Enable tedious baudot diagnostics\r\n");
    printf("  -A            : Enable audio playback diagnostics\r\n");
    printf("  -C            : Enable capture diagnostics\r\n");
    printf("  -X            : Enable capture tedious diagnostics\r\n");
    printf("  -G            : Enable megahal diagnostics\r\n");
    printf("  -S            : Enable power diagnostics\r\n");
    printf("  -I            : Always show text passed into and out of megahal in console\r\n");
    printf("  -D <filename> : Save captured audio passed to the baudot deocder to this file\r\n");
    printf("  -P <filename> : Save audio that is played back to this file\r\n");
    
}

static enum BOOLEAN_VALUE verifyParams(BauDot_Decoder_Options * params)
{
    if(params->bitLengthMs > params->bufferSizeMs)
    {
        printf("ERROR: bit length (%u ms) is too large, or read buffer (%u ms) too small\r\n",params->bitLengthMs, params->bufferSizeMs);
        return BOOL_FALSE;
    }

    if((params->windowSizeMs > params->bufferSizeMs) || (params->bufferSizeMs < 100))
    {
        printf("ERROR: window length (%u ms) is too large, or read buffer (%u ms) too small\r\n",params->windowSizeMs, params->bufferSizeMs);
        return BOOL_FALSE;
    }

    if(params->windowSizeMs > params->bitLengthMs)
    {
        printf("ERROR: window length (%u ms) is too large, or bit length (%u ms) is too small\r\n",params->windowSizeMs, params->bitLengthMs);
        return BOOL_FALSE;
    }

    if((params->silenceThreshold >= 1) || (params->silenceThreshold <= 0))
    {
        printf("ERROR: Silence threshold (%u) must be greater than 0, and less than 1\r\n", params->silenceThreshold);
        return BOOL_FALSE;
    }


    if(params->startFreq  > params->endFreq)
    {
        printf("ERROR: Start frequency (%u hz) must be lower than end frequency (%u hz)\r\n", params->startFreq, params->endFreq);
        return BOOL_FALSE;
    }

    if((params->lowFreq - params->freqErrorHz) < params->startFreq)
    {
        printf("ERROR: Low frequency (%u hz) and error tolerance (%u) [%u hz] must not be lower than start frequency (%u hz)\r\n", 
                params->lowFreq, params->freqErrorHz, params->lowFreq - params->freqErrorHz, params->startFreq);
        return BOOL_FALSE;
    }

    if((params->lowFreq + params->freqErrorHz) > params->endFreq)
    {
        printf("ERROR: Low frequency (%u hz) and error tolerance (%u) [%u hz] must not be higher than end frequency (%u hz)\r\n", 
                params->lowFreq, params->freqErrorHz, params->lowFreq + params->freqErrorHz, params->endFreq);
        return BOOL_FALSE;
    }

    if((params->highFreq - params->freqErrorHz) < params->startFreq)
    {
        printf("ERROR: high frequency (%u hz) and error tolerance (%u) [%u hz] must not be lower than start frequency (%u hz)\r\n", 
                params->highFreq, params->freqErrorHz, params->highFreq - params->freqErrorHz, params->startFreq);
        return BOOL_FALSE;
    }

    if((params->highFreq + params->freqErrorHz) > params->endFreq)
    {
        printf("ERROR: high frequency (%u hz) and error tolerance (%u) [%u hz] must not be higher than end frequency (%u hz)\r\n", 
                params->highFreq, params->freqErrorHz, params->highFreq + params->freqErrorHz, params->endFreq);
        return BOOL_FALSE;
    }

    if((params->lowFreq - params->freqErrorHz) < params->startFreq)
    {
        printf("ERROR: Low frequency (%u hz) and error tolerance (%u) [%u hz] must not be lower than start frequency (%u hz)\r\n", 
                params->lowFreq, params->freqErrorHz, params->lowFreq - params->freqErrorHz, 
                params->startFreq);
        return BOOL_FALSE;
    }

    if((params->highFreq + params->freqErrorHz) > params->endFreq)
    {
        printf("ERROR: high frequency (%u hz) and error tolerance (%u) [%u hz] must not be lower than start frequency (%u hz)\r\n", 
                params->highFreq, params->freqErrorHz, params->highFreq + params->freqErrorHz, 
                params->endFreq);
        return BOOL_FALSE;
    }


    if( ((params->lowFreq + params->freqErrorHz) > (params->highFreq -  params->freqErrorHz)) &&
        ((params->lowFreq + params->freqErrorHz) < (params->highFreq +  params->freqErrorHz)) )
    {
        printf("ERROR: Low frequency (%u hz) and error tolerance (%u) [%u hz] must not overlap into high frequency (%u hz) and error tolerance (%u) [%u hz - %u hz]\r\n",
                params->lowFreq, params->freqErrorHz, params->lowFreq + params->freqErrorHz, 
                params->highFreq, params->freqErrorHz, params->highFreq - params->freqErrorHz, params->highFreq + params->freqErrorHz);
        return BOOL_FALSE;
    }

    if( ((params->highFreq + params->freqErrorHz) > (params->lowFreq -  params->freqErrorHz)) &&
        ((params->highFreq + params->freqErrorHz) < (params->lowFreq +  params->freqErrorHz)) )
    {
        printf("ERROR: High frequency (%u hz) and error tolerance (%u) [%u hz] must not overlap into Low frequency (%u hz) and error tolerance (%u) [%u hz - %u hz]\r\n",
                params->highFreq, params->freqErrorHz, params->highFreq + params->freqErrorHz, 
                params->lowFreq, params->freqErrorHz, params->lowFreq - params->freqErrorHz, params->lowFreq + params->freqErrorHz);
        return BOOL_FALSE;
    }

  
    if(params->silenceBreakMs < 1000)
    {
        printf("ERROR: Silence break length (%u ms) must be more than 1000ms\r\n", params->silenceBreakMs);
        return BOOL_FALSE;
    }

    if((params->outputScale <= 0.01) || (params->outputScale >= 2))
    {
        printf("ERROR: Output scale value (%g) must be more than 0.01 and less than 2\r\n", params->outputScale);
        return BOOL_FALSE;
    }

  
    return BOOL_TRUE;
}



int main(int argv, const char ** argc)
{
    int x = 1;
    int ret = 0;
    enum BOOLEAN_VALUE shouldPrintHelp = BOOL_FALSE;
    enum BOOLEAN_VALUE stopCmdlineProcessing = BOOL_FALSE;
    BauDot_Decoder_Options processOptions;
    MegaHALOptions megahalopts;
    enum BOOLEAN_VALUE threadDebug = BOOL_FALSE;
    enum BOOLEAN_VALUE captureDebug = BOOL_FALSE;
    enum BOOLEAN_VALUE tediousCaptureDebug = BOOL_FALSE;
    enum BOOLEAN_VALUE playbackDebug = BOOL_FALSE;
    enum BOOLEAN_VALUE powerDebug = BOOL_FALSE;
    InputProcessStep inputProcessSteps = NULL;
    OutputProcessStep outputProcessSteps = NULL;
    MEGAHAL * megahalInstance = NULL;
    char * pathSep, *pathSep2;
    const char * filename;
    int samplerate;
    enum BOOLEAN_VALUE showInput = BOOL_FALSE;
    int pollTimeMs = 5000;
    enum BOOLEAN_VALUE enableConsole = BOOL_FALSE;
    const char * playbackWriteAudioFilename = NULL;
    enum BOOLEAN_VALUE consoleOutput = BOOL_FALSE;
    GPIOHandle gpiohnd = gpio_init(BOOL_FALSE);
    uint64_t debugStartTick = 0;
#ifndef _WIN32
    struct timespec ts;
#endif

    //Set up tick start for debugging
    #ifdef _WIN32
        debugStartTick = GetTickCount64();
    #else
        clock_gettime(CLOCK_MONOTONIC, &ts);
        debugStartTick =  (((unsigned long long)ts.tv_sec)*1000ULL) +  (((unsigned long long)ts.tv_nsec)/1000000ULL);
    #endif

    /*const char * writeWavFilename = NULL;
    int deviceId = 0;
    int sampleRate = 0;
    int channels = 0;
    int bps = 0;
    const char * trainFile = NULL;
    const char * replyWavFilename = NULL;
    int replySamplerate = 0;
    SoundPlaybackData sndplaybackdata = NULL;
    enum BOOLEAN_VALUE havePlaybackDevice = BOOL_FALSE;
    PlaybackDeviceSetup playbackDevData;*/

    //Set up baudot processing options

    processOptions.baudotDebug = BOOL_FALSE;
    processOptions.decoderOutAudioFile = NULL;
    processOptions.baudotTediousDebug = BOOL_FALSE;
    
    processOptions.startFreq = 0;
    processOptions.endFreq = 3000;
    processOptions.freqBlockSize = 25;
    
    processOptions.silenceThreshold = 0.1;
    processOptions.silenceBreakMs = 2500;

    processOptions.lowFreq = 1800;
    processOptions.highFreq = 1400;
    processOptions.freqErrorHz = 100;

    processOptions.bufferSizeMs = 250;
    processOptions.windowSizeMs = 8;
    
    processOptions.bitLengthMs = 22.2;
    processOptions.syncSizeBits = 1;
    processOptions.valueSizeBits = 5;
    processOptions.paddingSizeBits = 10;
    

    processOptions.outputScale = 0.8f;

    processOptions.playWakeDurationMs = 500;
    processOptions.playWakeFreq = 200;

    processOptions.charGapFreq = 0;
    processOptions.charGapDurationMs = 0;

    megahalopts.megaHALResponseTime = 5;
    megahalopts.megahalLearn = BOOL_FALSE;
    megahalopts.maxBrainSize = 10 * 1024 * 1024;
    megahalopts.debugMegahal = BOOL_FALSE;

    if(argv < 2)
        shouldPrintHelp = BOOL_TRUE;
    
    while(!stopCmdlineProcessing && (x < argv))
    {
        if(argc[x][0] == '-')
        {
            switch(argc[x][1])
            {
                 case 'a': //-a scale all generated audio by this value
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected scale value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.outputScale = atof(argc[x]);
                        ++x;
                    }
                    break;

                case 'b': //-b break threshold
                     ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.silenceBreakMs = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'c': //-c enable console
                    enableConsole = BOOL_TRUE;
                    ++x;
                    break;

                case 'd':   //-d : print list of audio devices
                    stopCmdlineProcessing = BOOL_TRUE;
                    shouldPrintHelp = BOOL_FALSE;
                    soundrec_PrintDevices();
                    break;

                case 'e': //-e end scanning frequency
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.endFreq = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'f': //-f Frequency block size
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.freqBlockSize = atoi(argc[x]);
                        ++x;
                    }
                    break;

                

                case 'h':   // -h : help
                    stopCmdlineProcessing = BOOL_TRUE;
                    shouldPrintHelp = BOOL_TRUE;
                    break;

                case 'i':   //-i <filename>: process baudot encoded audio
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected filename after %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        //Add the processing of the audio file to the processing steps
                        inputProcessSteps = process_add_input(inputProcessSteps, process_InputAudioCallback, 
                                                             process_initAudioFileInputHandle( argc[x], &processOptions),
                                                             process_freeAudioFileInputHandle, 
                                                             process_AudioFileInputPower,
                                                             BOOL_FALSE);

                        ++x;
                    }
                    break;

                case 'g': //-g Play silence (freq=0) or tone between each character
                case 'k': //-k wake playback device up before actual baudot tones <freq (0=silence)> <duration ms>
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected frequency in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        int freq = atoi(argc[x]);
                        
                        ++x;
                        if(x >= argv)
                        {
                            printf("ERROR: Expected duration after frequency in %s\r\n", argc[x-2]);
                            stopCmdlineProcessing = BOOL_TRUE;
                            shouldPrintHelp = BOOL_FALSE;
                        }
                        else if(argc[x-2][1] == 'k')
                        {
                            processOptions.playWakeFreq = freq;
                            processOptions.playWakeDurationMs = atoi(argc[x]);
                            ++x;
                        }
                        else if(argc[x-2][1] == 'g')
                        {
                            processOptions.charGapFreq = freq;
                            processOptions.charGapDurationMs =  atoi(argc[x]);
                            ++x;
                        }
                    }
                    break;

                case 'l': //-l enable megahal learning
                    ++x;
                    megahalopts.megahalLearn = BOOL_TRUE;
                    break;

                case 'm':   //-m feed input into megahal, use megahal response as output
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected megahal training file after %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        if(megahalInstance == NULL)
                        {
                            megahalInstance = mh_alloc();
                            megahalInstance->learningmode = BOOL_TRUE; //Control learning mode via megahalOpts->megahalLearn. This is not that parameter. This allows that parameter to turn learning on and off.
                            megahalInstance->debugStartTick =  debugStartTick;

                            //Use the same path as the training file for the other internal megahal files
                            strncpy(megahalInstance->directory_cache, argc[x], sizeof(megahalInstance->directory_cache) - 1);
                            pathSep = strrchr(megahalInstance->directory_cache, '\\');
                            pathSep2 = strrchr(megahalInstance->directory_cache, '/');
                            pathSep = ((pathSep == NULL) || ((pathSep2 != NULL) && (pathSep2 >= pathSep))) ? pathSep2 : pathSep;
                            if(pathSep != NULL)
                                *pathSep = 0;
                            else
                            {
                                //No path separator, use current directory instead
                                getcwd(megahalInstance->directory_cache, sizeof(megahalInstance->directory_cache) - 1);
                            }
                            
                            strcpy(megahalInstance->directory_resources, megahalInstance->directory_cache);

                            if(!mh_load_personality(megahalInstance, argc[x]))
                            {
                                printf("ERROR: Failed to load MegaHAL training file %s\r\n", argc[x]);
                                stopCmdlineProcessing = BOOL_TRUE;
                            }
                        }

                        ++x;
                    }
                    break;
                

                case 'n': //-n Set the length, in ms, of a bit tone
                     ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.bitLengthMs = atof(argc[x]);
                        ++x;
                    }
                    break;

                case 'o': //-o : Write output as baudot audio to this wav file
                    ++x;
                    if((x >= argv) || ((x + 1) >= argv))
                    {
                        printf("ERROR: Expected output wav filename and samplerate after %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        filename = argc[x];
                        ++x;
                        samplerate = atoi(argc[x]);
                        ++x;

                        if(samplerate == 0)
                            samplerate = 44100; //Default samplerate

                        outputProcessSteps = process_add_output( outputProcessSteps, process_OutputBaudotAudioCallback, 
                                                                 process_initBaudotAudioFileOutputHandle(&processOptions, filename, samplerate,  threadDebug),
                                                                 process_WaitForOutputBaudotAudioCallback,
                                                                 process_BaudotAudioFileOutputPower,
                                                                 process_freeBaudotAudioFileOutputHandle);
                    }

                    break;
                case 'p': //-p playback output audio
                case 'r': //-r record audio
                    {
                        enum BOOLEAN_VALUE isPlayback = (argc[x][1] == 'p') ? BOOL_TRUE : BOOL_FALSE;
                        SoundDevice dev;
                        memset(&dev,0,sizeof(dev));

                        ++x;
                        if(x >= argv)
                        {
                            printf("ERROR: Expected device id after %s\r\n", argc[x-1]);
                            stopCmdlineProcessing = BOOL_TRUE;
                            shouldPrintHelp = BOOL_FALSE;
                        }
                        else
                        {
                            dev.device = atoi(argc[x]);
                            ++x;
                            if(x >= argv)
                            {
                                printf("ERROR: Expected samplerate after device in %s\r\n", argc[x-2]);
                                stopCmdlineProcessing = BOOL_TRUE;
                                shouldPrintHelp = BOOL_FALSE;
                            }
                            else
                            {
                                dev.sample_rate = atoi(argc[x]);
                                ++x;
                                if(x >= argv)
                                {
                                    printf("ERROR: Expected channels after samplerate in %s\r\n", argc[x-3]);
                                    stopCmdlineProcessing = BOOL_TRUE;
                                    shouldPrintHelp = BOOL_FALSE;
                                }
                                else if((dev.sample_rate < 8000) || (dev.sample_rate > 48000))
                                {
                                    printf("ERROR: samplerate must be between 8000 and 48000\r\n");
                                    stopCmdlineProcessing = BOOL_TRUE;
                                    shouldPrintHelp = BOOL_FALSE;
                                }
                                else
                                {
                                    dev.channels = atoi(argc[x]);
                                    ++x;
                                    if(x >= argv)
                                    {
                                        printf("ERROR: Expected bits per second after channels in %s\r\n", argc[x-4]);
                                        stopCmdlineProcessing = BOOL_TRUE;
                                        shouldPrintHelp = BOOL_FALSE;
                                    }
                                    else if((dev.channels != 1) && (dev.channels != 2))
                                    {
                                        printf("ERROR: channels must be 1 or 2\r\n");
                                        stopCmdlineProcessing = BOOL_TRUE;
                                        shouldPrintHelp = BOOL_FALSE;
                                    }
                                    else
                                    {
                                        dev.bitsPerSampleValue = atoi(argc[x]);
                                        ++x;

                                        if((dev.bitsPerSampleValue != 8) && (dev.bitsPerSampleValue != 16))
                                        {
                                            printf("ERROR: bits per sample value must be 8 or 16\r\n");
                                            stopCmdlineProcessing = BOOL_TRUE;
                                            shouldPrintHelp = BOOL_FALSE;
                                        }

                                        if(isPlayback)
                                        {
                                            //Playback
                                            outputProcessSteps =  process_add_output( outputProcessSteps, process_OutputBaudotAudioPlaybackCallback, 
                                                                                      process_initBaudotAudioPlaybackOutputHandle(&processOptions, &dev, threadDebug, playbackDebug, powerDebug, &playbackWriteAudioFilename, gpiohnd),
                                                                                      process_WaitForBaudotAudioPlaybackOutputCallback,
                                                                                      process_BaudotAudioPlaybackPower,
                                                                                      process_freeBaudotAudioPlaybackHandle);
                                        }
                                        else
                                        {
                                            inputProcessSteps = process_add_input( inputProcessSteps, process_InputAudioCaptureCallback,
                                                                                    process_initAudioCaptureHandle(&processOptions, &dev,threadDebug, captureDebug, tediousCaptureDebug, powerDebug, gpiohnd),
                                                                                    process_freeAudioCaptureHandle, 
                                                                                    process_AudioCaptureInputPower, BOOL_TRUE);
                                        }
    
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 's': //-s start scanning frequency
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.startFreq = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 't': //-t max megahal response time (seconds)
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        megahalopts.megaHALResponseTime = atoi(argc[x]);
                        if(megahalopts.megaHALResponseTime < 1)
                        {
                            printf("ERROR: megahal response time must be at least 1 second in %s\r\n", argc[x-1]);
                            stopCmdlineProcessing = BOOL_TRUE;
                            shouldPrintHelp = BOOL_FALSE;
                        }

                        ++x;
                    }
                    break;

                case 'u':   //-u read buffer size
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.bufferSizeMs = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'v':   //-v : write output to console
                    outputProcessSteps = process_add_output( outputProcessSteps, process_OutputConsoleCallback, &debugStartTick, NULL, NULL, NULL);
                    consoleOutput = BOOL_TRUE;
                    ++x;
                    break;


                case 'w': //-w tone detection window size
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.windowSizeMs = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'x':   //-x <string> process text from command line
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected text after %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        //Add the processing of the audio file to the processing steps
                        inputProcessSteps = process_add_input(inputProcessSteps, process_StringCallback, 
                                                             process_initStringInputHandle(argc[x]), 
                                                             process_freeStringInputHandle, 
                                                             process_StringInputPower, 
                                                             BOOL_FALSE);

                        ++x;
                    }
                    break;
                
                case 'y': //-y GPIO <func> <pin> assignment
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected function after %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        const char * func = argc[x];
                        ++x;
                        if(x >= argv)
                        {
                            printf("ERROR: Expected pin number after function in %s\r\n", argc[x-2]);
                            stopCmdlineProcessing = BOOL_TRUE;
                            shouldPrintHelp = BOOL_FALSE;
                        }
                        else
                        {
                            int pin = atoi(argc[x]);
                            enum BOOLEAN_VALUE setResult;
                            ++x;

                            if(gpiohnd == NULL)
                            {
                                printf("ERROR: Failed to initialise GPIO interface\r\n");
                                stopCmdlineProcessing = BOOL_TRUE;
                                shouldPrintHelp = BOOL_FALSE;
                            }
                            else
                            {
                                switch(func[0])
                                {
                                    case 'l':
                                    case 'L':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Recording, pin);
                                        break;
                                    case 'q':
                                    case 'Q':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_Silence, pin);
                                        break;
                                    case 'f':
                                    case 'F':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_Searching, pin);
                                        break;
                                    case 'r':
                                    case 'R':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_Data, pin);
                                        break;
                                    case '-':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_DataValueLow, pin);
                                        break;
                                    case '#':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_DataValueHigh, pin);
                                        break;
                                    case 'p':
                                    case 'P':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_Padding, pin);
                                        break;
                                    case 'b':
                                    case 'B':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_EndOfBlock, pin);
                                        break;
                                    case 's':
                                    case 'S':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Sending, pin);
                                        break;
                                    case 'w':
                                    case 'W':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Tx_WakeUp, pin);
                                        break;
                                    case 'd':
                                    case 'D':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Tx_Data, pin);
                                        break;
                                    case 'g':
                                    case 'G':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Tx_BitGap, pin);
                                        break;
                                    case '1':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Tx_DataValueHigh, pin);
                                        break;
                                    case '0':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Tx_DataValueLow, pin);
                                        break;
                                    case 't':
                                    case 'T':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_MH_Thinking, pin);
                                        break;
                                    case 'v':
                                    case 'V':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_MH_Saving, pin);
                                        break;
                                    case 'm':
                                    case 'M':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_MH_Trimming, pin);
                                        break;
                                    case 'c':
                                    case 'C':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Rx_Clipping, pin);
                                        break;
                                    case 'e':
                                    case 'E':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Input_Learning, pin);
                                        break;
                                    case 'z':
                                    case 'Z':
                                        setResult = gpio_addConfig(gpiohnd, GPIOFunction_Input_Power, pin);
                                        break;
                                    default:
                                        printf("ERROR: Invalid pin function %s\r\n", func);
                                        stopCmdlineProcessing = BOOL_TRUE;
                                        shouldPrintHelp = BOOL_FALSE;
                                        break;
                                }
                            }
                        }
                    }

                    break;

                case 'z': //-z zero (silence) threshold
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.silenceThreshold = atof(argc[x]);
                        ++x;
                    }
                    break;

                case 'A': //-A enable playback debug
                    ++x;
                    playbackDebug = BOOL_TRUE;
                    break;

                case 'B': //-B baudot decoding debug
                    processOptions.baudotDebug = BOOL_TRUE;
                    ++x;
                    break;

                case 'C': //-C enable capture debug
                    ++x;
                    captureDebug = BOOL_TRUE;
                    break;

                case 'D': //-D baudot decoder output file
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected filename after %s.\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.decoderOutAudioFile = argc[x];
                        ++x;
                    }
                    break;

                case 'E': //-E allowed error size when detecting tones
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value after %s.\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.freqErrorHz = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'G': //-G enable megahal debugging
                    megahalopts.debugMegahal = BOOL_TRUE;
                    ++x;
                    break;

                case 'H': //-H Set frequency of high tone
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value after %s.\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.highFreq = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'I': //-I Always show input text in console
                    showInput = BOOL_TRUE;
                    ++x;
                    break;

                case 'L': //-L Set frequency of low tone
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value after %s.\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        processOptions.lowFreq = atoi(argc[x]);
                        ++x;
                    }
                    break;

                case 'M': // -M max brain size
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected value in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        megahalopts.maxBrainSize = atoi(argc[x]);
                        if(megahalopts.maxBrainSize < 512) 
                        {
                            printf("ERROR: Maximum brain size (%u) cannot be smaller than 512k\r\n", megahalopts.maxBrainSize);
                            stopCmdlineProcessing = BOOL_TRUE;
                            shouldPrintHelp = BOOL_FALSE;
                        }
                        else
                        {
                            megahalopts.maxBrainSize *= 1024; // Convert from K to bytes
                        }

                        ++x;
                    }
                    break;

                case 'P': //-P save playback audio to file
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected filename in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        playbackWriteAudioFilename = argc[x];
                        ++x;
                    }
                    break;
                case 'S': //-S power (sleep) debug
                    powerDebug = BOOL_TRUE;
                    ++x;
                    break;
                case 'T': //-T enable thread debug
                    threadDebug = BOOL_TRUE;
                    ++x;
                    break;
                
                case 'V': //-V tediou decoding debug
                    processOptions.baudotTediousDebug = BOOL_TRUE;
                    ++x;
                    break;

                case 'W': // -W <time ms>  Maximum wait time when using audio capture
                    ++x;
                    if(x >= argv)
                    {
                        printf("ERROR: Expected wait time after device in %s\r\n", argc[x-1]);
                        stopCmdlineProcessing = BOOL_TRUE;
                        shouldPrintHelp = BOOL_FALSE;
                    }
                    else
                    {
                        pollTimeMs = atoi(argc[x]);
                        if(pollTimeMs < 1000)
                        {
                            printf("ERROR: Wait time (%s) must be 1000 milliseconds or more\r\n", argc[x-1]);
                            stopCmdlineProcessing = BOOL_TRUE;
                            shouldPrintHelp = BOOL_FALSE;
                        }

                        ++x;
                    }
                    break;

                case 'X': //-X tedious capture/processing debug
                    tediousCaptureDebug = BOOL_TRUE;
                    ++x;
                    break;

                default:
                    printf("ERROR : Unknown option %s \r\n", argc[x]);
                    stopCmdlineProcessing = BOOL_TRUE;
                    shouldPrintHelp = BOOL_TRUE;
                    break;
            }
        }
        else
        {
            printf("ERROR : Unknown option %s\r\n", argc[x]);
            stopCmdlineProcessing = BOOL_TRUE;
            shouldPrintHelp = BOOL_TRUE;
        }
    }
    

    if(shouldPrintHelp || ((inputProcessSteps == NULL) && (!enableConsole)))
    {
        if(shouldPrintHelp )
            print_help(&processOptions, &megahalopts, pollTimeMs);
    }
    else if(!stopCmdlineProcessing )
    {
        if(!verifyParams(&processOptions))
        {
            printf("ERROR: Invalid baudot encode/decode parameters\r\n");
            ret = 1;
        }
        else if(!process_run(inputProcessSteps, outputProcessSteps,  megahalInstance, &megahalopts, 
                                showInput, 
                                ((outputProcessSteps == NULL) || (!consoleOutput && showInput)) ? BOOL_TRUE : BOOL_FALSE,   //Don't enable showing output if console mode is also enabled (prevents double output)
                                pollTimeMs, enableConsole, threadDebug, powerDebug, gpiohnd))
        {
            printf("ERROR\r\n");
            ret = 1;
        }

    }

    printf("------------------------------------\r\n");

    //Free processing data
    process_free_input(inputProcessSteps);
    process_free_output(outputProcessSteps);

    printf("------------------------------------\r\n");

    //Free megahal
    if(megahalInstance != NULL)
        mh_free(megahalInstance);

    if(gpiohnd != NULL)
        gpio_free(gpiohnd);

    return ret;
}