#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <string.h>
#include <time.h>


#include "internal.h"


typedef struct _WAVDESTDATA
{
    FILE * file;
    int sampleRate;
    double scaleFactor;
    ThreadHandle writeThread;
    enum BOOLEAN_VALUE shouldQuit;
    enum BOOLEAN_VALUE threadDebug;

    MemoryBuffer * sampleBufferToWrite;

    size_t totalBytesWritten;
    size_t hdr1WritePos;
    size_t hdr1StartPos;
    size_t hdr2WritePos ;
    size_t hdr2StartPos;

} WAVDESTDATA;


static void BD_CALLBACK wavdestWriteProc(void * param)
{
    WAVDESTDATA * wavdest = (WAVDESTDATA *)param;
    enum BOOLEAN_VALUE wroteHeader = BOOL_FALSE;
    enum BOOLEAN_VALUE updateHeader = BOOL_FALSE;
    char buffer[256];
    uint32_t dwValue = 0;
    uint16_t wValue = 0;
    uint64_t v;
    size_t x;
    double s;
    const uint16_t numBitsPerSample = 16;
    const int32_t maxSampleValue = (1 << (numBitsPerSample - 1));
    size_t endpos;
    const double * sampleDataPtr;
    size_t numSamplesToWrite;

    cppthrd_LockThread(wavdest->writeThread);

    if(wavdest->threadDebug)
        printf("\t\tWav writer thread start\r\n");

    while(!wavdest->shouldQuit)
    {
        //Wait - this unlocks while waiting, and re-locks when waiting has stopped
        cppthrd_WaitForEvent(wavdest->writeThread, 30000, BOOL_TRUE);
        if(membuf_getContentSize(wavdest->sampleBufferToWrite) > 0)
        {
            //Write header
            //This is prety fixed, as we support 1 channel, 16 bits per sample - at any sample rate
            if(!wroteHeader)
            {
                //write RIFF header
                sprintf(buffer,"RIFF");	
                dwValue = 0; //dont know the length yet... we'll fill this in later... much later... when it is dark and the vampires.........

                fwrite(buffer,1, 4, wavdest->file);	
                wavdest->hdr1WritePos = ftell(wavdest->file);
                fwrite(&dwValue,1, 4,wavdest->file);
                
                wavdest->hdr1StartPos = ftell(wavdest->file);
                
                sprintf(buffer,"WAVE");
                fwrite(buffer,1, 4,wavdest->file);

                //write format chunk
                sprintf(buffer,"fmt ");
                fwrite(buffer,1, 4,wavdest->file);	//chunk id
                dwValue = 16;
                fwrite(&dwValue,1, 4,wavdest->file);	//chunk data size

                wValue = 1;
                fwrite(&wValue, 1, 2,wavdest->file);	//compression code

                wValue = 1;
                fwrite(&wValue, 1, 2, wavdest->file);	//num channels

                dwValue = wavdest->sampleRate;
                fwrite(&dwValue,1, 4, wavdest->file);	//sample rate
                
                dwValue = wavdest->sampleRate * (numBitsPerSample / 8); //1 channel, 2 bytes per sample, <sample rate> samples per sec
                fwrite(&dwValue,1, 4,wavdest->file); //average bytes per second
                
                wValue = numBitsPerSample / 8; //1 channel, 2 bytes per sample (16 bits per sample)
                fwrite(&wValue,1, 2, wavdest->file); //block align

                wValue = numBitsPerSample;
                fwrite(&wValue,1, 2, wavdest->file); //bits per sample
                dwValue = 0;

                //write data chunk
                sprintf(buffer,"data");
                fwrite(buffer,1, 4,wavdest->file);	//chunk id
                wavdest->hdr2WritePos = ftell(wavdest->file);
                dwValue = 0;		//length of wave data only (not including header). Fill this in also later
                fwrite(&dwValue,1, 4,wavdest->file);	//new wave data len

                wavdest->hdr2StartPos = ftell(wavdest->file);

                wroteHeader = BOOL_TRUE;
            }

            sampleDataPtr = (const double *)membuf_ptr(wavdest->sampleBufferToWrite,0);
            numSamplesToWrite = membuf_getContentSize(wavdest->sampleBufferToWrite) / sizeof(double);
            for(x=0;x < numSamplesToWrite; ++x)
            {
                //Convert doubles into 16-bit signed values
                s = sampleDataPtr[x];
                s *= (maxSampleValue - 1);
                s *= wavdest->scaleFactor;

                if(s > (maxSampleValue - 1))
                    s = (maxSampleValue - 1);
                else if((s < 0) && ((s * -1)  > (maxSampleValue - 1)))
                {
                    s = (maxSampleValue - 1);
                    s *= -1;
                }
                
                if(s < 0)
                {
                    s *= -1;
                    v = (uint64_t)s;
                    --v;
                    v = ~v;
                    wValue = (uint16_t)v;
                }
                else
                {
                    wValue = (uint16_t)s;
                }
                
                fwrite(&wValue,1, 2, wavdest->file);
                wavdest->totalBytesWritten += 2;

                //We should update the header after this
                updateHeader = BOOL_TRUE;
            }

            if(updateHeader)
            {
                //Update the header values we left out
                endpos = ftell(wavdest->file);
                fseek(wavdest->file, wavdest->hdr1WritePos, SEEK_SET);
                x = endpos - wavdest->hdr1StartPos;
                fwrite(&x, 1, 4, wavdest->file);

                fseek(wavdest->file, wavdest->hdr2WritePos, SEEK_SET);
                x = endpos - wavdest->hdr2StartPos;
                fwrite(&x, 1, 4, wavdest->file);

                fseek(wavdest->file, endpos, SEEK_SET);
                updateHeader = BOOL_FALSE;
            }

            //Erase the shared sample buffer, now that we've written it out
            membuf_erase(wavdest->sampleBufferToWrite);
        }
    }

    cppthrd_UnlockThread(wavdest->writeThread);

    if(wavdest->threadDebug)
        printf("\t\tWav writer thread stop\r\n");
}



WAVDESTHND wavdest_init(const char * filename, int sampleRate, double scaleFactor, enum BOOLEAN_VALUE threadDebug)
{
    //Alloc data
    WAVDESTDATA * wavdest = (WAVDESTDATA *)malloc(sizeof(WAVDESTDATA));
    memset(wavdest,0,sizeof(WAVDESTDATA));

    //Open binary file
    wavdest->threadDebug = threadDebug;
    wavdest->shouldQuit = BOOL_FALSE;
    wavdest->sampleRate = sampleRate;
    wavdest->scaleFactor = scaleFactor;
    wavdest->file = fopen(filename, "wb");
    wavdest->sampleBufferToWrite = membuf_init();
    if(wavdest->file == NULL)
    {
        wavdest_free(wavdest);
        return NULL;
    }

    //Start writing thread
    wavdest->writeThread = cppthrd_CreateThread( wavdest, wavdestWriteProc, threadDebug);
    cppthrd_StartThread(wavdest->writeThread, BOOL_FALSE);

    return wavdest;
}


void wavdest_free(WAVDESTHND hnd)
{
    WAVDESTDATA * wavdest = (WAVDESTDATA *)hnd;
    uint32_t value = 0;
    size_t endpos;

    if(wavdest->writeThread != NULL)
    {
        if(wavdest->threadDebug)
            printf("\t\tSignalling wav writer to stop\r\n");

        cppthrd_LockThread(wavdest->writeThread);
        wavdest->shouldQuit = BOOL_TRUE;
        cppthrd_SignalThreadEvent(wavdest->writeThread, BOOL_TRUE);
        cppthrd_UnlockThread(wavdest->writeThread);
        
        cppthrd_WaitAndDestroyThread(wavdest->writeThread);

        if(wavdest->threadDebug)
            printf("\t\twav writer thread has stopped\r\n");
    }

    wavdest->writeThread= NULL;

    //Fill in the header values we left out
    endpos = ftell(wavdest->file);
    fseek(wavdest->file, wavdest->hdr1WritePos, SEEK_SET);
    value = endpos - wavdest->hdr1StartPos;
    fwrite(&value, 1, 4, wavdest->file);

    fseek(wavdest->file, wavdest->hdr2WritePos, SEEK_SET);
    value = endpos - wavdest->hdr2StartPos;
    fwrite(&value, 1, 4, wavdest->file);

    if(wavdest->file != NULL)
        fclose(wavdest->file);

    wavdest->file = NULL;

    if(wavdest->sampleBufferToWrite != NULL)
    {
        membuf_free(wavdest->sampleBufferToWrite);
        wavdest->sampleBufferToWrite = NULL;
    }

    free(wavdest);
}

void wavdest_write(WAVDESTHND hnd, const double * data, size_t numSampleValues)
{
    WAVDESTDATA * wavdest = (WAVDESTDATA *)hnd;

    if(wavdest->writeThread != NULL)
    {
        //Lock
        cppthrd_LockThread(wavdest->writeThread);

        //Append / copy
        membuf_append( wavdest->sampleBufferToWrite,  data, numSampleValues * sizeof(double));
        
        //signal thread and unlock mutex
        cppthrd_SignalThreadEvent(wavdest->writeThread, BOOL_TRUE);
        cppthrd_UnlockThread(wavdest->writeThread);
    }
}

void wavdest_wait(WAVDESTHND hnd)
{
    WAVDESTDATA * wavdest = (WAVDESTDATA *)hnd;

    if(wavdest->writeThread != NULL)
    {   
        //Lock
        cppthrd_LockThread(wavdest->writeThread);

        //While the sample buffer to write is not empty, then the thread is still busy
        while(membuf_getContentSize(wavdest->sampleBufferToWrite) > 0)
        {
            //Signal thread
            cppthrd_SignalThreadEvent(wavdest->writeThread, BOOL_TRUE);
         
            //Wait a bit
            cppthrd_UnlockThread(wavdest->writeThread);
            cppThread_sleep_ms(500);
            cppthrd_LockThread(wavdest->writeThread);
        }

        cppthrd_UnlockThread(wavdest->writeThread);
    }
}
