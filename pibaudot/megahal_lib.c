/*===========================================================================*/
/*
 *  Copyright (C) 2009 Zev Toledano
 *  Original Copyright (C) 1998/1999 Jason Hutchens
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the license or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the Gnu Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */
/*===========================================================================*/
/*
* MegaHAL module for pibaudot - a program to listen for baudot signals from
* and audio device, convert those signals into ASCII, and feed them into MegaHAL
* The response is then converted back into baudot audio signals, and played out
* of the sound device
*
* This modification needed parts of the MegaHAL eggdrop module - as I wanted
* the better memory management
*
* The original eggdrop source is included, but not built, with this project
*
* The version that is built is a cut-down version, that only has the parts 
* required, and were extracted and modified to be library based, rather
* than main/exe based
*
*/
/*===========================================================================*/

/*
 * MegaHAL module for eggdrop v3.7
 * By Zev ^Baron^ Toledano (megahal at thelastexit.net) and Jason Hutchens
 * Artificially Intelligent conversation with learning capability and
 * psychotic personality.
 *
 * Note: The original AI work and program was written by J. Hutchens in 1999.
 * The eggdrop port was coded by Zev Toledano and it necessitated
 * various major changes to the brain structure and code in order to keep the
 * brain at a manageable size. Various commands and fun features were added as
 * well.
 * Important: Old brains (1.x/2.x *.brn files) will NOT work with this version (3.0)! You
 * must start a new one.
 *
 * (all old comments were moved to the end of this file)
 *
 * Additions and changes by Nexor:
 */

/* See original megahal.c for full history - This version is based on v3.7*/

/*===========================================================================*/
#include <string.h>
#include <wchar.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "megahal_lib.h"
#include "megahal_internal.h"



#define error mh_error
#define putlog mh_putlog
#define warn mh_warn


// deletes a phrase from the model using all its contexts, decrementing counters or deleting branches where necessary
static void del_phrase(MODEL * model, unsigned long phrase)
{
	bool fnd = FALSE;
	unsigned int j, k;
	BYTE2 size;

	Context;
	if (phrase >= model->phrasecount) 
        return;
	
    size = model->phrase[phrase][0];

	// go through the words/symbols and start trimming sets of context branches one symbol at a time
	for (j=1; j<=size; j++) {
		mh_initialize_context(model);

		// build the context using the next batch of symbols
		model->halcontext[0] = model->forward->tree[mh_search_node(model->forward, model->phrase[phrase][j], &fnd)];
		for (k=1; k<=model->order; k++) {
			if ((k+j) > size)
				break;
			model->halcontext[k] = model->halcontext[k-1]->tree[mh_search_node(model->halcontext[k-1], model->phrase[phrase][j+k], &fnd)];
		}

		// now decrement the usage and delete unused branches - must go backwards in case branches are erased
		for (k=model->order; k>0; k--)
			mh_decrement_tree(model->halcontext[k], model->halcontext[k-1]);

		mh_decrement_tree(model->halcontext[0], model->forward);
	}


	// start the trimming in the backwards tree
	// first move the <FIN> symbol to the other side because we are going backwards now
	for (j=size; j>1; j--)
		model->phrase[phrase][j] = model->phrase[phrase][j-1];

	model->phrase[phrase][1] = 1;

	for (j=size; j>0; j--) {
		mh_initialize_context(model);

		model->halcontext[0] = model->backward->tree[mh_search_node(model->backward, model->phrase[phrase][j], &fnd)];
		for (k=1; k<=model->order; k++) {
			if (k>j-1) break;
			model->halcontext[k] = model->halcontext[k-1]->tree[mh_search_node(model->halcontext[k-1], model->phrase[phrase][j-k], &fnd)];
		}

		for (k=model->order; k>0; k--)
			mh_decrement_tree(model->halcontext[k], model->halcontext[k-1]);

		mh_decrement_tree(model->halcontext[0], model->backward);
	}

/*	// move the symbol back again
	for (j=1; j<size; j++)
		model->phrase[phrase][j] = model->phrase[phrase][j+1];
	model->phrase[phrase][size] = 1;
*/
	// remove the phrase from the model
	nfree(model->phrase[phrase]);
	for(j=phrase; j<model->phrasecount; j++)
		model->phrase[j] = model->phrase[j+1];

	model->phrasecount--;
	
    if(mh_realloc_phrase(model) == NULL) {
		error("del_phrase", "Unable to reallocate phrase");
		return;
	}

}

// rough estimate here - if you want precise numbers do it yourself :-p
static int megahal_expmem(MEGAHAL * mh)
{
	unsigned int i;
	int size = 0, tmp;
    MODEL * model = mh->model;

	Context;
	size += sizeof(MODEL);
	tmp = mh_recurse_tree(model->forward);
	size += tmp*sizeof(TREE);
	size += tmp*sizeof(TREE *);
	tmp = mh_recurse_tree(model->backward);
	size += tmp*sizeof(TREE);
	size += tmp*sizeof(TREE *);

	for(i=0; i<model->phrasecount; i++)
		size += model->phrase[i][0]+1*sizeof(BYTE2);

	size += model->phrasecount*sizeof(BYTE2 *);

	size += mh_dictionary_expmem(model->dictionary);
	size += mh_dictionary_expmem(mh->ban);
	size += mh_dictionary_expmem(mh->aux);
	size += mh_dictionary_expmem(mh->words);

	size += sizeof(SWAP);
	for(i=0; i< mh->swp->size; i++) {
        size += mh->swp->from[i].length*sizeof(CHARTYPE);
		size += mh->swp->to[i].length*sizeof(CHARTYPE);
	}
	size += mh->swp->size*sizeof(STRING)*2;

	return size;
}

 

/*
 *	Function:	Learn
 *
 *	Purpose:	Learn from the user's input.
 */
static void learn(MODEL *model, DICTIONARY *words)
{
	unsigned long i;
    long is;
	BYTE2 symbol;
	bool nospace = TRUE;

	Context;
	/*
	 *	We only learn from inputs which are long enough
	 */
	if(words->size <= (model->order))
		return;

	// check if there are spaces in the word or its merely one word+punctuation
	for(i=1; i < words->size; i++)
    {
		if(words->entry[i].word[0] != (CHARTYPE)31) {
			nospace = FALSE;
			break;
		}
    }

	if (nospace)
		return;

	// Add a new phrase to the model
	model->phrasecount++;
	if(mh_realloc_phrase(model)==NULL) {
		error("learn", "Unable to reallocate phrase");
		return;
	}
	model->phrase[model->phrasecount-1]=(BYTE2 *)nmalloc(sizeof(BYTE2)*(words->size+2));
	if (model->phrase[model->phrasecount-1] == NULL) {
		error("learn", "Unable to allocate phrase");
		return;
	}
	model->phrase[model->phrasecount-1][0] = (unsigned short)(words->size+1);

	/*
	 *	Train the model in the forwards direction.  Start by initializing
	 *	the context of the model.
	 */
	mh_initialize_context(model);
	model->halcontext[0] = model->forward;
	for(i=0; i<words->size; ++i) {
		/*
		 *	Add the symbol to the model's dictionary if necessary, and then
		 *	update the forward model accordingly.
		 */
		symbol = mh_add_word(model->dictionary, words->entry[i]);
		mh_update_model(model, symbol);
		model->phrase[model->phrasecount-1][i+1] = symbol;
	}
	/*
	 *	Add the sentence-terminating symbol.
	 */
	mh_update_model(model, 1);
	model->phrase[model->phrasecount-1][words->size+1] = 1;

	/*
	 *	Train the model in the backwards direction.  Start by initializing
	 *	the context of the model.
	 */
	mh_initialize_context(model);
	model->halcontext[0] = model->backward;
	for(is=words->size-1; is>=0; --is) {
		/*
		 *	Find the symbol in the model's dictionary, and then update
		 *	the backward model accordingly.
		 */
		symbol = mh_find_word(model->dictionary, words->entry[is]);
		mh_update_model(model, symbol);
	}
	/*
	 *	Add the sentence-terminating symbol.
	 */
	mh_update_model(model, 1);

	return;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Train
 *
 *	Purpose:	Infer a MegaHAL brain from the contents of a text file.
 */
static void train(MODEL *model, char *filename)
{
	FILE *file;
	char buffer[1024];
#ifdef USE_WCHAR
	CHARTYPE *wbuffer = NULL;
#endif
	DICTIONARY *words = NULL;
	int length;

	Context;
	if(filename == NULL)
		return;

	file=fopen(filename, "r");
	if(file == NULL) {
		putlog(LOG_MISC, "*", "Unable to find the personality %s\r\n", filename);
		return;
	}

	fseek(file, 0, 2);
	length = ftell(file);
	rewind(file);

	words = mh_new_dictionary();

	while(!feof(file)) {

		if(fgets(buffer, 1024, file)==NULL)
			break;
		if(buffer[0] == '#')
			continue; // comments

#ifdef USE_WCHAR
		wbuffer=mh_locale_to_wchar(buffer);
		wbuffer[wcslen(wbuffer)-1] = L'\0';

		mh_upper(wbuffer);
		mh_make_words(wbuffer, words);
		learn(model, words);
		nfree(wbuffer);
#else

        mh_upper(buffer);
		mh_make_words(buffer, words);
		learn(model, words);
#endif 
	}

	mh_free_dictionary(words);
	fclose(file);
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Make_Output
 *
 *	Purpose:	Generate a string from the dictionary of reply words.
 */
CHARTYPE *make_output(MEGAHAL * mh, DICTIONARY *words)
{
	unsigned long i;
    int j;
	int tmp = 0;
    size_t length;
    const CHARTYPE * output_none_1 = _T("I am utterly speechless!");
    
	Context;
	
	
	
	length = 1;
	if(words->size == 0) 
    {
		length += sizeof(output_none_1);
	}
    else
    {
        for(i=0; i<words->size; ++i)
		    length += (words->entry[i].length+1);
    }

	
    if(mh->output == NULL)
    {
        mh->output = (CHARTYPE *)nmalloc(sizeof(CHARTYPE)*length);
        mh->outputMaxLen = length;
    }
    else if(length > mh->outputMaxLen)
    {
        mh->output = (CHARTYPE *)nrealloc(mh->output, sizeof(CHARTYPE)*length);
        mh->outputMaxLen = length;
    }

    
	if(mh->output == NULL) {
		error("make_output", "Unable to reallocate output.");
		return NULL;
	}

    memset(mh->output,0, mh->outputMaxLen);

    if(words->size == 0) 
    {
        STRCPY(mh->output, output_none_1);
    }
    else
    {
	    length = 0;
	    for(i=0; i < words->size; ++i) 
        {
            if (words->entry[i].word[0] == (CHARTYPE)31)
			    tmp = 1;
		    else tmp = 0;

		    if (i>0 && (words->entry[i].word[0] != (CHARTYPE)31))
			    mh->output[length++]=L' ';

		    for(j=0; j < words->entry[i].length-tmp; ++j)
			    mh->output[length++]=words->entry[i].word[j+tmp];
	    }

	    mh->output[length]=L'\0';
    }

	return mh->output;
}


static void save_phrases(MEGAHAL * mh)
{
	unsigned long i, j;
	DICTIONARY *phrase;
	FILE *file;
	char *phrase2;
	char filename[512];
    MODEL * model = mh->model;

	Context;
	phrase = mh_new_dictionary();
	SNPRINTF(filename, sizeof(filename), "%s%smegahal.phr", mh->directory_cache, SEP);
	file = fopen(filename, "w");
	if(file == NULL) {
		warn("save_phrases", "Unable to open file");
		return;
	}

	for(i=0; i<model->phrasecount; ++i) {

		phrase->size = model->phrase[i][0]-1;
		if(mh_realloc_dictionary(phrase) == NULL) {
			error("save_phrases", "Unable to reallocate dictionary");
			return;
		}

		for(j=0; j<phrase->size; ++j)
			phrase->entry[j] = model->dictionary->entry[model->phrase[i][j+1]];

#ifdef USE_WCHAR
        phrase2 = mh_wchar_to_locale(make_output(mh,phrase));
#else
        phrase2 = make_output(mh,phrase);

#endif
        fputs(phrase2, file);
		fprintf(file, "\r\n");

#ifdef USE_WCHAR
		nfree(phrase2);
#endif
	}

	fclose(file);
	mh_free_dictionary(phrase);
    printf("MegaHAL: Phrases saved to %s\r\n", filename);
}
/*---------------------------------------------------------------------------*/

/*
 *	Function:	Save_Model
 *
 *	Purpose:	Save the current state to a MegaHAL brain file.
 */
static void save_model(MEGAHAL * mh, char *modelname)
{
	
    char filename[512];

	Context;

	//show_dictionary(mh->model->dictionary);
	save_phrases(mh);

	SNPRINTF(filename, sizeof(filename), "%s%s%s", mh->directory_cache, SEP, modelname);
    mh_save_model(filename, mh->model);

    printf("MegaHAL: Model saved to %s\r\n", filename);
}

MEGAHAL * mh_alloc()
{
    MEGAHAL * mh = (MEGAHAL *)malloc(sizeof(MEGAHAL));

    memset(mh, 0, sizeof(MEGAHAL));

    mh->timeout = 5;
    mh->order = 2;
    mh->surprise = 1;
    mh->maxreplywords = 0;

    strcpy(mh->directory_cache, DIR_DEFAULT_CACHE);
    strcpy(mh->directory_resources, DIR_DEFAULT_RESOURCES);

    mh->words=mh_new_dictionary();
	mh->prev1=mh_new_dictionary();
	mh->prev2=mh_new_dictionary();
	mh->prev3=mh_new_dictionary();
	mh->prev4=mh_new_dictionary();
	mh->prev5=mh_new_dictionary();

    return mh;

}

void mh_save(MEGAHAL * mh)
{
    if(mh != NULL)
    {
        save_model(mh, "megahal.brn");
    }
}

void mh_free(MEGAHAL * mh)
{
    if(mh != NULL)
    {
        save_model(mh, "megahal.brn");
	    /*rem_builtins(H_dcc, mega_dcc);
	    rem_builtins(H_pubm, mega_pubm);
	    rem_builtins(H_ctcp, mega_ctcp);
	    if((H_temp = find_bind_table("pub")))
		    rem_builtins(H_temp, mega_pub);
	    rem_tcl_ints(my_tcl_ints);
	    rem_tcl_coups(my_tcl_coups);
	    rem_tcl_strings(my_tcl_strings);
	    rem_tcl_commands(mytcls);
	    module_undepend(MODULE_NAME);*/

	    mh_free_model(mh->model);
	    mh_free_words(mh->ban);
	    mh_free_dictionary(mh->ban);
	    mh_free_words(mh->aux);
	    mh_free_dictionary(mh->aux);
	    mh_free_swap(mh->swp);
	    mh_free_words(mh->words);
	    mh_free_dictionary(mh->words);
	    mh_free_words(mh->prev1);
	    mh_free_dictionary(mh->prev1);
	    mh_free_words(mh->prev2);
	    mh_free_dictionary(mh->prev2);
	    mh_free_words(mh->prev3);
	    mh_free_dictionary(mh->prev3);
	    mh_free_words(mh->prev4);
	    mh_free_dictionary(mh->prev4);
	    mh_free_words(mh->prev5);
	    mh_free_dictionary(mh->prev5);

        free(mh);
    }
}

enum BOOLEAN_VALUE mh_load_personality(MEGAHAL * mh, const char * trainingTextFile)
{
	FILE *file;
	char filename[512];
	char filename_train[512];
	bool btrain = FALSE;

	Context;
	//setlocale(LC_ALL, "");

	/*
	 *	Check to see if the brain exists
	 */
	SNPRINTF(filename, sizeof(filename), "%s%smegahal.brn", mh->directory_cache, SEP);
	
    if((trainingTextFile == NULL) || (trainingTextFile[0] == 0))
    {
        SNPRINTF(filename_train, sizeof(filename_train), "%s%smegahal.trn", mh->directory_resources, SEP);
    }
    else
    {
        strncpy(filename_train, trainingTextFile, sizeof(filename_train) - 1);
    }

	file = fopen(filename, "r");
	if(file == NULL) {
		file = fopen(filename_train, "r");
		btrain = TRUE;
		if(file == NULL) {
			error("load_personality", "Unable to allocate directory");
            return BOOL_FALSE;
		}
        else
        {
            printf("MegaHAL : Initialised brain %s with training data %s\r\n", filename, filename_train);
        }
	}
    else
    {
        printf("MegaHAL : Loaded brain %s\r\n", filename);
    }

	if(!file) {
		error("load_personality", "Unable to find brain");
		return BOOL_FALSE;
	}
	fclose(file);
	putlog(LOG_MISC, "*", "Changing to MegaHAL personality brains = \"%s\", train = \"%s\".\r\n", mh->directory_cache, mh->directory_resources);

	/*
	 *	Free the current personality
	 */
	mh_free_model(mh->model);
	mh_free_words(mh->ban);
	mh_free_dictionary(mh->ban);
	mh_free_words(mh->aux);
	mh_free_dictionary(mh->aux);
	mh_free_swap(mh->swp);

	/*
	 *	Create a language model
	 */
	mh->model = mh_new_model(mh->order);

	/*
	 *	Train the model on a text if one exists
	 */
	if(btrain || mh_load_model(filename, mh->model) == FALSE) {
		train(mh->model, filename_train);
	}

	/*
	 *	Read a dictionary containing banned keywords, auxiliary keywords,
	 *	greeting keywords and swap keywords
	 */
	SNPRINTF(filename, sizeof(filename), "%s%smegahal.ban", mh->directory_resources, SEP);
	mh->ban = mh_initialize_list(filename);
	SNPRINTF(filename, sizeof(filename), "%s%smegahal.aux", mh->directory_resources, SEP);
	mh->aux = mh_initialize_list(filename);
	SNPRINTF(filename, sizeof(filename), "%s%smegahal.swp", mh->directory_resources, SEP);
	mh->swp = mh_initialize_swap(filename);
    return BOOL_TRUE;
}

/*---------------------------------------------------------------------------*/

static void change_personality(MEGAHAL * mh, const char * _directory_resources, const char * _directory_cache)
{
	if (_directory_resources) {
		SNPRINTF(mh->directory_resources, sizeof(mh->directory_resources), "%s", _directory_resources);
	}

	if (_directory_cache) {
		SNPRINTF(mh->directory_cache, sizeof(mh->directory_cache), "%s", _directory_cache);
	} else if (_directory_resources) {
		SNPRINTF(mh->directory_cache, sizeof(mh->directory_cache), "%s%s%s", mh->directory_resources, SEP, DIR_DEFAULT_CACHE);
	}

	mh_load_personality(mh, NULL);
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Make_Keywords
 *
 *	Purpose:	Put all the interesting words from the user's input into
 *			a keywords dictionary, which will be used when generating
 *			a reply.
 */
static DICTIONARY *make_keywords(MEGAHAL * mh, DICTIONARY *words)
{
	static DICTIONARY *keys=NULL;
	unsigned long i;
    unsigned long j;
	int c;

	Context;
	if(keys == NULL)
		keys = mh_new_dictionary();
	mh_free_words(keys);
	mh_free_dictionary(keys);

	for(i=0; i<words->size; ++i) {
		/*
		 *		Find the symbol ID of the word.  If it doesn't exist in
		 *		the model, or if it begins with a non-alphanumeric
		 *		wchar_tacter, or if it is in the exclusion array, then
		 *		skip over it.
		 */
		c = 0;
		for(j=0; j< mh->swp->size; ++j)
			if(mh_wordcmp(mh->swp->from[j], words->entry[i]) == 0) {
				mh_add_key(mh, keys, mh->swp->to[j]);
				++c;
			}
		if(c == 0)
			mh_add_key(mh, keys, words->entry[i]);
	}

	if(keys->size>0)
		for(i=0; i<words->size; ++i) {
			c=0;
			for(j=0; j< mh->swp->size; ++j)
				if(mh_wordcmp(mh->swp->from[j], words->entry[i]) == 0) {
					mh_add_aux(mh, keys, mh->swp->to[j]);
					++c;
				}
			if(c == 0)
				mh_add_aux(mh, keys, words->entry[i]);
		}

	return keys;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Seed
 *
 *	Purpose:	Seed the reply by guaranteeing that it contains a
 *			keyword, if one exists.
 */
static int seed(MEGAHAL * mh,  DICTIONARY *keys)
{
	unsigned long i;
	int symbol;
	int stop;
	bool fnd;
    MODEL * model = mh->model;

	Context;
	/*
	 *	Fix, thanks to Mark Tarrabain
	 */
	if(model->halcontext[0]->branch == 0)
		symbol = 0;
	else
		symbol = model->halcontext[0]->tree[mh_rnd(model->halcontext[0]->branch)]->symbol;

	if(keys->size>0) {
		i = mh_rnd(keys->size);
		stop = i;
		while(TRUE) {
			mh_search_dictionary(mh->aux, keys->entry[i], &fnd);
			if((mh_find_word(model->dictionary, keys->entry[i])!=0) && !fnd) {
				symbol = mh_find_word(model->dictionary, keys->entry[i]);
				return symbol;
			}
			++i;
			if(i == keys->size)
				i=0;
			if(i == stop)
				return symbol;
		}
	}

	return symbol;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Babble
 *
 *	Purpose:	Return a random symbol from the current context, or a
 *			zero symbol identifier if we've reached either the
 *			start or end of the sentence.  Select the symbol based
 *			on probabilities, favouring keywords.  In all cases,
 *			use the longest available context to choose the symbol.
 */
static int babble(MEGAHAL * mh, DICTIONARY *keys, DICTIONARY *words)
{
	TREE *node = NULL;
	long i;
	int count;
	int symbol = 0;
	bool fnd, fnd2;
    MODEL * model = mh->model;

	Context;
	/*
	 *	Select the longest available context.
	 */
	for(i=0; i<=model->order; ++i)
		if(model->halcontext[i] != NULL)
			node=model->halcontext[i];

	if(node->branch == 0)
		return 0;

	/*
	 *	Choose a symbol at random from this context.
	 */
	i = mh_rnd(node->branch);
	count = mh_rnd(node->usage);
	while(count >= 0) {
		/*
		 *	If the symbol occurs as a keyword, then use it.  Only use an
		 *	auxilliary keyword if a normal keyword has already been used.
		 */
		symbol = node->tree[i]->symbol;

		mh_search_dictionary(keys, model->dictionary->entry[symbol], &fnd);
		mh_search_dictionary(mh->aux, model->dictionary->entry[symbol], &fnd2);
		if(fnd && ((mh->used_key==TRUE) || !fnd2) && (mh_word_exists(words, model->dictionary->entry[symbol])==FALSE)) {
			mh->used_key = TRUE;
			break;
		}
		count -= node->tree[i]->count;
		i = (i >= (node->branch-1)) ? 0 : i+1;
	}

	return symbol;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Reply
 *
 *	Purpose:	Generate a dictionary of reply words appropriate to the
 *			given dictionary of keywords.
 */
static DICTIONARY *reply(MEGAHAL * mh, DICTIONARY *keys)
{
	long i;
	int symbol;
	bool start = TRUE;
	int basetime;
    DICTIONARY * replies;
    MODEL * model = mh->model;

	Context;
	if(mh->replies == NULL)
		mh->replies = mh_new_dictionary();
	mh_free_dictionary(mh->replies);

    replies = mh->replies;

	/*
	 *	Start off by making sure that the model's context is empty.
	 */
	mh_initialize_context(model);
	model->halcontext[0] = model->forward;
	mh->used_key = FALSE;

	/*
	 *	Generate the reply in the forward direction.
	 */
	/* This used to be while(TRUE) and while it should never get stuck in an infinite loop in theory, this was changed just in case to timeout cause it can grab ram like crazy until it sigterms */
	basetime = (int)time(NULL);
	while((time(NULL)-basetime) < mh->timeout+2) {
		/*
		 *	Get a random symbol from the current context.
		 */
		if(start == TRUE)
			symbol = seed(mh, keys);
		else
			symbol = babble(mh, keys, replies);
		if((symbol==0) || (symbol==1))
			break;
		start = FALSE;

		/*
		 *	Append the symbol to the reply dictionary.
		 */
		replies->size += 1;
		if(mh_realloc_dictionary(replies) == NULL) {
			error("reply", "Unable to reallocate dictionary");
			return NULL;
		}

		replies->entry[replies->size-1].length = model->dictionary->entry[symbol].length;
		replies->entry[replies->size-1].word = model->dictionary->entry[symbol].word;

		/*
		 *	Extend the current context of the model with the current symbol.
		 */
		mh_update_context(model, symbol);
	}
	if((time(NULL)-basetime) >= mh->timeout+2)
		putlog(LOG_MISC, "*", "MEGAHAL TIMEOUT1!");


	/*
	 *	Start off by making sure that the model's context is empty.
	 */
	mh_initialize_context(model);
	model->halcontext[0] = model->backward;

	/*
	 *	Re-create the context of the model from the current reply
	 *	dictionary so that we can generate backwards to reach the
	 *	beginning of the string.
	 */
	if(replies->size > 0)
		for(i=MIN(replies->size-1, model->order); i>=0; --i) {
			symbol = mh_find_word(model->dictionary, replies->entry[i]);
			mh_update_context(model, symbol);
		}

	/*
	 *	Generate the reply in the backward direction.
	 */
	basetime = (int)time(NULL);
	while((time(NULL)-basetime) < mh->timeout+2) {
		/*
		 *	Get a random symbol from the current context.
		 */
		symbol = babble(mh, keys, replies);
		if((symbol==0) || (symbol==1))
			break;

		/*
		 *	Prepend the symbol to the reply dictionary.
		 */
		replies->size += 1;
		if(mh_realloc_dictionary(replies) == NULL) {
			error("reply", "Unable to reallocate dictionary");
			return NULL;
		}

		/*
		 *	Shuffle everything up for the prepend.
		 */
		for(i=replies->size-1; i>0; --i) {
			replies->entry[i].length = replies->entry[i-1].length;
			replies->entry[i].word = replies->entry[i-1].word;
		}

		replies->entry[0].length = model->dictionary->entry[symbol].length;
		replies->entry[0].word = model->dictionary->entry[symbol].word;

		/*
		 *	Extend the current context of the model with the current symbol.
		 */
		mh_update_context(model, symbol);
	}
	if((time(NULL)-basetime) >= mh->timeout+2)
		putlog(LOG_MISC, "*", "MEGAHAL TIMEOUT2!");

	return replies;
}

static bool isinprevs(MEGAHAL * mh, DICTIONARY *words)
{
	Context;
	if (mh_dissimilar2(words, mh->prev1) && mh_dissimilar2(words, mh->prev2) && mh_dissimilar2(words, mh->prev3) &&
	    mh_dissimilar2(words, mh->prev4) && mh_dissimilar2(words, mh->prev5))
		return FALSE;

	return TRUE;
}


static void updateprevs(MEGAHAL * mh, CHARTYPE *words)
{
	Context;
	mh_free_words(mh->prev1);
	mh_free_dictionary(mh->prev1);
	nfree(mh->prev1);

	mh->prev1 = mh->prev2;
	mh->prev2 = mh->prev3;
	mh->prev3 = mh->prev4;
	mh->prev4 = mh->prev5;

	mh->prev5 = mh_new_dictionary();
	mh_make_words(words, mh->prev5);
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Evaluate_Reply
 *
 *	Purpose:	Measure the average surprise of keywords relative to the
 *			language model.
 */
static float evaluate_reply(MEGAHAL * mh, DICTIONARY *keys, DICTIONARY *words)
{
	unsigned long i;
    long is;
	int j;
	int symbol;
	float probability;
	int count;
	float entropy = (float)0.0;
	TREE *node;
	int num = 0;
	bool fnd;
    MODEL * model = mh->model;

	Context;
	if(words->size <= 0)
		return (float)0.0;
	mh_initialize_context(model);
	model->halcontext[0] = model->forward;
	for(i=0; i < words->size; ++i) 
    {
		symbol = mh_find_word(model->dictionary, words->entry[i]);

		// only calculate values for words in the reply that are also keywords in the original sentence
		mh_search_dictionary(keys, words->entry[i], &fnd);
		if(fnd) {
			probability = (float)0.0;
			count = 0;
			++num;
			for(j=0; j<model->order; ++j)
				if(model->halcontext[j] != NULL) {
					node = mh_find_symbol(model->halcontext[j], symbol);
					// the less that this word is used in this context, the higher the score
					// this is because we are dividing the amount of times the word is used in this context by the usage counter of the parent context
					if (mh->surprise)
						probability += (float)(node->count)/(float)(model->halcontext[j]->usage);
					else
						probability += (float)((float)1.0-((node->count)/(float)(model->halcontext[j]->usage)));
					
                    ++count;
				}

			// log of <1 numbers are negative which is why we do -=
			// this will weigh the result according to the size of the context i think
			// in other words, the bigger the context, the higher the result value becomes
			if(count > 0.0)
				entropy -= (float)log(probability/(float)count);
		}

		mh_update_context(model, symbol);
	}

	mh_initialize_context(model);
	model->halcontext[0] = model->backward;
	for(is=words->size-1; is>=0; --is) {
		symbol = mh_find_word(model->dictionary, words->entry[is]);

		mh_search_dictionary(keys, words->entry[is], &fnd);
		if(fnd) {
			probability = (float)0.0;
			count = 0;
			++num;
			for(j=0; j<model->order; ++j)
				if(model->halcontext[j] != NULL) {
					node = mh_find_symbol(model->halcontext[j], symbol);
					if (mh->surprise)
						probability += (float)(node->count)/(float)(model->halcontext[j]->usage);
					else
						probability += (float)((float)1.0-((node->count)/(float)(model->halcontext[j]->usage)));
					++count;
				}

			if(count > 0.0)
				entropy -= (float)log(probability/(float)count);
		}

		mh_update_context(model, symbol);
	}

	// hmm this looks like it helps to average out all sentences including long sentences with many keywords so that they are all comparable?
	if(num >= 8)
		entropy /= (float)sqrt((float)(num-1));
	if(num >= 16)
		entropy /= (float)num;

	return entropy;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Generate_Reply
 *
 *	Purpose:	Take a string of user input and return a string of output
 *			which may vaguely be construed as containing a reply to
 *			whatever is in the input string.
 */
static CHARTYPE * generate_reply(MEGAHAL * mh, DICTIONARY *words)
{	
	DICTIONARY *replywords;
	DICTIONARY *keywords;
	float surprise;
	float max_surprise;
	CHARTYPE *output;
		
	int basetime;
    DICTIONARY * dummy;

	Context;
	/*
	 *	Create an array of keywords from the words in the user's input
	 */
	keywords = make_keywords(mh, words);



	if(mh->dummy == NULL)
		mh->dummy = mh_new_dictionary();

    dummy = mh->dummy;

	replywords = reply(mh, dummy);

	basetime = (int)time(NULL);
    while(((mh->maxreplywords && replywords->size > mh->maxreplywords) || mh_dissimilar(words, replywords)==FALSE || mh_isrepeating(mh->model, replywords) || isinprevs(mh, replywords)) && (time(NULL)-basetime) < mh->timeout )
		replywords = reply(mh, dummy);
	
    output = make_output(mh, replywords);
    if(output == NULL)
    {
        return NULL;
    }

	/*
	 *	Loop for the specified waiting period, generating and evaluating
	 *	replies
	 */
	max_surprise = (float)-1.0;
	basetime = (int)time(NULL);
	do {
		replywords = reply(mh, keywords);
		
        if ((mh->maxreplywords && replywords->size > mh->maxreplywords) || mh_dissimilar(words, replywords)==FALSE ||
            mh_isrepeating(mh->model, replywords) || isinprevs(mh, replywords))
			continue;

		surprise = evaluate_reply(mh, keywords, replywords);
		if(surprise > max_surprise) {
			max_surprise = surprise;
			output = make_output(mh, replywords);
		}
	} while((time(NULL)-basetime) < mh-> timeout);
	
    updateprevs(mh, output);

	/*
	 *	Return the best answer we generated
	 */
	return output;
}

size_t mh_get_model_size(MEGAHAL * mh)
{
    if((mh == NULL) || (mh->model == NULL))
        return 0;

    return megahal_expmem(mh);
}

void mh_do_megahal(MEGAHAL * mh, int idx,  char *prefix, char *text, enum BOOLEAN_VALUE learnit,  MegahalResponseCallback responseCallback, void * callbackParam)
{
	char * stuff = (char *)malloc(strlen(prefix) + 50);
    char *lhalreply;//, *lmbotnick;
	CHARTYPE *halreply;
    CHARTYPE * wtext;
    static const char * output_none = "I forgot what I was going to say!";

    //init if required
    if(mh->model == NULL)
    {
        mh_load_personality(mh, NULL);
    }

	Context;
	/* Is there anything to parse? */
	if((text != NULL)  && (text[0] != 0))
    {
	    //setlocale(LC_ALL, "");
        #ifdef USE_WCHAR
            wtext = mh_locale_to_wchar(text);
        #else
            wtext = text;
        #endif

	    mh_upper(wtext);	    
        if(mh_mystrstr(wtext, _T("HTTP")) != NULL) {
            #ifdef USE_WCHAR
		        nfree(wtext);
            #endif
		    return;
	    }
	
        mh_make_words(wtext, mh->words);
	    Context;

	    if(mh->learningmode && learnit)
		    learn(mh->model, mh->words);
    }

	halreply = generate_reply(mh, mh->words);
    if(halreply == NULL)
    {
        #ifdef USE_WCHAR
		    nfree(wtext);
        #endif
        responseCallback(idx,  prefix, output_none, callbackParam);
        return;
    }
	
    Context;
	
    //mh_capitalize(halreply);
    #ifdef USE_WCHAR
	lhalreply = mh_wchar_to_locale(halreply);
    #else
    lhalreply = halreply;
    #endif

    responseCallback(idx,  prefix, lhalreply, callbackParam);
	
    
    //lmbotnick = mh_wchar_to_locale(mh->mbotnick);
	/*if(nick)
		putlog(LOG_PUBLIC, chan, "<%s> %s, %s", lmbotnick, nick, lhalreply);
	else if(chan)
		putlog(LOG_PUBLIC, chan, "<%s> %s", lmbotnick, lhalreply);
        */
	
    //nfree(lmbotnick);
	#ifdef USE_WCHAR
	nfree(lhalreply);	
    nfree(wtext);
    #endif
}


void mh_trimbrain(MEGAHAL * mh, const size_t maxsize)
{
    size_t curSize = megahal_expmem(mh);
    int i;
    if(curSize <= maxsize)
        return;

    
    while((mh->model->phrasecount > 0) && (curSize > maxsize))
    {
        putlog(LOG_MISC, "*", "Current size is %uk, Max size is %uk ... Trimming brain...", curSize / 1024, maxsize / 1024);

        //Repeat 10 times, to reduce the amount of times we query the model size
        for(i=0; i < 10; ++i)
        {
            if(mh->model->phrasecount < 1) 
                break;

            del_phrase(mh->model, 0);
        }

        curSize = megahal_expmem(mh);

    }

    mh_trimdictionary(mh->model);

	putlog(LOG_MISC, "*", "Brain trimmed to %uk", megahal_expmem(mh) / 1024);
}