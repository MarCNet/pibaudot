#include <string.h>

#include "megahal_lib.h"
#include "megahal_internal.h"

#define error mh_error

/*
 *	Function:	New_Swap
 *
 *	Purpose:	Allocate a new swap structure.
 */
static SWAP *new_swap(void)
{
	SWAP *list;

	Context;
	list = (SWAP *)nmalloc(sizeof(SWAP));
	if(list == NULL) {
		error("new_swap", "Unable to allocate swap");
		return NULL;
	}
	list->size = 0;
	list->from = NULL;
	list->to = NULL;

	return list;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Add_Swap
 *
 *	Purpose:	Add a new entry to the swap structure.
 */
static void add_swap(SWAP *list, CHARTYPE *s, CHARTYPE *d)
{
	Context;
	list->size += 1;

	if(list->from == NULL) {
		list->from = (STRING *)nmalloc(sizeof(STRING));
		if(list->from == NULL) {
			error("add_swap", "Unable to allocate list->from");
			return;
		}
	}

	if(list->to == NULL) {
		list->to = (STRING *)nmalloc(sizeof(STRING));
		if(list->to == NULL) {
			error("add_swap", "Unable to allocate list->to");
			return;
		}
	}

	list->from = (STRING *)nrealloc(list->from, sizeof(STRING)*(list->size));
	if(list->from == NULL) {
		error("add_swap", "Unable to reallocate from");
		return;
	}

	list->to = (STRING *)realloc(list->to, sizeof(STRING)*(list->size));
	if(list->to == NULL) {
		error("add_swap", "Unable to reallocate to");
		return;
	}

	list->from[list->size-1].length = STRLEN(s);
	list->from[list->size-1].word = mh_mystrdup(s);
	list->to[list->size-1].length = STRLEN(d);
	list->to[list->size-1].word = mh_mystrdup(d);
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Initialize_Swap
 *
 *	Purpose:	Read a swap structure from a file.
 */
SWAP * mh_initialize_swap(char *filename)
{
	SWAP *list;
	FILE *file = NULL;
	char buffer[1024];
	char *from;
	char *to;

	Context;
	//setlocale(LC_ALL, "");
	list = new_swap();

	if(filename == NULL)
		return list;

	file = fopen(filename, "r");
	if(file == NULL)
		return list;

	while(!feof(file)) {
		if(fgets(buffer, 1024, file)==NULL)
			break;
		if(buffer[0]=='#')
			continue;
		from = strtok(buffer, "\t ");
		to = strtok(NULL, "\t \n#");
		if (from && to) {
#ifdef USE_WCHAR
            wchar_t * wfrom = mh_locale_to_wchar(from);
			wchar_t * wto = mh_locale_to_wchar(to);
#else
            char * wfrom = STRDUP(from);
			char * wto = STRDUP(to);
#endif 
			add_swap(list, wfrom , wto);
		}
	}

	fclose(file);
	return list;
}

void mh_free_swap(SWAP *swap)
{
	register int i;

	Context;
	if(swap == NULL)
		return;

	for(i=0; i<swap->size; ++i) {
		mh_free_word(swap->from[i]);
		mh_free_word(swap->to[i]);
	}
	nfree(swap->from);
	nfree(swap->to);
	nfree(swap);
}