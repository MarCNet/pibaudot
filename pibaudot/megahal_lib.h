
/*===========================================================================*/

/*
 *  Copyright (C) 1998 Jason Hutchens
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the license or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the Gnu Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*===========================================================================*/

/*
 *		$Id: megahal.h,v 1.3 1998/09/03 03:15:40 hutch Exp hutch $
 *
 *		File:			megahal.h
 *
 *		Program:		MegaHAL v8r5
 *
 *		Purpose:		To simulate a natural language conversation with a psychotic
 *						computer.  This is achieved by learning from the user's
 *						input using a third-order Markov model on the word level.
 *						Words are considered to be sequences of characters separated
 *						by whitespace and punctuation.  Replies are generated
 *						randomly based on a keyword, and they are scored using
 *						measures of surprise.
 *
 *		Author:		Mr. Jason L. Hutchens
 *
 *		WWW:			http://ciips.ee.uwa.edu.au/~hutch/hal/
 *
 *		E-Mail:		hutch@ciips.ee.uwa.edu.au
 *
 *		Contact:		The Centre for Intelligent Information Processing Systems
 *						Department of Electrical and Electronic Engineering
 *						The University of Western Australia
 *						AUSTRALIA 6907
 *
 *		Phone:		+61-8-9380-3856
 *
 *		Facsimile:	+61-8-9380-1168
 *
 *		Notes:		This file is best viewed with tabstops set to three spaces.
 */

/*===========================================================================*/

#ifndef __MEGAHAL__LIB__H__
#define __MEGAHAL__LIB__H__

#include "internal.h"
#include <wchar.h>
#include <ctype.h>

//#define USE_WCHAR 

#define DIR_DEFAULT_RESOURCES "megahal.data/default"
#define DIR_DEFAULT_CACHE "brains"

#define BYTE1 unsigned char
#define BYTE2 unsigned short
#define BYTE4 unsigned long




#ifdef USE_WCHAR
        #define CHARTYPE wchar_t
#else
    #define CHARTYPE char
#endif

typedef struct {
	BYTE1 length;
	CHARTYPE *word;
} STRING;

typedef struct {
	BYTE4 size;
	STRING *entry;
	BYTE2 *index;
} DICTIONARY;

typedef struct {
	BYTE2 size;
	STRING *from;
	STRING *to;
} SWAP;

typedef struct NODE {
	BYTE2 symbol;
	BYTE4 usage;
	BYTE2 count;
	BYTE2 branch;
	struct NODE **tree;
} TREE;

typedef struct {
	BYTE1 order;
	TREE *forward;
	TREE *backward;
	TREE **halcontext;
	BYTE4 phrasecount;
	BYTE2 **phrase;
	DICTIONARY *dictionary;
} MODEL;

typedef struct _MEGAHAL
{
    int order;
    int timeout;
    size_t maxreplywords;
    int surprise;
        
    MODEL *model;
    DICTIONARY *ban;
    DICTIONARY *aux;
    SWAP *swp;
    DICTIONARY *words;
    enum BOOLEAN_VALUE learningmode;
    enum BOOLEAN_VALUE used_key;
    char directory_cache[513];
    char directory_resources[513];
    CHARTYPE mbotnick[32];
    uint64_t debugStartTick;

    //globals
    DICTIONARY *replies;
    DICTIONARY *dummy;
    DICTIONARY *prev1, *prev2, *prev3, *prev4, *prev5;
    CHARTYPE * output;
    size_t outputMaxLen;
}MEGAHAL;

MEGAHAL * mh_alloc();

void mh_free(MEGAHAL * mh);

enum BOOLEAN_VALUE mh_load_personality(MEGAHAL * mh, const char * trainingTextFile);

typedef void (BD_CALLBACK *MegahalResponseCallback)(int idx, const char *prefix, const char * reply, void * callbackParam);
void mh_do_megahal(MEGAHAL * mh,  int idx, char *prefix, char *text, enum BOOLEAN_VALUE learnit, MegahalResponseCallback responseCallback, void * callbackParam);

void mh_save(MEGAHAL * mh);

size_t mh_get_model_size(MEGAHAL * mh);

void mh_trimbrain(MEGAHAL * mh, const size_t maxsize);

#endif
