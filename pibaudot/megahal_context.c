#include "megahal_lib.h"
#include "megahal_internal.h"




/*---------------------------------------------------------------------------*/

/*
 *	Function:	Initialize_Context
 *
 *	Purpose:	Set the context of the model to a default value.
 */
void mh_initialize_context(MODEL *model)
{
	register int i;

	Context;
	for(i=0; i<=model->order; ++i)
		model->halcontext[i] = NULL;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Update_Context
 *
 *	Purpose:	Update the context of the model without adding the symbol.
 */
void mh_update_context(MODEL *model, int symbol)
{
	register int i;

	Context;
	for(i=(model->order+1); i>0; --i)
		if(model->halcontext[i-1] != NULL)
			model->halcontext[i] = mh_find_symbol(model->halcontext[i-1], symbol);
}
