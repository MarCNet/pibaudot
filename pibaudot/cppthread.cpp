#include <stdint.h>
#include <stddef.h>

extern "C"
{
    #include "internal.h"
}

#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <map>
#include <sstream>

typedef struct _ThreadData
{
    std::thread * thrd;
    std::mutex * mtx;
    enum BOOLEAN_VALUE debug;
    std::condition_variable * evt;
    std::condition_variable * startevt;
    std::condition_variable * goevt;
    enum BOOLEAN_VALUE signalled;
    enum BOOLEAN_VALUE quit;

} ThreadData;

extern "C" ThreadHandle cppthrd_CreateLockOnly()
{
    ThreadData * threadData = new ThreadData();

    threadData->mtx = new std::mutex();
    threadData->evt = new std::condition_variable();
    threadData->goevt = NULL;
    threadData->startevt = NULL;
    threadData->signalled = BOOL_FALSE;
    threadData->quit = BOOL_FALSE;

    threadData->thrd = NULL;
    threadData->debug = BOOL_FALSE;
    
    return threadData;
}

extern "C" ThreadHandle cppthrd_CreateThread(void * param, CPPThreadCallback threadProc, enum BOOLEAN_VALUE debug)
{
    ThreadData * threadData = (ThreadData * )cppthrd_CreateLockOnly();

    threadData->debug = debug;
    threadData->goevt = new std::condition_variable();
    threadData->startevt = new std::condition_variable();

    if(debug)
        printf("  main locked \r\n");

    std::unique_lock<std::mutex> ul(*threadData->mtx);

    threadData->thrd = new std::thread( [param, threadProc, threadData, debug]()
                                        {
                                            std::ostringstream threadidstrm;

                                            threadidstrm << std::this_thread::get_id();
                                            const char * threadid = threadidstrm.str().c_str();
    
                                            //Wait for start event
                                            if(debug)
                                                printf(" %s : thread lock\r\n",threadid);

                                            {
                                                std::unique_lock<std::mutex> ul(*threadData->mtx);

                                                //Signal start event
                                                if(debug)
                                                    printf("  %s : signal started \r\n", threadid);
                                                
                                                threadData->startevt->notify_all();

                                                //Wait for signal
                                                if(debug)
                                                    printf("  %s : wait for signal\r\n", threadid);
                                                
                                                threadData->goevt->wait(ul);

                                                if(debug)
                                                    printf("  %s : thread unlock \r\n", threadid);
                                            }

                                            if(!threadData->quit)
                                            {
                                                if(debug)
                                                    printf("  %s : thread run\r\n", threadid);
                                                
                                                threadProc(param);
                                            }
                                            else
                                            {
                                                if(debug)
                                                    printf("  %s : not running thread set as quit\r\n", threadid);
                                            }

                                            if(debug)
                                                printf("  %s : thread quit\r\n", threadid);
                                        });

  

    //Wait for thread to start
    if(debug)
        printf("  wait for start \r\n");

    threadData->startevt->wait(ul);

    if(debug)
        printf("  main unlock \r\n");

    ul.unlock();

    return threadData;
}

extern "C" void cppthrd_StartThread(ThreadHandle threadhnd, enum BOOLEAN_VALUE isLocked)
{
    ThreadData * threadData = (ThreadData *)threadhnd;
    if(threadData->thrd != NULL)
    {
        if(!isLocked)
            cppthrd_LockThread(threadData);

        if(threadData->goevt != NULL)
            threadData->goevt->notify_all();

        if(!isLocked)
            cppthrd_UnlockThread(threadData);
    }
}


extern "C" void cppthrd_WaitAndDestroyThread(ThreadHandle threadhnd)
{
    ThreadData * threadData = (ThreadData *)threadhnd;
    std::ostringstream threadidstrm;
    const char * threadid = "";
    const bool debug = (threadData->debug == BOOL_TRUE);

    
    if(threadData->thrd != NULL)
    {
        threadidstrm << threadData->thrd->get_id();
        threadid = threadidstrm.str().c_str();
        
        //Notify thread
        {
            if(debug)
                printf("  %s : Destroy : lock\r\n", threadid);

            std::lock_guard<std::mutex> lg(*threadData->mtx);

            threadData->quit = BOOL_TRUE;

            if(threadData->goevt != NULL)
            {
                if(debug)
                    printf("  %s : Destroy : notify go\r\n", threadid);

                threadData->goevt->notify_all();
            }
        
            if(threadData->evt != NULL)
            {
                if(debug)
                    printf("  %s : Destroy : notify event\r\n", threadid);
                
                threadData->evt->notify_all();
            }

            if(debug)
                printf("  %s : Destroy : unlock", threadid);
        }

        //Wait for thread to terminate
        if(debug)
            printf("  %s : Destroy : join\r\n", threadid);
        
        threadData->thrd->join();

        if(debug)
            printf("  %s : Destroy : join done\r\n", threadid);
    }

    if(debug)
        printf("  %s : Destroy : del evt\r\n", threadid);
    
    delete threadData->evt;
    
    if(debug)
        printf("  %s : Destroy : del startevt\r\n", threadid);
    
    delete threadData->startevt;
    
    if(debug)
        printf("  %s : Destroy : del mtx\r\n", threadid);
    
    delete threadData->mtx;
    
    if(debug)
        printf("  %s :  Destroy : del thrd\r\n", threadid);
    
    delete threadData->thrd;

    if(debug)
        printf("  %s : Destroy : del data\r\n", threadid);
    
    delete threadData;

    if(debug)
        printf("  Destroy : del done\r\n", threadid);
}

extern "C" void cppthrd_LockThread(ThreadHandle threadhnd)
{
    ThreadData * threadData = (ThreadData *)threadhnd;
    threadData->mtx->lock();
}

extern "C" void cppthrd_UnlockThread(ThreadHandle threadhnd)
{
    ThreadData * threadData = (ThreadData *)threadhnd;
    threadData->mtx->unlock();
}

extern "C" void cppthrd_SignalThreadEvent(ThreadHandle threadhnd, enum BOOLEAN_VALUE isLocked)
{
    ThreadData * threadData = (ThreadData *)threadhnd;

    
    if(!isLocked)
        threadData->mtx->lock();

    threadData->signalled = BOOL_TRUE;
    threadData->evt->notify_all();

    if(!isLocked)
        threadData->mtx->unlock();
}

extern "C" enum BOOLEAN_VALUE cppthrd_WaitForEvent(ThreadHandle threadhnd, unsigned int timeoutms, enum BOOLEAN_VALUE isLocked)
{
    ThreadData * threadData = (ThreadData *)threadhnd;
    enum BOOLEAN_VALUE ret = BOOL_FALSE;

    std::unique_lock<std::mutex> ul(*threadData->mtx, std::defer_lock);
    if(!isLocked)
        ul.lock();
   
    if(threadData->signalled)
    {
        ret = BOOL_TRUE;
    }
    else
    {   
        if(timeoutms == 0)
        {
            threadData->evt->wait( ul ); 
            ret = threadData->signalled;
        }
        else
        {
            ret = threadData->evt->wait_for( ul, std::chrono::milliseconds(timeoutms), [threadData]{ return threadData->signalled == BOOL_TRUE; }) ? BOOL_TRUE : BOOL_FALSE;
        }
    }

    threadData->signalled = BOOL_FALSE;

    if(!isLocked)
        ul.unlock();

    return ret;
}


#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif

extern "C" void cppThread_sleep_ms(int milliseconds)
{ // cross-platform sleep function
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    if (milliseconds >= 1000)
      sleep(milliseconds / 1000);
    usleep((milliseconds % 1000) * 1000);
#endif
}