#include <string.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

#define _USE_MATH_DEFINES
#include <math.h>

#include <ctype.h>


#include "internal.h"

enum ProcessState
{
    PROCSTATE_START = 0,
    PROCSTATE_SILENCE,
    PROCSTATE_SILENCE_AFTER_DATA,
    PROCSTATE_FIND_SIGNAL,
    PROCSTATE_FIND_DATA,
    PROCSTATE_DATA_START,
    PROCSTATE_DATA_PROC_LOW,
    PROCSTATE_DATA_PROC_HIGH,
    PROCSTATE_PADDING,
    PROCSTATE_DATA_BREAK,
};

typedef struct _ProcessStateData
{
    enum ProcessState state;
    double bitLengthMs;
    double transitionFrameMs;
    uint8_t currentValue;
    uint8_t valueShift;
    enum BOOLEAN_VALUE baudotShiftState;
    enum BOOLEAN_VALUE abort;

    uint64_t silenceAbsoluteStartPos;

} ProcessStateData;

typedef struct _BaudotData
{
    BauDot_Decoder_Options opts;
    unsigned int sampleRate;
    unsigned int channels;
    unsigned int bitsPerSample;
    enum WAV_FORMAT format;
    ProcessStateData stateData;
    GPIOHandle gpioHandle;
    
    CharacterReceivedCallback callback;
    void * callbackParam;
    
    uint64_t absoluteBufferStartPos;
    MemoryBuffer * sampBuffer;
    size_t sampleBufferReadPos;
    
    double * Qr;
    int numFreqPoints;
    
} BaudotData;


static uint64_t encode_baudot_character_ITA2(char c, enum BOOLEAN_VALUE * pbShift)
{
    //For each entry, top bit set means that shift mode needs to be enabled
    static const uint8_t conversion_array[] = 
    {
            4,              (13 | 0x80),    (5 | 0x80),     4,              (20 | 0x80),    (13 | 0x80),    (26 | 0x80),   (5 | 0x80),  (15 | 0x80),  (18 | 0x80),      //32 - 41
            (25 | 0x80),    (17 | 0x80),    (12 | 0x80),    (3 | 0x80),     (28 | 0x80),    (29 | 0x80),    //42-47
            (22 | 0x80),    (23 | 0x80),    (19 | 0x80),    (1 | 0x80),     (10 | 0x80),    (16 | 0x80),    (21 | 0x80),    (7 | 0x80), (6 | 0x80),  (24 | 0x80),    //48-57
            (14 | 0x80),    (30 | 0x80),    (25 | 0x80),    (25 | 0x80),    (25 | 0x80),    (25 | 0x80),    (26 | 0x80),   //58 - 64
            3,              25,             14,             9,              1,              13,             26,             20,         6,          11,     //65 - 74  (a-j)
            15,             18,             28,             12,             24,             22,             23,             10,         5,          16,     //75 - 84 (k - t)
            7,              30,             19,             29,             21,             17,  //85 - 90 (u - z)
    };

    //Force upper case
    char upperc = toupper(c);
    uint64_t code;

    if((upperc < ' ') || (upperc > 'Z'))
        return 0; //character not supported

    //Access our conversion array
    upperc -= ' ';
    
    //Extract code
    code = conversion_array[upperc];

    //If a shift toggle is required, then add that to the return code
    if((code & 0x80) && !(*pbShift))
    {
        //Strip our internal shift bit, and move to next character - as the shift activate needs to go first
        code &= 0x7F;
        code <<= 8; 

        //Shift mode needs to be activated first  (lowest byte is encoded first)
        code |= 27;

        //Set shifted mode
        *pbShift = BOOL_TRUE;
    }
    else if(((code & 0x80) == 0) && (*pbShift))
    {
        //Strip our internal shift bit, and move to next character - as the shift deactivate needs to go first
        code &= 0x7F;
        code <<= 8;

        //Shift mode needs to be deactivated first (lowest byte is encoded first)
        code |= 31;

        //Set non-shifted mode
        *pbShift = BOOL_FALSE;
    }
    else
    {
        //Strip our internal shift bit
        code &= 0x7F;
    }

    return code;
}


static char decode_baudot_value_ITA2(uint8_t value, enum BOOLEAN_VALUE * pbShift)
{
    const char * table = NULL;
    char convval;

    static char conversion_array_unshifted[32] = 
    {
        //          0       1       2       3

        /*0*/       0,      'E',    '\n',   'A',
        /*4*/       ' ',    'S',    'I',    'U', 
        /*8*/       '\r',   'D',    'R',    'J',
        /*12*/      'N',    'F',    'C',    'K',
        /*16*/      'T',    'Z',    'L',    'W',
        /*20*/      'H',    'Y',    'P',    'Q',
        /*24*/      'O',    'B',    'G',    '\x001',   //001 is an internal code to activate 'shift' mode. For our use only, and not in any existing baudot specification.
        /*28*/      'M',    'X',    'V',    '\x002',   //002 is an internal code to deactivate 'shift' mode. For our use only, and not in any existing baudot specification.
    };


    static char conversion_array_shifted[32] = 
    {
        //          0       1       2       3

        /*0*/       0,      '3',    '\n',   '-',
        /*4*/       ' ',    '\'',   '8',    '7', 
        /*8*/       '\r',  '\x003', '4',    '\x004',    //003 is an internal code for 'Who are you - (WRU), 004 is bell.  Both for our use only, and not in any existing baudot specification.
        /*12*/      ',',    '!',    ':',    '(',
        /*16*/      '5',    '+',    ')',    '2',
        /*20*/      '�',    '6',    '0',    '1',        //Currency symbol. We'll use �, because I'm English.  (TODO: avoid xenophobia, and make this an option?)
        /*24*/      '9',    '?',    '&',    '\x001',   //001 is an internal code to activate 'shift' mode. For our use only, and not in any existing baudot specification.
        /*28*/      '.',    '/',    ';',    '\x002',   //002 is an internal code to deactivate 'shift' mode. For our use only, and not in any existing baudot specification.
    };

    //===================================================

    //If the value is more than 31, then that's an error - (value too big for 5 bits!)
    if(value >= 32)
        return 0;

    //Convert the value, using the correct table (shifted vs non-shifted)
    table = (*pbShift == BOOL_TRUE) ? conversion_array_shifted : conversion_array_unshifted;
    convval = table[value];

    //Does the value use some special internal character code, for example switching on/off shifting?
    switch(convval)
    {
        case 0:
            //Treat a zero converted code into backspace (ascii code 8)
            convval = '\b';
            break;
        case 1: //001 is an internal code to activate 'shift' mode. 
            *pbShift = BOOL_TRUE;
            convval = 0; //Don't return a character 
            break;

        case 2: //002 is an internal code to deactivate 'shift' mode. 
            *pbShift = BOOL_FALSE;
            convval = 0; //Don't return a character 
            break;

        case 3: //003 is an internal code for 'Who are you - (WRU) 
        case 4: //004 is bell. 
            convval = 0; //Don't return a character 
            break;
    }

    return convval;
}


//=================================================================================================================
static void processValue(BaudotData * baudotdata)
{
    ProcessStateData * currentState = &baudotdata->stateData;
    const BauDot_Decoder_Options * opts = &baudotdata->opts;
    size_t currentValue = currentState->currentValue;
    char decvalue;
    enum BOOLEAN_VALUE ret;

    //There are always 'low' bits before the actual useful bits, used for syncronisation - so strip the sync bits off
    currentValue >>= baudotdata->opts.syncSizeBits;

    //Decode
    decvalue = decode_baudot_value_ITA2(currentValue, &currentState->baudotShiftState);
    if(baudotdata->opts.baudotTediousDebug)
        printf("\t\tGot value %u (0x%02x) (from 0x%02x). Shift state is %u, Decoded to %u - character %c\r\n", currentValue, currentValue, currentState->currentValue, currentState->baudotShiftState, decvalue, decvalue);
    
    //Call the receive callback
    //This may return false to indicate we should stop
    ret = baudotdata->callback(decvalue, (uint8_t)currentValue, baudotdata->callbackParam);
    if(!ret)
        currentState->abort = BOOL_TRUE;
    
    if(baudotdata->opts.baudotTediousDebug)
        printf("\t\tAbort=%u\n",  currentState->abort);
}

double roundDouble(double value) 
{ 
    return value < 0 ? -floor(0.5 - value) : floor(0.5 + value); 
}

static enum BOOLEAN_VALUE processBits(BaudotData * baudotdata, enum BOOLEAN_VALUE bit)
{
    ProcessStateData * currentState = &baudotdata->stateData;
    const BauDot_Decoder_Options * opts = &baudotdata->opts;
    size_t b;
    size_t numBits;

    //How many bits have we processed?  
    //Use the length of the tone to work that out
    //
    //Because we use a fixed frame size to do frequency analysis, the number of bits
    //above may be a bit short if the total tone length is just short of a complete frame
    //Compensate by rounding
    numBits =  (size_t)roundDouble(currentState->bitLengthMs / opts->bitLengthMs);

    //Check we have not exceeded the maximum number of bits - including the extra sync bit...
    //(We may have done, due to the padding block)
    if((currentState->valueShift + numBits) >= (unsigned int)(opts->syncSizeBits +  opts->valueSizeBits))
        numBits = opts->syncSizeBits + opts->valueSizeBits - currentState->valueShift;

    //For 1, or the bit into the correct place
    //For 0, just update the shift value
    if(bit == BOOL_TRUE)
    {
        //Add the appropriate 1's to the value
        for(b=0;b < numBits ; ++b)
        {
            currentState->currentValue |= 1 << currentState->valueShift;
            currentState->valueShift += 1;
        }
    }
    else
    {
        currentState->valueShift += numBits;
    }

    if(baudotdata->opts.baudotTediousDebug)
        printf("\t\t Found %u bits of value %u. Value Length=%f. Total bits=%u. Current value now 0x%02x\n",  numBits, bit, currentState->bitLengthMs, currentState->valueShift, currentState->currentValue);

    if(currentState->valueShift >= (opts->syncSizeBits + opts->valueSizeBits))
    {
        //Process the current value we've built up
        processValue(baudotdata);
        
        //Consume the remainder frequency stuff as padding 
        currentState->state = PROCSTATE_PADDING;
    }

    return BOOL_TRUE;
}


static enum BOOLEAN_VALUE  processSignalFrame(BaudotData * baudotData)
{
    ProcessStateData * currentState = &baudotData->stateData;
    const BauDot_Decoder_Options * opts = &baudotData->opts;
    size_t windowStart = 0;
    size_t windowEnd;
    const size_t numValuesInWindow =  ((size_t)baudotData->sampleRate * baudotData->opts.windowSizeMs) / 1000;
    enum BOOLEAN_VALUE isLow, isHigh, isSilent;
    double noiseVal;
    double lowf,highf;
    double freq;
    size_t findex;
    double windowSizeMs;
    enum BOOLEAN_VALUE repeatSignalStateProc;
    const double * sampleBuffer = (const double *)membuf_ptr( baudotData->sampBuffer,0);
    const size_t numSampleBufferValues = membuf_getContentSize( baudotData->sampBuffer) / sizeof(double);

    //Ensure there is at least 2 windows worth of data in the buffer we can process
    if((baudotData->sampleBufferReadPos + (numValuesInWindow * 2)) >= numSampleBufferValues)
        return BOOL_FALSE; //Return BOOL_FALSE to get more data

    //Work out where the window should start
    //The sample values at window start and end should be close to 0 to allow the goertzel algorithm to work at its best.
    
    if(smputil_findNextNonSilence(sampleBuffer, numSampleBufferValues, baudotData->sampleBufferReadPos, opts->silenceThreshold, &windowStart))
    {
        //Is the entire window silence?
        if(windowStart >= (baudotData->sampleBufferReadPos + numValuesInWindow))
        {
            //Current window is just silence
            isSilent = BOOL_TRUE;
            isLow = BOOL_FALSE;
            isHigh = BOOL_FALSE;

            //Set up the window size to be the amount of silence detected
            windowEnd = windowStart; //Where we found the next non-silence
            windowStart = baudotData->sampleBufferReadPos; //Where we started the search from

            if(baudotData->opts.baudotTediousDebug)
                printf("[%llu - %llu]: Window Silence - state=%u\n", (baudotData->absoluteBufferStartPos + windowStart), (baudotData->absoluteBufferStartPos + windowEnd), currentState->state);
        }
        else
        {
            isSilent = BOOL_FALSE;
            
            windowStart = smputil_findWindowStartEdge(sampleBuffer, numSampleBufferValues, windowStart, numValuesInWindow,  opts->silenceThreshold);
            windowEnd = smputil_findWindowEndEdge(sampleBuffer, numSampleBufferValues,  baudotData->sampleBufferReadPos + numValuesInWindow, numValuesInWindow,  opts->silenceThreshold);
                            
            //Use the goertzel to determine the sample frame characteristics
            noiseVal = goertzel_process_buffer( baudotData->numFreqPoints, 
                                                opts->startFreq, opts->endFreq, baudotData->sampleRate, 
                                                &sampleBuffer[windowStart], windowEnd - windowStart, 
                                                baudotData->Qr);
            
            //Check the frequency results to see if the high and/or low frequencies are present
            //
            //Check for Low
            //
            //If the average amplitude of the low tone frequencies is more than the overall average loudness, then
            //we have a low tone frequency
            isLow = BOOL_FALSE;
            lowf = 0;
            for(freq = (opts->lowFreq - opts->freqErrorHz) ;  freq < (opts->lowFreq + opts->freqErrorHz); freq += opts->freqBlockSize)
            {
                findex = (size_t)(freq / opts->freqBlockSize);
                lowf += baudotData->Qr[findex];
            }

            lowf /= (opts->freqErrorHz * 2);
            lowf *= opts->freqBlockSize;
            if(lowf >= noiseVal)
                isLow = BOOL_TRUE;

            //Check for High
            //
            //If the average amplitude of the high tone frequencies is more than the overall average loudness, then
            //we have a high tone frequency
            isHigh = BOOL_FALSE;
            highf = 0;
            for(freq = (opts->highFreq - opts->freqErrorHz) ;  freq < (opts->highFreq + opts->freqErrorHz); freq += opts->freqBlockSize)
            {
                findex = (size_t)(freq / opts->freqBlockSize);
                highf += baudotData->Qr[findex];
            }
            highf /= (opts->freqErrorHz * 2);
            highf *= opts->freqBlockSize;
            if(highf >= noiseVal)
                isHigh = BOOL_TRUE;

            //work out the actual window size we've used
            windowSizeMs = windowEnd - windowStart;
            windowSizeMs /= baudotData->sampleRate;
            windowSizeMs *= 1000;

         

            if(baudotData->opts.baudotTediousDebug)
            {
                printf("[%llu - %llu]: nse:%f  l:%u (%f) h:%u (%f) | frmlen=%f, trnslen=%f,  bitlen=%f,  state=%u\n", 
                    (baudotData->absoluteBufferStartPos + windowStart), (baudotData->absoluteBufferStartPos + windowEnd), 
                    noiseVal, isLow, lowf, isHigh, highf, 
                    windowSizeMs, currentState->transitionFrameMs, currentState->bitLengthMs,   currentState->state);
            }
        }
    }
    else
    {
        //No content left - everything is just silence
        isSilent = BOOL_TRUE;
        isLow = BOOL_FALSE;
        isHigh = BOOL_FALSE;

        windowEnd = numSampleBufferValues;
        windowStart = baudotData->sampleBufferReadPos;

        if(baudotData->opts.baudotTediousDebug)
            printf("[%llu - %llu]: Remaining Silence - state=%u\n", (baudotData->absoluteBufferStartPos + windowStart), (baudotData->absoluteBufferStartPos + windowEnd), currentState->state);
    }

    //------------------------------------------------------------
    //Run signal processer
    
    do
    {
        repeatSignalStateProc = BOOL_FALSE;
        switch(currentState->state)
        {
            case PROCSTATE_FIND_DATA:
                
                //Signal listening and searching for data
                gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Finding_Data);

                //Process the sync state - here we're looking for the 'low' (0) frequency
                //The first bit is always zero, so ignore any 'high' frequencies (1) here
                if(isSilent)
                {
                    //Error - restart
                    if(currentState->silenceAbsoluteStartPos > 0)
                    {
                        if(baudotData->opts.baudotDebug)
                            printf("\t\t Find Data Aborted because of silence - Restart data breaK detect\r\n");

                        currentState->state = PROCSTATE_SILENCE_AFTER_DATA;
                    }
                    else
                    {
                        if(baudotData->opts.baudotDebug)
                            printf("\t\t Find Data Aborted - Silence\r\n");

                        currentState->state = PROCSTATE_SILENCE;
                    }
                }
                else if(isLow && !isHigh)
                {
                    if(baudotData->opts.baudotDebug)
                        printf("\t\t Found data at %llu\r\n", baudotData->absoluteBufferStartPos + baudotData->sampleBufferReadPos);

                    //Found data bit start, process it.
                    currentState->state = PROCSTATE_DATA_START;
                    repeatSignalStateProc = BOOL_TRUE; //Reprocess the same frame in the new state
                }
                else
                {
                    currentState->state = PROCSTATE_START;

                    if(baudotData->opts.baudotTediousDebug)
                        printf("\t\t Find Data Aborted\r\n");
                }
                break;
            case PROCSTATE_DATA_START:

                //Data always starts with a 0 - 
                //so ignore 1's at this stage
                if(isLow)
                {   
                    if(isHigh)
                    {
                        //Signal listening and receiving a data transition, both high and low are detected
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Transition);

                        if(baudotData->opts.baudotTediousDebug)
                            printf("\t\t Data Start on transition\n");

                        currentState->bitLengthMs = windowSizeMs / 2;
                    }
                    else
                    {
                        //Signal listening and receiving a data transition, both high and low are detected
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Low);

                        if(baudotData->opts.baudotTediousDebug)
                            printf("\t\t Data Start - 0\n");

                        currentState->bitLengthMs = windowSizeMs;
                    }
                    
                    currentState->transitionFrameMs = 0;
                    currentState->state = PROCSTATE_DATA_PROC_LOW;
                    currentState->valueShift = 0;
                    currentState->currentValue = 0;
                    currentState->silenceAbsoluteStartPos = 0;
                }
                else
                {
                    currentState->state = PROCSTATE_DATA_START;
                    currentState->silenceAbsoluteStartPos = 0;

                    gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_EndOfBlock, GPIOFunction_None);
                }
                break;
            case PROCSTATE_DATA_PROC_LOW:

                if(isLow && ((currentState->bitLengthMs / opts->bitLengthMs) <= (opts->syncSizeBits  + opts->valueSizeBits - currentState->valueShift)))
                {
                    currentState->bitLengthMs += windowSizeMs;

                    //If there is also a high frequency, then this could be a transition frame that
                    //contains both high and low frequencies
                    //We need to handle this on the next frame, and then assume half of this frame is for low and the rest is for high.
                    if(isHigh)
                    {
                        //If there has already been a transition frame, then we are in some kind of unknown state here
                        //Give up!
                        if(currentState->transitionFrameMs > 0)
                        {
                            currentState->state = PROCSTATE_START;
                            repeatSignalStateProc = BOOL_TRUE;
                            
                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\tLOW: NOISE!\n");
                        }
                        else
                        {
                            //Signal listening and receiving a data low and high (transition)
                            gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Transition);

                            //Only add half the length to the current bit length for transition frames
                            //(we've already added the full window size, so just subtract half)
                            currentState->transitionFrameMs = (windowSizeMs / 2);
                            currentState->bitLengthMs -= currentState->transitionFrameMs;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\tLOW: Transition frame %f\r\n", currentState->transitionFrameMs);
                        }
                    }
                    else
                    {
                        //Signal listening and receiving a data low
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Low);

                        currentState->transitionFrameMs = 0;
                    }
                }
                else
                {
                    //Was low, now not - update current value
                    
                    //Process the bits received
                    processBits(baudotData, BOOL_FALSE);
                    
                    //Have we processed all the bits (including the passing bit)
                    if(currentState->state != PROCSTATE_PADDING)
                    {   
                        if(isHigh)
                        {
                            //Signal listening and receiving a data high
                            gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Low);

                            //Switch from low to high
                            currentState->bitLengthMs = windowSizeMs + currentState->transitionFrameMs;
                            currentState->state = PROCSTATE_DATA_PROC_HIGH;
                            currentState->transitionFrameMs = 0;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\t End of LOW on previous frame, now start of HIGH %f\r\n", currentState->bitLengthMs);
                        }
                        else if(isSilent)
                        {   
                            currentState->state = PROCSTATE_SILENCE_AFTER_DATA;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\t End of LOW, start of silence\r\n");
                        }
                        else
                        {
                            //Not a high tone, or a low tone - something else is an error
                            currentState->state = PROCSTATE_START;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\t End of LOW on previous frame, unknown state - restart\r\n");
                        }
                    }
                    else
                    {
                        //Signal listening and receiving padding
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Padding);

                        currentState->bitLengthMs = 0;
                        currentState->transitionFrameMs = 0;
                        repeatSignalStateProc = BOOL_TRUE;

                        if(baudotData->opts.baudotTediousDebug)
                            printf("\t\t End of LOW on previous frame, now padding\r\n");
                    }
                }
                break;
            case PROCSTATE_DATA_PROC_HIGH:
                if(isHigh &&  ((currentState->bitLengthMs / opts->bitLengthMs) <= (opts->syncSizeBits + opts->valueSizeBits - currentState->valueShift)))
                {
                    currentState->bitLengthMs +=  windowSizeMs;

                    //If there is also a low frequency, then this could be a transition frame that
                    //contains both high and low frequencies
                    //We need to handle this on the next frame, and then assume half of this frame is for high and the rest is for low.
                    if(isLow)
                    {
                        //Signal listening and receiving a data high and low
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Transition);

                        //If there has already been a transition frame, then we are in some kind of unknown state here
                        //Give up!
                        if(currentState->transitionFrameMs > 0)
                        {
                            currentState->state = PROCSTATE_START;
                            repeatSignalStateProc = BOOL_TRUE;
                            
                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\tHIGH: NOISE!\n");
                        }
                        else
                        {
                            //Only add half the length to the current bit length for transition frames
                            //(we've already added the full window size, so just subtract half)
                            currentState->transitionFrameMs = (windowSizeMs / 2);
                            currentState->bitLengthMs -= currentState->transitionFrameMs ;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\tHIGH Transition frame %f\r\n", currentState->transitionFrameMs);
                        }
                    }
                    else
                    {
                        //Signal listening and receiving a data high
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_High);

                        currentState->transitionFrameMs = 0;
                    }
                     
                }
                else
                {
                    //Was high, now not - update current value

                    
                    //Process the bits received
                    processBits(baudotData, BOOL_TRUE);
                    
                    //Have we processed all the bits (including the passing bit)
                    if(currentState->state != PROCSTATE_PADDING)
                    { 
                        if(isLow)
                        {
                            //Signal listening and receiving a data low
                            gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Low);

                            //Switch from low to high
                            currentState->bitLengthMs = windowSizeMs + currentState->transitionFrameMs;
                            currentState->state = PROCSTATE_DATA_PROC_LOW;
                            currentState->transitionFrameMs = 0;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\t End of HIGH on previous frame, now start of LOW - %f\r\n", currentState->bitLengthMs);
                        }
                        else if(isSilent)
                        {
                            currentState->state = PROCSTATE_SILENCE_AFTER_DATA;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\t End of HIGH  on previous frame, start of silence\r\n");
                        }
                        else
                        {
                            //Not a high tone, or a low tone - something else is an error
                            currentState->state = PROCSTATE_START;

                            if(baudotData->opts.baudotTediousDebug)
                                printf("\t\t End of HIGH on previous frame, unknown state - restart\r\n");
                        }
                    }
                    else if(isLow)
                    {
                        //Signal listening and receiving a data low
                        gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Low);

                        //Gone into padding state, and the tone has gone from high to low
                        //This can happen if the next frame *immediatly* follows the previous, without any gap
                        currentState->state = PROCSTATE_DATA_START;
                        repeatSignalStateProc = BOOL_TRUE; //Re-process this frame in new state

                        if(baudotData->opts.baudotTediousDebug)
                            printf("\t\t End of LOW on previous frame, immediate start of new frame at LOW\r\n");
                    }
                    else
                    {
                        currentState->bitLengthMs = 0;
                        currentState->transitionFrameMs = 0;
                        repeatSignalStateProc = BOOL_TRUE;

                        if(baudotData->opts.baudotTediousDebug)
                            printf("\t\t End of LOW on previous frame, now padding\r\n");
                    }
                }
                break;
            case PROCSTATE_PADDING:
                //After 5 bits, there is a lot of padding. This state will skip all of it until the next silence or low tone
                if(isLow)
                {
                    //Signal listening and receiving a data low
                    gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Data_Low);

                    currentState->state = PROCSTATE_DATA_START;
                    repeatSignalStateProc = BOOL_TRUE;

                    if(baudotData->opts.baudotDebug)
                        printf("\t\t End of padding, start of data\r\n");
                }
                else if(isSilent)
                {   
                    currentState->state = PROCSTATE_SILENCE_AFTER_DATA;
                    windowEnd = windowStart; //Force re-processing of the silence
                    if(baudotData->opts.baudotDebug)
                        printf("\t\t End of padding, start of silence\r\n");
                }
                else
                {
                    //Still receiving padding
                    gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Receiving_Padding);
                }
                break;
        }
    }
    while(repeatSignalStateProc);

    //Start next frame processing at the end of the current frame
    baudotData->sampleBufferReadPos = windowEnd;

    return BOOL_TRUE;
}


static enum BOOLEAN_VALUE processSampleFrame(BaudotData * baudotData)
{
    ProcessStateData * currentState = &baudotData->stateData;
    enum BOOLEAN_VALUE repeatStateProc;
    size_t windowStart = 0;
    size_t soundPos = 0;
    size_t findStartPos;
    const size_t numSampleBufferValues = membuf_getContentSize( baudotData->sampBuffer) / sizeof(double);
    const double * sampleBuffer = (const double *)membuf_ptr( baudotData->sampBuffer,0);
    const uint64_t breakSizeSamples = (((uint64_t)baudotData->opts.silenceBreakMs) * ((uint64_t)baudotData->sampleRate)) / 1000;
    uint64_t breakEndAbsolutePos, curAbsolutePos;

    do
    {
        if(baudotData->sampleBufferReadPos >= numSampleBufferValues)
            break; //Return false, as we need more data

        repeatStateProc = BOOL_FALSE;
        switch(currentState->state)
        {
            case PROCSTATE_START:
            case PROCSTATE_SILENCE:
                //Window start and end should be default here - we're just looking for sound at this point
                windowStart=0;
                currentState->bitLengthMs = 0;
                currentState->transitionFrameMs = 0;

                //Signal listening and silence state
                gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Start);
                
                //We pass the entire sample buffer here, as the function may need the prior frame data
                // (baudotData->sampleBufferWritePos is the total number of samples in the buffer at the moment)
                findStartPos = baudotData->sampleBufferReadPos; //For debugging
                if(smputil_findNextNonSilence(sampleBuffer, numSampleBufferValues, findStartPos, baudotData->opts.silenceThreshold, &windowStart))
                {
                    //Go into the sync start state - where we look for the start of the signal, and for a sync tone
                    //Re-process the same sample within the new state
                    repeatStateProc = BOOL_TRUE;
                    currentState->state = PROCSTATE_FIND_SIGNAL;

                    //Start signal search where we found non-silence
                    baudotData->sampleBufferReadPos = windowStart;
                }
                else
                {   
                    //Go into silence state - where we keep going and look for non-silence
                    currentState->state = PROCSTATE_SILENCE;

                    //We've also determined the remainder of the buffer is silent - so consume it all
                    baudotData->sampleBufferReadPos = numSampleBufferValues;
                }

                //If not a data break, and we found a signal
                breakEndAbsolutePos = (currentState->silenceAbsoluteStartPos == 0) ? 0 : (currentState->silenceAbsoluteStartPos + breakSizeSamples);
                curAbsolutePos = baudotData->absoluteBufferStartPos + baudotData->sampleBufferReadPos;
                if((breakEndAbsolutePos > 0) &&  (curAbsolutePos >= breakEndAbsolutePos))
                {
                    //Next processing position is beyond the break position, so this is a data break....
                    currentState->state = PROCSTATE_DATA_BREAK;
                    
                    //Set next processing position to the break position
                    //This will cause that amount to be removed from the buffer, while preserving the rest.
                    baudotData->sampleBufferReadPos = (size_t)(breakEndAbsolutePos - baudotData->absoluteBufferStartPos);
                    repeatStateProc = BOOL_TRUE; //... and don't auto-advance the next processing position

                    if(baudotData->opts.baudotDebug)
                        printf("[%llu - %llu] : DATA BREAK at %llu\r\n", baudotData->absoluteBufferStartPos + findStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues,  breakEndAbsolutePos);

                    gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_EndOfDataBlk);
                }
                else if(currentState->state == PROCSTATE_FIND_SIGNAL)
                {
                    //Not beyond the break position, or the break position is not yet set 
                    //Either way, we've found the start of some signal...
                    if(baudotData->opts.baudotDebug)
                    {
                        printf("[%llu - %llu] : Found possible signal at %llu \r\n",  baudotData->absoluteBufferStartPos + findStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues, 
                               baudotData->absoluteBufferStartPos + windowStart);
                    }
                }
                else if(breakEndAbsolutePos > 0)
                {
                    //Silence detected, and data break detection in progress ... 

                    if(baudotData->opts.baudotDebug)
                    {
                        printf("[%llu - %llu] : Silence - Data break detection pos: %llu  Current pos:%llu \r\n", baudotData->absoluteBufferStartPos + findStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues,                     
                                breakEndAbsolutePos, curAbsolutePos);
                    }
                }
                else
                {
                    //Just some silence....
                    if(baudotData->opts.baudotDebug)
                        printf("[%llu - %llu] : Silence \r\n", baudotData->absoluteBufferStartPos + findStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues);
                }
                
                break;
            case PROCSTATE_SILENCE_AFTER_DATA:

                //Signal listening and no longer handling data
                gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Start);

                windowStart=0;
                currentState->bitLengthMs = 0;
                currentState->transitionFrameMs = 0;
                if(!smputil_findNextNonSilence(sampleBuffer,  numSampleBufferValues, baudotData->sampleBufferReadPos, baudotData->opts.silenceThreshold, &windowStart))
                {
                    //Silence
                    currentState->silenceAbsoluteStartPos = baudotData->absoluteBufferStartPos + baudotData->sampleBufferReadPos;
                    currentState->state = PROCSTATE_SILENCE;
                    repeatStateProc = BOOL_TRUE;

                    if(baudotData->opts.baudotDebug)
                        printf("[%llu - %llu] : Silence after data - Start Data Break Detect at %llu - end at %llu \r\n", baudotData->absoluteBufferStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues, 
                                                                                                                          currentState->silenceAbsoluteStartPos, currentState->silenceAbsoluteStartPos + breakSizeSamples);
                 
                    gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_EndOfDataBlk);
                }
                else
                {
                    //Not silence, but still set the data gap detection to be at the end of the silence
                    repeatStateProc = BOOL_TRUE;
                    currentState->state = PROCSTATE_START;
                    currentState->silenceAbsoluteStartPos = baudotData->absoluteBufferStartPos +  baudotData->sampleBufferReadPos;

                    if(baudotData->opts.baudotDebug)
                        printf("[%llu - %llu] : Non-Silence after data - Start Data Break Detect at %llu - end at %llu \r\n", baudotData->absoluteBufferStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues, 
                                                                                                                              currentState->silenceAbsoluteStartPos, currentState->silenceAbsoluteStartPos + breakSizeSamples);
                    
                    gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_EndOfDataBlk);
                }
                break;
            case PROCSTATE_FIND_SIGNAL:
                
                //Signal listening and searching for data
                gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_Finding_Data);

                //Find the start of the
                soundPos = 0;
                currentState->bitLengthMs = 0;
                currentState->transitionFrameMs = 0;

                //We pass the entire sample buffer here, as the function may need the prior frame data
                // (baudotData->sampleBufferWritePos is the total number of samples in the buffer at the moment)
                findStartPos = baudotData->sampleBufferReadPos; //For debugging
                if(!smputil_findNextNonSilence(sampleBuffer, numSampleBufferValues, findStartPos, baudotData->opts.silenceThreshold, &soundPos))
                {
                    //Error - Go back into the start state
                    currentState->state = PROCSTATE_START;
                }
                else
                {
                    //Find the start point
                    //We pass the entire sample buffer here, as the function may need the prior frame data
                    // (baudotData->sampleBufferWritePos is the total number of samples in the buffer at the moment)
                    windowStart = smputil_seekBackToSilenceEnd(sampleBuffer,  numSampleBufferValues, soundPos, baudotData->sampleBufferReadPos, baudotData->opts.silenceThreshold);

                    if(baudotData->opts.baudotDebug)
                    {
                        printf("[%llu - %llu] : Start point at %llu from %llu\r\n",  baudotData->absoluteBufferStartPos + findStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues, 
                                                                                     baudotData->absoluteBufferStartPos + windowStart, baudotData->absoluteBufferStartPos + soundPos);
                    }

                    //We have signal,
                    //Go and process the current sample frame in the next state - 'find data'
                    currentState->state = PROCSTATE_FIND_DATA;
                    repeatStateProc = BOOL_TRUE;
                    baudotData->sampleBufferReadPos = windowStart;
                }
                break;
            case PROCSTATE_DATA_BREAK:
                if(baudotData->opts.baudotDebug)
                    printf("[%llu - %llu] : DATA BREAK\r\n", baudotData->absoluteBufferStartPos, baudotData->absoluteBufferStartPos + numSampleBufferValues);
               
                gpio_set( baudotData->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_Rx_EndOfDataBlk);
                break;
            default:
                //Process the frequencies....
                if(!processSignalFrame(baudotData))
                {
                    return BOOL_FALSE; //More data required
                }
                break;
        }
    }
    while(repeatStateProc && !currentState->abort && (currentState->state != PROCSTATE_DATA_BREAK));

    //Return BOOL_FALSE to get more data, return BOOL_TRUE to keep processing the current buffer
    return (currentState->state != PROCSTATE_DATA_BREAK) && (baudotData->sampleBufferReadPos < numSampleBufferValues) ? BOOL_TRUE : BOOL_FALSE;
}

BaudotHandle baudot_init(const BauDot_Decoder_Options * opts,  enum WAV_FORMAT format, unsigned int sampleRate, unsigned int channels, unsigned int bitsPerSample, 
                         CharacterReceivedCallback receiveCallaback,void * recvCallbackParam,
                         GPIOHandle gpioHandle)
{
    BaudotData * baudotdata = (BaudotData *)malloc(sizeof(BaudotData));
    uint64_t memBufBlockSize;

    //Copy options across
    memcpy(&baudotdata->opts, opts, sizeof(BauDot_Decoder_Options));

    baudotdata->gpioHandle = gpioHandle;
    baudotdata->sampleRate = sampleRate;
    baudotdata->channels = channels;
    baudotdata->bitsPerSample = bitsPerSample;
    baudotdata->format = format;
    
    //Init state data
    memset(&baudotdata->stateData,0,sizeof(ProcessStateData));
    baudotdata->absoluteBufferStartPos = 0;
    
    //We want blocks of frequencies every 50hz, so work out the Nf (number of frequency points) we want from goertzel
    baudotdata->numFreqPoints = (size_t)((opts->endFreq - opts->startFreq) / opts->freqBlockSize);

    //Allocate frequency block buffer
    baudotdata->Qr = (double *)malloc(baudotdata->numFreqPoints * sizeof(double));

     //How big a buffer, in terms of sample values, do we need
    //Work it out from the bufferSizeMs (milliseconds) and the samples per second
    //Ignore the number of channels for now, since we will be converting the sample data to mono double values.
    memBufBlockSize = (((uint64_t)baudotdata->sampleRate * baudotdata->opts.bufferSizeMs) / 1000);

    //Allocate sample buffer
    baudotdata->sampBuffer = membuf_init_blk( (size_t)memBufBlockSize * sizeof(double) );
    baudotdata->sampleBufferReadPos = 0;
    
    baudotdata->callback = receiveCallaback;
    baudotdata->callbackParam = recvCallbackParam;

    return baudotdata;
}

void baudot_free(BaudotHandle handle)
{
    BaudotData * baudotdata = (BaudotData *)handle;

    //Signal no longer listening
    if(baudotdata->gpioHandle != NULL)
    {
        gpio_set( baudotdata->gpioHandle, GPIOFunction_Rx_All, GPIOFunction_None);
        gpio_set(baudotdata->gpioHandle, GPIOFunction_Rx_Volume, GPIOFunction_None);
    }

    free(baudotdata->Qr);
    membuf_free(baudotdata->sampBuffer);
    free(baudotdata);
}


enum BaudotProcessState baudot_process_samples(BaudotHandle handle, const unsigned char * sampleData, size_t numBytes, WAVDESTHND writeWavDest)
{
    BaudotData * baudotdata = (BaudotData *)handle;
    size_t totalSampleValueCount, numNewValues, origSz;
    size_t numBytesProcessed = 0;
    enum BOOLEAN_VALUE clipping = BOOL_FALSE;
    enum BOOLEAN_VALUE silence = BOOL_TRUE;
    
    //If a data break occured, then reset that information to start again
    if(baudotdata->stateData.state == PROCSTATE_DATA_BREAK)
    {
        const size_t numSampleBufferValues = membuf_getContentSize( baudotdata->sampBuffer) / sizeof(double);

        baudotdata->stateData.state = PROCSTATE_START;
        baudotdata->stateData.silenceAbsoluteStartPos = 0;

        if(baudotdata->opts.baudotDebug)
            printf("[%llu - %llu] : RESTART AFTER DATA BREAK\r\n", baudotdata->absoluteBufferStartPos, baudotdata->absoluteBufferStartPos + numSampleBufferValues);

        gpio_set( baudotdata->gpioHandle, GPIOFunction_Rx_EndOfBlock, GPIOFunction_None);
    }
    

    //Convert the input buffer to our internal buffer - which may have left some stuff over from last time
    origSz = membuf_getContentSize(baudotdata->sampBuffer) / sizeof(double);
    if((sampleData != NULL) && (numBytes > 0))
    {
        numBytesProcessed = smputil_convertSampleBytesToDoubles(sampleData, numBytes,
                                                                baudotdata->format, baudotdata->channels, baudotdata->sampleRate, baudotdata->bitsPerSample, baudotdata->opts.silenceThreshold,
                                                                baudotdata->sampBuffer,  &clipping, &silence);

        //Update clipping and silent flags
        if(baudotdata->gpioHandle != NULL)
            gpio_set(baudotdata->gpioHandle, GPIOFunction_Rx_Volume, clipping ? GPIOFunction_Rx_Clipping : (silence ? GPIOFunction_Rx_Silence : GPIOFunction_None));
    }
    else
    {
        numBytesProcessed = 0;
    }
        
    totalSampleValueCount = membuf_getContentSize(baudotdata->sampBuffer) / sizeof(double);
    numNewValues = totalSampleValueCount - origSz;

     //Write new audio data to disk, if requested
    if((writeWavDest != NULL) && (numNewValues > 0))
    {
        const double * sampleBuffer = (const double *)membuf_ptr(baudotdata->sampBuffer,0);
        wavdest_write(writeWavDest,  &sampleBuffer[origSz], numNewValues);
    }

    //Process baudot frames until break or error or end of buffer
    while((baudotdata->sampleBufferReadPos < totalSampleValueCount) && (!baudotdata->stateData.abort))
    {
        //This will return BOOL_FALSE once there is not enough remaining data
        if(!processSampleFrame(baudotdata))
        { 
            break;
        }
    }

    //Move everything left over to the start of the sample buffer
    //Don't do this in the main loop as baudot processing might want to handle previous frames
    if(baudotdata->opts.baudotDebug == BOOL_TRUE)
        printf("Remove %u / %u / %u samples from buffer\r\n",    baudotdata->sampleBufferReadPos, membuf_getContentSize(baudotdata->sampBuffer), totalSampleValueCount);
    
    membuf_removeFromFront( baudotdata->sampBuffer, baudotdata->sampleBufferReadPos * sizeof(double));
    baudotdata->absoluteBufferStartPos += (baudotdata->sampleBufferReadPos >= totalSampleValueCount) ? totalSampleValueCount : baudotdata->sampleBufferReadPos;
    baudotdata->sampleBufferReadPos = 0;
    
    if(baudotdata->stateData.abort)
        return BaudotProcessState_Error;

    if(baudotdata->stateData.state == PROCSTATE_DATA_BREAK)
        return BaudotProcessState_Break;

    return BaudotProcessState_NeedMoreData;
}

void baudot_encode_string(const BauDot_Decoder_Options * opts, int sampleRate, double scale, const char * str, enum BOOLEAN_VALUE * currentShiftState, 
                          MemoryBuffer * outputBuffer, MemoryBuffer * outBitSeqBuffer)
{
    uint64_t code;
    const char * curpos = str;
    uint8_t b;
    const size_t numBitsPerCode = (opts->valueSizeBits + opts->syncSizeBits + opts->paddingSizeBits);
    const size_t numSamplesPerBit = (size_t)((opts->bitLengthMs * sampleRate) / 1000);
    unsigned int freq, prevfreq;
    uint8_t curCode;
    size_t t;
    const double twopi = M_PI * 2;
    double phaseIncrement;
    double phase = 0;
    size_t gapSize = (sampleRate * opts->charGapDurationMs) / 1000;
    double outputVal;
    
    prevfreq=0;
    curpos = str;
    while((*curpos != 0) || *currentShiftState)
    {
        //If shift state is enabled, then encode 'deactivate shift mode'

         
        if((*curpos == 0) && *currentShiftState)
        {
            code = 31; //Code to deactivate shift mode
            *currentShiftState = BOOL_FALSE;
        }
        else
        {
            //Encode to baudot
            code = encode_baudot_character_ITA2(*curpos, currentShiftState);
        }

        while(code != 0)
        {
            curCode = (uint8_t)(code & 0xFF);

            //Add sync bits
            curCode = curCode << opts->syncSizeBits;

            for(b=0; b < numBitsPerCode; ++b)
            {
                //Use low frequencey if:
                //  - On sync bit        or
                //  - On a low data bit    
                // AND
                //   not on a padding bit
                freq = (((b < opts->syncSizeBits) || ((curCode & 1) == 0)) && (b < (opts->syncSizeBits + opts->valueSizeBits))) ? opts->lowFreq : opts->highFreq;
                phaseIncrement = freq;
                phaseIncrement /= sampleRate;

                //Append to bit sequence buffer
                if(outBitSeqBuffer != NULL)
                {
                    enum BOOLEAN_VALUE bitVal = (freq == opts->lowFreq) ? BOOL_FALSE : BOOL_TRUE;
                    membuf_append(outBitSeqBuffer, &bitVal, sizeof(bitVal));
                }
                
                //Generate full sine waves - 
                //This means generating extra samples until the sine we're generating crosses from  negative to positive.
                //The theory is that each sine wave will start from zero and go positive, reach it's peak, and then decreased, hit zero again, and then go negative, and repeat
                //So if we continue until the sine crosses from negative to positive (or zero), then we can match up to the start of the next sine wave
                for(t=0; t < numSamplesPerBit; ++t)
                {

                    outputVal = sin( phase * twopi  ) * scale;

                    //Store
                    membuf_append( outputBuffer, &outputVal, sizeof(double));
                    
                    //Adjust phase value for next sample
                    phase += phaseIncrement;
                }

                prevfreq = freq;

                curCode = curCode >> 1;
            }
        
            //Process any additional codes for the current character
            //There may be extra shift or unshift codes that also need to be included, and these would have be put into the next byte(s) of 'code'
            code >>= 8;
        }


        //Generate gap 
        if(opts->charGapDurationMs > 0)
        {   
            phaseIncrement = opts->charGapFreq;
            phaseIncrement /= sampleRate;

            for(t=0; t < gapSize; ++t)
            {
                if(opts->charGapFreq > 0)
                {
                    outputVal = sin( phase * twopi  ) * scale;

                    phase += phaseIncrement;
                }
                else
                {
                    outputVal = 0;
                }

                //Store
                membuf_append( outputBuffer, &outputVal, sizeof(double));
            }
        }

        //Advnace to next character
        if(*curpos != 0)
            ++curpos;
    }


}
