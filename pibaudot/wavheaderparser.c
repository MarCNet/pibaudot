#include <stdio.h>
#include <memory.h>
#include "internal.h"


#ifdef BIG_ENDIAN 
static enum BOOLEAN_VALUE  is_big_endian_host_ = BOOL_TRUE;
#else
static enum BOOLEAN_VALUE  is_big_endian_host_ = BOOL_FALSE;
#endif 

static unsigned count_channel_mask_bits(uint32_t mask)
{
    unsigned count = 0;
    while(mask) 
    {
        if(mask & 1)
            count++;

        mask >>= 1;
    }
    
    return count;
}

static int read_bytes(const unsigned char * dataptr, uint64_t dataSize, uint64_t * curOffset, void * dest, int amount, enum BOOLEAN_VALUE eofok)
{
    int amountToRead = amount;
    if(*curOffset >= dataSize)
    {
         if(!eofok) 
        {
            return -1; //ERROR
        }

        //No data
        return 0;
    }
    
    if((*curOffset + amount) > dataSize)
    {
        amountToRead = (int)(dataSize - *curOffset);
    }

    memcpy(dest, dataptr + *curOffset, amountToRead);

    *curOffset += amountToRead;
    return amountToRead;
}

static uint64_t read_uint64(const unsigned char * dataptr, uint64_t dataSize, uint64_t * curOffset, enum BOOLEAN_VALUE big_endian, enum BOOLEAN_VALUE * retErr)
{
    uint64_t val=0;
    int res =read_bytes(dataptr, dataSize, curOffset, &val, 8, /*eof_ok=*/BOOL_FALSE);
    if(res < 8)
        *retErr = BOOL_TRUE;

    if(is_big_endian_host_ != big_endian) 
    {
        unsigned char tmp, *b = (unsigned char *)&val;
        tmp = b[7]; b[7] = b[0]; b[0] = tmp;
        tmp = b[6]; b[6] = b[1]; b[1] = tmp;
        tmp = b[5]; b[5] = b[2]; b[2] = tmp;
        tmp = b[4]; b[4] = b[3]; b[3] = tmp;
    }

    return val;
}

static uint32_t read_uint32(const unsigned char * dataptr, uint64_t dataSize, uint64_t * curOffset, enum BOOLEAN_VALUE big_endian, enum BOOLEAN_VALUE * retErr)
{
    uint32_t val=0;
    int res =read_bytes(dataptr, dataSize, curOffset, &val, 4, /*eof_ok=*/BOOL_FALSE);
    if(res < 4)
        *retErr = BOOL_TRUE;
    
    if(is_big_endian_host_ != big_endian) 
    {
        unsigned char tmp, *b = (unsigned char *)&val;
        tmp = b[3]; b[3] = b[0]; b[0] = tmp;
        tmp = b[2]; b[2] = b[1]; b[1] = tmp;
    }

    *retErr = BOOL_FALSE;
    return val;
}

static uint16_t read_uint16(const unsigned char * dataptr, uint64_t dataSize, uint64_t * curOffset, enum BOOLEAN_VALUE big_endian, enum BOOLEAN_VALUE * retErr)
{
    uint16_t val = 0;
    int res =read_bytes(dataptr, dataSize, curOffset, &val, 2, /*eof_ok=*/BOOL_FALSE);
    if(res < 2)
        *retErr = BOOL_TRUE;

    if(is_big_endian_host_ != big_endian) 
    {
        unsigned char tmp, *b = (unsigned char *)&val;
        tmp = b[1]; b[1] = b[0]; b[0] = tmp;
    }

    return val;
}


enum BOOLEAN_VALUE get_sample_info_wave(const unsigned char * dataptr, uint64_t dataSize, SampleInfo * outInfo)
{
    enum BOOLEAN_VALUE got_fmt_chunk = BOOL_FALSE, got_data_chunk = BOOL_FALSE, got_ds64_chunk = BOOL_FALSE;
    unsigned sample_rate = 0, channels = 0, bps = 0, shift = 0;
    uint32_t channel_mask = 0;
    uint64_t ds64_data_size = 0;
    uint64_t dataoffset = 0;
    int res;
    uint32_t data_bytes, xx;
    enum BOOLEAN_VALUE bErr = BOOL_FALSE;
    enum Format format = FORMAT_INVALID;
    uint16_t x;
    const enum BOOLEAN_VALUE channel_map_none = BOOL_FALSE;
    const enum BOOLEAN_VALUE ignore_chunk_sizes = BOOL_FALSE;
    const enum BOOLEAN_VALUE treat_warnings_as_errors = BOOL_TRUE;
    const enum BOOLEAN_VALUE format_options_iff_foreign_metadata = BOOL_FALSE;
    char chunk_id[16] = { '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0' };

    outInfo->is_unsigned_samples = BOOL_FALSE;
    outInfo->is_big_endian = BOOL_FALSE;

    //Check we have RIFFxxxxWAV or RF64xxxWAVE
    res = read_bytes(dataptr, dataSize, &dataoffset, chunk_id, 12, BOOL_FALSE); 
    if(res < 12)
    {
        return BOOL_FALSE;
    }

    if(!memcmp(chunk_id, "RIFF", 4))
    {
        if(!memcmp(chunk_id + 8, "WAVE", 4))
        {
            format = FORMAT_WAVE;
        }
    }
    else if(!memcmp(chunk_id, "RF64", 4))
    {
        if(!memcmp(chunk_id + 8, "WAVE", 4))
        {
            format = FORMAT_WAVE64;
        }
    }

    if(format == FORMAT_INVALID)
        return BOOL_FALSE;

    outInfo->fmt = format;


    if(format == FORMAT_WAVE64) 
    {
        /*
         * lookahead[] already has "riff\x2E\x91\xCF\x11\xA5\xD6\x28\xDB", skip over remaining header
         */
        dataoffset += 16+8+16-12; 
    }
    /* else lookahead[] already has "RIFFxxxxWAVE" or "RF64xxxxWAVE" */
    
    
    while((dataoffset < dataSize) && !got_data_chunk) 
    {
        /* chunk IDs are 4 bytes for WAVE/RF64, 16 for Wave64 */
        /* for WAVE/RF64 we want the 5th char zeroed so we can treat it like a C string */
        memset(chunk_id,0,sizeof(chunk_id));
        res = read_bytes(dataptr, dataSize, &dataoffset, chunk_id, format==FORMAT_WAVE64?16:4, /*eof_ok=*/BOOL_TRUE); 
        if((res < 0) || ((dataoffset + res) >= dataSize))
            return BOOL_FALSE;
        
        if(format == FORMAT_RF64 && !memcmp(chunk_id, "ds64", 4)) 
        { /* RF64 64-bit sizes chunk */
            
            if(got_ds64_chunk) 
            {
                return BOOL_FALSE;//ERROR: file has multiple 'ds64' chunks
            }
            
            if(got_fmt_chunk || got_data_chunk) 
            {
                return BOOL_FALSE;//"ERROR: 'ds64' chunk appears after 'fmt ' or 'data' chunk
            }

            /* ds64 chunk size */
            data_bytes = read_uint32(dataptr, dataSize, &dataoffset, BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;
            
            if(data_bytes < 28) 
            {
                return BOOL_FALSE;//ERROR: non-standard 'ds64' chunk has incorrect length
            }
            
            if(data_bytes & 1) /* should never happen, but enforce WAVE alignment rules */
                data_bytes++;

            /* RIFF 64-bit size, lo/hi */
            xx = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            xx= read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            /* 'data' 64-bit size */
            ds64_data_size = read_uint64(dataptr, dataSize, &dataoffset,  /*big_endian=*/BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            data_bytes -= 16;

            /* skip any extra data in the ds64 chunk */
            dataoffset += data_bytes; 
            if(dataoffset >= dataSize)
            {
                return BOOL_FALSE;//ERROR during read while skipping over extra 'ds64' data
            }

            got_ds64_chunk = BOOL_TRUE;
        }
        else if(
            !memcmp(chunk_id, "fmt ", 4) &&
            (format!=FORMAT_WAVE64 || !memcmp(chunk_id, "fmt \xF3\xAC\xD3\x11\x8C\xD1\x00\xC0\x4F\x8E\xDB\x8A", 16))
        ) 
        { /* format chunk */
            uint16_t wFormatTag; /* wFormatTag word from the 'fmt ' chunk */

            if(got_fmt_chunk) 
            {
                return BOOL_FALSE; //ERROR: file has multiple 'fmt ' chunks
            }

            /* see
             *   http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
             *   http://windowssdk.msdn.microsoft.com/en-us/library/ms713497.aspx
             *   http://msdn.microsoft.com/library/default.asp?url=/library/en-us/audio_r/hh/Audio_r/aud-prop_d40f094e-44f9-4baa-8a15-03e4fb369501.xml.asp
             *
             * WAVEFORMAT is
             * 4 byte: chunk size
             * 2 byte: format type: 1 for WAVE_FORMAT_PCM, 65534 for WAVE_FORMAT_EXTENSIBLE
             * 2 byte: # channels
             * 4 byte: sample rate (Hz)
             * 4 byte: avg bytes per sec
             * 2 byte: block align
             * 2 byte: bits per sample (not necessarily all significant)
             * WAVEFORMATEX adds
             * 2 byte: extension size in bytes (usually 0 for WAVEFORMATEX and 22 for WAVEFORMATEXTENSIBLE with PCM)
             * WAVEFORMATEXTENSIBLE adds
             * 2 byte: valid bits per sample
             * 4 byte: channel mask
             * 16 byte: subformat GUID, first 2 bytes have format type, 1 being PCM
             *
             * Current spec says WAVEFORMATEX with PCM must have bps == 8 or 16, or any multiple of 8 for WAVEFORMATEXTENSIBLE.
             * Lots of old broken WAVEs/apps have don't follow it, e.g. 20 bps but a block align of 3/6 for mono/stereo.
             *
             * Block align for WAVE_FORMAT_PCM or WAVE_FORMAT_EXTENSIBLE is also supposed to be channels*bps/8
             *
             * If the channel mask has more set bits than # of channels, the extra MSBs are ignored.
             * If the channel mask has less set bits than # of channels, the extra channels are unassigned to any speaker.
             *
             * Data is supposed to be unsigned for bps <= 8 else signed.
             */

            /* fmt chunk size */
            data_bytes = read_uint32(dataptr, dataSize, &dataoffset, BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            if(format == FORMAT_WAVE64) 
            {
                /* other half of the size field should be 0 */
                xx = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;

                if(xx != 0) 
                {
                    return BOOL_FALSE;//ERROR: freakishly large Wave64 'fmt ' chunk has incorrect length
                }

                /* subtract size of header */
                if (data_bytes < 16+8) 
                {
                    return BOOL_FALSE; //ERROR: freakishly small Wave64 'fmt ' chunk has incorrect length;
                }

                data_bytes -= (16+8);
            }

            if(data_bytes < 16) 
            {
                return BOOL_FALSE;//ERROR: non-standard 'fmt ' chunk has incorrect length
            }
            
            if(format != FORMAT_WAVE64) 
            {
                if(data_bytes & 1) /* should never happen, but enforce WAVE alignment rules */
                    data_bytes++;
            }
            else 
            { /* Wave64 */
                data_bytes = (data_bytes+7) & (~7u); /* should never happen, but enforce Wave64 alignment rules */
            }

            /* format code */
            wFormatTag = read_uint16(dataptr, dataSize, &dataoffset,  /*big_endian=*/BOOL_FALSE, &bErr);
            if(bErr || (wFormatTag != 1 /*WAVE_FORMAT_PCM*/ && 
                        wFormatTag != 65534 /*WAVE_FORMAT_EXTENSIBLE*/ &&
                        wFormatTag != 7 /*WAVE_FORMAT_mulaw*/ ))
                return BOOL_FALSE; //ERROR: unsupported format type

            /* number of channels */
            channels = read_uint16(dataptr, dataSize, &dataoffset,  /*big_endian=*/BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

           
            /* sample rate */
            sample_rate = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            /* avg bytes per second (ignored) */
            xx = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            /* block align */
            x = read_uint16(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            /* bits per sample */
            bps = read_uint16(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
            if(bErr)
                return BOOL_FALSE;

            outInfo->is_unsigned_samples = (bps <= 8) ? BOOL_TRUE : BOOL_FALSE;

            switch(wFormatTag)
            {
                case 7:
                    outInfo->wvformat = WAV_FORMAT_ULAW;
                    break;
                case 6:
                    outInfo->wvformat = WAV_FORMAT_ALAW;
                    break;
                default:
                    outInfo->wvformat = WAV_FORMAT_PCM;
                    break;
            }

            if((wFormatTag == 1)  || (wFormatTag == 7))
            {
                if(bps != 8 && bps != 16) {
                    if(bps == 24 || bps == 32) {
                        /* let these slide with a warning since they're unambiguous */
                        //flac__utils_printf(stderr, 1, "%s: WARNING: legacy WAVE file has format type %u but bits-per-sample=%u\n", e->inbasefilename, (unsigned)wFormatTag, bps);
                        if(treat_warnings_as_errors)
                        {
                            return BOOL_FALSE;// WARNING: legacy WAVE file has known format type but unknown bits-per-sample
                        }
                    }
                    else 
                    {
                        /* @@@ we could ad an option to specify left- or right-justified blocks so we knew how to set 'shift' */
                        return BOOL_FALSE;//ERROR: legacy WAVE file has known format type %u but bits-per-sample is not supported
                    }
                }
#if 0 /* @@@ reinstate once we can get an answer about whether the samples are left- or right-justified */
                if((bps+7)/8 * channels == block_align) {
                    if(bps % 8) {
                        /* assume legacy file is byte aligned with some LSBs zero; this is double-checked in format_input() */
                        flac__utils_printf(stderr, 1, "%s: WARNING: legacy WAVE file (format type %d) has block alignment=%u, bits-per-sample=%u, channels=%u\n", e->inbasefilename, (unsigned)wFormatTag, block_align, bps, channels);
                        if(e->treat_warnings_as_errors)
                            return false;
                        shift = 8 - (bps % 8);
                        bps += shift;
                    }
                    else
                        shift = 0;
                }
                else {
                    flac__utils_printf(stderr, 1, "%s: ERROR: illegal WAVE file (format type %d) has block alignment=%u, bits-per-sample=%u, channels=%u\n", e->inbasefilename, (unsigned)wFormatTag, block_align, bps, channels);
                    return false;
                }
#else
                shift = 0;
#endif
                if(channels > 2 && !channel_map_none) 
                {
                    return BOOL_FALSE; //ERROR: WAVE has >2 channels but is not WAVE_FORMAT_EXTENSIBLE; cannot assign channels
                }
                
                if(data_bytes < 16)
                {
                    return BOOL_FALSE; //ERROR : Not enough data bytes;
                }

                data_bytes -= 16;
            }
            else 
            {
                if(data_bytes < 40) 
                {
                    return BOOL_FALSE; //ERROR: invalid WAVEFORMATEXTENSIBLE chunk 
                }
                /* cbSize */
                x = read_uint16(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;

                if(x < 22) 
                {
                    return BOOL_FALSE;//ERROR: invalid WAVEFORMATEXTENSIBLE chunk with cbSize
                }
                /* valid bps */
                x = read_uint16(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;

                if((unsigned)x > bps) 
                {
                    return BOOL_FALSE;//ERROR: invalid WAVEFORMATEXTENSIBLE chunk with wValidBitsPerSample
                }
                
                shift = bps - (unsigned)x;
                /* channel mask */

                channel_mask = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;

                /* for mono/stereo and unassigned channels, we fake the mask */
                if(channel_mask == 0) 
                {
                    if(channels == 1)
                        channel_mask = 0x0001;
                    else if(channels == 2)
                        channel_mask = 0x0003;
                }
                /* set channel mapping */
                /* FLAC order follows SMPTE and WAVEFORMATEXTENSIBLE but with fewer channels, which are: */
                /* front left, front right, center, LFE, back left, back right, surround left, surround right */
                /* the default mapping is sufficient for 1-6 channels and 7-8 are currently unspecified anyway */
#if 0
                /* @@@ example for dolby/vorbis order, for reference later in case it becomes important */
                if(
                    options.channel_map_none ||
                    channel_mask == 0x0001 || /* 1 channel: (mono) */
                    channel_mask == 0x0003 || /* 2 channels: front left, front right */
                    channel_mask == 0x0033 || /* 4 channels: front left, front right, back left, back right */
                    channel_mask == 0x0603    /* 4 channels: front left, front right, side left, side right */
                ) {
                    /* keep default channel order */
                }
                else if(
                    channel_mask == 0x0007 || /* 3 channels: front left, front right, front center */
                    channel_mask == 0x0037 || /* 5 channels: front left, front right, front center, back left, back right */
                    channel_mask == 0x0607    /* 5 channels: front left, front right, front center, side left, side right */
                ) {
                    /* to dolby order: front left, center, front right [, surround left, surround right ] */
                    channel_map[1] = 2;
                    channel_map[2] = 1;
                }
                else if(
                    channel_mask == 0x003f || /* 6 channels: front left, front right, front center, LFE, back left, back right */
                    channel_mask == 0x060f || /* 6 channels: front left, front right, front center, LFE, side left, side right */
                    channel_mask == 0x070f || /* 7 channels: front left, front right, front center, LFE, back center, side left, side right */
                    channel_mask == 0x063f    /* 8 channels: front left, front right, front center, LFE, back left, back right, side left, side right */
                ) {
                    /* to dolby order: front left, center, front right, surround left, surround right, LFE */
                    channel_map[1] = 2;
                    channel_map[2] = 1;
                    channel_map[3] = 5;
                    channel_map[4] = 3;
                    channel_map[5] = 4;
                }
#else
                if(
                    channel_map_none ||
                    channel_mask == 0x0001 || /* 1 channel: (mono) */
                    channel_mask == 0x0003 || /* 2 channels: front left, front right */
                    channel_mask == 0x0007 || /* 3 channels: front left, front right, front center */
                    channel_mask == 0x0033 || /* 4 channels: front left, front right, back left, back right */
                    channel_mask == 0x0603 || /* 4 channels: front left, front right, side left, side right */
                    channel_mask == 0x0037 || /* 5 channels: front left, front right, front center, back left, back right */
                    channel_mask == 0x0607 || /* 5 channels: front left, front right, front center, side left, side right */
                    channel_mask == 0x003f || /* 6 channels: front left, front right, front center, LFE, back left, back right */
                    channel_mask == 0x060f || /* 6 channels: front left, front right, front center, LFE, side left, side right */
                    channel_mask == 0x070f || /* 7 channels: front left, front right, front center, LFE, back center, side left, side right */
                    channel_mask == 0x063f    /* 8 channels: front left, front right, front center, LFE, back left, back right, side left, side right */
                ) {
                    /* keep default channel order */
                }
#endif
                else 
                {
                    return BOOL_FALSE;// ERROR: WAVEFORMATEXTENSIBLE chunk with unsupported channel mask=0x%04X\n\nUse --channel-map=none option to store channels in current order; FLAC files\nmust also be decoded with --channel-map=none to restore correct order
                }

                if(!channel_map_none) 
                {
                    if(count_channel_mask_bits(channel_mask) < channels) 
                    {
                        return BOOL_FALSE;//ERROR: WAVEFORMATEXTENSIBLE chunk: channel mask 0x%04X has unassigned channels
                    }
#if 0
                    /* supporting this is too difficult with channel mapping; e.g. what if mask is 0x003f but #channels=4?
                     * there would be holes in the order that would have to be filled in, or the mask would have to be
                     * limited and the logic above rerun to see if it still fits into the FLAC mapping.
                     */
                    else if(count_channel_mask_bits(channel_mask) > channels)
                        channel_mask = limit_channel_mask(channel_mask, channels);
#else
                    else if(count_channel_mask_bits(channel_mask) > channels) 
                    {
                        return BOOL_FALSE;//ERROR: WAVEFORMATEXTENSIBLE chunk: channel mask 0x%04X has extra bits for non-existant channels 
                    }
#endif
                }
                /* first part of GUID */
                x = read_uint16(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;

                if(x != 1) 
                {
                    return BOOL_FALSE;//ERROR: unsupported WAVEFORMATEXTENSIBLE chunk with non-PCM format
                }

                data_bytes -= 26;
            }

            outInfo->bytes_per_wide_sample = channels * (bps / 8);

            /* skip any extra data in the fmt chunk */
            dataoffset += data_bytes;
            if(dataoffset >= dataSize)
            {
                return BOOL_FALSE;//ERROR during read while skipping over extra 'fmt' data
            }

            got_fmt_chunk = BOOL_TRUE;
        }
        else if(
            !memcmp(chunk_id, "data", 4) &&
            (format!=FORMAT_WAVE64 || !memcmp(chunk_id, "data\xF3\xAC\xD3\x11\x8C\xD1\x00\xC0\x4F\x8E\xDB\x8A", 16))
        ) 
        { /* data chunk */
            uint64_t data_bytes;

            if(!got_fmt_chunk) 
            {
                return BOOL_FALSE;//FLAC ERROR: got 'data' chunk before 'fmt' chunk
            }

            /* data size */
            if(format != FORMAT_WAVE64) 
            {
                data_bytes = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;
            }
            else 
            { /* Wave64 */
                data_bytes = read_uint64(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;
                
                data_bytes -= (16+8);
            }
            if(format == FORMAT_RF64)
            {
                if(!got_ds64_chunk) 
                {
                    return BOOL_FALSE;//ERROR: RF64 file has no 'ds64' chunk before 'data' chunk
                }
                
                if(data_bytes == 0xffffffff)
                    data_bytes = ds64_data_size;
            }

            if(ignore_chunk_sizes) {
                //FLAC__ASSERT(!options.sector_align);
                if(data_bytes) {
                    
                    if(treat_warnings_as_errors)
                    {
                        BOOL_FALSE;//WARNING: 'data' chunk has non-zero size, using --ignore-chunk-sizes is probably a bad idea
                    }
                }
                data_bytes = (uint64_t)0 - (uint64_t)outInfo->bytes_per_wide_sample; /* max out data_bytes; we'll use EOF as signal to stop reading */
            }
            else if(0 == data_bytes) 
            {
                return BOOL_FALSE;//ERROR: 'data' chunk has size of 0
            }

            outInfo->dataSize = data_bytes;

            got_data_chunk = BOOL_TRUE;
            break;
        }
        else 
        {			
            uint64_t skip;
            if(!format_options_iff_foreign_metadata) 
            {
                if(format != FORMAT_WAVE64)
                {
                    //flac__utils_printf(stderr, 1, "%s: WARNING: skipping unknown chunk '%s' (use --keep-foreign-metadata to keep)\n", e->inbasefilename, chunk_id);
                }
                else
                {
                    if(treat_warnings_as_errors)
                    {
                        return BOOL_FALSE;//ERROR skipping unknown chunk
                    }
                        /*
                    flac__utils_printf(stderr, 1, "%s: WARNING: skipping unknown chunk %02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X (use --keep-foreign-metadata to keep)\n",
                        e->inbasefilename,
                        (unsigned)((const unsigned char *)chunk_id)[3],
                        (unsigned)((const unsigned char *)chunk_id)[2],
                        (unsigned)((const unsigned char *)chunk_id)[1],
                        (unsigned)((const unsigned char *)chunk_id)[0],
                        (unsigned)((const unsigned char *)chunk_id)[5],
                        (unsigned)((const unsigned char *)chunk_id)[4],
                        (unsigned)((const unsigned char *)chunk_id)[7],
                        (unsigned)((const unsigned char *)chunk_id)[6],
                        (unsigned)((const unsigned char *)chunk_id)[9],
                        (unsigned)((const unsigned char *)chunk_id)[8],
                        (unsigned)((const unsigned char *)chunk_id)[10],
                        (unsigned)((const unsigned char *)chunk_id)[11],
                        (unsigned)((const unsigned char *)chunk_id)[12],
                        (unsigned)((const unsigned char *)chunk_id)[13],
                        (unsigned)((const unsigned char *)chunk_id)[14],
                        (unsigned)((const unsigned char *)chunk_id)[15]
                    );
                if(e->treat_warnings_as_errors)
                    return false;
                    */
                }
            }

            /* chunk size */
            if(format != FORMAT_WAVE64) 
            {
                skip = read_uint32(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;
                
                skip += skip & 1;
            }
            else 
            { /* Wave64 */
                
                skip = read_uint64(dataptr, dataSize, &dataoffset, /*big_endian=*/ BOOL_FALSE, &bErr);
                if(bErr)
                    return BOOL_FALSE;

                skip = (skip+7) & (~(uint64_t)7);
                /* subtract size of header */
                if (skip < 16+8) 
                {
                    return BOOL_FALSE;//ERROR: freakishly small Wave64 chunk has bad length
                }

                skip -= (16+8);
            }

            if(skip) 
            {
                dataoffset += skip;
                if(dataoffset >= dataSize)
                {
                    return BOOL_FALSE;
                }
            }
        }
    }

    if(!got_fmt_chunk) 
    {
        return   BOOL_FALSE; //ERROR: didn't find fmt chunk
    }
    
    if(!got_data_chunk) 
    {
        return  BOOL_FALSE; //ERROR: didn't find data chunk
    }

    
    outInfo->sample_rate = sample_rate;
    outInfo->channels = channels;
    outInfo->bits_per_sample = bps;
    outInfo->shift = shift;
    outInfo->channel_mask = channel_mask;
    outInfo->dataPos = dataoffset;

    return BOOL_TRUE;

}
