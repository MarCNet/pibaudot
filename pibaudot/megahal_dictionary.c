#include <string.h>

#include "megahal_lib.h"
#include "megahal_internal.h"

#define error mh_error



DICTIONARY *mh_realloc_dictionary(DICTIONARY *dictionary)
{
	Context;
	if(dictionary->index == NULL)
		dictionary->index = (BYTE2 *)nmalloc(sizeof(BYTE2)*(dictionary->size));
	else
		dictionary->index = (BYTE2 *)nrealloc((BYTE2 *)(dictionary->index),sizeof(BYTE2)*(dictionary->size));

	if(dictionary->index == NULL)
		return NULL;

	if(dictionary->entry == NULL)
		dictionary->entry = (STRING *)nmalloc(sizeof(STRING)*(dictionary->size));
	else
		dictionary->entry = (STRING *)nrealloc((STRING *)(dictionary->entry),sizeof(STRING)*(dictionary->size));
	if(dictionary->entry == NULL)
		return NULL;

	return dictionary;
}





/*---------------------------------------------------------------------------*/

/*
 *	Function:	Search_Dictionary
 *
 *	Purpose:	Search the dictionary for the specified word, returning its
 *			position in the index if found, or the position where it
 *			should be inserted otherwise.
 */
int mh_search_dictionary(DICTIONARY *dictionary, STRING word, bool *find)
{
	int position;
	int min;
	int max;
	int middle;
	int compar;

	Context;
	/*
	 *	If the dictionary is empty, then obviously the word won't be found
	 */
	if(dictionary->size == 0) {
		position = 0;
		goto notfound;
	}

	/*
	 *	Initialize the lower and upper bounds of the search
	 */
	min = 0;
	max = dictionary->size-1;
	/*
	 *	Search repeatedly, halving the search space each time, until either
	 *	the entry is found, or the search space becomes empty
	 */
	while(TRUE) {
		/*
		 *	See whether the middle element of the search space is greater
		 *	than, equal to, or less than the element being searched for.
		 */
		middle = (min+max)/2;
		compar = mh_wordcmp(word, dictionary->entry[dictionary->index[middle]]);
		/*
		 *	If it is equal then we have found the element.  Otherwise we
		 *	can halve the search space accordingly.
		 */
		if(compar == 0) {
			position = middle;
			goto found;
		} else if(compar > 0) {
			if(max == middle) {
				position = middle+1;
				goto notfound;
			}
			min = middle+1;
		} else {
			if(min == middle) {
				position = middle;
				goto notfound;
			}
			max = middle-1;
		}
	}

found:
	*find=TRUE;
	return position;

notfound:
	*find=FALSE;
	return position;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Add_Key
 *
 *	Purpose:	Add a word to the keyword dictionary.
 */
void mh_add_key(MEGAHAL * mh,  DICTIONARY *keys, STRING word)
{
	int symbol;
	bool fnd;

	Context;
	symbol = mh_find_word(mh->model->dictionary, word);
	if(symbol == 0)
		return;
    if((word.word[0]!=(CHARTYPE)31 && ISALNUM(word.word[0])==0) || (word.word[0]==(CHARTYPE)31 && ISALNUM(word.word[1])==0))
		return;
	mh_search_dictionary(mh->ban, word, &fnd);
	if(fnd)
		return;
	mh_search_dictionary(mh->aux, word, &fnd);
	if(fnd)
		return;

	mh_add_word(keys, word);
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Add_Aux
 *
 *	Purpose:	Add an auxilliary keyword to the keyword dictionary.
 */
void mh_add_aux(MEGAHAL * mh, DICTIONARY *keys, STRING word)
{
	int symbol;
	bool fnd;

	Context;
	symbol = mh_find_word(mh->model->dictionary, word);
	if(symbol == 0)
		return;
	if(ISALNUM(word.word[0]) == 0)
		return;
	mh_search_dictionary(mh->aux, word, &fnd);
	if(!fnd)
		return;

	mh_add_word(keys, word);
}

/*
 *	Function:	Add_Word
 *
 *	Purpose:	Add a word to a dictionary, and return the identifier
 *			assigned to the word.  If the word already exists in
 *			the dictionary, then return its current identifier
 *			without adding it again.
 */
BYTE2 mh_add_word(DICTIONARY *dictionary, STRING word)
{
	register int i;
	int position;
	bool found;

	Context;
	/*
	 *	If the word's already in the dictionary, there is no need to add it
	 */
	position = mh_search_dictionary(dictionary, word, &found);
	if(found == TRUE)
		goto succeed;

	/*
	 *	Increase the number of words in the dictionary
	 */
	dictionary->size += 1;

	/*
	 *	Allocate one more entry for the word index
	 */
	if(mh_realloc_dictionary(dictionary) == NULL) {
		error("add_word", "Unable to reallocate the dictionary.");
		goto fail;
	}

	/*
	 *	Copy the new word into the word array
	 */
	dictionary->entry[dictionary->size-1].length = word.length;
	dictionary->entry[dictionary->size-1].word = (CHARTYPE *)nmalloc(sizeof(CHARTYPE)*(word.length));
	if(dictionary->entry[dictionary->size-1].word == NULL) {
		error("add_word", "Unable to allocate the word.");
		goto fail;
	}
	for(i=0; i<word.length; ++i)
		dictionary->entry[dictionary->size-1].word[i] = word.word[i];

	/*
	 *	Shuffle the word index to keep it sorted alphabetically
	 */
	for(i=(dictionary->size-1); i>position; --i)
		dictionary->index[i] = dictionary->index[i-1];

	/*
	 *	Copy the new symbol identifier into the word index
	 */
	dictionary->index[position] = (unsigned short)(dictionary->size-1);

succeed:
	return dictionary->index[position];

fail:
	return 0;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Initialize_List
 *
 *	Purpose:	Read a dictionary from a file.
 */
DICTIONARY * mh_initialize_list(char *filename)
{
	DICTIONARY *list;
	FILE *file = NULL;
	STRING word = {0,NULL};
	char *string = NULL;
	CHARTYPE *wstring = NULL;
	char buffer[1024];

	Context;
	//setlocale(LC_ALL, "");
	list = mh_new_dictionary();

	if(filename == NULL)
		return list;

	file = fopen(filename, "r");
	if(file == NULL)
		return list;

	while(!feof(file)) {

		if(fgets(buffer, 1024, file) == NULL)
			break;
		if(buffer[0] == '#')
			continue;
		string = strtok(buffer, "\t \n#");
		if((string!=NULL) && (strlen(string)>0)) {

#ifdef USE_WCHAR

			wstring = mh_locale_to_wchar(string);
			word.length = STRLEN(wstring);
			word.word = mh_locale_to_wchar(buffer);
			mh_add_word(list, word);
			nfree(word.word);
			nfree(wstring);
#else

            word.length = STRLEN(string);
            word.word = STRDUP(buffer);
            mh_add_word(list, word);
            nfree(word.word);
#endif 
		}
	}

	fclose(file);
	return list;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Initialize_Dictionary
 *
 *	Purpose:	Add dummy words to the dictionary.
 */
void mh_initialize_dictionary(DICTIONARY *dictionary)
{
	STRING word = { 12, _T("<BRAINSTART>") };
	STRING end = { 5, _T("<FIN>") };

	Context;
	(void)mh_add_word(dictionary, word);
	(void)mh_add_word(dictionary, end);
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	New_Dictionary
 *
 *	Purpose:	Allocate room for a new dictionary.
 */
 DICTIONARY * mh_new_dictionary(void)
{
	DICTIONARY *dictionary = NULL;

	Context;
	dictionary = (DICTIONARY *)nmalloc(sizeof(DICTIONARY));
	if(dictionary == NULL) {
		error("new_dictionary", "Unable to allocate dictionary.");
		return NULL;
	}

	dictionary->size = 0;
	dictionary->index = NULL;
	dictionary->entry = NULL;

	return dictionary;
}

 
/*---------------------------------------------------------------------------*/

/*
 *	Function:	Load_Word
 *
 *	Purpose:	Load a dictionary word from a file.
 */
static void load_word(FILE *file, DICTIONARY *dictionary)
{
	int i = 0;
	STRING word;

	Context;
	if ( fread(&(word.length), sizeof(BYTE1), 1, file) ) {
        word.word = (CHARTYPE *)nmalloc(sizeof(CHARTYPE)*word.length);
		if(word.word == NULL) {
			error("load_word", "Unable to allocate word %u", i);
			return;
		}
		for(i=0; i<word.length; ++i) {
			if (!fread(&(word.word[i]), sizeof(CHARTYPE), 1, file) ) {
				error("load_word", "Unable to load word %u", i);
                break;
			}
		}
		mh_add_word(dictionary, word);
		nfree(word.word);
	}
}

void mh_free_word(STRING word)
{
	Context;
	nfree(word.word);
}


void mh_free_words(DICTIONARY *words)
{
	unsigned long i;

	Context;
	if(words == NULL)
		return;

	if(words->entry != NULL)
		for(i=0; i<words->size; ++i)
			mh_free_word(words->entry[i]);
}


/*
 *	Function:	Free_Dictionary
 *
 *	Purpose:	Release the memory consumed by the dictionary.
 */
void mh_free_dictionary(DICTIONARY *dictionary)
{
	Context;
	if(dictionary == NULL)
		return;
	if(dictionary->entry != NULL) {
		nfree(dictionary->entry);
		dictionary->entry = NULL;
	}
	if(dictionary->index != NULL) {
		nfree(dictionary->index);
		dictionary->index = NULL;
	}
	dictionary->size = 0;
}


/*
 *	Function:	Load_Dictionary
 *
 *	Purpose:	Load a dictionary from the specified file.
 */
void mh_load_dictionary(FILE *file, DICTIONARY *dictionary)
{
	register int i;
	int size;

	Context;
	if ( fread(&size, sizeof(BYTE4), 1, file) ) {
		for(i=0; i<size; ++i) {
			load_word(file, dictionary);
		}
	}
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Find_Word
 *
 *	Purpose:	Return the symbol corresponding to the word specified.
 *			We assume that the word with index zero is equal to a
 *			NULL word, indicating an error condition.
 */
BYTE2 mh_find_word(DICTIONARY *dictionary, STRING word)
{
	int position;
	bool found;

	Context;
	position = mh_search_dictionary(dictionary, word, &found);

	if(found == TRUE)
		return dictionary->index[position];
	else
		return 0;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Dissimilar
 *
 *	Purpose:	Return TRUE or FALSE depending on whether the dictionaries
 *			are the same or not.
 */
bool mh_dissimilar(DICTIONARY *words1, DICTIONARY *words2)
{
	unsigned long i;

	Context;
	if(words1->size != words2->size)
		return TRUE;
	for(i=0; i<words1->size; ++i)
		if(mh_wordcmp(words1->entry[i], words2->entry[i])!=0)
			return TRUE;
	return FALSE;
}


bool mh_isrepeating(MODEL * model, DICTIONARY *replywords)
{
	unsigned long i,j,k;
	bool same;

	Context;
	if (replywords->size <= ((size_t)(model->order)*2+1))
		return FALSE;

	for (i=model->order+1; i<replywords->size-model->order; i++) {
		for (j=0; j<i-model->order; j++) {
			same = TRUE;
			for (k=0; k<=model->order; k++) {
				if (mh_wordcmp(replywords->entry[i+k], replywords->entry[j+k]) != 0) {
					same = FALSE;
					break;
				}
			}
			if (same)
				return TRUE;
		}
	}

	return FALSE;
}

bool mh_dissimilar2(DICTIONARY *words1, DICTIONARY *words2)
{
	unsigned long i,j;
	unsigned long  count = 0;

	Context;
	for(i=0; i<words1->size; ++i) {
		for(j=0; j<words2->size; ++j) {
			if(mh_wordcmp(words1->entry[i], words2->entry[j])==0) {
				count++;
				break;
			}
		}
	}
	if (count > (words1->size/4*3))
		return FALSE;
	return TRUE;
}

/*
 *	Function:	Word_Exists
 *
 *	Purpose:	A silly brute-force searcher for the reply string.
 */
bool mh_word_exists(DICTIONARY *dictionary, STRING word)
{
	unsigned long i;

	Context;
	for(i=0; i<dictionary->size; ++i)
		if(mh_wordcmp(dictionary->entry[i], word) == 0)
			return TRUE;
	return FALSE;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Save_Word
 *
 *	Purpose:	Save a dictionary word to a file.
 */
static void save_word(FILE *file, STRING word)
{
	unsigned long i;

	Context;
	fwrite(&(word.length), sizeof(BYTE1), 1, file);
	for(i=0; i<word.length; ++i)
        fwrite(&(word.word[i]), sizeof(CHARTYPE), 1, file);
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Save_Dictionary
 *
 *	Purpose:	Save a dictionary to the specified file.
 */
void mh_save_dictionary(FILE *file, DICTIONARY *dictionary)
{
	unsigned long i;

	Context;
	fwrite(&(dictionary->size), sizeof(BYTE4), 1, file);
	for(i=0; i<dictionary->size; ++i)
		save_word(file, dictionary->entry[i]);
}


int mh_dictionary_expmem(DICTIONARY *dictionary)
{
	unsigned int i;
	int size = 0;

	Context;
	size += sizeof(DICTIONARY);
	for (i=0; i<dictionary->size; i++)
        size += dictionary->entry[i].length * sizeof(CHARTYPE);

	size += dictionary->size*sizeof(STRING);
	size += dictionary->size*sizeof(BYTE2);

	return size;
}

static int amount_bigger_than(int *syms, int size, int sym)
{
	register int i, j=0;

	Context;
	for(i=0; i<size; i++)
		if(sym > syms[i])
			j++;

	return j;
}

/* This recurses through the model and decrements the symbols that are above the specified symbol
   used when an entry in the dictionary is deleted and everything is shifted down for example. */
static void recurse_tree_and_decrement_symbols(TREE *node, int smallestsymbol, int amount, int *syms)
{
	register int i;

	Context;
	if (node->symbol > smallestsymbol) {
		node->symbol -= amount_bigger_than(syms, amount, node->symbol);
}
	for(i=0; i<node->branch; ++i)
		recurse_tree_and_decrement_symbols(node->tree[i], smallestsymbol, amount, syms);
}


// tries to find words in the main dictionary that arent being used in the model anymore and deletes them and updates everything thats necessary
void mh_trimdictionary(MODEL * model)
{
	unsigned int i, j, k;
	int tmp = 0, tmp2 = 0, smallest = model->dictionary->size;
	int *syms = NULL;

	Context;
	/* First find words that arent being used in the dictionary, mark them with NULL
	   but dont remove them yet because the symbols will shift down and we wont be able to search properly for the rest!
	   Also mark the references in the dictionary index to these words with a unique high number but dont remove yet because the order is different there
	   we need a separate loop to remove them */
	syms = (int *)nmalloc(sizeof(int)*1);
	for(i=0; i<model->dictionary->size; ++i) { // must iterate over index
		if (model->dictionary->index[i]==0 || model->dictionary->index[i]==1)
			continue; // skip default words created when dic init?
		if ((mh_find_symbol(model->forward, model->dictionary->index[i]) == NULL) && (mh_find_symbol(model->backward, model->dictionary->index[i]) == NULL)) 
        { // symbol
			mh_free_word(model->dictionary->entry[model->dictionary->index[i]]);  // symbol
		
            model->dictionary->entry[model->dictionary->index[i]].word = NULL; // symbol
			if(tmp>0)
				syms = (int *)nrealloc((int *)(syms), sizeof(int)*(tmp+1));
			
            syms[tmp] = model->dictionary->index[i];
			smallest = MIN(model->dictionary->index[i], smallest);
			model->dictionary->index[i] = (unsigned short)model->dictionary->size; // index!
			tmp++;
		}
	}

	// only if there is anything to trim:
	if(tmp) {
		/* change all references to words/symbols that were located above the deleted entries
		   this includes the model branches and the index and the phrases!
		   Do this according to the smallest entry and decreasing anything above it by the amount of entries */
		recurse_tree_and_decrement_symbols(model->forward, smallest, tmp, syms);
		recurse_tree_and_decrement_symbols(model->backward, smallest, tmp, syms);

		for(j=0; j<model->dictionary->size; ++j) //  // iterate over index
			if ((model->dictionary->index[j] > smallest) && (model->dictionary->index[j] != model->dictionary->size))
				model->dictionary->index[j] -= amount_bigger_than(syms, tmp, model->dictionary->index[j]);

		for(j=0; j<model->phrasecount; ++j)
			for(k=1; k<=model->phrase[j][0]; ++k)
				if(model->phrase[j][k] > smallest)
					model->phrase[j][k] -= amount_bigger_than(syms, tmp, model->phrase[j][k]);

		// now is a safe time to actually remove the dictionary entries as well (shift them all down)
		tmp2 = 0;
		for(i=0; i<model->dictionary->size; ++i) {
			if(model->dictionary->entry[i].word == NULL) {
				tmp2++;
				continue;
			}
			if(tmp2 == 0) continue;
			model->dictionary->entry[i-tmp2] = model->dictionary->entry[i];
		}

		// next, shift all the index entries down to replace the empty ones
		tmp2 = 0;
		for(i=0; i<model->dictionary->size; ++i) {
			if(model->dictionary->index[i] == model->dictionary->size) {
				tmp2++;
				continue;
			}
			if(tmp2 == 0) continue;
			model->dictionary->index[i-tmp2] = model->dictionary->index[i];
		}

		// resize the dictionary and reallocate the mem
		model->dictionary->size -= tmp;
		if(mh_realloc_dictionary(model->dictionary) == NULL) {
			error("trimdictionary", "Unable to reallocate dictionary.");
			nfree(syms);
			return;
		}
	}
	nfree(syms);
}