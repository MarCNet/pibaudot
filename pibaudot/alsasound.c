#ifdef __linux__ 

#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include "internal.h"

/*
typedef struct _ALSAData
{

    
    unsigned int bitsPerSample;
    unsigned int channels;
    unsigned int rate;
    unsigned int deviceid;
    size_t bufferSizeMs;

} ALSAData;
*/

typedef struct _CaptureData
{
    snd_pcm_hw_params_t * hwParams;
    snd_pcm_t * pcmHandle;
    ThreadHandle recordThread;
    enum BOOLEAN_VALUE debugRecord;
    enum BOOLEAN_VALUE shouldQuit;
    HaveRecordSoundCallback callback;
    void * callbackparam;


}CaptureData;

enum PlaybackState
{
    PlaybackState_WaitForData,      //Wait for more sample data to convert and play
    PlaybackState_PrepareData,      //Convert the sample data (doubles) into byte values for the hardware
    PlaybackState_Play,        //Start playing converted data
    PlaybackState_Drain,
    
    PlaybackState_Error
};


typedef struct _PlaybackData
{
    snd_pcm_hw_params_t * hwParams;
    snd_pcm_t * pcmHandle;
    unsigned int deviceId;
    char deviceName[50];
    const char * playbackWriteAudioFilename;
    ThreadHandle playbackThread;
    WAVDESTHND debugdesthnd;
    enum BOOLEAN_VALUE debugPlayback;
    enum BOOLEAN_VALUE shouldQuit;
    enum BOOLEAN_VALUE isWaiting;
    
    GPIOHandle gpiohandle;
    unsigned int wakeLengthBytes; //We don't generate the wake tone. We just need to know how long it is so we can correctly set the GPIO bits
    unsigned int dataBitLengthBytes;
    unsigned int gapLengthBytes;

    MemoryBuffer * samplePlayData;
    MemoryBuffer * sampleByteData;
    MemoryBuffer * bitSeqData;

} PlaybackData;

/*
static void freeAlsaData(ALSAData * data, enum BOOLEAN_VALUE enableDebug)
{
    if(data->pcmHandle != NULL)
    {
        if(enableDebug)
            printf("\t close pcm handle\r\n");
        
        snd_pcm_close(data->pcmHandle);
        data->pcmHandle=NULL;
        
        if(enableDebug)
            printf("\t close pcm handle done\r\n");
    }

    if(data->hwParams != NULL)
    {
        if(enableDebug)
            printf("\t free hw params\r\n");
        
        snd_pcm_hw_params_free(data->hwParams);
        data->hwParams=NULL;
        
        if(enableDebug)
            printf("\t free hw params done\r\n");
    }
}*/


static snd_pcm_hw_params_t *  setupDeviceParameters(const char * nameToOpen, snd_pcm_t * pcmHandle, unsigned int bitsPerSample, unsigned int sample_rate, unsigned int channels)
{
    int res;
    snd_pcm_hw_params_t * retHwParams = NULL;
    const snd_pcm_format_t format = (bitsPerSample == 8) ? SND_PCM_FORMAT_S8 : SND_PCM_FORMAT_S16;

    res = snd_pcm_hw_params_malloc(&retHwParams);
    if(res < 0) 
    {
        printf("ERROR : Failed to allocate capture device parameters for device [%s] : %s \r\n",   nameToOpen, snd_strerror(res));
        return NULL;
    }

    res = snd_pcm_hw_params_any(pcmHandle, retHwParams);
    if(res < 0) 
    {
        printf("ERROR : Failed to initialise capture device parameters for device [%s] : %s \r\n",   nameToOpen, snd_strerror(res));
        snd_pcm_hw_params_free(retHwParams);
        return NULL;
    }

    res = snd_pcm_hw_params_set_access(pcmHandle, retHwParams, SND_PCM_ACCESS_RW_INTERLEAVED);
    if(res < 0)
    {
        printf("ERROR : Failed to set capture access type or device  [%s] : %s\r\n",  nameToOpen, snd_strerror(res));
        snd_pcm_hw_params_free(retHwParams);
        return NULL;
    }

    res = snd_pcm_hw_params_set_format(pcmHandle, retHwParams, format);
    if(res < 0)
    {
        printf("ERROR : Failed to set capture format %u (%d) for device  [%s] : %s\r\n", format, bitsPerSample,  nameToOpen,  snd_strerror(res));
        snd_pcm_hw_params_free(retHwParams);
        return NULL;
    }

    res = snd_pcm_hw_params_set_rate(pcmHandle, retHwParams, sample_rate, 0);
    if(res < 0)
    {
        printf("ERROR : Failed to set capture rate %u for device [%s] : %s\r\n", sample_rate,   nameToOpen, snd_strerror(res));
        snd_pcm_hw_params_free(retHwParams);
        return NULL;
    }

    res = snd_pcm_hw_params_set_channels(pcmHandle, retHwParams, channels);
    if(res < 0)
    {
        printf("ERROR : Failed to set capture channels %u for device  [%s] : %s\r\n",  channels,  nameToOpen, snd_strerror(res));
        snd_pcm_hw_params_free(retHwParams);
        return NULL;
    }

    return retHwParams;
}


static enum BOOLEAN_VALUE findDevice(enum BOOLEAN_VALUE isCapture, int deviceid, enum BOOLEAN_VALUE enableDebug, char * retNameToOpen)
{
    int curIdx = isCapture ? 1 : 1001;
    int devIdx, cardIdx;
    snd_pcm_info_t *pcmInfo = NULL;
    snd_ctl_card_info_t *cardInfo = NULL;
    enum BOOLEAN_VALUE foundDevice = BOOL_FALSE;
    char alsaCardName[50];
    

    //Find the device
    snd_ctl_card_info_alloca( &cardInfo );
    snd_pcm_info_alloca( &pcmInfo );

    cardIdx = -1;
    while( (snd_card_next( &cardIdx ) == 0) && (cardIdx >= 0 ))
    {
        snd_ctl_t *ctl = NULL;

        snprintf( alsaCardName, sizeof (alsaCardName), "hw:%d", cardIdx );
        if(enableDebug)
            printf("\t open card %s\r\n", alsaCardName);

        if( snd_ctl_open( &ctl, alsaCardName, 0 ) >= 0 )
        {  
            if(enableDebug)
                printf("\t find device\r\n");

            devIdx = -1;
            while( (snd_ctl_pcm_next_device( ctl, &devIdx ) == 0) && (devIdx >= 0 ))
            {
                if(enableDebug)
                    printf("\t dev idx = %u\r\n", devIdx);

                snd_pcm_info_set_device( pcmInfo, devIdx );
                snd_pcm_info_set_subdevice( pcmInfo, 0 );
                snd_pcm_info_set_stream( pcmInfo, isCapture ? SND_PCM_STREAM_CAPTURE : SND_PCM_STREAM_PLAYBACK );
                if( snd_ctl_pcm_info( ctl, pcmInfo ) >= 0 )
                {
                    if(enableDebug)
                        printf("\t got info for device = %u\r\n", devIdx);

                    if(curIdx == deviceid)
                    {
                        sprintf(retNameToOpen, "hw:CARD=%d,%d", cardIdx, devIdx);
                        
                        if(enableDebug)
                            printf("\t got device %s\r\n", retNameToOpen);

                        foundDevice = BOOL_TRUE;
                        break;
                    }

                    ++curIdx;
                }
            }

            if(foundDevice)
                break;
        }
    }
}


static void BD_CALLBACK playbackThreadProc(void * param)
{
    PlaybackData * playdata = (PlaybackData  *)param; 
    enum PlaybackState currentState = PlaybackState_WaitForData;
    uint8_t * newDataPtr;
    size_t numSamplesToWrite, numSampDataBytesToWrite, numSamplesWritten;
    size_t sampleByteBufferContentSz, sampleByteBufferContentPos;
    size_t frameSizeBytes;
    int res;
    snd_pcm_uframes_t frames;
    const uint8_t * sampleByteDataPtr;
    size_t x;
    const uint64_t zero = 0;
    int dir = 0;
    unsigned int currate, curchannels, curbits;
    MemoryBuffer * bitSeqBuffer = membuf_init();
    
    currate = 0;
    dir=0;
    curchannels=0;
    snd_pcm_hw_params_get_rate( playdata->hwParams, &currate, &dir);
    snd_pcm_hw_params_get_channels(playdata->hwParams, &curchannels);
    curbits = snd_pcm_hw_params_get_sbits(playdata->hwParams);

    if(playdata->debugPlayback)
        printf(" Start Playback thread: Samplerate=%u channels=%u  Bits=%u\r\n", currate, curchannels, curbits);
    
    //main playback loop
    while(!playdata->shouldQuit)
    {   
        switch(currentState)
        {
            case PlaybackState_WaitForData:
                
                //Signal not playing
                gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
                
                //Lock sample buffer
                cppthrd_LockThread(playdata->playbackThread);

                //Set waiting
                playdata->isWaiting = BOOL_TRUE;

                if(playdata->debugPlayback)
                    printf("ALSA PLAYBACK Wait\r\n");

                if(cppthrd_WaitForEvent(playdata->playbackThread, 30000, BOOL_TRUE))
                {
                    if(playdata->debugPlayback)
                        printf("ALSA PLAYBACK Buffer Notification %u\r\n", membuf_getContentSize(playdata->samplePlayData));

                    if(membuf_getContentSize(playdata->samplePlayData) > 0)
                    {
                        //We have data to play, move to next state
                        currentState = PlaybackState_PrepareData;

                        //Set no longer waiting
                        playdata->isWaiting = BOOL_FALSE;
                    }
                }

                //Unlock
                cppthrd_UnlockThread(playdata->playbackThread);
                break;
            case PlaybackState_PrepareData:
                {
                    //query alsa for playback parameters
                    currate = 0;
                    dir=0;
                    curchannels=0;
                    curbits = snd_pcm_hw_params_get_sbits(playdata->hwParams);
                    
                    if((curbits < 0) ||
                       (snd_pcm_hw_params_get_rate( playdata->hwParams, &currate, &dir) < 0) ||
                       (snd_pcm_hw_params_get_channels(playdata->hwParams, &curchannels) < 0) )
                    {
                        printf("ALSA PLAYBACK ERROR : Failed to query sound format \r\n");
                        currentState = PlaybackState_Error;
                    }
                    else
                    {
                        //Lock sample buffer
                        cppthrd_LockThread(playdata->playbackThread);

                        //How many samples are there
                        numSamplesToWrite =  membuf_getContentSize( playdata->samplePlayData ) / sizeof(double);
                        numSampDataBytesToWrite = (numSamplesToWrite  * curchannels * curbits) / 8;
                        if(numSamplesToWrite == 0)
                        {
                            sampleByteBufferContentPos = 0;
                            currentState = PlaybackState_WaitForData;
                            membuf_erase(bitSeqBuffer);
                        }
                        else
                        {
                            //Open wav file that writes what is played
                            if((playdata->debugdesthnd == NULL) && (playdata->playbackWriteAudioFilename != NULL) && (playdata->playbackWriteAudioFilename[0] != 0))
                            {
                                char acfullname[260];
                                snprintf(acfullname, sizeof(acfullname)- 1, "%s.play%04u.wav", playdata->playbackWriteAudioFilename, playdata->deviceId);
                                playdata->debugdesthnd = wavdest_init(acfullname, currate, 1.0f, BOOL_FALSE);
                            }

                            if(playdata->debugPlayback)
                                printf("ALSA PLAYBACK Prepare - Num samples=%u  Num Bytes=%u  Samplerate=%u Channels=%u Bits=%u\r\n",numSamplesToWrite, numSampDataBytesToWrite, currate, curchannels, curbits);

                            //Make sure our storage buffer has enough space
                            newDataPtr = membuf_reserveSpaceForNewData(playdata->sampleByteData, numSampDataBytesToWrite);

                            //Write actual data
                            numSamplesWritten = smputil_convertDoublesToSampleBytes( (const double *)membuf_ptr(playdata->samplePlayData, 0 ), numSamplesToWrite,
                                                                                        curchannels, curbits, 1.0f, 
                                                                                        newDataPtr, numSampDataBytesToWrite);
                            if(playdata->debugdesthnd != NULL)
                                wavdest_write(playdata->debugdesthnd,  (const double *)membuf_ptr(playdata->samplePlayData, 0), numSamplesWritten);
                        
                            if(playdata->debugPlayback)
                                printf("ALSA PLAYBACK Prepare done - Num samples=%u\r\n",numSamplesWritten);

                            //Set up copy position and amount to copy into the play buffer
                            sampleByteBufferContentSz = (numSamplesWritten  * curchannels * curbits) / 8;
                            sampleByteBufferContentPos = 0;

                            //Copy bit sequence as well
                            //This is used to set the 'data value' bit on the gpio
                            if(playdata->bitSeqData != NULL)
                            {
                                const size_t bitseqSize = membuf_getContentSize(playdata->bitSeqData);
                                if(bitseqSize > 0)
                                {
                                    membuf_erase(bitSeqBuffer);
                                    membuf_append_copy(bitSeqBuffer, playdata->bitSeqData);
                                    membuf_erase(playdata->bitSeqData);
                                }
                            }

                            //Remove the data we've converted from
                            membuf_removeFromFront( playdata->samplePlayData, numSamplesWritten * sizeof(double));

                            //Set next state to set up for copying the converted data to the playback buffer 
                            currentState = PlaybackState_Play;
                        }

                        //Finally, unlock the sample buffer
                        cppthrd_UnlockThread(playdata->playbackThread);
                    }
                }
                break;
            case PlaybackState_Play:

                currate = 0;
                dir=0;
                frames = 0;
                curchannels=0;
                curbits = snd_pcm_hw_params_get_sbits(playdata->hwParams);
                if((curbits < 0) ||
                    (snd_pcm_hw_params_get_rate( playdata->hwParams, &currate, &dir) < 0) ||
                    (snd_pcm_hw_params_get_period_size(playdata->hwParams, &frames, 0) < 0) ||
                    (snd_pcm_hw_params_get_channels(playdata->hwParams, &curchannels) < 0) )
                {
                    printf("ALSA PLAYBACK ERROR : Failed to query sound format and period size for device  [%s] \r\n", playdata->deviceName);
                    currentState = PlaybackState_Error;
                }
                else
                {
                    size_t frameSizeBytes = (frames * curchannels * curbits) / 8;
                    
                    if(playdata->debugPlayback)
                        printf("ALSA PLAYBACK : Frames=%u  bytes=%u  samplerate=%u  channels=%u  bits=%u\r\n", frames, frameSizeBytes, currate, curchannels, curbits);

                    //Play data 
                    while( (!playdata->shouldQuit) && (sampleByteBufferContentPos < sampleByteBufferContentSz) )
                    {
                        if(playdata->debugPlayback)
                            printf("ALSA PLAYBACK : play %u/%u\r\n", sampleByteBufferContentPos, sampleByteBufferContentSz);

                        const size_t copysz = (frameSizeBytes > (sampleByteBufferContentSz - sampleByteBufferContentPos)) ? (sampleByteBufferContentSz  - sampleByteBufferContentPos): frameSizeBytes;
                        sampleByteDataPtr = membuf_ptr(playdata->sampleByteData, sampleByteBufferContentPos);
                        if(sampleByteDataPtr == NULL)
                            break;

                        res = snd_pcm_writei(playdata->pcmHandle, sampleByteDataPtr, frames);
                        if(res == -EPIPE)
                        {
                            if(playdata->debugPlayback)
                                printf("ALSA PLAYBACK : XRun %u/%u\r\n", sampleByteBufferContentPos, sampleByteBufferContentSz);

                            snd_pcm_prepare(playdata->pcmHandle);
                        }
                        else if(res < 0)
                        {
                            printf("ERROR : Playback failed to write to PCM device  [%s] : %s\r\n",  playdata->deviceName, snd_strerror(res));
                            break;
                        }

                        sampleByteBufferContentPos += copysz;

                        //Work out play position for GPIO
                        if(sampleByteBufferContentPos < playdata->wakeLengthBytes)
                        {
                            gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Wait);
                        }
                        else if(sampleByteBufferContentPos < sampleByteBufferContentSz)
                        {
                            const size_t playPosNoWake = (sampleByteBufferContentPos - playdata->wakeLengthBytes);
                            const size_t playPosForCurBit = playPosNoWake % (playdata->gapLengthBytes + playdata->dataBitLengthBytes);
                            const size_t bitseqSize = membuf_getContentSize(bitSeqBuffer);
                            const enum BOOLEAN_VALUE * bitseq = (const enum BOOLEAN_VALUE *)membuf_ptr(bitSeqBuffer, 0);
                            if(bitseq == NULL)
                            {
                                gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data);
                            }
                            else if(playPosForCurBit < playdata->dataBitLengthBytes)
                            {
                                const size_t dwBitNum = playPosNoWake / (playdata->gapLengthBytes + playdata->dataBitLengthBytes);
                                if(dwBitNum >= bitseqSize)
                                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data);
                                else if(bitseq[dwBitNum] == BOOL_TRUE)
                                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data_High);
                                else 
                                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data_Low);
                            }
                            else
                            {
                                gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Gap);
                            }
                        }
                        else
                        {
                            gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
                        }
                    }


                    if(playdata->debugPlayback)
                        printf("ALSA PLAYBACK : Erase Byte Data\r\n");

                    //Erase sample byte data buffer, now that it has been played
                    if(sampleByteBufferContentPos >= sampleByteBufferContentSz)
                    {
                        membuf_erase(playdata->sampleByteData);
                    }
                    else
                    {
                        membuf_removeFromFront(playdata->sampleByteData, sampleByteBufferContentPos);
                    }

                    if(!playdata->shouldQuit) 
                    {
                        //Lock sample buffer
                        cppthrd_LockThread(playdata->playbackThread);

                        if(membuf_getContentSize(playdata->samplePlayData) > 0)
                        {
                            if(playdata->debugPlayback)
                                printf("ALSA PLAYBACK : Prepare next data\r\n");

                            //We have data to play, move to next state
                            currentState = PlaybackState_PrepareData;
                        }
                        else
                        {
                            if(playdata->debugPlayback)
                                printf("ALSA PLAYBACK : need to drain\r\n");

                            currentState = PlaybackState_Drain;
                        }

                        //unlock the sample buffer
                        cppthrd_UnlockThread(playdata->playbackThread);
                    }
                    else
                    {
                        if(playdata->debugPlayback)
                            printf("ALSA PLAYBACK : Should quit...\r\n");
                    }
                }
                break;
            case PlaybackState_Drain:
                if(!playdata->shouldQuit)
                {
                    //Drain
                    if(playdata->debugPlayback)
                        printf("ALSA PLAYBACK : Drain \r\n");

                    res =  snd_pcm_drain(playdata->pcmHandle);

                    if(playdata->debugPlayback)
                        printf("ALSA PLAYBACK : Drain done %u %s\r\n", res, (res >= 0) ? "ok" : snd_strerror(res));

                     //Signal not playing
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
                    
                    //Lock sample buffer
                    cppthrd_LockThread(playdata->playbackThread);

                    if(membuf_getContentSize(playdata->samplePlayData) > 0)
                    {
                        if(playdata->debugPlayback)
                            printf("ALSA PLAYBACK : Drain -> Prepare next data\r\n");

                        //We have data to play, move to next state
                        currentState = PlaybackState_PrepareData;
                    }
                    else
                    {
                        if(playdata->debugPlayback)
                            printf("ALSA PLAYBACK : Drain -> Wait for data\r\n");

                        currentState = PlaybackState_WaitForData;
                    }

                    //unlock the sample buffer
                    cppthrd_UnlockThread(playdata->playbackThread);
                }
                else
                {
                    if(playdata->debugPlayback)
                        printf("ALSA PLAYBACK : Drain -> Should quit...\r\n");
                }
                break;
            default:
                printf("ERROR : Playback, invalid state %u\r\n",currentState);
                currentState = PlaybackState_Error;
                break;
        }
    }

    membuf_free(bitSeqBuffer);
}


static void BD_CALLBACK recordThreadProc(void * param)
{
    int res;
    CaptureData * capdata = (CaptureData *)param;
    snd_pcm_uframes_t framesInOnePeriod = 0;
    int dir=0;
    uint64_t totBufferSize;
    unsigned char * buffer;
    unsigned int currate, curchannels, curbits;
    
    currate = 0;
    dir=0;
    curchannels=0;
    snd_pcm_hw_params_get_rate( capdata->hwParams, &currate, &dir);
    snd_pcm_hw_params_get_channels(capdata->hwParams, &curchannels);
    curbits = snd_pcm_hw_params_get_sbits(capdata->hwParams);

    if(capdata->debugRecord)
        printf(" Start Record: Samplerate=%u channels=%u  Bits=%u\r\n", currate, curchannels, curbits);

    //Use a buffer size large enough to hold one 'period'    
    res = snd_pcm_hw_params_get_period_size(capdata->hwParams, &framesInOnePeriod , &dir);
    if(res < 0)
    {
        printf("ERROR : Failed to query number of frames : %s \r\n", snd_strerror(res));
        return;
    }

    totBufferSize = ((uint64_t)framesInOnePeriod * curchannels * curbits) / 8;
    if(capdata->debugRecord)
        printf("  Period_Frames=%u  bytes=%llu\r\n",  framesInOnePeriod, totBufferSize);


    buffer = (unsigned char *)malloc(totBufferSize);
    
    res = snd_pcm_prepare(capdata->pcmHandle);
    if(res < 0)
    {
        printf("ERROR : Failed to prepare capture : %s \r\n", snd_strerror(res));
        return;
    }

    while(!capdata->shouldQuit)
    {
        res = snd_pcm_readi(capdata->pcmHandle, buffer, framesInOnePeriod);
        if(res == -EPIPE)
        {
            //Overrun
            if(capdata->debugRecord)
                printf("ERROR : Overrun\r\n");
            
            snd_pcm_prepare(capdata->pcmHandle);
        }
        else if(res < 0)
        {
            printf("ERROR : Recording Error %s\r\n", snd_strerror(res));
            break;
        }
        else if(!capdata->shouldQuit)
        {
           
            currate = 0;
            dir=0;
            curchannels=0;
            snd_pcm_hw_params_get_rate( capdata->hwParams, &currate, &dir);
            snd_pcm_hw_params_get_channels(capdata->hwParams, &curchannels);
            curbits = snd_pcm_hw_params_get_sbits(capdata->hwParams);

            /*if(capdata->debugRecord)
                printf(" Got data %u - samplerate=%u channels=%u BitsPerSample=%u\r\n", res, currate, curchannels, curbits);*/


            capdata->callback(currate,curchannels, curbits,  buffer, (res * curbits * curchannels) / 8, capdata->callbackparam);

        }
    }

    if(capdata->debugRecord)
        printf(" Recording thread stopped\r\n");
}


SoundRecData soundrec_Start(unsigned int device, unsigned int channels, unsigned int sample_rate, unsigned int bitsPerSample, size_t bufferSizeMs,  HaveRecordSoundCallback callback, void * callbackparam, 
                            enum BOOLEAN_VALUE debugThread, enum BOOLEAN_VALUE debugRecord)
{
    CaptureData * capdata = (CaptureData *)malloc(sizeof(CaptureData));
    char nameToOpen[50];
    int res;
    int dir;
    snd_pcm_uframes_t periods;
    const uint64_t bufferSizeFrames = ((uint64_t)sample_rate  * (uint64_t)bufferSizeMs) / 1000;

    memset(capdata,0, sizeof(CaptureData));

    if((bitsPerSample != 8) && (bitsPerSample != 16))
    {
        printf("ERROR: Invalid bits per sample - expected 8 or 16\r\n");
        soundrec_Stop(capdata);
        return NULL;
    }


    if(!findDevice(BOOL_TRUE, device, debugRecord, nameToOpen))
    {
        printf("ERROR : Failed to find capture device %d\r\n", device);
        soundrec_Stop(capdata);
        return NULL;
    }

    //==============================================================================
    
    if(debugRecord)
        printf("\t Open %s \r\n", nameToOpen);
    
    res = snd_pcm_open(&capdata->pcmHandle, nameToOpen, SND_PCM_STREAM_CAPTURE, 0);
    if(res < 0)
    {
        printf("ERROR : Failed to open capture device [%s] : %s\r\n", nameToOpen, snd_strerror(res));
        soundrec_Stop(capdata);
        return NULL;
    }

    capdata->hwParams = setupDeviceParameters(nameToOpen, capdata->pcmHandle, bitsPerSample, sample_rate,channels);
    if(capdata->hwParams == NULL)
    {
        printf("ERROR : Failed to set up capture device [%s] : %s\r\n", nameToOpen, snd_strerror(res));
        soundrec_Stop(capdata);
        return NULL;
    }
    

    //Use two periods, each of the specified buffer length
    periods = (snd_pcm_uframes_t)(bufferSizeFrames * 2); 
    if(debugRecord)
        printf("\t Periods for %s = %d \r\n", nameToOpen, periods);

    res = snd_pcm_hw_params_set_period_size_near(capdata->pcmHandle,capdata->hwParams, &periods, &dir);
    if(res < 0)
    {
        printf("ERROR : Failed to set period size %u for device [%s] : %s\r\n", periods, nameToOpen, snd_strerror(res));
        soundrec_Stop(capdata);
        return NULL;
    }

    res = snd_pcm_hw_params(capdata->pcmHandle, capdata->hwParams);
    if(res < 0)
    {
        printf("ERROR : Failed to set capture parameters for device  [%s] : %s\r\n",  nameToOpen, snd_strerror(res));
        soundrec_Stop(capdata);
        return NULL;
    }

    //Create & Start thread
    capdata->callback = callback;
    capdata->debugRecord = debugRecord;
    capdata->shouldQuit = BOOL_FALSE;
    capdata->callbackparam = callbackparam;
    capdata->recordThread = cppthrd_CreateThread( capdata, recordThreadProc, debugThread);
    cppthrd_StartThread(capdata->recordThread, BOOL_FALSE);
    
    return capdata;
}

void soundrec_Stop(SoundRecData sndrec)
{   
    CaptureData * capdata = (CaptureData *)sndrec;

    capdata->shouldQuit = BOOL_TRUE;

    if(capdata->recordThread != NULL)
    {
        if(capdata->debugRecord)
            printf("\t wait and destroy sound capture thread\r\n");
        
        cppthrd_WaitAndDestroyThread(capdata->recordThread);
        capdata->recordThread=NULL;

        if(capdata->debugRecord)
            printf("\t destroyed sound capture thread\r\n");
    }


    if(capdata->debugRecord)
        printf("Free alsa \r\n");

    if(capdata->pcmHandle != NULL)
    {
        if(capdata->debugRecord)
            printf("\t close pcm handle\r\n");
        
        snd_pcm_close(capdata->pcmHandle);
        capdata->pcmHandle=NULL;
        
        if(capdata->debugRecord)
            printf("\t close pcm handle done\r\n");
    }

    if(capdata->hwParams != NULL)
    {
        if(capdata->debugRecord)
            printf("\t free hw params\r\n");
        
        snd_pcm_hw_params_free(capdata->hwParams);
        capdata->hwParams=NULL;
        
        if(capdata->debugRecord)
            printf("\t free hw params done\r\n");
    }

    free(capdata);
}



void soundrec_PrintDevices()
{
    snd_pcm_info_t *pcmInfo = NULL;
    snd_ctl_card_info_t *cardInfo = NULL;
    int cardIdx = -1;
    char alsaCardName[50];
    int outIdx = 1;

    snd_ctl_card_info_alloca( &cardInfo );
    snd_pcm_info_alloca( &pcmInfo );

    printf("CAPTURE DEVICES:\r\n------------------\r\n");
    while( (snd_card_next( &cardIdx ) == 0) && (cardIdx >= 0 ))
    {
        snd_ctl_t *ctl = NULL;

        snprintf( alsaCardName, sizeof (alsaCardName), "hw:%d", cardIdx );

        //Acquire name of card
        if( snd_ctl_open( &ctl, alsaCardName, 0 ) >= 0 )
        {   
            int devIdx = -1;

            snd_ctl_card_info( ctl, cardInfo );

            while( (snd_ctl_pcm_next_device( ctl, &devIdx ) == 0) && (devIdx >= 0 ))
            {
                // Obtain info about this particular device
                snd_pcm_info_set_device( pcmInfo, devIdx );
                snd_pcm_info_set_subdevice( pcmInfo, 0 );
                snd_pcm_info_set_stream( pcmInfo, SND_PCM_STREAM_CAPTURE );
                if( snd_ctl_pcm_info( ctl, pcmInfo ) >= 0 )
                {
                    //This device has capture
                    printf(" %d : [%d,%d] : %s : %s\r\n", outIdx , cardIdx, devIdx, snd_ctl_card_info_get_name( cardInfo ), snd_pcm_info_get_name( pcmInfo ));
                    ++outIdx;
                }
            }

            snd_ctl_close( ctl );
            ctl= NULL;
        }
    }

    printf("\r\nPLAYBACK DEVICES:\r\n------------------\r\n");
    cardIdx = -1;
    outIdx = 1;
    while( (snd_card_next( &cardIdx ) == 0) && (cardIdx >= 0 ))
    {
        snd_ctl_t *ctl = NULL;

        snprintf( alsaCardName, sizeof (alsaCardName), "hw:%d", cardIdx );

        //Acquire name of card
        if( snd_ctl_open( &ctl, alsaCardName, 0 ) >= 0 )
        {   
            int devIdx = -1;

            snd_ctl_card_info( ctl, cardInfo );

            while( (snd_ctl_pcm_next_device( ctl, &devIdx ) == 0) && (devIdx >= 0 ))
            {
                // Obtain info about this particular device
                snd_pcm_info_set_device( pcmInfo, devIdx );
                snd_pcm_info_set_subdevice( pcmInfo, 0 );
                snd_pcm_info_set_stream( pcmInfo, SND_PCM_STREAM_PLAYBACK );
                if( snd_ctl_pcm_info( ctl, pcmInfo ) >= 0 )
                {
                    //This device has capture
                    printf(" %d : [%d,%d] : %s : %s\r\n", outIdx + 1000, cardIdx, devIdx, snd_ctl_card_info_get_name( cardInfo ), snd_pcm_info_get_name( pcmInfo ));
                    ++outIdx;
                }
            }

            snd_ctl_close( ctl );
            ctl= NULL;
        }
    }
}



SoundPlaybackData soundplay_init(unsigned int device, unsigned int channels, unsigned int sample_rate, unsigned int bitsPerSample, unsigned int bufferSizeMs, 
                                enum BOOLEAN_VALUE debugThread, enum BOOLEAN_VALUE debugPlayback, const char * playbackWriteAudioFilename, GPIOHandle gpiohandle, 
                                unsigned int wakeUpLengthMs, unsigned int dataBitLengthMs, unsigned int dataGapLengthMs)
{
    int res;
    enum BOOLEAN_VALUE foundDevice= BOOL_FALSE;
    char nameToOpen[50];
    PlaybackData * playdata = (PlaybackData *)malloc(sizeof(PlaybackData));
    
    memset(playdata,0,sizeof(PlaybackData));

    if((bitsPerSample != 8) && (bitsPerSample != 16))
    {
        printf("ERROR: Invalid bits per sample - expected 8 or 16\r\n");
        soundplay_deinit(playdata);
        return NULL;
    }

    //Find the device
    if(!findDevice(BOOL_FALSE, device, debugPlayback,nameToOpen))
    {
        printf("ERROR : Failed to find playback device %d\r\n", device);
        soundplay_deinit(playdata);
        return NULL;
    }

    //Open device
    res = snd_pcm_open(&playdata->pcmHandle, nameToOpen, SND_PCM_STREAM_PLAYBACK, 0);
    if(res < 0)
    {
        printf("ERROR : Failed to open playback device [%s] : %s\r\n", nameToOpen, snd_strerror(res));
        soundplay_deinit(playdata);
        return NULL;
    }

    //Set up device
    playdata->hwParams = setupDeviceParameters(nameToOpen, playdata->pcmHandle, bitsPerSample, sample_rate,channels);
    if(playdata->hwParams == NULL)
    {
        printf("ERROR : Failed to set up playback device [%s] : %s\r\n", nameToOpen, snd_strerror(res));
        soundplay_deinit(playdata);
        return NULL;
    }

    res = snd_pcm_hw_params(playdata->pcmHandle, playdata->hwParams);
    if(res < 0)
    {
        printf("ERROR : Failed to set playback parameters for device  [%s] : %s\r\n",  nameToOpen, snd_strerror(res));
        soundplay_deinit(playdata);
        return NULL;
    }

  
    //Create & Start thread
    playdata->debugPlayback = debugPlayback;
    playdata->shouldQuit = BOOL_FALSE;
    playdata->deviceId = device;
    playdata->samplePlayData = membuf_init();
    playdata->sampleByteData = membuf_init();
    playdata->isWaiting = BOOL_TRUE;
    playdata->playbackWriteAudioFilename = playbackWriteAudioFilename;
    memcpy(playdata->deviceName, nameToOpen, (sizeof(nameToOpen) < sizeof(playdata->deviceName)) ? sizeof(nameToOpen) : sizeof(playdata->deviceName));
    playdata->gpiohandle = gpiohandle;
    playdata->wakeLengthBytes = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)wakeUpLengthMs ) / 8000;
    playdata->dataBitLengthBytes = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)dataBitLengthMs ) / 8000;
    playdata->gapLengthBytes = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)dataGapLengthMs ) / 8000;
    playdata->playbackThread = cppthrd_CreateThread( playdata, playbackThreadProc, debugThread);
    playdata->bitSeqData = membuf_init();
    cppthrd_StartThread(playdata->playbackThread, BOOL_FALSE);

    return playdata;
}

void soundplay_deinit(SoundPlaybackData data)
{
    PlaybackData * playdata = (PlaybackData *)data;
     
    playdata->shouldQuit = BOOL_TRUE;

    if(playdata->playbackThread != NULL)
    {
        if(playdata->debugPlayback)
            printf("\t wait and destroy sound playback thread\r\n");
        

        cppthrd_SignalThreadEvent(playdata->playbackThread, BOOL_TRUE);
        cppthrd_WaitAndDestroyThread(playdata->playbackThread);
        playdata->playbackThread=NULL;

        if(playdata->debugPlayback)
            printf("\t destroyed sound playback thread\r\n");
    }

    if(playdata->debugPlayback)
        printf("Free alsa \r\n");

    if(playdata->pcmHandle != NULL)
    {
        if(playdata->debugPlayback)
            printf("\t close pcm handle\r\n");
        
        snd_pcm_close(playdata->pcmHandle);
        playdata->pcmHandle=NULL;
        
        if(playdata->debugPlayback)
            printf("\t close pcm handle done\r\n");
    }

    if(playdata->hwParams != NULL)
    {
        if(playdata->debugPlayback)
            printf("\t free hw params\r\n");
        
        snd_pcm_hw_params_free(playdata->hwParams);
        playdata->hwParams=NULL;
        
        if(playdata->debugPlayback)
            printf("\t free hw params done\r\n");
    }

    if(playdata->samplePlayData != NULL)
    {
        membuf_free(playdata->samplePlayData);
        playdata->samplePlayData=NULL;
    }

    if(playdata->sampleByteData != NULL)
    {
        membuf_free(playdata->sampleByteData);
        playdata->sampleByteData=NULL;
    }

    if(playdata->bitSeqData != NULL)
    {
        membuf_free(playdata->bitSeqData);
        playdata->bitSeqData=NULL;
    }

    free(playdata);
}

void soundplay_play(SoundPlaybackData data, const double * samples, size_t numSamples, MemoryBuffer * gpioBitSequence)
{
    PlaybackData * sndplaydata = (PlaybackData *)data;

    //Lock buffer
    cppthrd_LockThread(sndplaydata->playbackThread);

    //Allocate / Reallocate buffer
    membuf_append(sndplaydata->samplePlayData, samples, numSamples * sizeof(double));

    //Allocate / Reallocate bit sequence data buffer
    if(gpioBitSequence != NULL)
        membuf_append_copy(sndplaydata->bitSeqData, gpioBitSequence);

    //Signal playback thread
    cppthrd_SignalThreadEvent(sndplaydata->playbackThread, BOOL_TRUE);
    cppthrd_UnlockThread(sndplaydata->playbackThread);
}

void soundplay_wait(SoundPlaybackData playdata)
{
    PlaybackData * sndplaydata = (PlaybackData * )playdata;

    //Lock
    cppthrd_LockThread(sndplaydata->playbackThread);

    //Loop until the playback thread is back to waiting
    while(!sndplaydata->isWaiting)
    {
        if(sndplaydata->debugPlayback)
            printf("\tALSA waiting for playback thread to finish\r\n");

        //Wait for a bit
        cppthrd_UnlockThread(sndplaydata->playbackThread);
        cppThread_sleep_ms(500);
        cppthrd_LockThread(sndplaydata->playbackThread);
    }

    //Wait for wavdest to finish as well
    if(sndplaydata->debugdesthnd != NULL)
    {
        if(sndplaydata->debugPlayback)
            printf("\tALSA wait for debug wav writer thread to finish\r\n");
        
        wavdest_wait(sndplaydata->debugdesthnd);
    }

    if(sndplaydata->debugPlayback)
        printf("\tALSA wait done\r\n");

    cppthrd_UnlockThread(sndplaydata->playbackThread);
}

#endif