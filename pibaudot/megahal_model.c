#include <string.h>

#include "megahal_lib.h"
#include "megahal_internal.h"


#define error mh_error
#define warn mh_warn
#define putlog mh_putlog

#define COOKIE "MegaHAL83"


/*---------------------------------------------------------------------------*/

BYTE2 ** mh_realloc_phrase(MODEL *model)
{
	Context;
	if(model->phrasecount == 0 && model->phrase != NULL) {
		nfree(model->phrase);
		model->phrase = NULL;
	} else if(model->phrase == NULL) {
		model->phrase = (BYTE2 **)nmalloc(sizeof(BYTE2 *)*(model->phrasecount));
	} else {
		model->phrase = (BYTE2 **)nrealloc((BYTE2 **)(model->phrase),sizeof(BYTE2 *)*(model->phrasecount));
	}

	if(model->phrase == NULL) {
		return NULL;
	}

	return model->phrase;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	New_Model
 *
 *	Purpose:	Create and initialise a new ngram model.
 */
MODEL * mh_new_model(int order)
{
	MODEL *model = NULL;

	Context;
	model = (MODEL *)nmalloc(sizeof(MODEL));
	if(model == NULL) {
		error("new_model", "Unable to allocate model.");
		goto fail;
	}

	model->order = order;
	model->forward = mh_new_node();
	model->backward = mh_new_node();
	model->halcontext = (TREE **)nmalloc(sizeof(TREE *)*(order+2));
	if(model->halcontext == NULL) {
		error("new_model", "Unable to allocate context array.");
		goto fail;
	}
	mh_initialize_context(model);
	model->phrasecount = 0;
	model->phrase = NULL;
	model->dictionary = mh_new_dictionary();
	mh_initialize_dictionary(model->dictionary);

	return model;

fail:
	return NULL;
}

void mh_free_model(MODEL *model)
{
	unsigned long i;

	Context;
	if(model == NULL)
		return;
	
    if(model->forward != NULL) {
		mh_free_tree(model->forward);
	}
	
    if(model->backward != NULL) {
		mh_free_tree(model->backward);
	}
	
    if(model->halcontext != NULL) {
		nfree(model->halcontext);
	}

	for (i=0; i<model->phrasecount; i++)
		nfree(model->phrase[i]);

	if(model->phrase != NULL)
		nfree(model->phrase);

	if(model->dictionary != NULL) {
		mh_free_words(model->dictionary);
		mh_free_dictionary(model->dictionary);
		nfree(model->dictionary);
	}
	nfree(model);
}



/*---------------------------------------------------------------------------*/

/*
 *	Function:	Load_Model
 *
 *	Purpose:	Load a model into memory.
 */
bool mh_load_model( char *filename, MODEL *model)
{
	unsigned long i,j;
	BYTE2 size;
	FILE *file;
    CHARTYPE cookie[16];

	Context;
	if(filename == NULL)
		return FALSE;

	file = fopen(filename, "rb");
	if(file == NULL) {
		warn("load_model", "Unable to open file `%s'", filename);
		return FALSE;
	}

    if (!fread(cookie, sizeof(CHARTYPE), STRLEN(_T(COOKIE)), file) ||
	    STRNCMP(cookie, _T(COOKIE), STRLEN(_T(COOKIE))) != 0 ||
	    !fread(&(model->order), sizeof(BYTE1), 1, file)
	   ) {
		warn("load_model", "File `%s' is not a MegaHAL brain", filename);
		goto fail;
	}
    

	//order = model->order;
	mh_load_tree(file, model->forward);
	mh_load_tree(file, model->backward);
	mh_load_dictionary(file, model->dictionary);

	if ( !fread(&(model->phrasecount), sizeof(BYTE4), 1, file) ||
	    mh_realloc_phrase(model) == NULL ) {
		error("load_model", "Unable to reallocate phrase");
		return FALSE;
	}
	for(i=0; i<model->phrasecount; ++i) {
		if ( fread(&size, sizeof(BYTE2), 1, file) ) {
			model->phrase[i]=(BYTE2 *)nmalloc(sizeof(BYTE2)*(size+2));
			if (model->phrase[i] == NULL) {
				error("learn", "Unable to allocate phrase %u", i);
				return FALSE;
			}
		}
		model->phrase[i][0] = size;
		for(j=0; j<size; ++j) {
			if (!fread(&(model->phrase[i][j+1]), sizeof(BYTE2), 1, file)) {
                error("learn", "Unable to read phrase %u", i);
				break;
			}
		}
		model->phrase[i][size+1] = 1; // terminator
	}

	return TRUE;
fail:
	fclose(file);

	return FALSE;
}

void mh_save_model(char *filename, MODEL *model)
{
    FILE *file;
    unsigned long i;
    int j;

    file = fopen(filename, "wb");
	if(file == NULL) {
		warn("save_model", "Unable to open file `%s'", filename);
		return;
	}

	fwrite(_T(COOKIE), sizeof(CHARTYPE), STRLEN(_T(COOKIE)), file);
	fwrite(&(model->order), sizeof(BYTE1), 1, file);
	mh_save_tree(file, model->forward);
	mh_save_tree(file, model->backward);
	mh_save_dictionary(file, model->dictionary);
	fwrite(&(model->phrasecount), sizeof(BYTE4), 1, file);

	for(i=0; i<model->phrasecount; ++i)
		for(j=0; j<model->phrase[i][0]+1; ++j)
			fwrite(&(model->phrase[i][j]), sizeof(BYTE2), 1, file);
	fclose(file);
}


/*
 *	Function:	Update_Model
 *
 *	Purpose:	Update the model with the specified symbol.
 */
void mh_update_model(MODEL *model, int symbol)
{
	register int i;

	Context;
	/*
	 *	Update all of the models in the current context with the specified
	 *	symbol.
	 */

	// add the symbol to all contexts - some may be at different levels deep and there may be max [order] different branches being built at once
	// Question: Why does it go up to order+1? Nothing ever uses that halcontext as far as I can see.Maybe its only here so that the i-1 can be 'add_symbol'ed within the same loop and the programmer was lazy ;)
	for(i=(model->order+1); i>0; --i)
		if(model->halcontext[i-1] != NULL)
			model->halcontext[i] = mh_add_symbol(model->halcontext[i-1], (BYTE2)symbol);

	return;
}



