#include "megahal_lib.h"
#include "megahal_internal.h"

#define error mh_error




/*---------------------------------------------------------------------------*/

/*
 *	Function:	New_Node
 *
 *	Purpose:	Allocate a new node for the n-gram tree, and initialise
 *			its contents to sensible values.
 */
TREE * mh_new_node(void)
{
	TREE *node = NULL;

	Context;
	/*
	 *	Allocate memory for the new node
	 */
	node = (TREE *)nmalloc(sizeof(TREE));
	if(node == NULL) {
		error("new_node", "Unable to allocate the node.");
		goto fail;
	}

	/*
	 *	Initialise the contents of the node
	 */
	node->symbol = 0;
	node->usage = 0;
	node->count = 0;
	node->branch = 0;
	node->tree = NULL;

	return node;

fail:
	if(node != NULL)
		nfree(node);
	return NULL;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Add_Node
 *
 *	Purpose:	Attach a new child node to the sub-tree of the tree
 *			specified.
 */
void mh_add_node(TREE *tree, TREE *node, int position)
{
	register int i;

	Context;
	/*
	 *	Allocate room for one more child node, which may mean allocating
	 *	the sub-tree from scratch.
	 */
	tree->branch += 1;
	if(mh_realloc_tree(tree) == NULL) {
		error("add_node", "Unable to reallocate subtree.");
		return;
	}

	/*
	 *	Shuffle the nodes down so that we can insert the new node at the
	 *	subtree index given by position.
	 */
	for(i=tree->branch-1; i>position; --i)
		tree->tree[i] = tree->tree[i-1];

	/*
	 *	Add the new node to the sub-tree.
	 */
	tree->tree[position]=node;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Search_Node
 *
 *	Purpose:	Perform a binary search for the specified symbol on the
 *			subtree of the given node.  Return the position of the
 *			child node in the subtree if the symbol was found, or the
 *			position where it should be inserted to keep the subtree
 *			sorted if it wasn't.
 */
int mh_search_node(TREE *node, int symbol, bool *found_symbol)
{
	register int position;
	int min;
	int max;
	int middle;
	int compar;

	Context;
	/*
	 *	Handle the special case where the subtree is empty.
	 */
	if(node->branch == 0) {
		position = 0;
		goto notfound;
	}

	/*
	 *	Perform a binary search on the subtree.
	 */
	min = 0;
	max = node->branch-1;
	while(TRUE) {
		middle = (min+max)/2;
		compar = symbol-node->tree[middle]->symbol;
		if(compar == 0) {
			position = middle;
			goto found;
		} else if(compar > 0) {
			if(max == middle) {
				position = middle+1;
				goto notfound;
			}
			min = middle+1;
		} else {
			if(min == middle) {
				position = middle;
				goto notfound;
			}
			max = middle-1;
		}
	}

found:
	*found_symbol = TRUE;
	return position;

notfound:
Context;
	*found_symbol = FALSE;
	return position;
}