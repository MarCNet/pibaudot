#include <string.h>
#include <wchar.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


#include "megahal_lib.h"
#include "megahal_internal.h"

#ifdef WIN32

static void srand48(time_t t)
{
    srand((unsigned int)t);
}

static double drand48()
{
    double x = rand();
    x /= RAND_MAX;
    return x;
}

#endif

#define snprintf _snprintf
#define error mh_error
#define putlog mh_putlog

// find pointer to sub inside s
const CHARTYPE * mh_mystrstr(const CHARTYPE *s, const CHARTYPE *sub)
{
	Context;
	if (!*sub)
		return s;
	for(; *s; ++s) {
		if(*s == *sub) {
			/*
			*	Matched starting char -- loop through remaining chars.
			*/
			const CHARTYPE *h, *n;
			for(h = s, n = sub; *h && *n; ++h, ++n) {
				if (*h != *n)
					break;
			}
			if(!*n) /* matched all of 'sub' to null termination */
				return s; /* return the start of the match */
		}
	}
	return NULL;
}

// Next, a hacked attempt at strdup(), since I can't use malloc() anywhere.
CHARTYPE * mh_mystrdup(const CHARTYPE *s)
{
	CHARTYPE *mytmp = (CHARTYPE *)nmalloc((STRLEN(s)+1)*sizeof(CHARTYPE));

	Context;
	if(mytmp == NULL)
		return NULL;
    else STRCPY(mytmp, s);

	return mytmp;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Capitalize
 *
 *	Purpose:	Convert a string to look nice.
 */
void mh_capitalize(CHARTYPE *string)
{
	size_t i;
	bool start = TRUE;
    size_t sz = STRLEN(string);

	Context;
	for(i=0; i< sz; ++i) {
		if(ISALPHA(string[i])) {
			if(start == TRUE)
                string[i] = (CHARTYPE)TOUPPER(string[i]);
			else
				string[i] = (CHARTYPE)TOLOWER(string[i]);
			start = FALSE;
		}
		if((i>2)&&(STRCHR(_T("!.?"), string[i-1])!=NULL)&&(ISSPACE(string[i])))
			start = TRUE;
	}
}

#ifdef USE_WCHAR
wchar_t * mh_locale_to_wchar(char *str)
{
	wchar_t *ptr;
	size_t s;

	Context;
	/* first arg == NULL means 'calculate needed space' */
	s = mbstowcs(NULL, str, 0);

	/* a size of -1 is triggered by an error in encoding; never
	   happen in ISO-8859-* locales, but possible in UTF-8 */
	if(s == -1)
		return NULL;

	/* malloc the necessary space */
	if((ptr = (wchar_t *)nmalloc((s+1)*sizeof(wchar_t))) == NULL)
		return NULL;

	/* really do it */
	mbstowcs(ptr, str, s);

	/* ensure NULL-termination */
	ptr[s] = L'\0';

	/* remember to free() ptr when done */
	return ptr;
}


char * mh_wchar_to_locale(wchar_t *str)
{
	char *ptr;
	size_t s;

	Context;
	/* first arg == NULL means 'calculate needed space' */
	s = wcstombs(NULL, str, 0);

	/* a size of -1 means there are characters that could not
	   be converted to current locale */
	if(s == -1)
		return NULL;

	/* malloc the necessary space */
	if((ptr = (char *)nmalloc(s+1)) == NULL)
		return NULL;

	/* really do it */
	wcstombs(ptr, str, s);

	/* ensure NULL-termination */
	ptr[s] = '\0';

	/* remember to free() ptr when done */
	return ptr;
}
#endif
/*---------------------------------------------------------------------------*/

/*
 *	Function:	Upper
 *
 *	Purpose:	Convert a string to its uppercase representation.
 */
void mh_upper(CHARTYPE *string)
{
	size_t i;
    size_t sz = STRLEN(string)


	Context;
	for(i=0; i < sz; ++i)
		string[i] = (CHARTYPE)TOUPPER(string[i]);
}


static void strip_codes(CHARTYPE *text)
{
    CHARTYPE *dd = text;

	Context;
	while (*text) {
		switch (*text) {
		case 2:						/* Bold text */
			text++;
			continue;
		case 3:						/* mIRC colors? */
			if (ISDIGIT(text[1])) {		/* Is the first wchar_t a number? */
				text += 2;			/* Skip over the ^C and the first digit */
				if (ISDIGIT(*text))
					text++;			/* Is this a double digit number? */
				if (*text == L',') {		/* Do we have a background color next? */
					if (ISDIGIT(text[1]))
						text += 2;	/* Skip over the first background digit */
					if (ISDIGIT(*text))
						text++;		/* Is it a double digit? */
				}
			} else
				text++;
			continue;
		case 7:
			text++;
			continue;
		case 0x16:					/* Reverse video */
			text++;
			continue;
		case 0x1f:					/* Underlined text */
			text++;
			continue;
		case 033:
			text++;
			if (*text == L'[') {
				text++;
				while ((*text == L';') || ISDIGIT(*text))
					text++;
				if (*text)
					text++;			/* also kill the following char */
			}
			continue;
		}
		*dd++ = *text++;				/* Move on to the next char */
	}
	*dd = 0;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Boundary
 *
 *	Purpose:	Return whether or not a word boundary exists in a string
 *			at the specified location.
 */
static bool boundary(CHARTYPE *string, int position)
{

	Context;
	if(position == 0)
		return FALSE;

    if(position == (int)STRLEN(string))
		return TRUE;

	if(
		((string[position] == L'\'')) &&
        (ISALNUM(string[position-1]) != 0) &&
		(ISALNUM(string[position+1]) != 0)
	)
		return FALSE;

	if(
		(position > 1) &&
		((string[position-1] == L'\'')) &&
		(ISALNUM(string[position-2]) !=0) &&
		(ISALNUM(string[position]) != 0)
	)
		return FALSE;

	if(
		((string[position] == L'-')) &&
		(ISALNUM(string[position-1]) != 0) &&
		(ISALNUM(string[position+1]) != 0)
	)
		return FALSE;

	if(
		(position > 1) &&
		((string[position-1] == L'-')) &&
		(ISALNUM(string[position-2]) != 0) &&
		(ISALNUM(string[position]) != 0)
	)
		return FALSE;

	if(
		(ISALNUM(string[position]) != 0) &&
		(ISALNUM(string[position-1]) == 0)
	)
		return TRUE;

	if(
		(ISALNUM(string[position]) == 0) &&
		(ISALNUM(string[position-1]) != 0)
	)
		return TRUE;

/*	if(isdigit(string[position])!=isdigit(string[position-1]))
		return(TRUE);
*/
	return FALSE;
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Make_Words
 *
 *	Purpose:	Break a string into an array of words.
 */
void mh_make_words(CHARTYPE *pinput, DICTIONARY *words)
{
	size_t offset = 0, tmp = 0;
	size_t i;
	CHARTYPE iinput[512] = _T("");
	CHARTYPE *input = iinput;
    size_t inputlen;

	Context;
	/*
	 *	Clear the entries in the dictionary
	 */
	mh_free_words(words);
	mh_free_dictionary(words);
    STRCPY(iinput,pinput);
	strip_codes(input);
	/*
	 *	If the string is empty then do nothing, for it contains no words.
	 */
	inputlen = STRLEN(input);
    if(inputlen == 0)
		return;
	/*
	 *	Loop forever.
	 */
    
	while(1) {

		/*
		 *	If the current character is of the same type as the previous
		 *	character, then include it in the word.  Otherwise, terminate
		 *	the current word.
		 */
		while(input[0] == L' ')
			input++;
		if(!input[0])
			break;
		if((boundary(input, offset) || (input[offset] == L' '))) {

			/*
			 *	Add the word to the dictionary
			 */
			words->size += 1;
			if(mh_realloc_dictionary(words) == NULL) {
				error("make_words", "Unable to reallocate dictionary");
				return;
			}
			if(((input-1)[0] != L' ') && (words->size>1))
				tmp = 1;
			words->entry[words->size-1].length = offset+tmp;
            words->entry[words->size-1].word = (CHARTYPE *)nmalloc(sizeof(CHARTYPE)*(offset+tmp));
			if(tmp)
				words->entry[words->size-1].word[0]=(CHARTYPE)31;
			
            for(i=0; i< offset; i++)
				words->entry[words->size-1].word[i+tmp]=input[i];

			if(offset == inputlen)
				break;

			input += offset;
			offset = tmp = 0;
		} else {
			++offset;
		}
	}
	/*
	 *	If the last word isn't punctuation, then replace it with a
	 *	full-stop character.
	 */
	if (words->size == 0) {
		return;
	}

	if(ISALNUM(words->entry[words->size-1].word[0]) || (words->entry[words->size-1].word[0]==(CHARTYPE)31 && ISALNUM(words->entry[words->size-1].word[1])) ) {
		words->size += 1;
		if(mh_realloc_dictionary(words) == NULL) {
			error("make_words", "Unable to reallocate dictionary");
			return;
		}

        words->entry[words->size-1].word = (CHARTYPE *)nmalloc(sizeof(CHARTYPE)*(2));
		words->entry[words->size-1].length = 2;
		words->entry[words->size-1].word[0] = (CHARTYPE)31;
		words->entry[words->size-1].word[1] = L'.';
	} else if(wcschr(L"!.?", words->entry[words->size-1].word[words->entry[words->size-1].length-1]) == NULL) {
		words->entry[words->size-1].word = (CHARTYPE *)nrealloc(words->entry[words->size-1].word, sizeof(CHARTYPE)*(2));
		words->entry[words->size-1].length = 2;
		words->entry[words->size-1].word[0] = (CHARTYPE)31;
		words->entry[words->size-1].word[1] = L'.';
	}
	return;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Error
 *
 *	Purpose:	Print the specified message to the error file.
 */
void mh_error(char *title, char *fmt, ...)
{
	va_list argp;
	
	Context;
	printf( "MEGAHAL ERROR: %s: ", title);
	va_start(argp, fmt);
	vprintf( fmt, argp);
	va_end(argp);
	printf(".\n");

	/* FIXME - I think I need to die here */
}

void mh_warn(char *title, char *fmt, ...)
{
	va_list argp;
	
	Context;
	printf( "MEGAHAL WARNING: %s: ", title);
	va_start(argp, fmt);
	vprintf( fmt, argp);
	va_end(argp);
	printf(".\n");

	/* FIXME - I think I need to die here */
}

void mh_putlog(int lvl, char *title, char *fmt, ...)
{
	va_list argp;
	
	Context;
	printf( "%s: ", title);
	va_start(argp, fmt);
	vprintf( fmt, argp);
	va_end(argp);
	printf(".\n");

	/* FIXME - I think I need to die here */
}



/*---------------------------------------------------------------------------*/

/*
 *	Function:	Wordcmp
 *
 *	Purpose:	Compare two words, and return an integer indicating whether
 *			the first word is less than, equal to or greater than the
 *			second word.
 */
int mh_wordcmp(STRING word1, STRING word2)
{
	register int i;
	int bound;

	bound = MIN(word1.length,word2.length);

	for(i=0; i<bound; ++i)
		if(TOUPPER(word1.word[i])!=TOUPPER(word2.word[i]))
			return (int)(TOUPPER(word1.word[i])-TOUPPER(word2.word[i]));

	if(word1.length<word2.length)
		return -1;
	if(word1.length>word2.length)
		return 1;

	return 0;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Rnd
 *
 *	Purpose:	Return a random integer between 0 and range-1.
 */
int mh_rnd(int range)
{
	static bool flag=FALSE;

	Context;
	if(flag==FALSE) {

        srand48(time(NULL));

	}
	flag = TRUE;
	return (int) (floor(drand48()*(double)(range - 1)));
}

