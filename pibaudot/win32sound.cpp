/**
* Direct sound - Windows only - implementation of soundrec API
*/

#include <windows.h>
#include <stdio.h>
#include <dsound.h>

#define _USE_MATH_DEFINES
#include <math.h>


#define NOTIFICATIONS 2

extern "C"
{
    #include "internal.h"
}

#include <vector>

#define MAXDEVCAPNAMELEN	MAX_PATH

typedef struct _CAPDEVICE
{
    GUID guid;
    WCHAR awcDescription[MAXDEVCAPNAMELEN + 1];
    WCHAR awcModule[MAXDEVCAPNAMELEN+ 1];

}CAPDEVICE, *LPCAPDEVICE;

typedef struct _WIN32SoundRecData
{
    LPDIRECTSOUNDCAPTURE dscap;
    LPDIRECTSOUNDCAPTUREBUFFER dscapbuff;
    LPDIRECTSOUNDNOTIFY dscapnotify;
    HANDLE hNotificationEvent;
    size_t totalBufSize;
    unsigned int bufferSizeMs;
    bool debugCapture;

    DSCBUFFERDESC bufdesc;

    ThreadHandle threadhandle;
    bool shouldQuit;
    HaveRecordSoundCallback callback;
    void * callbackparam;

} WIN32SoundRecData;

typedef struct _WIN32SoundPlaybackData
{
    LPDIRECTSOUND   dsplay;
    LPDIRECTSOUNDBUFFER		  dsplaybuff;
    LPDIRECTSOUNDNOTIFY        dsplaynotify;
    WAVDESTHND debugdesthnd;
    DSBUFFERDESC bufdesc;
    size_t totalBufSize;
    unsigned int bufferSizeMs;
    const char * playbackWriteAudioFilename;
    unsigned int deviceId;
    HANDLE hBufferNotificationEvent;
    HANDLE hThreadNotifyEvent;
    GPIOHandle gpiohandle;
    unsigned int wakeLengthBytes; //We don't generate the wake tone. We just need to know how long it is so we can correctly set the GPIO bits
    unsigned int dataBitLengthBytes;
    unsigned int gapLengthBytes;
    
    ThreadHandle playbackThread;
    MemoryBuffer * samplePlayData;
    MemoryBuffer * bitSeqData;

    bool debugPlay;
    bool shouldQuit;
    bool isPlaying;
    bool isWaiting;

}WIN32SoundPlaybackData;

static VOID GetWaveFormatFromIndex( INT nIndex, WAVEFORMATEX* pwfx )
{
    INT iSampleRate = nIndex / 4;
    INT iType = nIndex % 4;

    switch( iSampleRate )
    {
        case 0: pwfx->nSamplesPerSec =  8000; break;
        case 1: pwfx->nSamplesPerSec = 11025; break;
        case 2: pwfx->nSamplesPerSec = 22050; break;
        case 3: pwfx->nSamplesPerSec = 44100; break;
    }

    switch( iType )
    {
        case 0: pwfx->wBitsPerSample =  8; pwfx->nChannels = 2; break;
        case 1: pwfx->wBitsPerSample = 16; pwfx->nChannels = 2; break;
        case 2: pwfx->wBitsPerSample =  8; pwfx->nChannels = 2; break;
        case 3: pwfx->wBitsPerSample = 16; pwfx->nChannels = 2; break;
    }

    pwfx->nBlockAlign = pwfx->nChannels * ( pwfx->wBitsPerSample / 8 );
    pwfx->nAvgBytesPerSec = pwfx->nBlockAlign * pwfx->nSamplesPerSec;
}

enum PlaybackState
{
    PlaybackState_WaitForData,      //Wait for more sample data to convert and play
    PlaybackState_StartPlaying,     //Start playing (and move to PlaybackState_CopyStart)
    PlaybackState_PrepareData,      //Convert the sample data (doubles) into byte values for the hardware
    PlaybackState_CopyStart,
    PlaybackState_CopyData,         //Main state for copying from source buffer, converting and writing to the playback buffer
    PlaybackState_CopyData_UpdateState, //Check the current write position of the playback buffer, and adjust the amount left to playback during a copydata state
    PlaybackState_ClearData,        //After copying everything, clear the playback buffer to prevent looping sounds
    PlaybackState_ClearData_UpdateState, //Check the current write position of the playback buffer, and adjust the amount left to playback during a cleardata state
    PlaybackState_StopPlaying,      //Nothing else to do - so stop playing
    
    PlaybackState_Error
};

static void updateGPIO(const  WIN32SoundPlaybackData * playdata, const size_t totalBufferSizeBytes, const size_t sampleByteBufferContentSize, DWORD * dwPrevPlayPos, DWORD * dwPlayTotal,
                       const std::vector<enum BOOLEAN_VALUE> & bitseq)
{
     //Set the gpio state, based on the current play position
    DWORD dwPlayPos = 0;
    HRESULT hr = playdata->dsplaybuff->GetCurrentPosition(&dwPlayPos, NULL);
    if(SUCCEEDED(hr))
    {
        if(*dwPrevPlayPos <= dwPlayPos)
        {
            *dwPlayTotal += (dwPlayPos - *dwPrevPlayPos);
        }
        else
        {
            *dwPlayTotal += (totalBufferSizeBytes - *dwPrevPlayPos);
            *dwPlayTotal += dwPlayPos;
        }

        if(*dwPlayTotal < playdata->wakeLengthBytes)
        {
            gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Wait);
        }
        else if(*dwPlayTotal < sampleByteBufferContentSize)
        {
            const DWORD dwPlayPosNoWake = ((*dwPlayTotal) - playdata->wakeLengthBytes);
            const DWORD dwPlayPosForCurBit = dwPlayPosNoWake % (playdata->gapLengthBytes + playdata->dataBitLengthBytes);
            if(dwPlayPosForCurBit < playdata->dataBitLengthBytes)
            {
                const DWORD dwBitNum = dwPlayPosNoWake / (playdata->gapLengthBytes + playdata->dataBitLengthBytes);
                if(dwBitNum >= bitseq.size())
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data);
                else if(bitseq[dwBitNum] == BOOL_TRUE)
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data_High);
                else 
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Data_Low);
            }
            else
            {
                gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_Tx_Sending_Gap);
            }
        }
        else
        {
            gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
        }

        *dwPrevPlayPos = dwPlayPos;
    }
}

static void BD_CALLBACK playbackThreadProc(void * param)
{
    static const double twopi = M_PI * 2;
    static const DWORD dwMaxOpWaitTime = 10;

    WIN32SoundPlaybackData * playdata = (WIN32SoundPlaybackData * )param;
    HANDLE ahEvents[3];
    const size_t totalBufferSizeBytes = playdata->totalBufSize;    
    enum PlaybackState currentState = PlaybackState_WaitForData;
    DWORD dwWaitRes;
    HRESULT hr;
    DWORD dwCurPlaybackBufferWritePos, dwPrevPlaybackBufferWritePos;
    DWORD dwDataRemainToPlayInPlaybackBuffer = 0; //How much data we've put into the buffer that has yet to play
    DWORD dwNextPlaybackBufferWritePos = 0; //The next position to lock from
    DWORD dwAmountToClear = 0;
    ULONGLONG lastOpStartTick, curtick;
    DWORD dwPlayPos, dwPrevPlayPos,dwPlayTotal;
    std::vector<unsigned char> sampleByteBuffer;
    std::vector<enum BOOLEAN_VALUE> bitSeqBuffer;
    size_t sampleByteBufferContentSize = 0;
    size_t sampleByteBufferContentPos = 0;
    const bool debugPlayback = playdata->debugPlay;
    
    ahEvents[0] = playdata->hThreadNotifyEvent;
    ahEvents[1] = playdata->hBufferNotificationEvent;
    

    //main playback loop
    while(!playdata->shouldQuit)
    {   
        switch(currentState)
        {
            case PlaybackState_WaitForData:
                if(debugPlayback)
                    printf("DS Wait\r\n");

                //Signal not playing
                gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);

                playdata->isWaiting = true;
                dwWaitRes = WaitForMultipleObjects(2, ahEvents, FALSE, INFINITE);
                if(!playdata->shouldQuit)
                {   
                    switch(dwWaitRes)
                    {
                        case WAIT_OBJECT_0:     //Buffer notification

                            //Lock sample buffer
                            cppthrd_LockThread(playdata->playbackThread);

                            if(debugPlayback)
                                printf("DS Buffer Notification %u\r\n", membuf_getContentSize(playdata->samplePlayData));

                            if(membuf_getContentSize(playdata->samplePlayData) > 0)
                            {
                                currentState = PlaybackState_PrepareData;

                                //Set no longer waiting
                                playdata->isWaiting = false;
                            }

                            //unlock the sample buffer
                            cppthrd_UnlockThread(playdata->playbackThread);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case PlaybackState_PrepareData:
                {
                    //Query play format
                    DWORD dwWritten = 0;
                    WAVEFORMATEX wavfmtex;

                    memset(&wavfmtex,0,sizeof(wavfmtex));
                    wavfmtex.cbSize = sizeof(wavfmtex);
                    hr = playdata->dsplaybuff->GetFormat(&wavfmtex, sizeof(wavfmtex), &dwWritten);
                    if(FAILED(hr))
                    {
                        printf("DS PLAY ERROR : Failed to query sound format 0x%08x\r\n",hr);
                        currentState = PlaybackState_Error;
                    }
                    else
                    {
                        //Workout how big a sample value is in bytes
                        const size_t sampleValueSizeBytes =  (((size_t)wavfmtex.nChannels) * ((size_t)wavfmtex.wBitsPerSample)) / 8;

                        if(debugPlayback)
                            printf("DS Prepare - Samplerate=%u  Channels=%u  BitsPerSample=%u\r\n",wavfmtex.nSamplesPerSec, wavfmtex.nChannels, wavfmtex.wBitsPerSample);

                        //Lock sample buffer
                        cppthrd_LockThread(playdata->playbackThread);

                        //How many samples are there?
                        const size_t numSamplesToWrite =  membuf_getContentSize( playdata->samplePlayData ) / sizeof(double);
                        const size_t numSampDataBytesToWrite = (numSamplesToWrite  *sampleValueSizeBytes);
                        if(numSamplesToWrite == 0)
                        {
                            sampleByteBufferContentSize = 0;
                            sampleByteBufferContentPos = 0;
                            currentState = PlaybackState_WaitForData;
                            bitSeqBuffer.clear();
                        }
                        else
                        {
                            if((playdata->debugdesthnd == NULL) && (playdata->playbackWriteAudioFilename != NULL) && (playdata->playbackWriteAudioFilename[0] != 0))
                            {
                                char acfullname[MAX_PATH];
                                _snprintf_s(acfullname, MAX_PATH, "%s.play%04u.wav", playdata->playbackWriteAudioFilename, playdata->deviceId);
                                playdata->debugdesthnd = wavdest_init(acfullname, wavfmtex.nSamplesPerSec, 1.0f, BOOL_FALSE);
                            }

                            if(debugPlayback)
                                printf("DS Prepare - Num samples=%u  Num bytes=%u\r\n",numSamplesToWrite, numSampDataBytesToWrite);

                            //Make sure our storage buffer has enough space
                            if(sampleByteBuffer.size() < numSampDataBytesToWrite)
                                sampleByteBuffer.resize(numSampDataBytesToWrite);

                            //Convert sample data from double format, to the appropriate byte format
                            const size_t numSamplesWritten = smputil_convertDoublesToSampleBytes( (const double *)membuf_ptr(playdata->samplePlayData, 0 ), numSamplesToWrite,
                                                                                                  wavfmtex.nChannels, wavfmtex.wBitsPerSample, 1.0f, 
                                                                                                  &sampleByteBuffer[0], numSampDataBytesToWrite);
                            if(playdata->debugdesthnd != NULL)
                                wavdest_write(playdata->debugdesthnd,  (const double *)membuf_ptr(playdata->samplePlayData, 0), numSamplesWritten);
                        
                            //Set up copy position and amount to copy into the play buffer
                            sampleByteBufferContentSize = (numSamplesWritten  * sampleValueSizeBytes);
                            sampleByteBufferContentPos = 0;

                            //Copy bit sequence as well
                            //This is used to set the 'data value' bit on the gpio
                            if(playdata->bitSeqData != NULL)
                            {
                                const size_t bitseqSize = membuf_getContentSize(playdata->bitSeqData);
                                if(bitseqSize > 0)
                                {
                                    bitSeqBuffer.resize(bitseqSize / sizeof(BOOLEAN_VALUE));
                                    memcpy(&bitSeqBuffer[0], membuf_ptr(playdata->bitSeqData,0), bitseqSize);
                                    membuf_erase(playdata->bitSeqData);
                                }
                            }

                            //Remove the data we've converted from
                            membuf_removeFromFront( playdata->samplePlayData, numSamplesWritten * sizeof(double));
                            
                            //Set next state to set up for copying the converted data to the playback buffer 
                            currentState = PlaybackState_CopyStart;
                        }

                        //Finally, unlock the sample buffer
                        cppthrd_UnlockThread(playdata->playbackThread);
                    }
                }
                break;

            case PlaybackState_StartPlaying:
                hr = playdata->dsplaybuff->Play(0,0, DSBPLAY_LOOPING);
                if(FAILED(hr))
                {
                    currentState = PlaybackState_Error;
                }
                else
                {
                    playdata->isPlaying = true;
                    currentState = PlaybackState_CopyData_UpdateState;
                    
                    if(playdata->debugPlay)
                        printf("DS PLAY\r\n");
                }
                break;
            case PlaybackState_StopPlaying:
                hr = playdata->dsplaybuff->Stop();
                if(FAILED(hr))
                {
                    currentState = PlaybackState_Error;
                }
                else
                {
                    if(playdata->debugPlay)
                        printf("DS STOP\r\n");
                    
                    playdata->isPlaying = false;
                    currentState = PlaybackState_WaitForData;

                    //Signal not playing
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
                }
                break;
            case PlaybackState_CopyStart:
                
                //Get the current write position, so we know where to start writing to
                //Remember, we're only in this state because we're only starting to play - and/or the buffer has been wiped from last time
                dwCurPlaybackBufferWritePos = 0;
                dwPrevPlayPos = 0;
                dwPlayPos = 0;
                dwPlayTotal=0;
                hr = playdata->dsplaybuff->GetCurrentPosition(&dwPlayPos, &dwCurPlaybackBufferWritePos);
                if(FAILED(hr))
                {
                    currentState = PlaybackState_Error;
                }
                else
                {
                    if(debugPlayback)
                        printf("DS Start - PlayPos=%u  PlayWrite=%u\r\n", dwPlayPos, dwCurPlaybackBufferWritePos);

                    //Set the previous write position to the current, so the next time around we know how much
                    //buffer has been consumed
                    dwPrevPlaybackBufferWritePos = dwCurPlaybackBufferWritePos;

                    //Same for the play position
                    dwPrevPlayPos = dwPlayPos;

                    //Set where to start copying data to the playback buffer
                    dwDataRemainToPlayInPlaybackBuffer = 0;
                    dwNextPlaybackBufferWritePos = dwCurPlaybackBufferWritePos;
                    
                    //Start copying data
                    currentState = PlaybackState_CopyData;

                    //Signal not yet playing
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
                }
                break;
            case PlaybackState_CopyData:
                {
                    DWORD adwSizes[2] = {0,0};
                    LPBYTE alpbtPtrs[2] = {NULL,NULL};
                    
                    //We want to measure the amount of time this takes, so start counting....
                    lastOpStartTick = GetTickCount64();

                    if(debugPlayback)
                        printf("DS: Data - LOCK at %u  PlaybackRemain=%u  SampleBytesRemain=%u\r\n",dwNextPlaybackBufferWritePos, dwDataRemainToPlayInPlaybackBuffer, (sampleByteBufferContentSize - sampleByteBufferContentPos));
                   
                    //Update GPIO state
                    updateGPIO(playdata, totalBufferSizeBytes, sampleByteBufferContentSize, &dwPrevPlayPos, &dwPlayTotal, bitSeqBuffer);

                    //Lock the playback buffer
                    hr = playdata->dsplaybuff->Lock(dwNextPlaybackBufferWritePos, 0,(LPVOID *)&alpbtPtrs[0],&adwSizes[0],(LPVOID *)&alpbtPtrs[1],&adwSizes[1], DSBLOCK_ENTIREBUFFER);
                    if(FAILED(hr))
                    {
                        currentState = PlaybackState_Error;
                    }
                    else
                    {
                        //How much source data is there?
                        size_t numBytesToWrite = sampleByteBufferContentSize - sampleByteBufferContentPos;
                        if(numBytesToWrite > 0)
                        {
                            //Write to each buffer, if possible
                            for(size_t bufidx=0; bufidx < 2; ++bufidx)
                            {
                                const DWORD dwCurLockedBufSz = adwSizes[bufidx];
                                LPBYTE lpbtCurLockBufPtr = alpbtPtrs[bufidx];
                                if((lpbtCurLockBufPtr != NULL) && (dwCurLockedBufSz > 0) && (numBytesToWrite > 0))
                                { 
                                    //How much can be actually write to? - Don't overwrite anything that has yet to be played
                                    DWORD dwMaxAmountToWriteBytes = (totalBufferSizeBytes - dwDataRemainToPlayInPlaybackBuffer);
                                    if(dwMaxAmountToWriteBytes > dwCurLockedBufSz) //Don't exceed the current locked buffer!
                                        dwMaxAmountToWriteBytes = dwCurLockedBufSz;

                                    //Dont' exceed the source buffer
                                    if(dwMaxAmountToWriteBytes > numBytesToWrite)
                                        dwMaxAmountToWriteBytes = numBytesToWrite;

                                    if(dwMaxAmountToWriteBytes > 0)
                                    {
                                        //Write as many bytes as possible
                                        if(playdata->debugPlay)
                                            printf("DS COPYDATA : From %u amount %u\r\n", sampleByteBufferContentPos, dwMaxAmountToWriteBytes);
                                        
                                        memcpy(lpbtCurLockBufPtr, &sampleByteBuffer[sampleByteBufferContentPos], dwMaxAmountToWriteBytes);
                                        
                                        //Update positions
                                        sampleByteBufferContentPos += dwMaxAmountToWriteBytes;       //Update the position of where we will next read byte values from
                                        numBytesToWrite -= dwMaxAmountToWriteBytes;                //Reduce the number of bytes left to write
                                        dwDataRemainToPlayInPlaybackBuffer += dwMaxAmountToWriteBytes; //Add to the amount that has to be played
                                        dwNextPlaybackBufferWritePos += dwMaxAmountToWriteBytes;       //Add to where our next write position will be, and where we will request the lock from
                                    }
                                }
                            }
                        }

                        //Unlock the playback buffer
                        playdata->dsplaybuff->Unlock(alpbtPtrs[0], adwSizes[0],alpbtPtrs[1],adwSizes[1]);

                        if(debugPlayback)
                            printf("DS DATA : Next Lock is %u  Playback Remain=%u\r\n", dwNextPlaybackBufferWritePos, dwDataRemainToPlayInPlaybackBuffer);
                
                        //If we've run out of samples to read, then go into clearing mode
                        if(sampleByteBufferContentPos >= sampleByteBufferContentSize)
                        {
                            currentState = PlaybackState_ClearData_UpdateState;
                            dwAmountToClear = totalBufferSizeBytes;
                            //sampleByteBufferContentPos = 0; -- Don't clear this!. Remember what we've played - to avoid repeat playing the same thing again
                            
                            if(debugPlayback)
                                printf("DS DATA : Finished with sample bytes\r\n");
                        }
                    }

                    //Make sure we can cope with the playback buffer being circular
                    while(dwNextPlaybackBufferWritePos >= totalBufferSizeBytes)
                        dwNextPlaybackBufferWritePos -= totalBufferSizeBytes;
                    
                    
                    if(!playdata->isPlaying)
                        currentState = PlaybackState_StartPlaying; //We should start playing what we've copied
                    else if(currentState == PlaybackState_CopyData)
                        currentState = PlaybackState_CopyData_UpdateState; //Take a breather next time around
                }
                break;
            case PlaybackState_ClearData: 
                //We want to measure the amount of time this takes, so start counting....
                lastOpStartTick = GetTickCount64();
                
                if(dwAmountToClear > 0)
                {
                    if(debugPlayback)
                        printf("DS: CLEAN - LOCK at %u  ClearRemain=%u PlaybackRemain=%u\r\n",dwNextPlaybackBufferWritePos, dwAmountToClear, dwDataRemainToPlayInPlaybackBuffer);
                    
                    //Update GPIO state
                    updateGPIO(playdata, totalBufferSizeBytes, sampleByteBufferContentSize, &dwPrevPlayPos, &dwPlayTotal, bitSeqBuffer);

                    //Lock the playback buffer
                    DWORD adwSizes[2] = {0,0};
                    LPBYTE alpbtPtrs[2] = {NULL,NULL};
                    hr = playdata->dsplaybuff->Lock(dwNextPlaybackBufferWritePos, 0,(LPVOID *)&alpbtPtrs[0],&adwSizes[0],(LPVOID *)&alpbtPtrs[1],&adwSizes[1], DSBLOCK_ENTIREBUFFER);
                    if(FAILED(hr))
                    {
                        currentState = PlaybackState_Error;
                    }
                    else
                    {
                        //Write to each buffer, if possible
                        for(size_t bufidx=0; bufidx < 2; ++bufidx)
                        {
                            const DWORD dwCurLockedBufSz = adwSizes[bufidx];
                            LPBYTE lpbtCurLockBufPtr = alpbtPtrs[bufidx];
                            if((lpbtCurLockBufPtr != NULL) && (dwCurLockedBufSz > 0) && (dwAmountToClear > 0))
                            { 
                                //How much can be actually write to? - Don't overwrite anything that has yet to be played
                                DWORD dwMaxAmountToWriteBytes = (totalBufferSizeBytes - dwDataRemainToPlayInPlaybackBuffer);
                                if(dwMaxAmountToWriteBytes > dwCurLockedBufSz) //Don't exceed the current locked buffer!
                                    dwMaxAmountToWriteBytes = dwCurLockedBufSz;

                                if(dwMaxAmountToWriteBytes > dwAmountToClear)
                                    dwMaxAmountToWriteBytes = dwAmountToClear;

                                //Use memset to zero all that data
                                if(dwMaxAmountToWriteBytes > 0)
                                {
                                    memset( lpbtCurLockBufPtr, 0, dwMaxAmountToWriteBytes);
                                    dwDataRemainToPlayInPlaybackBuffer += dwMaxAmountToWriteBytes; //Add to the amount that has to be played
                                    dwNextPlaybackBufferWritePos += dwMaxAmountToWriteBytes;       //Add to where our next write position will be, and where we will request the lock from
                                    dwAmountToClear -= dwMaxAmountToWriteBytes; //Subtract the amount of data we've just cleared
                                }
                            }
                        }

                         //Unlock the playback buffer
                        playdata->dsplaybuff->Unlock(alpbtPtrs[0], adwSizes[0],alpbtPtrs[1],adwSizes[1]);

                        //Make sure we can cope with the playback buffer being circular
                        while(dwNextPlaybackBufferWritePos >= totalBufferSizeBytes)
                            dwNextPlaybackBufferWritePos -= totalBufferSizeBytes;

                        //If we've been signalled, or we have data, then go back to the copy data state
                        //Otherwise, we've finished, so 
                        cppthrd_LockThread(playdata->playbackThread);

                        if(membuf_getContentSize(playdata->samplePlayData) > 0)
                        {
                            //If there is more data to play, then go back and prepare it
                            currentState = PlaybackState_PrepareData;
                            sampleByteBufferContentSize = 0;
                            sampleByteBufferContentPos = 0;
                        }
                        else if(dwAmountToClear == 0)
                        {
                            //Stop. Just stop. 
                            currentState = PlaybackState_StopPlaying;
                            sampleByteBufferContentSize = 0;
                            sampleByteBufferContentPos = 0;
                        }
                        else
                        {
                            //Update amount left to play, then go back to clearing data
                            currentState = PlaybackState_ClearData_UpdateState; 
                        }
                      
                        cppthrd_UnlockThread(playdata->playbackThread);
                    }
                }
                else
                {
                    //No more data the clear, so stop playing
                    currentState = PlaybackState_StopPlaying;
                    sampleByteBufferContentSize = 0;
                    sampleByteBufferContentPos = 0;
                }
                break;
            case PlaybackState_ClearData_UpdateState:
            case PlaybackState_CopyData_UpdateState:

                //Update GPIO state
                updateGPIO(playdata, totalBufferSizeBytes, sampleByteBufferContentSize, &dwPrevPlayPos, &dwPlayTotal, bitSeqBuffer);

                //How much time has passed since the last operation (clear/copy) start?
                //If it's less than the minimum wait time, then we need to pause a bit and let some of 
                //the other threads have a go....
                curtick = GetTickCount64();
                if((curtick - lastOpStartTick) < dwMaxOpWaitTime)
                {
                    if(dwDataRemainToPlayInPlaybackBuffer > (totalBufferSizeBytes / 2))
                    {

                        if(debugPlayback)
                            printf("DS WAIT %u  PlaybackRemain=%u\r\n", dwMaxOpWaitTime - (DWORD)((curtick - lastOpStartTick)), dwDataRemainToPlayInPlaybackBuffer );
                    
                        WaitForMultipleObjects(2, ahEvents, FALSE, dwMaxOpWaitTime - (DWORD)((curtick - lastOpStartTick)) );
                    }
                    else
                    {
                        if(debugPlayback)
                            printf("DS NO WAIT %u  PlaybackRemain=%u\r\n",  dwDataRemainToPlayInPlaybackBuffer );
                    }
                }

                //Update GPIO state (again)
                updateGPIO(playdata, totalBufferSizeBytes, sampleByteBufferContentSize, &dwPrevPlayPos, &dwPlayTotal, bitSeqBuffer);

                lastOpStartTick = curtick;
                dwCurPlaybackBufferWritePos = 0;
                dwPlayPos = 0;
                hr = playdata->dsplaybuff->GetCurrentPosition(&dwPlayPos, &dwCurPlaybackBufferWritePos);
                if(FAILED(hr))
                {
                    currentState = PlaybackState_Error;
                }
                else
                {
                    DWORD dwAmountPlayed = 0;
                    if(dwCurPlaybackBufferWritePos > dwPrevPlaybackBufferWritePos)
                    {
                        //Write position has moved forward, and not wrapped around
                        dwAmountPlayed = dwCurPlaybackBufferWritePos - dwPrevPlaybackBufferWritePos;
                    }
                    else if(dwCurPlaybackBufferWritePos < dwPrevPlaybackBufferWritePos)
                    {
                        //Write position got to the end of the playback buffer, and wrapped to the start of the playback buffer
                        dwAmountPlayed = (dwPrevPlaybackBufferWritePos >=  totalBufferSizeBytes) ? 0 :  (totalBufferSizeBytes -  dwPrevPlaybackBufferWritePos);
                        dwAmountPlayed += dwCurPlaybackBufferWritePos;
                    }

                    //Update the amount left to play, based on our above calculation
                    if(dwAmountPlayed > dwDataRemainToPlayInPlaybackBuffer)
                        dwDataRemainToPlayInPlaybackBuffer = 0;
                    else
                        dwDataRemainToPlayInPlaybackBuffer -= dwAmountPlayed;

                    if(debugPlayback)
                        printf("DS Update - Playpos=%u  PlayWrite=%u  Played=%u  PlaybackRemain=%u\r\n", dwPlayPos, dwCurPlaybackBufferWritePos, dwAmountPlayed, dwDataRemainToPlayInPlaybackBuffer);

                    //Set the previous write pos for the next time around
                    //We use this to work out how much data has been played since the last time we checked
                    dwPrevPlaybackBufferWritePos = dwCurPlaybackBufferWritePos;

                    //Go to next state
                    if (currentState == PlaybackState_CopyData_UpdateState)
                    {
                        currentState = PlaybackState_CopyData;
                    }
                    else
                    {
                        currentState =  PlaybackState_ClearData;
                    }
                }
                break;
            case PlaybackState_Error:
                if(playdata->isPlaying)
                {
                    playdata->dsplaybuff->Stop();

                    //Signal not playing
                    gpio_set(playdata->gpiohandle, GPIOFunction_Tx_All, GPIOFunction_None);
                }

                return;
            default:
                break;
        }
    }
}

static void BD_CALLBACK recordThreadProc(void * param)
{
    WIN32SoundRecData * sndrecData = (WIN32SoundRecData *)param;
    LPBYTE lpbtPtr1,lpbtPtr2;

    std::vector<uint8_t> sampleBuffer(sndrecData->totalBufSize);
    const size_t sampleBufferSize = sampleBuffer.size();
    const bool debugRecord = sndrecData->debugCapture;

    //Zero all capture memory	
    lpbtPtr2 = NULL;
    lpbtPtr1 = NULL;
    DWORD dwSz1 = 0;
    DWORD dwSz2 = 0;
    HRESULT hr = sndrecData->dscapbuff->Lock(0, sndrecData->totalBufSize,(LPVOID *)&lpbtPtr1,&dwSz1,(LPVOID *)&lpbtPtr2,&dwSz2,0 );
    if(lpbtPtr1 != NULL)
        memset(lpbtPtr1,0,dwSz1);
    
    if(lpbtPtr2 != NULL)
        memset(lpbtPtr2,0,dwSz2);
    
    sndrecData->dscapbuff->Unlock(lpbtPtr1,dwSz1,lpbtPtr2,dwSz2);

    //main capture loop
    DWORD dwNextCaptureOffset = 0;
    DWORD dwSoundBufSz = 0;
    while(!sndrecData->shouldQuit)
    {
        //Wait for event or timeout
        bool capReady = false;
        switch(WaitForSingleObject(sndrecData->hNotificationEvent, sndrecData->bufferSizeMs))
        {
            case WAIT_OBJECT_0 + 0: //DSound buffer Notification
                capReady = true; //Ready to capture
                break;
            case WAIT_TIMEOUT:
                /*if(debugRecord)
                    printf("DS Capture : Timeout\r\n");*/
                break;
        }

        if(capReady)
        {
            if(debugRecord)
                printf("DS Capture : Ready\r\n");

            //Get current buffer positions
            DWORD dwCurCapPos = 0;
            DWORD dwCurReadPos = 0;
            hr = sndrecData->dscapbuff->GetCurrentPosition(&dwCurCapPos,&dwCurReadPos); 

            //determine lock size
            long lLockSize = dwCurReadPos - dwNextCaptureOffset;
            if( lLockSize < 0 )
            {
                //Buffer has wrapped
                lLockSize = dwCurReadPos + (sndrecData->totalBufSize - dwNextCaptureOffset);
            }

            // Block align lock size so that we are always write on a boundary
            lLockSize -= (lLockSize % (sndrecData->totalBufSize / NOTIFICATIONS));
            lLockSize &= 0xFFFFFFFC;

            //Lock entire capture region
            hr = sndrecData->dscapbuff->Lock(dwNextCaptureOffset,lLockSize,(LPVOID *)&lpbtPtr1,&dwSz1,(LPVOID *)&lpbtPtr2,&dwSz2,0 );
            if(hr == DS_OK)
            {
                //add first region to sample buffer
                if( (lpbtPtr1 != NULL) && (dwSz1 != 0) && (dwSoundBufSz < sampleBufferSize) )
                {
                    const DWORD dwCopySz = ((sampleBufferSize - dwSoundBufSz) > dwSz1) ? dwSz1 : (sampleBufferSize - dwSoundBufSz);
                    memcpy(&sampleBuffer[dwSoundBufSz], lpbtPtr1, dwCopySz);
                    dwSoundBufSz += dwCopySz;

                    // Move the capture offset along
                    dwNextCaptureOffset += dwCopySz; 
                    //dwNextCaptureOffset %= TOTALBUFSIZE; // Circular buffer

                    if(dwNextCaptureOffset > sndrecData->totalBufSize)
                    {
                        dwNextCaptureOffset = 0;
                    }
                    else if(dwNextCaptureOffset ==  sndrecData->totalBufSize)
                    {
                        dwNextCaptureOffset = 0;
                    }
                }

                //add second region to sample buffer 
                if( (lpbtPtr2 != NULL) && (dwSz2 != 0) && (dwSoundBufSz < sampleBufferSize) )
                {
                    const DWORD dwCopySz = ((sampleBufferSize - dwSoundBufSz) > dwSz2) ? dwSz2 : (sampleBufferSize - dwSoundBufSz);
                    memcpy(&sampleBuffer[dwSoundBufSz], lpbtPtr2, dwCopySz);
                    dwSoundBufSz += dwCopySz;

                    dwNextCaptureOffset += dwCopySz; 
                    //dwNextCaptureOffset %= TOTALBUFSIZE; // Circular buffer

                    if(dwNextCaptureOffset > sndrecData->totalBufSize)
                    {
                        dwNextCaptureOffset = 0;
                    }
                    else if(dwNextCaptureOffset == sndrecData->totalBufSize)
                    {
                        dwNextCaptureOffset = 0;
                    }
                }

                //unlock
                sndrecData->dscapbuff->Unlock(lpbtPtr1,dwSz1,lpbtPtr2,dwSz2);	
            }

            //Do we have a complete buffer yet?
            if(dwSoundBufSz >= (sndrecData->totalBufSize / 2))
            {
                //Query format
                WAVEFORMATEX wavfmtex;
                DWORD dwWritten = 0;
                memset(&wavfmtex,0,sizeof(wavfmtex));
                wavfmtex.cbSize = sizeof(wavfmtex);
                hr = sndrecData->dscapbuff->GetFormat( &wavfmtex, sizeof(wavfmtex), &dwWritten);

                
                if(debugRecord)
                    printf("DS Capture : Have Data %u - Samplerate=%u  channels=%u BitsPerSample=%u\r\n", dwSoundBufSz, wavfmtex.nSamplesPerSec, wavfmtex.nChannels, wavfmtex.wBitsPerSample);
                
                //Call the callback with the raw data
                if(!sndrecData->callback(wavfmtex.nSamplesPerSec, wavfmtex.nChannels, wavfmtex.wBitsPerSample,  &sampleBuffer[0], dwSoundBufSz, sndrecData->callbackparam))
                {
                    //We should quit... The callback told us to
                    sndrecData->shouldQuit = true;
                }

                dwSoundBufSz = 0;
            }
        }
    }
}

static BOOL CALLBACK DSEnumCallback(LPGUID lpGuid, LPCWSTR lpwstrDescription,  LPCWSTR lpwstrModule, LPVOID lpContext)
{
    if(lpGuid == NULL)
    {
        return TRUE;
    }

    std::vector<CAPDEVICE> * devices = (std::vector<CAPDEVICE> *)lpContext;

    size_t newEntryPos = devices->size();
    devices->resize( newEntryPos + 1);
    CAPDEVICE & newentry = (*devices)[newEntryPos];

    wcsncpy(newentry.awcDescription, lpwstrDescription,MAXDEVCAPNAMELEN);
    wcsncpy(newentry.awcModule, lpwstrModule,MAXDEVCAPNAMELEN);
    memcpy(&newentry.guid, lpGuid,sizeof(GUID));

    return TRUE;
}

extern "C" void soundrec_PrintDevices()
{
    std::vector<CAPDEVICE> capdevices;
    std::vector<CAPDEVICE> playdevices;
    
    HRESULT hr = DirectSoundCaptureEnumerate(DSEnumCallback, &capdevices); 
    if(FAILED(hr))
    {
        printf("ERROR: Failed to enumerate capture devices - 0x%08x\r\n", hr);
        return;
    }

    hr = DirectSoundEnumerate( DSEnumCallback,  &playdevices);
    if(FAILED(hr))
    {
        printf("ERROR: Failed to enumerate playback devices - 0x%08x\r\n", hr);
        return;
    }

    printf("CAPTURE DEVICES:\r\n------------------\r\n");
    for(size_t x=0; x < capdevices.size(); ++x)
    {
        printf(" %u : %ws : %ws\r\n", (x+1), capdevices[x].awcModule, capdevices[x].awcDescription);
    }

    printf("\r\nPLAYBACK DEVICES:\r\n------------------\r\n");
    for(size_t x=0; x < playdevices.size(); ++x)
    {
        printf(" %u : %ws : %ws\r\n", (x+1) + 1000, playdevices[x].awcModule, playdevices[x].awcDescription);
    }
}

//Release everything
static void freeSoundRecData(WIN32SoundRecData * sndrecData)
{
    if(sndrecData->threadhandle != NULL)
    {
        sndrecData->shouldQuit = true;
        SetEvent(sndrecData->hNotificationEvent);
        cppthrd_WaitAndDestroyThread(sndrecData->threadhandle);
        sndrecData->threadhandle = NULL;
    }

    if(sndrecData->dscapnotify != NULL)
        sndrecData->dscapnotify->Release();

    sndrecData->dscapnotify = NULL;

    if(sndrecData->dscapbuff != NULL)
        sndrecData->dscapbuff->Release();

    sndrecData->dscapbuff= NULL;

    if(sndrecData->dscap != NULL)
        sndrecData->dscap->Release();

    sndrecData->dscap=NULL;

    if(sndrecData->hNotificationEvent != NULL)
        CloseHandle(sndrecData->hNotificationEvent);

    sndrecData->hNotificationEvent = NULL;

    delete sndrecData;
}

static void freeSoundPlayData(WIN32SoundPlaybackData * sndplaydata)
{
    if(sndplaydata->playbackThread != NULL)
    {
        if((sndplaydata->isPlaying) && (sndplaydata->dsplaybuff != NULL))
        {
            sndplaydata->dsplaybuff->Stop();
        }

        sndplaydata->shouldQuit = true;
        SetEvent(sndplaydata->hThreadNotifyEvent);
        cppthrd_WaitAndDestroyThread(sndplaydata->playbackThread);
        sndplaydata->playbackThread = NULL;
    }

    if(sndplaydata->dsplaynotify != NULL)
        sndplaydata->dsplaynotify->Release();

    sndplaydata->dsplaynotify = NULL;

    if(sndplaydata->dsplaybuff != NULL)
        sndplaydata->dsplaybuff->Release();

    sndplaydata->dsplaybuff = NULL;

    if(sndplaydata->dsplay != NULL)
        sndplaydata->dsplay->Release();

    sndplaydata->dsplay= NULL;

    if(sndplaydata->hThreadNotifyEvent != NULL)
        CloseHandle(sndplaydata->hThreadNotifyEvent);

    if(sndplaydata->hBufferNotificationEvent != NULL)
        CloseHandle(sndplaydata->hBufferNotificationEvent);

    if(sndplaydata->samplePlayData != NULL)
        membuf_free(sndplaydata->samplePlayData);

    if(sndplaydata->bitSeqData != NULL)
        membuf_free(sndplaydata->bitSeqData);

    sndplaydata->samplePlayData = NULL;
    sndplaydata->bitSeqData = NULL;
    
    free(sndplaydata);
}

static HRESULT createbuffer( DSCBUFFERDESC * bufdesc, LPDIRECTSOUNDCAPTUREBUFFER * buf,  LPDIRECTSOUNDCAPTURE dscap)
{
    return dscap->CreateCaptureBuffer(bufdesc,buf,NULL);
}

static HRESULT createbuffer( DSBUFFERDESC * bufdesc, LPDIRECTSOUNDBUFFER * buf,  LPDIRECTSOUND dscap)
{
    bufdesc->dwFlags |= DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_GLOBALFOCUS;	
    return dscap->CreateSoundBuffer(bufdesc,buf,NULL);
}

template<typename BUFFERDESCTYPE, typename LPCAPBUFFERTYPE, typename IFACETYPE>
static void searchForFormat(WAVEFORMATEX * retWavfmtex, BUFFERDESCTYPE * retBufdesc, LPCAPBUFFERTYPE  * retDscapbuff, IFACETYPE iface, uint64_t totBufferSize, 
                            unsigned int sample_rate, unsigned int bitsPerSample, unsigned int channels)
{
    HRESULT hr;

    //Scan for matching bits / samplerate
    for( DWORD iIndex = 0; iIndex < 16; iIndex++ )
    {
        memset(retBufdesc,0,sizeof(BUFFERDESCTYPE));
        memset(retWavfmtex,0,sizeof(WAVEFORMATEX));

        //Set up wave format struct
        GetWaveFormatFromIndex( iIndex,retWavfmtex );
        retWavfmtex->wFormatTag = WAVE_FORMAT_PCM;
        retWavfmtex->cbSize = sizeof(WAVEFORMATEX);

        //set up capture buffer
        retBufdesc->dwBufferBytes = (DWORD)totBufferSize ;//wavfmtex.nAvgBytesPerSec;  //;44100 * 2 * 2 * 5;
        retBufdesc->dwSize = sizeof(BUFFERDESCTYPE);
        retBufdesc->lpwfxFormat = retWavfmtex;

        if((retWavfmtex->wBitsPerSample == bitsPerSample) && (retWavfmtex->nSamplesPerSec == sample_rate) && (retWavfmtex->nChannels == channels))
        {
            
            *retDscapbuff = NULL;
            hr = createbuffer(retBufdesc,  retDscapbuff, iface); 
            if(hr == DS_OK)
            {
                //Found match!
                break;
            }
        }

        if(*retDscapbuff != NULL)
        {
            (*retDscapbuff)->Release();
            *retDscapbuff = NULL;
        }
    }
}

template<typename NOTIFY_IFACE>
static HANDLE setupNotificaitonEvent(uint64_t totBufferSize, NOTIFY_IFACE * dsnotify)
{
    DSBPOSITIONNOTIFY aPosNotify[2];
    DWORD dwNotifySize = (DWORD)(totBufferSize / NOTIFICATIONS);
    HANDLE hNotificationEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
    //set up notifications array
    for(DWORD i=0; i < NOTIFICATIONS; i++)
    {
        aPosNotify[i].dwOffset = (i * dwNotifySize) + dwNotifySize - 1;
        aPosNotify[i].hEventNotify = hNotificationEvent;;
    }

    if( FAILED( dsnotify->SetNotificationPositions( NOTIFICATIONS,  aPosNotify ) ) )
    {
        CloseHandle(hNotificationEvent);
        return NULL;
    }

    return hNotificationEvent;
}


extern "C" SoundRecData soundrec_Start(unsigned int device, unsigned int channels, unsigned int sample_rate, unsigned int bitsPerSample, size_t bufferSizeMs,  HaveRecordSoundCallback callback, void * param, 
                                       enum BOOLEAN_VALUE debugThread, enum BOOLEAN_VALUE debugRecord)
{
    WAVEFORMATEX wavfmtex;
    

    CoInitialize(NULL);

    const uint64_t totBufferSize = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)bufferSizeMs) / 2000;
    
    //Get devices
    std::vector<CAPDEVICE> devices;
    HRESULT hr = DirectSoundCaptureEnumerate(DSEnumCallback, &devices); 
    if(FAILED(hr))
    {
        printf("ERROR: Failed to enumerate devices 0x%08x\r\n", hr);
        return NULL;
    }
    
    if((device == 0) || (device > devices.size()))
    {
        printf("ERROR: Invalid device id %u\r\n", device);
        return NULL;
    }

    WIN32SoundRecData * sndrecData = new WIN32SoundRecData();
    memset(sndrecData, 0, sizeof(WIN32SoundRecData));


    hr = DirectSoundCaptureCreate( &devices[device - 1].guid, &sndrecData->dscap, NULL );
    if(hr != DS_OK)
    {
        freeSoundRecData(sndrecData);
        return NULL;
    }

    //Search for format
    memset(&wavfmtex,0,sizeof(wavfmtex));
    searchForFormat(&wavfmtex, &sndrecData->bufdesc, &sndrecData->dscapbuff, sndrecData->dscap, totBufferSize, sample_rate, bitsPerSample, channels);

    //Did we get a matching format?
    if(sndrecData->dscapbuff == NULL)
    {
        printf("ERROR: Failed to initialise capture buffer for device %u\r\n", device);

        //No = return error
        freeSoundRecData(sndrecData);
        return NULL;
    }

    //Set up notification
    if( FAILED(sndrecData->dscapbuff->QueryInterface( IID_IDirectSoundNotify, (VOID**)&sndrecData->dscapnotify ) ) )
    {
        freeSoundRecData(sndrecData);
        return NULL;
    }

    sndrecData->hNotificationEvent = setupNotificaitonEvent(totBufferSize,  sndrecData->dscapnotify);
    if(sndrecData->hNotificationEvent == NULL)
    {
        freeSoundRecData(sndrecData);
        return NULL;
    }

    //start capturing
    hr = sndrecData->dscapbuff->Start(DSCBSTART_LOOPING );
    if(FAILED(hr))
    {
        freeSoundRecData(sndrecData);
        return NULL;
    }

    //Start capture thread
    sndrecData->shouldQuit = false;
    sndrecData->totalBufSize = (size_t)totBufferSize;
    sndrecData->bufferSizeMs = bufferSizeMs;
    sndrecData->callback = callback;
    sndrecData->callbackparam = param;
    sndrecData->debugCapture = (debugRecord == BOOL_TRUE);
    sndrecData->threadhandle = cppthrd_CreateThread( sndrecData, recordThreadProc, debugThread);
    
    cppthrd_StartThread(sndrecData->threadhandle, BOOL_FALSE);

    return sndrecData;
}


extern "C" void soundrec_Stop(SoundRecData sndrec)
{
    WIN32SoundRecData * sndrecData = (WIN32SoundRecData * )sndrec;
    freeSoundRecData(sndrecData);
}

extern "C" SoundPlaybackData soundplay_init(unsigned int device, unsigned int channels, unsigned int sample_rate, unsigned int bitsPerSample, unsigned int bufferSizeMs, 
                                            enum BOOLEAN_VALUE debugThread, enum BOOLEAN_VALUE debugPlayback, const char * playbackWriteAudioFilename, GPIOHandle gpiohandle, 
                                            unsigned int wakeUpLengthMs, unsigned int dataBitLengthMs, unsigned int dataGapLengthMs)
{
    WAVEFORMATEX wavfmtex;

    //Divide by 8 for bits
    //Divide by 1000 for milliseconds to seconds
    //Multiply by 2 for 2 buffers of that length 
    const uint64_t totBufferSize = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)bufferSizeMs) / 4000;
    

    //Get playback devices
    std::vector<CAPDEVICE> devices;
    HRESULT hr = DirectSoundEnumerate( DSEnumCallback,  &devices);
    if(FAILED(hr))
    {
        printf("ERROR: Failed to enumerate playback devices - 0x%08x\r\n", hr);
        return NULL;
    }

    if((device <= 1000) || ((device - 1001) >= devices.size()))
    {
        printf("ERROR: Invalid device id %u\r\n", device);
        return NULL;
    }

    WIN32SoundPlaybackData * sndplaydata = (WIN32SoundPlaybackData *)malloc(sizeof(WIN32SoundPlaybackData));
    memset(sndplaydata, 0, sizeof(WIN32SoundPlaybackData));
    
    //init playback
    hr = DirectSoundCreate(&devices[device - 1001].guid,&sndplaydata->dsplay,NULL);
    if(hr != DS_OK)
    {
        printf("ERROR: Failed to initialise playback device %u - error code 0x%08x\r\n", device, hr);
        freeSoundPlayData(sndplaydata);
        return NULL;
    }

    hr = sndplaydata->dsplay->SetCooperativeLevel(GetDesktopWindow(),DSSCL_NORMAL);
    if(hr != DS_OK)
    {
        printf("ERROR: Failed to set Cooperative Level on playback device %u - error code 0x%08x\r\n", device, hr);
        freeSoundPlayData(sndplaydata);
        return FALSE;
    }

    //Search for format
    searchForFormat(&wavfmtex, &sndplaydata->bufdesc, &sndplaydata->dsplaybuff, sndplaydata->dsplay, totBufferSize, sample_rate, bitsPerSample, channels);

    //Did we get a matching format?
    if(sndplaydata->dsplaybuff == NULL)
    {
        printf("ERROR: Failed to initialise capture buffer for device %u\r\n", device);

        //No = return error
        freeSoundPlayData(sndplaydata);
        return NULL;
    }

    //Set up notification
    if( FAILED(sndplaydata->dsplaybuff->QueryInterface( IID_IDirectSoundNotify, (VOID**)&sndplaydata->dsplaynotify ) ) )
    {
        freeSoundPlayData(sndplaydata);
        return NULL;
    }

    sndplaydata->hBufferNotificationEvent = setupNotificaitonEvent(totBufferSize,  sndplaydata->dsplaynotify);
    if(sndplaydata->hBufferNotificationEvent == NULL)
    {
        freeSoundPlayData(sndplaydata);
        return NULL;
    }

    //create and start thread
    sndplaydata->hThreadNotifyEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
    sndplaydata->shouldQuit = false;
    sndplaydata->isWaiting = false;
    sndplaydata->totalBufSize = (size_t)totBufferSize;
    sndplaydata->bufferSizeMs = bufferSizeMs;
    sndplaydata->samplePlayData = membuf_init();
    sndplaydata->bitSeqData = membuf_init();
    sndplaydata->debugPlay = (debugPlayback == BOOL_TRUE);
    sndplaydata->playbackWriteAudioFilename = playbackWriteAudioFilename;
    sndplaydata->deviceId = device;
    sndplaydata->playbackThread = cppthrd_CreateThread(sndplaydata, playbackThreadProc, debugThread);
    sndplaydata->gpiohandle = gpiohandle;
    sndplaydata->wakeLengthBytes = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)wakeUpLengthMs ) / 8000;
    sndplaydata->dataBitLengthBytes = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)dataBitLengthMs ) / 8000;
    sndplaydata->gapLengthBytes = ((uint64_t)channels * (uint64_t)sample_rate * (uint64_t)bitsPerSample * (uint64_t)dataGapLengthMs ) / 8000;
    cppthrd_StartThread(sndplaydata->playbackThread, BOOL_FALSE);

    return sndplaydata;
}


extern "C" void soundplay_deinit(SoundPlaybackData data)
{
    WIN32SoundPlaybackData * sndplaydata = (WIN32SoundPlaybackData * )data;
    freeSoundPlayData(sndplaydata);
}

extern "C" void soundplay_play(SoundPlaybackData playdata, const double * samples, size_t numSamples, MemoryBuffer * gpioBitSequence)
{
    WIN32SoundPlaybackData * sndplaydata = (WIN32SoundPlaybackData * )playdata;

    //Lock buffer
    cppthrd_LockThread(sndplaydata->playbackThread);

    //Allocate / Reallocate sample buffer
    membuf_append(sndplaydata->samplePlayData, samples, numSamples * sizeof(double));

    //Allocate / Reallocate bit sequence data buffer
    if(gpioBitSequence != NULL)
        membuf_append_copy(sndplaydata->bitSeqData, gpioBitSequence);

    //Signal playback thread
    SetEvent(sndplaydata->hThreadNotifyEvent);
    cppthrd_UnlockThread(sndplaydata->playbackThread);
}

extern "C" void soundplay_wait(SoundPlaybackData playdata)
{
    WIN32SoundPlaybackData * sndplaydata = (WIN32SoundPlaybackData * )playdata;

    //Lock
    cppthrd_LockThread(sndplaydata->playbackThread);

    //Loop until the playback thread is back to waiting
    while(!sndplaydata->isWaiting)
    {
        if(sndplaydata->debugPlay)
            printf("DS waiting for playback thread to finish\r\n");

        //Wait for a bit
        cppthrd_UnlockThread(sndplaydata->playbackThread);
        cppThread_sleep_ms(500);
        cppthrd_LockThread(sndplaydata->playbackThread);
    }

    //Wait for wavdest to finish as well
    if(sndplaydata->debugdesthnd != NULL)
    {
        if(sndplaydata->debugPlay)
            printf("DS waiting for debug wav writer thread to finish\r\n");

        wavdest_wait(sndplaydata->debugdesthnd);
    }

    if(sndplaydata->debugPlay)
            printf("DS wait done\r\n");

    cppthrd_UnlockThread(sndplaydata->playbackThread);
}