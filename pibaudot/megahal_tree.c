#include "megahal_lib.h"
#include "megahal_internal.h"

#define error mh_error

void mh_free_tree(TREE *tree)
{
	static int level = 0;
	register int i;

	Context;
	if(tree == NULL)
		return;

	if(tree->tree!=NULL) {
		for(i=0; i<tree->branch; ++i) {
			++level;
			mh_free_tree(tree->tree[i]);
			--level;
		}
		nfree(tree->tree);
	}
	nfree(tree);
}

TREE * mh_realloc_tree(TREE *tree)
{
	Context;
	if(tree->branch == 0 && tree->tree != NULL) {
		nfree(tree->tree);
		tree->tree = NULL;
	} else if(tree->tree == NULL)
		tree->tree = (TREE **)nmalloc(sizeof(TREE *)*(tree->branch));
	else
		tree->tree = (TREE **)nrealloc((TREE **)(tree->tree),sizeof(TREE *)*(tree->branch));

	if(tree->tree == NULL)
		return NULL;

	return tree;
}

/*
 *	Function:	Load_Tree
 *
 *	Purpose:	Load a tree structure from the specified file.
 */
void mh_load_tree(FILE *file, TREE *node)
{
	static int level = 0;
	register int i;

	Context;
	if ( fread(&(node->symbol), sizeof(BYTE2), 1, file) &&
	     fread(&(node->usage), sizeof(BYTE4), 1, file) &&
	     fread(&(node->count), sizeof(BYTE2), 1, file) &&
	     fread(&(node->branch), sizeof(BYTE2), 1, file) ) {

		if(node->branch==0) {
			return;
		}

		node->tree = (TREE **)nmalloc(sizeof(TREE *)*(node->branch));
		if(node->tree == NULL) {
			error("load_tree", "Unable to allocate subtree");
			return;
		}

		for(i=0; i<node->branch; ++i) {
			node->tree[i] = mh_new_node();
			++level;
			mh_load_tree(file, node->tree[i]);
			--level;
		}
	}
}


/*---------------------------------------------------------------------------*/

/*
 *	Function:	Find_Symbol_Add
 *
 *	Purpose:	This function is conceptually similar to find_symbol,
 *			apart from the fact that if the symbol is not found,
 *			a new node is automatically allocated and added to the
 *			tree.
 */
static TREE *find_symbol_add(TREE *node, int symbol)
{
	register int i;
	TREE *found = NULL;
	bool found_symbol = FALSE;

	Context;
	/*
	 *		Perform a binary search for the symbol.  If the symbol isn't found,
	 *		attach a new sub-node to the tree node so that it remains sorted.
	 */
	i = mh_search_node(node, symbol, &found_symbol);
	if(found_symbol == TRUE) {
		found = node->tree[i];
	} else {
		found = mh_new_node();
		found->symbol = symbol;
		mh_add_node(node, found, i);
	}

	return found;
}

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Add_Symbol
 *
 *	Purpose:	Update the statistics of the specified tree with the
 *			specified symbol, which may mean growing the tree if the
 *			symbol hasn't been seen in this context before.
 */
TREE * mh_add_symbol(TREE *tree, BYTE2 symbol)
{
	TREE *node=NULL;

	Context;
	/*
	 *	Search for the symbol in the subtree of the tree node.
	 */
	node = find_symbol_add(tree, symbol);

	/*
	 *	Increment the symbol counts
	 */
	if((node->count < 65535)) {
		node->count += 1;
		tree->usage += 1;
	}

	return node;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Find_Symbol
 *
 *	Purpose:	Return a pointer to the child node, if one exists, which
 *			contains the specified symbol.
 */
TREE * mh_find_symbol(TREE *node, int symbol)
{
	register int i;
	TREE *found = NULL;
	bool found_symbol = FALSE;

	Context;
	/*
	 *	Perform a binary search for the symbol.
	 */
	i = mh_search_node(node, symbol, &found_symbol);
	if(found_symbol == TRUE)
		found=node->tree[i];

	return found;
}

/*---------------------------------------------------------------------------*/
/*
 *	Function:	Save_Tree
 *
 *	Purpose:	Save a tree structure to the specified file.
 */
void mh_save_tree(FILE *file, TREE *node)
{
	static int level=0;
	register int i;

	Context;
	fwrite(&(node->symbol), sizeof(BYTE2), 1, file);
	fwrite(&(node->usage), sizeof(BYTE4), 1, file);
	fwrite(&(node->count), sizeof(BYTE2), 1, file);
	fwrite(&(node->branch), sizeof(BYTE2), 1, file);

	for(i=0; i<node->branch; ++i) {
		++level;
		mh_save_tree(file, node->tree[i]);
		--level;
	}
}


// returns the amount of nodes/leaves in a tree by recursing through all its branches
int mh_recurse_tree(TREE *node)
{
	int size = 0;
	register int i;

	Context;
	size++;
	for(i=0; i<node->branch; ++i)
		size += mh_recurse_tree(node->tree[i]);

	return size;
}


// this decrements the usage and count counters in a branch and deletes branches that arent used anymore
void mh_decrement_tree(TREE *node, TREE *parent)
{
	register int i;
	int position;
	bool fnd;

	Context;
	if(node == NULL || parent == NULL)
		return;

	position = mh_search_node(parent, node->symbol, &fnd);
	--parent->usage;
	if(--node->count < 1) {
		mh_free_tree(node);

		for(i=position; i<parent->branch-1; i++)
			parent->tree[i] = parent->tree[i+1];
		
        --parent->branch;
		mh_realloc_tree(parent);
	}
}