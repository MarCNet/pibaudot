#include <string.h>
#include <malloc.h>
#include <stdio.h>

#include "internal.h"
#include "megahal_lib.h"


//=========================================================================================================
//                     Audio capture input processing
//=========================================================================================================

typedef struct _ProcessInputFromAudioCapture
{
    const BauDot_Decoder_Options * processOptions;    //Baudot options
    SoundDevice device;
    SoundRecData captureHnd;
    enum BOOLEAN_VALUE threadDebug;
    enum BOOLEAN_VALUE captureDebug;
    enum BOOLEAN_VALUE tediousCaptureDebug;
    enum BOOLEAN_VALUE debugPower;
    ThreadHandle captureBufferLock;
    GPIOHandle gpiohandle;

    BaudotHandle baudothnd;
    MemoryBuffer * samplebuffer;

    MemoryBuffer * strBuffer;

    WAVDESTHND decodedAudioWavDest;
    ThreadHandle notifyEvent;

} ProcessInputFromAudioCapture;

static enum BOOLEAN_VALUE BD_CALLBACK baudotCharacterReceivedFromAudioCapture(char charValue, uint8_t baudotValue,  void * param)
{
    ProcessInputFromAudioCapture * data = (ProcessInputFromAudioCapture *)param;

    if(charValue >= ' ')
    {   
        //Append to character buffer
        membuf_append(data->strBuffer, &charValue, 1);

        if(data->processOptions->baudotDebug)
            printf("%u Got char 0x%02x \r\n", membuf_getContentSize(data->strBuffer),  (unsigned int)charValue);

        if((charValue == 0x2e) && (membuf_getContentSize(data->strBuffer) == 1))
        {
            data = NULL;
        }
    }
    else if(charValue == '\b')
    {
        //Backspace
        //Remove character from buffer
        membuf_removeFromBack(data->strBuffer, 1);
    }

    return BOOL_TRUE;
}


//Wav data callback, called when sound capture has data for us
//NOTE: This is to do as little processing as possible, so that the capture thread can go back to capturing audio data asap
static enum BOOLEAN_VALUE BD_CALLBACK capturedWavDataCallback(unsigned int sampleRate, unsigned int channels, unsigned int bitsPerSec, const uint8_t * sampledata, size_t dataAmount, void * param)
{
    ProcessInputFromAudioCapture * data = (ProcessInputFromAudioCapture *)param;

    //Check the audio is the correct format
    if((sampleRate != data->device.sample_rate) || (channels != data->device.channels) || (bitsPerSec != data->device.bitsPerSampleValue))
    {
        printf("ERROR: Captured WAV Data is wrong format - wanted [%u %u %u], but got [%u %u %u]\r\n", data->device.sample_rate, data->device.channels, data->device.bitsPerSampleValue,
                                                                                                        sampleRate, channels, bitsPerSec);
        return BOOL_FALSE;
    }

    if(data->tediousCaptureDebug)
        printf(" Got sample data %u - samplerate=%u channels=%u BitsPerSample=%u\r\n", dataAmount, sampleRate, channels, bitsPerSec);

     //Lock
    cppthrd_LockThread(data->captureBufferLock);
    
    //Append sample data to our buffer
    membuf_append( data->samplebuffer, sampledata, dataAmount);

    //Unlock
    cppthrd_UnlockThread(data->captureBufferLock);

     //We (may) have data! Wake the main process up to process the received data
    if(data->notifyEvent != NULL)
        cppthrd_SignalThreadEvent(data->notifyEvent, BOOL_FALSE);
    
    //Return success
    return BOOL_TRUE; 
}

enum ProcessStatus BD_CALLBACK process_InputAudioCaptureCallback(MemoryBuffer * recvBuffer, void * param)
{
    ProcessInputFromAudioCapture * data = (ProcessInputFromAudioCapture *)param;
    size_t copysz, contentsz;
    size_t copytotal = 0;
    size_t sourceOffset = 0;
    enum BOOLEAN_VALUE isBreak = BOOL_FALSE;
    unsigned char tempSampleData[16384] = {0};
    
    //Start up if currently off
    if((data->captureHnd == NULL) || (data->baudothnd == NULL))
    {
        if(data->debugPower)
            printf("Audio capture not yet powered\r\n");

        return ProcessStatus_MoreToCome;
    }

    do
    {
        copysz = 0;
        
        {
             //Lock
            cppthrd_LockThread(data->captureBufferLock);

            //Check if there is any data to process
            contentsz = membuf_getContentSize(data->samplebuffer);
            if(contentsz >= sourceOffset)
            {
                //Copy to temp buffer
                copysz = (contentsz - sourceOffset) > sizeof(tempSampleData) ? sizeof(tempSampleData) : (contentsz - sourceOffset);
                memcpy(tempSampleData, membuf_ptr(data->samplebuffer, sourceOffset), copysz);
                
                if(data->tediousCaptureDebug)
                    printf("Processing captured sample data %u from offset %u - contentsz=%u\r\n",  (contentsz - sourceOffset), sourceOffset, contentsz);

                sourceOffset += copysz;
                copytotal += copysz;
            }

            cppthrd_UnlockThread(data->captureBufferLock);
        }


        //Process copied samples
        if(copysz > 0)
        {
            switch(baudot_process_samples(data->baudothnd, tempSampleData, copysz, data->decodedAudioWavDest))
            {   
                case BaudotProcessState_NeedMoreData:
                    break;

                case BaudotProcessState_Break:
                    //If we've reached a break, then copy our content into the callers receive buffer
                    if(membuf_getContentSize(data->strBuffer) > 0)
                    {
                        membuf_append_copy( recvBuffer, data->strBuffer);

                        //Remove what we've copied from the character buffer that our baudot callback appends to
                        membuf_erase(data->strBuffer);
                    }

                    isBreak = BOOL_TRUE;
                    break;

                case BaudotProcessState_Error:
                    if(membuf_getContentSize(data->strBuffer) > 0)
                        membuf_append_copy( recvBuffer, data->strBuffer);

                    printf("\r\nERROR: Aborted!\r\n");
                    return ProcessStatus_Error;
                
                default:
                    printf("\r\nERROR: unknown state!\r\n");
                    return ProcessStatus_Error;
            }
        }
    }
    while(copytotal < contentsz);

    {
         //Lock
        cppthrd_LockThread(data->captureBufferLock);

        if(data->tediousCaptureDebug)
            printf("Process done %u / %u \r\n", copytotal, membuf_getContentSize(data->samplebuffer));

        //Empty sample buffer, now that we've processed the audio from it
        membuf_removeFromFront(data->samplebuffer, copytotal);

        cppthrd_UnlockThread(data->captureBufferLock);
    }
    
    
    //Unlock
    cppthrd_UnlockThread(data->captureBufferLock);

    return  isBreak ? ProcessStatus_ReadyToProcess : ProcessStatus_MoreToCome;
}

AudioCaptureInputHandle process_initAudioCaptureHandle(const BauDot_Decoder_Options * processOptions, const SoundDevice * device, 
                                                       enum BOOLEAN_VALUE threadDebug, enum BOOLEAN_VALUE captureDebug, enum BOOLEAN_VALUE tediousCaptureDebug, enum BOOLEAN_VALUE powerDebug, 
                                                       GPIOHandle gpiohandle)
{
    ProcessInputFromAudioCapture * ret = (ProcessInputFromAudioCapture *)malloc(sizeof(ProcessInputFromAudioCapture));
    memset(ret,0,sizeof(ProcessInputFromAudioCapture));

    memcpy(&ret->device, device, sizeof(SoundDevice));
    ret->processOptions = processOptions;
    ret->captureDebug = captureDebug;
    ret->threadDebug = threadDebug;
    ret->debugPower = powerDebug;
    ret->gpiohandle = gpiohandle;
    ret->tediousCaptureDebug = tediousCaptureDebug;

    //Init working sample data buffer
    ret->samplebuffer = membuf_init_blk( (device->sample_rate * device->channels * device->bitsPerSampleValue) / 8  );
    
    //Init string buffer
    ret->strBuffer = membuf_init();

    //Init locks and events
    ret->captureBufferLock = cppthrd_CreateLockOnly();

    return ret;
}

static void deinitAudioCapture(ProcessInputFromAudioCapture * data)
{
    if(data->captureHnd != NULL)
    {
        soundrec_Stop(data->captureHnd);
        data->captureHnd = NULL;
    }

    //Close the decoder debug audio file
    if(data->decodedAudioWavDest != NULL)
    {
        wavdest_free(data->decodedAudioWavDest);
        data->decodedAudioWavDest=NULL;
    }

    //Deinit baudot
    if(data->baudothnd != NULL)
    {
        baudot_free(data->baudothnd);
        data->baudothnd=NULL;
    }

    //Release buffers (but keep them valid, so that they can be re-used)
    if(data->strBuffer != NULL)
        membuf_reset(data->strBuffer);

    if(data->samplebuffer != NULL)
        membuf_reset(data->samplebuffer);

}

void process_AudioCaptureInputPower(AudioCaptureInputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent)
{
    ProcessInputFromAudioCapture * data = (ProcessInputFromAudioCapture *)hnd;
    char acFilename[260];

    if(data->debugPower)
        printf("Audio capture input power %s\r\n", (pwrState) ? "on" : "off");

    if(pwrState)
    {
        //Lock
        cppthrd_LockThread(data->captureBufferLock);

        //Set up debug audio file, where audio passed to the baudot decoder is written
        if((data->decodedAudioWavDest == NULL) && (data->processOptions->decoderOutAudioFile != NULL))
        {
    #ifdef WIN32
            _snprintf(acFilename, sizeof(acFilename), "%s.cap%02u.wav", data->processOptions->decoderOutAudioFile, data->device.device);
    #else
            snprintf(acFilename, sizeof(acFilename), "%s.cap%02u.wav", data->processOptions->decoderOutAudioFile, data->device.device);
    #endif

            data->decodedAudioWavDest = wavdest_init(acFilename, data->device.sample_rate, 1.0f, data->threadDebug);
            if(data->decodedAudioWavDest == NULL)
            {
                printf("ERROR: Failed to audio file %s to save captured audio\r\n", acFilename);
            }
        }

         //Init baudot if required    
        if(data->baudothnd == NULL)
        {
            data->baudothnd = baudot_init( data->processOptions, WAV_FORMAT_PCM, data->device.sample_rate, data->device.channels, data->device.bitsPerSampleValue, 
                                           baudotCharacterReceivedFromAudioCapture, data, data->gpiohandle);
        }

        //Start capture if required
        if(data->captureHnd == NULL)
        {
            data->captureHnd = soundrec_Start( data->device.device, data->device.channels, data->device.sample_rate, data->device.bitsPerSampleValue,
                                               data->processOptions->bufferSizeMs, capturedWavDataCallback, data, 
                                               data->threadDebug, data->captureDebug );

            if(data->captureHnd == NULL)
            {
                printf("ERROR: Failed to start capture from audio device\r\n");
            }
        }

        //Unlock
        cppthrd_UnlockThread(data->captureBufferLock);
    }
    else if(!pwrState)
    {
        cppthrd_LockThread(data->captureBufferLock);

        //Deinit, but don't free 
        deinitAudioCapture(data);

        cppthrd_UnlockThread(data->captureBufferLock);
    }
}

void process_freeAudioCaptureHandle(AudioCaptureInputHandle hnd)
{
    ProcessInputFromAudioCapture * data = (ProcessInputFromAudioCapture *)hnd;
    
    //Turn stuff off.....
    deinitAudioCapture(data);

    //Free sample buffer
    if(data->samplebuffer != NULL)
    {
        membuf_free(data->samplebuffer);
        data->samplebuffer=NULL;
    }

    //Free string buffer
    if(data->strBuffer != NULL)
    {
        membuf_free(data->strBuffer);
        data->strBuffer=NULL;
    }

    //Free the mutex (this is a threadless lock)
    if(data->captureBufferLock != NULL)
    {
        cppthrd_WaitAndDestroyThread(data->captureBufferLock );
        data->captureBufferLock = NULL;
    }

    free(data);
}

//=========================================================================================================
//                     Audio file input processing
//=========================================================================================================



typedef struct _ProcessInputFromAudioFile
{
    const BauDot_Decoder_Options * processOptions;    //Baudot options    
    FILE * wavfile;                             //File handle
    BaudotHandle baudotHandle;                  //Baudot processing handle
    enum BOOLEAN_VALUE lastProcessStateWasBreak; //Did we get a break result the last time we run the baudot decoder?
    const char * wavfilename;                   //input filename
    SampleInfo wavSampinfo;                     //WAV format data, parsed from the file
    
    MemoryBuffer * currentStrBuffer;            //Decoded character buffer
    
    MemoryBuffer * sampleBuffer;               //Current sample buffer to process

}ProcessInputFromAudioFile;


static enum BOOLEAN_VALUE BD_CALLBACK baudotCharacterReceivedFromAudioFile(char charValue, uint8_t baudotValue,  void * param)
{
    static const int strBlockSize = 1024;
    ProcessInputFromAudioFile * data = (ProcessInputFromAudioFile *)param;

    if(charValue >= ' ')
    {   
        //Append to character buffer
        membuf_append(data->currentStrBuffer, &charValue, 1);
    }
    else if(charValue == '\b')
    {
        //Backspace
        //Remove character from buffer
        membuf_removeFromBack(data->currentStrBuffer, 1);
    }


    return BOOL_TRUE;
}

enum ProcessStatus BD_CALLBACK process_InputAudioCallback(MemoryBuffer * recvBuffer, void * param)
{
    ProcessInputFromAudioFile * data = (ProcessInputFromAudioFile *)param;
    unsigned char tempBuffer[4096]; //Should be big enough to parse the wav header....
    size_t amountRead;
    enum BOOLEAN_VALUE isBreak = BOOL_FALSE;
    enum BOOLEAN_VALUE wasBreak = BOOL_FALSE;
    
    //Init the file
    if(data->wavfile == NULL)
    {
        //Read start of wav file
        data->wavfile = fopen(data->wavfilename, "rb");
        if(data->wavfile == NULL)
        {
            printf("\nERROR: Failed to open file %s\n", data->wavfilename);
            return ProcessStatus_Error;
        }

        //Read the wav header
        amountRead = fread(tempBuffer, 1, sizeof(tempBuffer), data->wavfile);
        if(amountRead == 0)
        {
            printf("\nERROR: Failed to read file %s\n", data->wavfilename);
            return ProcessStatus_Error;
        }

        //Parse the wav file header
        memset(&data->wavSampinfo,0,sizeof(SampleInfo));
        if((!get_sample_info_wave(tempBuffer, amountRead, &data->wavSampinfo)) || (&data->wavSampinfo.dataSize == 0))
        {
            printf("\nERROR: Failed to parse file header %s\n", data->wavfilename);
            return ProcessStatus_Error;
        }

        //Seek to start of data
        if(fseek(data->wavfile, (long)data->wavSampinfo.dataPos, SEEK_SET) != 0)
        {
            printf("\nERROR: Failed to seek to data position %u in file %s\n", (long)data->wavSampinfo.dataPos, data->wavfilename);
            return ProcessStatus_Error;
        }
    }

    //Init baudot decoder
    if(data->baudotHandle == NULL)
    {
         //Init the baudot processor - Don't use GPIO here
        data->baudotHandle = baudot_init(data->processOptions, 
                                         data->wavSampinfo.wvformat,  data->wavSampinfo.sample_rate, data->wavSampinfo.channels, data->wavSampinfo.bits_per_sample, 
                                         baudotCharacterReceivedFromAudioFile, data, NULL);
    }
    
    {
        //Work out how much data to read
        const size_t maxDestValues = (size_t)(((int64_t)data->wavSampinfo.sample_rate * data->processOptions->bufferSizeMs) / 1000);
        const size_t bytes_per_wide_sample = data->wavSampinfo.channels * (data->wavSampinfo.bits_per_sample / 8);
        const size_t maxSampleDataToReadBytes = (size_t)((int64_t)(maxDestValues * bytes_per_wide_sample));

        //Init sample buffer
        if(data->sampleBuffer == NULL)
            data->sampleBuffer = membuf_init_blk(maxSampleDataToReadBytes);

        //If the previous time resulted in a BaudotProcessState_Break, then there may be stuff left over from that last time
        //In which case, don't read any more
        if(data->lastProcessStateWasBreak)
        {
            amountRead = 0;
        }
        else
        {
            //Read sample data    
            amountRead = membuf_fread_append( data->sampleBuffer, data->wavfile, maxSampleDataToReadBytes);
        }
        
        if((amountRead > 0) || (data->lastProcessStateWasBreak))
        {
            //Process samples, and store the state for next time
            switch(baudot_process_samples(data->baudotHandle, membuf_ptr(data->sampleBuffer, 0), amountRead, NULL))
            {   
                case BaudotProcessState_NeedMoreData:
                    break;

                case BaudotProcessState_Break:
                    isBreak = BOOL_TRUE;
                    break;

                case BaudotProcessState_Error:
                    printf("\r\nERROR: Aborted!\r\n");
                    return ProcessStatus_Error;
                
                default:
                    printf("\r\nERROR: unknown state!\r\n");
                    return ProcessStatus_Error;
            }

            //Erase the sample data we've processed
            membuf_erase(data->sampleBuffer);
        }

        //Copy the decoded characters into the receive buffer if we've reached a break, or the end
        if( (isBreak || ((amountRead == 0) && (!data->lastProcessStateWasBreak))) &&
            (membuf_getContentSize(data->currentStrBuffer) > 0) )
        {
            membuf_append_copy( recvBuffer, data->currentStrBuffer);

            //Remove what we've copied from the character buffer that our baudot callback appends to
            membuf_erase(data->currentStrBuffer);
        }

        //If the entire file has been processed, then there is no point calling us back
        if((amountRead == 0) && (!data->lastProcessStateWasBreak))
        {
            data->lastProcessStateWasBreak = isBreak;
            return ProcessStatus_NoMoreData;
        }
    }

    //Not finished yet, we need to process more audio data from the file
    data->lastProcessStateWasBreak = isBreak;
    return isBreak ? ProcessStatus_ReadyToProcess : ProcessStatus_MoreToCome; 
}

AudioFileInputHandle process_initAudioFileInputHandle(const char * filename, const BauDot_Decoder_Options * processOptions)
{
    ProcessInputFromAudioFile * ret = (ProcessInputFromAudioFile *)malloc(sizeof(ProcessInputFromAudioFile));
    memset(ret,0,sizeof(ProcessInputFromAudioFile));
    ret->processOptions = processOptions;
    ret->wavfilename = filename;
    
    //Init character buffer
    ret->currentStrBuffer = membuf_init();

    //Init sample buffer
    ret->sampleBuffer = NULL;
    return ret;
}

void process_AudioFileInputPower(AudioFileInputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent)
{
}

void process_freeAudioFileInputHandle(AudioFileInputHandle hnd)
{
    ProcessInputFromAudioFile * data = (ProcessInputFromAudioFile *)hnd;

    if(data->wavfile != NULL)
    {
        fclose(data->wavfile);
        data->wavfile=NULL;
    }

    if(data->baudotHandle != NULL)
    {
        baudot_free(data->baudotHandle);
        data->baudotHandle = NULL;
    }

    if(data->currentStrBuffer != NULL)
    {
        membuf_free(data->currentStrBuffer);
        data->currentStrBuffer=NULL;
    }

    if(data->sampleBuffer != NULL)
    {
        membuf_free(data->sampleBuffer);
        data->sampleBuffer = NULL;
    }

    free(data);
}

//=========================================================================================================
//                     string input processing
//=========================================================================================================

typedef struct _ProcessInputFromString
{
    MemoryBuffer * data;
    ThreadHandle lock;
    enum BOOLEAN_VALUE oneShot;
    enum BOOLEAN_VALUE externalBuffer;

} ProcessInputFromString;

enum ProcessStatus BD_CALLBACK process_StringCallback(MemoryBuffer * recvBuffer, void * param)
{
    ProcessInputFromString * data = (ProcessInputFromString *)param;
    enum BOOLEAN_VALUE copiedData = BOOL_FALSE;

    if(data->lock != NULL)
        cppthrd_LockThread(data->lock);

    if(membuf_getContentSize(data->data) > 0)
    {
        //Copy to receive buffer
        membuf_append_copy(recvBuffer, data->data);

        //Erase source buffer
        membuf_erase(data->data);

        copiedData = BOOL_TRUE;
    }

    if(data->lock != NULL)
        cppthrd_UnlockThread(data->lock);

    if(data->oneShot)
    {
        //No more data to follow - we're in one shot mode
        return ProcessStatus_NoMoreData;
    }
     
    //Receive buffer is ready to process
    return copiedData ? ProcessStatus_ReadyToProcess : ProcessStatus_MoreToCome;
}

StringInputHandle process_initStringInputHandle(const char * str)
{
    ProcessInputFromString * ret = (ProcessInputFromString *)malloc(sizeof(ProcessInputFromString));
    ret->data = membuf_init();
    ret->lock = NULL;
    ret->oneShot = BOOL_TRUE;
    ret->externalBuffer = BOOL_FALSE; //Free the buffer when done
    
    membuf_append(ret->data, str, strlen(str) + 1);

    return ret;
}

void process_StringInputPower(StringInputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent)
{
}

void process_freeStringInputHandle(StringInputHandle hnd)
{
    ProcessInputFromString * data = (ProcessInputFromString *)hnd;
    
    if(!data->externalBuffer)
        membuf_free(data->data);
}

static StringInputHandle process_initBufferInputHandle(MemoryBuffer * strbuf, ThreadHandle lock)
{
    ProcessInputFromString * ret = (ProcessInputFromString *)malloc(sizeof(ProcessInputFromString));
    ret->data = strbuf;
    ret->lock = lock;
    ret->externalBuffer = BOOL_TRUE; //Do not free the buffer when done, it belongs to the caller

    //Set one shot to false, as the buffer may be updated multiple times (e.g: from console)
    ret->oneShot = BOOL_FALSE;
    
    return ret;
}


//=========================================================================================================
// Output to playing Baudot audio to audio device
//=========================================================================================================

typedef struct _BaudotAudioPlaybackData
{
    int samplerate;                         //Encoding samplerate
    const BauDot_Decoder_Options * options; //Baudot tone options
    enum BOOLEAN_VALUE threadDebug;         //enable wavdest thread debugging
    SoundDevice soundDevice;                //Sound device for playback
    enum BOOLEAN_VALUE playbackDebug;       //enable audio playback debugging
    enum BOOLEAN_VALUE debugPower;          //enable power debugging
    GPIOHandle gpiohandle;

    enum BOOLEAN_VALUE baudotShiftState;         //Current shift state  
    SoundPlaybackData  sndhnd;
    const char ** playbackWriteAudioFilename;

    MemoryBuffer * sampleData;                    //Temp working sample buffer
    MemoryBuffer * bitSeqData;                    //Temp bit sequence data
    

}BaudotAudioPlaybackData;

enum BOOLEAN_VALUE BD_CALLBACK process_OutputBaudotAudioPlaybackCallback(const char * str, void * param)
{
    BaudotAudioPlaybackData * data = (BaudotAudioPlaybackData *)param;
    double zero = 0.0f;
    const size_t numWakeupSamples = (data->samplerate * data->options->playWakeDurationMs) / 1000;
    
    if(data->sndhnd == NULL)
    {
        if(data->debugPower)
            printf("Output playback not yet initialised\r\n");

        return BOOL_TRUE;
    }

    //If there is any 'wake up' padding, then generate that first
    if(data->options->playWakeDurationMs > 0)
    {   
        //First generate the wakeup tone (or silence)
        if(data->options->playWakeFreq > 0)
        {
            smputil_generateToneDoubles( data->options->playWakeFreq, data->options->playWakeDurationMs, data->samplerate,  data->options->outputScale, data->sampleData);
        }
        else
        {   
            membuf_append_value( data->sampleData, &zero, sizeof(zero), numWakeupSamples);
        }
    }

    //Convert the string into baudot audio
    membuf_erase(data->bitSeqData);
    baudot_encode_string( data->options, data->samplerate, data->options->outputScale, str, &data->baudotShiftState, data->sampleData, data->bitSeqData);
   
    //Output to sound device
    soundplay_play(data->sndhnd, (const double *)membuf_ptr(data->sampleData,0), membuf_getContentSize(data->sampleData) / sizeof(double), data->bitSeqData);

    //Clear out the sample buffer - as soundplay copied the data we've just given it
    membuf_erase(data->sampleData);

    return BOOL_TRUE;
}

void BD_CALLBACK process_WaitForBaudotAudioPlaybackOutputCallback(void * param)
{
    BaudotAudioPlaybackData * data = (BaudotAudioPlaybackData *)param;
    if(data->sndhnd != NULL)
    {
        soundplay_wait(data->sndhnd);
    }
}

BaudotAudioPlaybackHandle process_initBaudotAudioPlaybackOutputHandle(const BauDot_Decoder_Options * options,  const SoundDevice * soundDevice, 
                                                                      enum BOOLEAN_VALUE threadDebug, enum BOOLEAN_VALUE playbackDebug, enum BOOLEAN_VALUE powerDebug,
                                                                      const char ** playbackWriteAudioFilename, GPIOHandle gpiohandle)
{
    BaudotAudioPlaybackData * data = (BaudotAudioPlaybackData *)malloc(sizeof(BaudotAudioPlaybackData));
    memset(data,0,sizeof(BaudotAudioPlaybackData));

    data->baudotShiftState = BOOL_FALSE;
    data->options = options;
    data->threadDebug = threadDebug;
    data->playbackDebug = playbackDebug;
    data->debugPower = powerDebug;
    memcpy(&data->soundDevice, soundDevice, sizeof(SoundDevice));    
    data->samplerate = soundDevice->sample_rate;
    data->playbackWriteAudioFilename = playbackWriteAudioFilename;
    data->gpiohandle = gpiohandle;
    data->sampleData = membuf_init_blk(soundDevice->sample_rate * sizeof(double));
    data->bitSeqData = membuf_init();
    return data;
}

void process_BaudotAudioPlaybackPower(BaudotAudioPlaybackHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent)
{
    BaudotAudioPlaybackData * data = (BaudotAudioPlaybackData *)hnd;

    if(data->debugPower)
        printf("Audio playback output power %s\r\n", (pwrState) ? "on" : "off");

    if(!pwrState)
    {
        if(data->sndhnd != NULL)
        {
            soundplay_deinit(data->sndhnd);
            data->sndhnd = NULL;
        }

        if(data->sampleData != NULL)
            membuf_reset(data->sampleData);

        if(data->bitSeqData != NULL)
            membuf_reset(data->bitSeqData);
    }
    else
    {
        //Init sound device, if required
        if(data->sndhnd == NULL)
        {
            data->sndhnd = soundplay_init( data->soundDevice.device, data->soundDevice.channels, data->soundDevice.sample_rate, data->soundDevice.bitsPerSampleValue, data->options->bufferSizeMs,
                                           data->threadDebug,  data->playbackDebug,
                                           (data->playbackWriteAudioFilename == NULL) ? NULL : (*data->playbackWriteAudioFilename), 
                                           data->gpiohandle, data->options->playWakeDurationMs, (unsigned int)data->options->bitLengthMs, data->options->charGapDurationMs);

            if(data->sndhnd == NULL)
            {
                printf("ERROR: Failed to initialise sound device\r\n");
            }
        }
    }
}

void process_freeBaudotAudioPlaybackHandle(BaudotAudioPlaybackHandle hnd)
{
    BaudotAudioPlaybackData * data = (BaudotAudioPlaybackData *)hnd;
 
    if(data->sndhnd != NULL)
    {
        soundplay_deinit(data->sndhnd);
        data->sndhnd = NULL;
    }

    if(data->sampleData != NULL)
    {
        membuf_free(data->sampleData);
        data->sampleData=NULL;
    }

    if(data->bitSeqData != NULL)
    {
        membuf_free(data->bitSeqData);
        data->bitSeqData=NULL;
    }

    free(data);
}

//=========================================================================================================
// Output to Baudot audio file processing
//=========================================================================================================
typedef struct _BaudotAudioFileData
{
    const char * filename;
    int samplerate;                         //Encoding samplerate
    const BauDot_Decoder_Options * options; //Baudot tone options
    enum BOOLEAN_VALUE threadDebug;         //enable wavdest thread debugging

    enum BOOLEAN_VALUE baudotShiftState;         //Current shift state     
    WAVDESTHND wavdest;                     //Wav file destination handle

    MemoryBuffer * sampleData;                    //Temp working sample buffer
    
}BaudotAudioFileData;

enum BOOLEAN_VALUE BD_CALLBACK process_OutputBaudotAudioCallback(const char * str, void * param)
{
    BaudotAudioFileData * data = (BaudotAudioFileData *)param;
    double zero = 0.0f;
    const size_t numWakeupSamples = (data->samplerate * data->options->playWakeDurationMs) / 1000;

    if(data->wavdest == NULL)
    {
        printf("Output audio file not yet initialised\r\n");
        return BOOL_TRUE;
    }

    //If there is any 'wake up' padding, then generate that first
    if(data->options->playWakeDurationMs > 0)
    {   
        //First generate the wakeup tone (or silence)
        if(data->options->playWakeFreq > 0)
        {
            smputil_generateToneDoubles( data->options->playWakeFreq, data->options->playWakeDurationMs, data->samplerate,  data->options->outputScale, data->sampleData);
        }
        else
        {   
            membuf_append_value( data->sampleData, &zero, sizeof(zero), numWakeupSamples);
        }
    }

    //Convert the string into baudot audio, and append that to the sample buffer
    baudot_encode_string( data->options, data->samplerate, data->options->outputScale , str, &data->baudotShiftState, data->sampleData, NULL);

    //Output to wav file
    wavdest_write(data->wavdest, (double *)data->sampleData->ptr, data->sampleData->contentsz / sizeof(double));

    //Clear sample buffer
    membuf_erase(data->sampleData);

    return BOOL_TRUE;
}

void BD_CALLBACK process_WaitForOutputBaudotAudioCallback(void * param)
{
    BaudotAudioFileData * data = (BaudotAudioFileData *)param;
    if(data->wavdest != NULL)
    {
        wavdest_wait(data->wavdest);
    }
}


BaudotAudioFileOutputHandle process_initBaudotAudioFileOutputHandle(const BauDot_Decoder_Options * options, const char * outputFilename, int samplerate,  enum BOOLEAN_VALUE threadDebug)
{
    BaudotAudioFileData * data = (BaudotAudioFileData *)malloc(sizeof(BaudotAudioFileData));
    memset(data,0,sizeof(BaudotAudioFileData));

    data->options = options;
    data->samplerate = samplerate;
    data->baudotShiftState = BOOL_FALSE;
    data->filename = outputFilename;
    data->threadDebug = threadDebug;
    data->sampleData = membuf_init_blk(samplerate * sizeof(double));
    return data;
}

void process_BaudotAudioFileOutputPower(BaudotAudioFileOutputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent)
{
    BaudotAudioFileData * data = (BaudotAudioFileData *)hnd;

    if(!pwrState)
    {
        if(data->sampleData != NULL)
            membuf_reset(data->sampleData);

        if(data->wavdest != NULL)
        {
            wavdest_free(data->wavdest);
            data->wavdest=NULL;
        }
    }
    else
    {
        //Open the file, if needed
        if(data->wavdest == NULL)
        {
            data->wavdest = wavdest_init(data->filename, data->samplerate, data->options->outputScale, data->threadDebug);   
            if(data->wavdest == NULL)
            {
                printf("ERROR: Failed to open output wav destinaiton %s\r\n",data->filename);
            }
        }
    }
}

void process_freeBaudotAudioFileOutputHandle(BaudotAudioFileOutputHandle hnd)
{
    BaudotAudioFileData * data = (BaudotAudioFileData *)hnd;
    
    if(data->wavdest != NULL)
    {
        wavdest_free(data->wavdest);
        data->wavdest=NULL;
    }

    if(data->sampleData != NULL)
    {
        membuf_free(data->sampleData);
        data->sampleData=NULL;
    }

    free(data);
}

//=========================================================================================================
// Output processing
//=========================================================================================================

enum BOOLEAN_VALUE BD_CALLBACK process_OutputConsoleCallback(const char * str, void * param)
{
    const uint64_t * debugStartTick = (const uint64_t *)param;
    char acTickBuf[128];

    printf("%s\tSent:    %s\r\n",curtime_to_str(*debugStartTick, acTickBuf, sizeof(acTickBuf)), str);

    return BOOL_TRUE;
}


typedef struct _OutputProcessStepData
{
    OutputProcessCallback callback;
    void * param;
    struct _OutputProcessStepData * next;
    ProcessStepFreeCallback freecallback;
    ProcessWaitForOutputFinishCallback waitCallback;
    ProcessStepPowerCallback powerCallback;
    enum BOOLEAN_VALUE pwrState;
    enum BOOLEAN_VALUE callPowerCallback;
    enum BOOLEAN_VALUE debugPower;
} OutputProcessStepData;

OutputProcessStep process_add_output(OutputProcessStep first, OutputProcessCallback outputCallback, void * outputCallbackParam, ProcessWaitForOutputFinishCallback waitCallback, 
                                     ProcessStepPowerCallback pwrCallback, ProcessStepFreeCallback freeCallback)
{   
    OutputProcessStepData * firstdata = (OutputProcessStepData *)first;
    OutputProcessStepData * newEntry;

    //Find last entry
    OutputProcessStepData * prev = NULL;
    OutputProcessStepData * last = firstdata;
    while(last != NULL)
    {
        prev = last;
        last = last->next;
    }
    
    last = prev;

    //Create new entry
    newEntry = (OutputProcessStepData *)malloc(sizeof(OutputProcessStepData));
    newEntry->callback = outputCallback;
    newEntry->next = NULL;
    newEntry->param = outputCallbackParam;
    newEntry->freecallback = freeCallback;
    newEntry->waitCallback = waitCallback;
    newEntry->powerCallback = pwrCallback;
    newEntry->pwrState = BOOL_TRUE;
    newEntry->callPowerCallback = BOOL_TRUE;
    newEntry->debugPower = BOOL_FALSE;

    //Always return first in list
    if(prev != NULL)
        prev->next = newEntry;
    else
        firstdata = newEntry;


    return firstdata;
}

void process_free_output(OutputProcessStep first)
{
    OutputProcessStepData * entry = (OutputProcessStepData *)first;
    OutputProcessStepData * next;
    while(entry != NULL)
    {
        //Use the free callback to free the parameter data
        if(entry->freecallback != NULL)
            entry->freecallback(entry->param);


        next = entry->next;
        free(entry);
        entry = next;
    }
}

//=========================================================================================================
// Input processing
//=========================================================================================================

typedef struct _InputProcessStepData
{
    enum BOOLEAN_VALUE isAsync;
    enum BOOLEAN_VALUE debugPower;
    InputProcessCallback callback;
    void * param;
    struct _InputProcessStepData * next;
    ProcessStepFreeCallback freecallback;
    ProcessStepPowerCallback powerCallback;
    MemoryBuffer * inputReceiveBuffer;    
    ThreadHandle procEvent;
    enum BOOLEAN_VALUE pwrState;
    enum BOOLEAN_VALUE callPowerCallback;
} InputProcessStepData;

InputProcessStep process_add_input(InputProcessStep first, InputProcessCallback inputCallback, void * inputCallbackParam,  ProcessStepFreeCallback freeCallback, ProcessStepPowerCallback pwrCallback,
                                   enum BOOLEAN_VALUE isAsync)
{
    InputProcessStepData * firstdata = (InputProcessStepData *)first;
    InputProcessStepData * newEntry;
    
    //Find last entry
    InputProcessStepData * prev = NULL;
    InputProcessStepData * last = firstdata;
    while(last != NULL)
    {
        prev = last;
        last = last->next;
    }
    
    last = prev;

    
    //Create new entry
    newEntry = (InputProcessStepData *)malloc(sizeof(InputProcessStepData));
    newEntry->callback = inputCallback;
    newEntry->next = NULL;
    newEntry->param = inputCallbackParam;
    newEntry->freecallback = freeCallback;
    newEntry->isAsync = isAsync;
    newEntry->inputReceiveBuffer = membuf_init();
    newEntry->procEvent = NULL;
    newEntry->powerCallback = pwrCallback;
    newEntry->pwrState = BOOL_TRUE;
    newEntry->callPowerCallback = BOOL_TRUE;
    newEntry->debugPower = BOOL_FALSE;
    
    //Always return first in list
    if(prev != NULL)
        prev->next = newEntry;
    else
        firstdata = newEntry;


    return firstdata;
}

void process_free_input(InputProcessStep first)
{
    InputProcessStepData * entry = (InputProcessStepData *)first;
    InputProcessStepData * next;

    //The procEvent is actually the thread handle, so no need to free it
    while(entry != NULL)
    {
        //Use the free callback to free the parameter data
        if(entry->freecallback != NULL)
            entry->freecallback(entry->param);

        if(entry->inputReceiveBuffer != NULL)
        {
            membuf_free(entry->inputReceiveBuffer);
            entry->inputReceiveBuffer=NULL;
        }

        
        next = entry->next;
        free(entry);
        entry = next;
    }
}


//=========================================================================================================
//                     Main Processing
//=========================================================================================================

typedef struct _MEGAHAL_DATA
{
    MemoryBuffer * responseBuffer;
    enum BOOLEAN_VALUE debugProc;
    uint64_t debugStartTick;
}MEGAHAL_DATA;


static void BD_CALLBACK megahal_callback(int idx, const char *prefix, const char * reply, void * callbackParam)
{
    MEGAHAL_DATA * mhdata = (MEGAHAL_DATA *)callbackParam;
    const char * replyPos = reply;
    enum BOOLEAN_VALUE isStart = BOOL_TRUE;
    enum BOOLEAN_VALUE isEndPunc = BOOL_FALSE;
    enum BOOLEAN_VALUE wasNum = BOOL_FALSE;
    enum BOOLEAN_VALUE wasPunc = BOOL_FALSE;
    int isAlpha;
    int isNumeric;
    int isUpper;
    int isLower;
    char newChar;
    int bracketCount  = 0;
    int replyStartPos;
    char acTickBuf[128];

    if(mhdata->debugProc)
        printf("%s\tREPLY:          [%s]\r\n",curtime_to_str(mhdata->debugStartTick, acTickBuf, sizeof(acTickBuf)), reply);

    if(mhdata->responseBuffer == NULL)
        mhdata->responseBuffer = membuf_init();
    
    replyStartPos = membuf_getContentSize(mhdata->responseBuffer);

    //Parse the reply, making it a bit more readbale
    while(*replyPos != 0)
    {
        isAlpha = isalpha(*replyPos);
        isNumeric = isdigit(*replyPos);
        isUpper = isupper(*replyPos);
        isLower = islower(*replyPos);
        isEndPunc = ((*replyPos == '?') || (*replyPos == '.') || (*replyPos == '!')) ? BOOL_TRUE : BOOL_FALSE;

        if(*replyPos == '(')
        {
            membuf_append(mhdata->responseBuffer, replyPos, 1);
            ++bracketCount;
        }
        else if((*replyPos == ')') && (bracketCount > 0))
        {
            membuf_append(mhdata->responseBuffer, replyPos, 1);
            --bracketCount;
        }
        else if((*replyPos == ' ') && ((!isStart) || (wasPunc)))
        {
            membuf_append(mhdata->responseBuffer, replyPos, 1);
        }
        else if(isEndPunc)
        {
            if(!wasNum)
                isStart = BOOL_TRUE;

            membuf_append(mhdata->responseBuffer, replyPos, 1);
        }
        else if(isNumeric)
        {
            membuf_append(mhdata->responseBuffer, replyPos, 1);
        }
        else if(isAlpha && isStart)
        {
            if(isUpper)
                newChar = *replyPos;
            else
                newChar = (char)toupper(*replyPos);

            membuf_append(mhdata->responseBuffer, &newChar, 1);
            isStart = BOOL_FALSE;
        }
        else if(isAlpha && !isStart)
        {   
            if(isLower)
                newChar = *replyPos;
            else
                newChar = (char)tolower(*replyPos);

            membuf_append(mhdata->responseBuffer, &newChar, 1);
        }
        else if(!isAlpha && !isNumeric && (*replyPos > ' ') && (*replyPos <= 127))
        {
            membuf_append(mhdata->responseBuffer, replyPos, 1);
        }
        
        
        wasNum = isNumeric ? BOOL_TRUE : BOOL_FALSE;
        wasPunc = isEndPunc;
        ++replyPos;
    }

    //Complete brackets
    newChar = ')';
    while(bracketCount > 0)
    {
        membuf_append(mhdata->responseBuffer, &newChar, 1);
        --bracketCount;
    }

    //Was there an end?
    if(!isStart && !wasPunc)
    {
        newChar = '.';
        membuf_append(mhdata->responseBuffer, &newChar, 1);
    }

    //Better append a null terminator
    newChar = 0;
    membuf_append(mhdata->responseBuffer, &newChar, 1);
}

static enum BOOLEAN_VALUE process_run_internal(InputProcessStep first, OutputProcessStep outputSteps, struct _MEGAHAL * megahalHandle, const MegaHALOptions * megahalOpts, 
                                               enum BOOLEAN_VALUE showInput, enum BOOLEAN_VALUE showOutput, enum BOOLEAN_VALUE debugPower,
                                               int pollTimeMs, ThreadHandle threadHandle, GPIOHandle gpiohandle,  enum BOOLEAN_VALUE * quitRequest, 
                                                enum BOOLEAN_VALUE * saveRequest)
{
    enum ProcessStatus currentState = ProcessStatus_MoreToCome;
    enum ProcessStatus outState = ProcessStatus_Success;
    enum BOOLEAN_VALUE processRecvBuffer = BOOL_FALSE;
    enum BOOLEAN_VALUE keepInputStep = BOOL_FALSE;
    enum BOOLEAN_VALUE haveSyncStep = BOOL_FALSE;
    enum BOOLEAN_VALUE isError = BOOL_FALSE;
    enum BOOLEAN_VALUE isIdle = BOOL_FALSE;
    enum BOOLEAN_VALUE isOff = BOOL_FALSE;
    enum BOOLEAN_VALUE allOff = BOOL_FALSE;
    InputProcessStepData * firstEntry = (InputProcessStepData *)first;
    InputProcessStepData * entry = firstEntry;
    MEGAHAL_DATA megahalData;
    const char * output;
    size_t outputlen = 0;
    size_t curoutlen;
    OutputProcessStepData * outputEntry;
    const char zerochar = 0;
    int firstEncounters = 0;
    enum BOOLEAN_VALUE megahalNeedsSaving = BOOL_FALSE;
    int idleCount = 0;
    size_t mhSize;
    char acTickBuf[128];
    
    memset(&megahalData,0,sizeof(megahalData));
    megahalData.debugProc = (megahalOpts == NULL) ? BOOL_FALSE : megahalOpts->debugMegahal;
    megahalData.debugStartTick = (megahalHandle == NULL) ? 0 : megahalHandle->debugStartTick;

    while((entry != NULL) && (currentState != ProcessStatus_Error) && !isError && ((quitRequest == NULL) || (*quitRequest == BOOL_FALSE)))
    {
        //If entry has no proc event set, then do that now
        //This will allow power on to trigger us and wake the main processing loop up
        if(entry->procEvent == NULL)
        {
            entry->debugPower = debugPower;
            entry->procEvent = threadHandle;
        }

        //If entry has no callback, then it should not be called
        if(entry->callback == NULL) 
        {
            //Move onto the next entry
            entry = entry->next;
            if(entry == NULL)
            {
                ++firstEncounters;
                entry = firstEntry;
                haveSyncStep = BOOL_FALSE;
            }

            //If we're back at the first entry twice, then there are no valid entries, so exit
            if(firstEncounters > 2)
                break;

            //Otherwise check the next entry
            continue;
        }

        //A valid entry - reset first encounters check count
        firstEncounters = 0;

        //Remember if there are any valid syncronous steps left in the processing list
        if(!entry->isAsync)
            haveSyncStep = BOOL_TRUE;
        
        //Receive input into the input steps buffer 
        //If the entry if powered off, then don't call the callback
        if(!entry->pwrState)
        {
            currentState = ProcessStatus_MoreToCome;
            processRecvBuffer = BOOL_FALSE;
            keepInputStep = BOOL_TRUE;
            isIdle = BOOL_TRUE;
            isOff= BOOL_TRUE;
            haveSyncStep = BOOL_FALSE;

            if((entry->callPowerCallback) && (entry->powerCallback != NULL))
            {
                if(debugPower)
                    printf("Calling input power state callback OFF\r\n");

                entry->powerCallback(entry->param, BOOL_FALSE, threadHandle);
                entry->callPowerCallback = BOOL_FALSE;
            }
        }
        else
        {
            //At least one entry is still powered on
            if((entry->callPowerCallback) && (entry->powerCallback != NULL))
            {
                if(debugPower)
                    printf("Calling input power state callback ON\r\n");
                
                entry->powerCallback(entry->param, BOOL_TRUE,  threadHandle);
                entry->callPowerCallback = BOOL_FALSE;
            }

            allOff = BOOL_FALSE;
            isOff = BOOL_FALSE;
            currentState = entry->callback(entry->inputReceiveBuffer, entry->param);
            switch(currentState)
            {
                case ProcessStatus_MoreToCome: //Data has been added to the receive buffer, but is not yet finished - so don't process it yet
                    processRecvBuffer = BOOL_FALSE;
                    keepInputStep = BOOL_TRUE;
                    isIdle = BOOL_TRUE;
                    break;
                case ProcessStatus_Success:
                case ProcessStatus_ReadyToProcess: //Data has been added, and is ready to process
                    processRecvBuffer = BOOL_TRUE;
                    keepInputStep = BOOL_TRUE;
                    isIdle = BOOL_FALSE;
                    break;
                case ProcessStatus_NoMoreData: //No more data, and process what ever is remaining in the receive buffer
                    processRecvBuffer = BOOL_TRUE;
                    keepInputStep = BOOL_FALSE;
                    isIdle = BOOL_TRUE;
                    break;
        
                case ProcessStatus_AbortedByOutputCallback:
                case ProcessStatus_Error:   //Error encountered
                default:
                    isIdle = BOOL_FALSE;
                    processRecvBuffer = BOOL_FALSE;
                    keepInputStep = BOOL_FALSE;
                    isError= BOOL_TRUE;
                    break;
            }
        }

        //Are we idle or off?
        //If we've been idle for a while, then save the megahal brain - as we've nothing better to do
        //Also save if manually requested
        if(isIdle || isOff || (*saveRequest))
        {   
            ++idleCount;
            if((idleCount > 60) || (isOff) ||  (*saveRequest))
            {
                ///Check if megahal needs trimming to reduce memory usage. This is best to do here - while we're idle
                if(megahalHandle != NULL)
                {   
                    mhSize = mh_get_model_size(megahalHandle);

                    if(megahalData.debugProc)
                        printf("%s MegaHAL size=%uk (max is %uk)\r\n", curtime_to_str(megahalData.debugStartTick, acTickBuf, sizeof(acTickBuf)), mhSize / 1024, megahalOpts->maxBrainSize / 1024);
                    
                    if(mhSize >= megahalOpts->maxBrainSize)
                    {
                        //Signal trimming started
                        gpio_set(gpiohandle, GPIOFunction_MH_Trimming, GPIOFunction_MH_Trimming);

                        mh_trimbrain(megahalHandle, megahalOpts->maxBrainSize);
                        megahalNeedsSaving = BOOL_TRUE;

                        //Signal trimming stopped
                        gpio_set(gpiohandle, GPIOFunction_MH_Trimming, GPIOFunction_None);
                    }
                }


                //Check if megahal needs saving. This is best to do here - while we're idle
                if((megahalNeedsSaving ||  (*saveRequest)) && (megahalHandle != NULL))
                {  
                    //Signal saving started
                    gpio_set(gpiohandle, GPIOFunction_MH_Saving, GPIOFunction_MH_Saving);

                    mh_save(megahalHandle);
                    megahalNeedsSaving = BOOL_FALSE;
                    *saveRequest =  BOOL_FALSE;

                    //Signal saving stopped
                    gpio_set(gpiohandle, GPIOFunction_MH_Saving, GPIOFunction_None);
                }

                idleCount = 0;
            }
        }
        else
        {
            idleCount = 0;
        }

        //Should we process the receive buffer yet?
        if(processRecvBuffer)
        {
            //Add a null terminator, as the input receive buffer is now complete
            membuf_append(entry->inputReceiveBuffer, &zerochar, sizeof(zerochar));
            
            //By default, Use the receive buffer as the input text
            output = (const char *)membuf_ptr(entry->inputReceiveBuffer, 0);
            outputlen = membuf_getContentSize(entry->inputReceiveBuffer);

            //Check if there actually is anything to process
            if((output != NULL) && (outputlen > 0))
            {
                //Trim spaces from the start
                while((*output <= ' ') && (outputlen > 0))
                {
                    --outputlen;
                    ++output;
                }

                //If megahal is set up, then run the receive buffer through megahal
                if(megahalHandle != NULL)
                {
                    //Signal thinking
                    gpio_set(gpiohandle, GPIOFunction_MH_Thinking, GPIOFunction_MH_Thinking);

                    //Set up response buffer
                    if(megahalData.responseBuffer == NULL)
                        megahalData.responseBuffer = membuf_init();
                    else
                        membuf_erase(megahalData.responseBuffer);

                    if(showInput)
                        printf("%s Received %s (%u):\t[%s]\r\n",  curtime_to_str(megahalData.debugStartTick, acTickBuf, sizeof(acTickBuf)), megahalOpts->megahalLearn ? "& learn " : "        ",  outputlen, output);

                    //Pump the input into megahal
                    megahalHandle->timeout = megahalOpts->megaHALResponseTime;
                    mh_do_megahal(megahalHandle, 0, "", (char *)membuf_ptr(entry->inputReceiveBuffer, 0), megahalOpts->megahalLearn, megahal_callback, &megahalData);
                
                    //Use the output of megahal as the input text
                    output = (const char *)membuf_ptr(megahalData.responseBuffer, 0);
                    outputlen = membuf_getContentSize(megahalData.responseBuffer);

                    if(showOutput)
                        printf("%s\t-->:           [%s]\r\n", curtime_to_str(megahalData.debugStartTick, acTickBuf, sizeof(acTickBuf)), output);

                    //If learning mode is enabled, then the megahal brain has been updated and needs saving to disk
                    if(megahalOpts->megahalLearn)
                        megahalNeedsSaving = BOOL_TRUE;

                    //No longer thinking.....
                    gpio_set(gpiohandle, GPIOFunction_MH_Thinking, GPIOFunction_None);
                }
                else
                {
                    if(showInput)
                        printf("%s Received:\t[%s]\r\n", curtime_to_str(megahalData.debugStartTick, acTickBuf, sizeof(acTickBuf)), output);
                }

                //Process the output steps on the output string
                curoutlen = strlen(output);
                outputEntry = (OutputProcessStepData *)outputSteps;
                outState = ProcessStatus_Success;
                while((outputEntry != NULL) && (outState == ProcessStatus_Success))
                {
                    if((outputEntry->callPowerCallback) && (outputEntry->powerCallback != NULL))
                    {
                        outputEntry->debugPower = debugPower;

                        if(debugPower)
                            printf("Calling output power state callback %s\r\n", isOff ? "OFF" : "ON");

                        outputEntry->powerCallback(outputEntry->param, isOff ? BOOL_FALSE : BOOL_TRUE, threadHandle );
                        outputEntry->callPowerCallback = BOOL_FALSE;
                    }

                    if((outputEntry->pwrState) && (!outputEntry->callback( output, outputEntry->param)))
                    {
                        //Abort!
                        currentState = ProcessStatus_AbortedByOutputCallback;
                        isError = BOOL_TRUE;
                    }
                    else
                    {
                        //Move onto the next output step
                        outputEntry = outputEntry->next;
                    }
                }

                //Erase the receive buffer to start over
                membuf_erase(entry->inputReceiveBuffer);

                //Erase the megahal response buffer
                if(megahalData.responseBuffer != NULL)
                    membuf_erase(megahalData.responseBuffer);
            }
        }


        //Check for output call-back error
        if(outState == ProcessStatus_AbortedByOutputCallback)
            break;

        //If we're keeping the current input step, then don't free it yet!
        //This is so we can continue calling the input step to get the next chunk of data.
        if(!keepInputStep)
        {
            //Call the entry free routine
            //Do this now to avoid conflicts with previous steps (such as open file handles)
            if(entry->freecallback != NULL)
            {
                entry->freecallback(entry->param);

                //Set these to NULL to prevent re-freeing the already released memory
                entry->param= NULL;
                entry->freecallback = NULL;
            }

            //Set callback as null to mark it as done
            entry->callback = NULL;
        }
        else
        {
            //Move onto the next entry
            entry = entry->next;
            if(entry == NULL)
            {
                entry = firstEntry;

                //If we've reached the end of the list, and there are only asyncronous steps left
                //then wait for the processing event before continuing
                if(!haveSyncStep)
                {
                    if(allOff && (megahalData.debugProc || showInput || showOutput || debugPower))
                        printf("  Sleeping....\r\n");

                    //If off, then use an infinite (or very long) wait time
                    cppthrd_WaitForEvent(threadHandle, allOff ? 0x7FFFFFFF : pollTimeMs, BOOL_FALSE);
                    
                    if(allOff && (megahalData.debugProc || showInput || showOutput || debugPower))
                        printf("  Woken\r\n");
                }

                haveSyncStep = BOOL_FALSE;
                allOff = BOOL_TRUE; //We will loop and re-check to see if any inputs are powered on - if they are, this becomes false.
            }
        }
    }

    //Free response buffer
    if(megahalData.responseBuffer != NULL)
    {
        membuf_free(megahalData.responseBuffer);
        megahalData.responseBuffer=NULL;
    }

    return (isError) ? BOOL_FALSE : BOOL_TRUE;
}

typedef struct _ProcessThreadParams
{
    InputProcessStep inputSteps;
    OutputProcessStep outputSteps;
    struct _MEGAHAL * megahalHandle;
    const MegaHALOptions * megahalOpts;
    enum BOOLEAN_VALUE showInput;
    enum BOOLEAN_VALUE showOutput;
    int pollTimeMs;
    ThreadHandle procThread;
    enum BOOLEAN_VALUE * quitRequest;
    enum BOOLEAN_VALUE * saveRequest;
    enum BOOLEAN_VALUE * result;
    MemoryBuffer * inputBuffer;
    GPIOHandle gpiohandle;
    enum BOOLEAN_VALUE debugPower;
}ProcessThreadParams;

static void BD_CALLBACK processRunThread(void * param)
{
    ProcessThreadParams * params = (ProcessThreadParams *)param;

    *(params->result) = process_run_internal(params->inputSteps, params->outputSteps, params->megahalHandle, params->megahalOpts, 
                                             params->showInput, params->showOutput, params->debugPower, params->pollTimeMs, params->procThread, params->gpiohandle, params->quitRequest,
                                             params->saveRequest);

}

typedef struct _ProcessGPIOCallbackData
{
    MegaHALOptions * megahalOpts;
    InputProcessStep inputSteps;
    OutputProcessStep outputSteps;
    enum BOOLEAN_VALUE powerDebug;

} ProcessGPIOCallbackData;



void process_power(InputProcessStep inputSteps, OutputProcessStep outputSteps, enum BOOLEAN_VALUE newPowerState)
{
    ThreadHandle procEvt = NULL;
    InputProcessStepData * inentry = (InputProcessStepData *)inputSteps;
    OutputProcessStepData * outentry = (OutputProcessStepData *)outputSteps;
    enum BOOLEAN_VALUE powerChanged = BOOL_FALSE;

    while(inentry != NULL)
    {
        if(inentry->debugPower)
            printf("input entry %s to %s\r\n", inentry->pwrState ? "on" : "off",  newPowerState ? "on" : "off");
        
        powerChanged = (inentry->pwrState != newPowerState) ? BOOL_TRUE : BOOL_FALSE;
        inentry->pwrState = newPowerState;
        
        if((inentry->procEvent != NULL) && (procEvt == NULL))
            procEvt = inentry->procEvent;

        if(powerChanged)
            inentry->callPowerCallback = BOOL_TRUE;

        inentry = inentry->next;
    }

    while(outentry != NULL)
    {
        if(outentry->debugPower)
            printf("output entry %s to %s \r\n", outentry->pwrState ? "on" : "off", newPowerState ? "on" : "off");

        powerChanged = (outentry->pwrState != newPowerState) ? BOOL_TRUE : BOOL_FALSE;
        outentry->pwrState = newPowerState;

        if(powerChanged)
            outentry->callPowerCallback = BOOL_TRUE;

        outentry = outentry->next;
    }

    if(newPowerState && (procEvt != NULL))
        cppthrd_SignalThreadEvent(procEvt, BOOL_FALSE);
}


static void BD_CALLBACK process_gpioTriggerCallback(void * param,  int pin, enum GPIOFunction func, enum BOOLEAN_VALUE newState)
{
    ProcessGPIOCallbackData * gpioCallbackData = (ProcessGPIOCallbackData *)param;
    if((func & GPIOFunction_Input_Learning) == GPIOFunction_Input_Learning)
    {
        if(gpioCallbackData->megahalOpts != NULL)
        {
            gpioCallbackData->megahalOpts->megahalLearn = newState; 

            if(gpioCallbackData->powerDebug)
                printf(" GPIO : Megahal learning state is now %s \r\n", newState ? "enabled" : "disabled");
        }
    }
    else if((func & GPIOFunction_Input_Power) == GPIOFunction_Input_Power)
    {
        if(gpioCallbackData->powerDebug)
            printf(" GPIO : Power %s\r\n", newState ? "ON" : "OFF");
        
        process_power( gpioCallbackData->inputSteps, gpioCallbackData->outputSteps, newState);
    }
}


enum BOOLEAN_VALUE process_run(InputProcessStep inputSteps, OutputProcessStep outputSteps, struct _MEGAHAL * megahalHandle, MegaHALOptions * megahalOpts, 
                                enum BOOLEAN_VALUE showInput, enum BOOLEAN_VALUE showOutput, int pollTimeMs, enum BOOLEAN_VALUE consolemode, enum BOOLEAN_VALUE debugThreads, enum BOOLEAN_VALUE debugPower,
                                GPIOHandle gpiohandle)
{
    ProcessThreadParams params;
    ProcessGPIOCallbackData gpioCallbackData;
    ThreadHandle threadHnd;
    enum BOOLEAN_VALUE quitFlag = BOOL_FALSE;
    enum BOOLEAN_VALUE saveFlag = BOOL_FALSE;
    enum BOOLEAN_VALUE result = BOOL_FALSE;
    enum BOOLEAN_VALUE showConsoleHelp = BOOL_TRUE;
    MemoryBuffer * currentCmd;
    MemoryBuffer * inputBuffer;
    int inputch;
    const char * cmd;
    OutputProcessStepData * outputEntry;
    enum BOOLEAN_VALUE ret;

    if(!consolemode)
    {
        //Create proc event - used by everything to notifiy the processer
        ThreadHandle procEvent = cppthrd_CreateLockOnly();

        //First set up gpio input triggers
        if(gpiohandle != NULL)
        {
            gpioCallbackData.megahalOpts = megahalOpts;
            gpioCallbackData.inputSteps = inputSteps;
            gpioCallbackData.outputSteps = outputSteps;
            gpioCallbackData.powerDebug = debugPower;
            gpio_setInputTriggerCallback(gpiohandle, process_gpioTriggerCallback, &gpioCallbackData);
        }

        ret = process_run_internal(inputSteps,outputSteps, megahalHandle, megahalOpts, showInput, showOutput, debugPower, pollTimeMs, procEvent, gpiohandle, NULL, &saveFlag);
        
        //Wait for outputs to finish
        outputEntry = (OutputProcessStepData *)outputSteps; 
        while(outputEntry != NULL)
        {
            if(outputEntry->waitCallback != NULL)
                outputEntry->waitCallback(outputEntry->param);
            
            outputEntry = outputEntry->next;
        }

        cppthrd_WaitAndDestroyThread(procEvent);

        return ret;
    }

    currentCmd = membuf_init();
    inputBuffer = membuf_init();

    params.megahalHandle = megahalHandle;
    params.megahalOpts = megahalOpts;
    params.outputSteps = outputSteps;
    params.pollTimeMs = pollTimeMs;
    params.showInput = showInput;
    params.showOutput = showOutput;
    params.quitRequest = &quitFlag;
    params.saveRequest = &saveFlag;
    threadHnd = cppthrd_CreateThread( &params, processRunThread, debugThreads);
    params.procThread = threadHnd;
    params.result = &result;
    params.inputBuffer = inputBuffer;
    params.gpiohandle = gpiohandle;
    params.debugPower = debugPower;

    //Add console input buffer as an extra input step
    params.inputSteps = process_add_input( inputSteps, process_StringCallback, process_initBufferInputHandle(inputBuffer, threadHnd), process_freeStringInputHandle, process_StringInputPower, BOOL_TRUE);
    
    //Now set up gpio input triggers
    if(gpiohandle != NULL)
    {
        gpioCallbackData.megahalOpts = megahalOpts;
        gpioCallbackData.inputSteps = params.inputSteps;
        gpioCallbackData.outputSteps = outputSteps;
        gpioCallbackData.powerDebug = debugPower;
        gpio_setInputTriggerCallback(gpiohandle, process_gpioTriggerCallback, &gpioCallbackData);
    }

    //Start thread
    cppthrd_StartThread(threadHnd, BOOL_FALSE);

    //Do console stuff

    while(quitFlag == BOOL_FALSE)
    {
        inputch = getchar();
        if(inputch == '\b')
        {
            membuf_removeFromBack(currentCmd,1);
        }
        else if(inputch >= ' ')
        {
            membuf_append(currentCmd, &inputch, 1);
        }
        else if(inputch == '\n')
        {
            inputch=0;
            membuf_append(currentCmd, &inputch, 1);
            cmd = (const char *)membuf_ptr(currentCmd, 0);
            
            if(strstr(cmd,"#quit") == cmd)
            {
                //Flag to the thread to quit
                quitFlag = BOOL_TRUE;
            }
            else if(strstr(cmd,"#learn") == cmd)
            {
                megahalOpts->megahalLearn = megahalOpts->megahalLearn ? BOOL_FALSE : BOOL_TRUE;
                printf("\tMegahal learning turned %s\r\n", megahalOpts->megahalLearn ? "on" : "off");
            }
            else if(strstr(cmd,"#off") == cmd)
            {
                process_power( params.inputSteps,  params.outputSteps, BOOL_FALSE);
            }
            else if(strstr(cmd,"#on") == cmd)
            {
                process_power( params.inputSteps,  params.outputSteps, BOOL_TRUE);
            }
            else if(strstr(cmd,"#save") == cmd)
            {
                printf("\tMegahal save requested\r\n");
                saveFlag = BOOL_TRUE;
            }            
            else if(cmd[0] != '#')
            {
                //Lock thread 
                cppthrd_LockThread(threadHnd);

                //Add to buffer
                membuf_append(inputBuffer, membuf_ptr(currentCmd,0), membuf_getContentSize(currentCmd));

                //Signal thread
                cppthrd_SignalThreadEvent(threadHnd, BOOL_TRUE);

                //Unlock thread
                cppthrd_UnlockThread(threadHnd);
            }

            //Erase the input
            membuf_erase(currentCmd);
        }
    }
    
    //Signal thread
    cppthrd_SignalThreadEvent(threadHnd, BOOL_FALSE);

    //Wait for thread, and destroy it
    cppthrd_WaitAndDestroyThread(threadHnd);

    //Wait for outputs to finish
    outputEntry = (OutputProcessStepData *)outputSteps; 
    while(outputEntry != NULL)
    {
        if(outputEntry->waitCallback != NULL)
            outputEntry->waitCallback(outputEntry->param);
        
        outputEntry = outputEntry->next;
    }

    //Free other buffers. This is now safe to do as the processing thread has quit by this point
    membuf_free(currentCmd);
    membuf_free(inputBuffer);
    
    return result;
}

