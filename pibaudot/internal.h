#ifndef PIBAUDOT__INTERNAL__H
#define PIBAUDOT__INTERNAL__H
//========================================================
//Types
//========================================================


#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

enum BOOLEAN_VALUE
{
    BOOL_FALSE = 0,
    BOOL_TRUE = 1,
};

#if defined _X86_  || defined _M_IX86
#define BD_CALLBACK __cdecl
#else
#define BD_CALLBACK
#endif

#ifdef WIN32
#define INLINE static _inline
__declspec(dllimport) unsigned long long __stdcall GetTickCount64();
#else
#define INLINE static inline
    #include <time.h>
#endif

struct _MEGAHAL;



INLINE const char * curtime_to_str(unsigned long long startTick, char * buffer, size_t bufsz)
{
    uint64_t 

    #ifdef _WIN32
        curtick = GetTickCount64();
    #else
        curtick = 0;
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        curtick =  (((unsigned long long)ts.tv_sec)*1000ULL) +  (((unsigned long long)ts.tv_nsec)/1000000ULL);
    #endif

    #ifdef WIN32
        _snprintf(buffer, bufsz, "%llu", curtick - startTick);
    #else
        snprintf(buffer, bufsz, "%llu", curtick - startTick);
    #endif

    return buffer;
}

//========================================================
//Memory buffer
//========================================================
typedef struct _MemoryBuffer
{
    void * ptr;
    size_t maxsize;
    size_t contentsz;
    size_t blksize;
} MemoryBuffer;

MemoryBuffer * membuf_init();

MemoryBuffer * membuf_init_blk(size_t blksize);

void membuf_free(MemoryBuffer * buf);

void membuf_append(MemoryBuffer * buf, const void * data, size_t amount);

void membuf_append_value(MemoryBuffer * buf, const void * valueptr, size_t valuesize, size_t repeatCount);

INLINE uint8_t * membuf_ptr(const MemoryBuffer * buf, size_t offset) { return ((buf->ptr == NULL) || (offset >= buf->contentsz)) ? 0 : (((uint8_t *)buf->ptr) + offset); }

void membuf_erase(MemoryBuffer * buf);

void membuf_reset(MemoryBuffer * buf);

INLINE size_t membuf_getContentSize(MemoryBuffer * buf) { return (buf->ptr == NULL) ? 0 : buf->contentsz; }

size_t membuf_fread_append(MemoryBuffer * buf, FILE * file, size_t maxAmountToRead);

size_t membuf_copyToExternalBuffer(const MemoryBuffer * buf, void * dest, size_t offset,  size_t amount);

void membuf_removeFromFront(MemoryBuffer * buf, size_t amount);

void membuf_append_copy(MemoryBuffer * dest, const MemoryBuffer * src);

void membuf_removeFromBack(MemoryBuffer * buf, size_t amount);

uint8_t * membuf_reserveSpaceForNewData(MemoryBuffer * buf, size_t amount);

//========================================================
//CPP Threading
//========================================================
typedef void * ThreadHandle;

typedef void (BD_CALLBACK *CPPThreadCallback)(void * param);

ThreadHandle cppthrd_CreateLockOnly();

ThreadHandle cppthrd_CreateThread(void * param, CPPThreadCallback threadProc, enum BOOLEAN_VALUE debug);

void cppthrd_StartThread(ThreadHandle threadhnd, enum BOOLEAN_VALUE isLocked);

void cppthrd_WaitAndDestroyThread(ThreadHandle threadhnd);

void cppthrd_SignalThreadEvent(ThreadHandle threadhnd, enum BOOLEAN_VALUE isLocked);

enum BOOLEAN_VALUE cppthrd_WaitForEvent(ThreadHandle threadhnd, unsigned int timeoutms, enum BOOLEAN_VALUE isLocked);

void cppthrd_LockThread(ThreadHandle threadhnd);

void cppthrd_UnlockThread(ThreadHandle threadhnd);

void cppThread_sleep_ms(int milliseconds);

//========================================================
//Wav header parser
//========================================================

enum Format
{
    FORMAT_INVALID = 0,
    FORMAT_WAVE = 1,
    FORMAT_WAVE64 = 2,
    FORMAT_RF64 = 3
};

/*
* Format of PCM data
*/
enum WAV_FORMAT
{
    WAV_FORMAT_PCM,
    WAV_FORMAT_ALAW,
    WAV_FORMAT_ULAW
};


typedef struct _SampleInfo
{
    enum Format fmt;
    enum WAV_FORMAT wvformat;
    uint32_t sample_rate;
    uint32_t channels;
    uint32_t bits_per_sample; /* width of sample point, including 'shift' bits, valid bps is bits_per_sample-shift */
    uint32_t shift; /* # of LSBs samples have been shifted left by */
    uint32_t bytes_per_wide_sample; /* for convenience, always == channels*((bps+7)/8), or 0 if N/A to input format (like FLAC) */
    enum BOOLEAN_VALUE is_unsigned_samples;
    enum BOOLEAN_VALUE is_big_endian;
    uint32_t channel_mask;
    uint64_t dataPos;
    uint64_t dataSize;
} SampleInfo;


enum BOOLEAN_VALUE get_sample_info_wave(const unsigned char * dataptr, uint64_t dataSize, SampleInfo * outInfo);


//======================================================
//GPIO functionality
//======================================================

enum GPIOFunction
{
    GPIOFunction_None        = 0,
    
    GPIOFunction_MH_Saving   = (1 << 1),
    GPIOFunction_MH_Trimming = (1 << 2),
    GPIOFunction_MH_Thinking = (1 << 3),

    GPIOFunction_Sending            = (1 << 4),
    GPIOFunction_Tx_WakeUp          = (1 << 5),
    GPIOFunction_Tx_Data            = (1 << 6),
    GPIOFunction_Tx_BitGap          = (1 << 7),
    GPIOFunction_Tx_DataValueLow    = (1 << 8),
    GPIOFunction_Tx_DataValueHigh   = (1 << 9),
    
    GPIOFunction_Recording          = (1 << 10),
    GPIOFunction_Rx_Silence         = (1 << 11),
    GPIOFunction_Rx_Searching       = (1 << 12),
    GPIOFunction_Rx_Data            = (1 << 13),
    GPIOFunction_Rx_DataValueLow    = (1 << 14),
    GPIOFunction_Rx_DataValueHigh   = (1 << 15),
    GPIOFunction_Rx_Padding         = (1 << 16),
    GPIOFunction_Rx_EndOfBlock      = (1 << 17),
    GPIOFunction_Rx_Clipping        = (1 << 18),

    GPIOFunction_Input_Power        = (1 << 30),
    GPIOFunction_Input_Learning     = (1 << 31),

    GPIOFunction_All_Input_Functions = GPIOFunction_Input_Learning | GPIOFunction_Input_Power,

    GPIOFunction_Tx_All = (GPIOFunction_Tx_WakeUp | GPIOFunction_Tx_Data | GPIOFunction_Sending | GPIOFunction_Tx_BitGap | GPIOFunction_Tx_DataValueLow | GPIOFunction_Tx_DataValueHigh),
    GPIOFunction_Tx_Sending_Wait = (GPIOFunction_Sending | GPIOFunction_Tx_WakeUp),
    GPIOFunction_Tx_Sending_Data = (GPIOFunction_Sending | GPIOFunction_Tx_Data),
    GPIOFunction_Tx_Sending_Data_Low = (GPIOFunction_Sending | GPIOFunction_Tx_Data | GPIOFunction_Tx_DataValueLow),
    GPIOFunction_Tx_Sending_Data_High = (GPIOFunction_Sending | GPIOFunction_Tx_Data | GPIOFunction_Tx_DataValueHigh),
    GPIOFunction_Tx_Sending_Gap = (GPIOFunction_Sending | GPIOFunction_Tx_BitGap),
    
    GPIOFunction_Rx_Volume = GPIOFunction_Rx_Clipping | GPIOFunction_Rx_Silence,
    GPIOFunction_Rx_Start = (GPIOFunction_Recording),
    GPIOFunction_Rx_EndOfDataBlk = (GPIOFunction_Recording | GPIOFunction_Rx_EndOfBlock),
    GPIOFunction_Rx_Finding_Data = (GPIOFunction_Recording | GPIOFunction_Rx_Searching),
    GPIOFunction_Rx_Receiving_Data = (GPIOFunction_Recording | GPIOFunction_Rx_Data),
    GPIOFunction_Rx_Receiving_Padding = (GPIOFunction_Recording | GPIOFunction_Rx_Padding),
    GPIOFunction_Rx_Receiving_Data_Low = (GPIOFunction_Recording | GPIOFunction_Rx_Data | GPIOFunction_Rx_DataValueLow),
    GPIOFunction_Rx_Receiving_Data_High = (GPIOFunction_Recording | GPIOFunction_Rx_Data | GPIOFunction_Rx_DataValueHigh),
    GPIOFunction_Rx_Receiving_Data_Transition = (GPIOFunction_Recording | GPIOFunction_Rx_Data | GPIOFunction_Rx_DataValueHigh | GPIOFunction_Rx_DataValueLow),
    GPIOFunction_Rx_All = (GPIOFunction_Recording | GPIOFunction_Rx_Data | GPIOFunction_Rx_EndOfBlock | GPIOFunction_Rx_Searching | GPIOFunction_Rx_DataValueHigh | GPIOFunction_Rx_DataValueLow),
};

typedef void * GPIOHandle;

typedef void (BD_CALLBACK * GPIOInputTriggerEventCallback)(void * param,  int pin, enum GPIOFunction func, enum BOOLEAN_VALUE newState);

GPIOHandle gpio_init(enum BOOLEAN_VALUE debugThread);

void gpio_free(GPIOHandle gpiohnd);

enum BOOLEAN_VALUE gpio_addConfig(GPIOHandle gpiohnd, enum GPIOFunction func, int pin);

void gpio_set( GPIOHandle gpiohnd, enum GPIOFunction mask, enum GPIOFunction values );

void gpio_setInputTriggerCallback(GPIOHandle gpiohnd, GPIOInputTriggerEventCallback callback, void * param);

//========================================================
//Sound Recording (ALSA / Windows)
//========================================================


typedef struct _SoundDevice
{
    unsigned int device;
    unsigned int channels;
    unsigned int sample_rate;
    unsigned int bitsPerSampleValue;
}SoundDevice;


typedef enum BOOLEAN_VALUE (BD_CALLBACK *HaveRecordSoundCallback)(unsigned int sampleRate, unsigned int channels, unsigned int bitsPerSec, const uint8_t * sampledata, size_t sampleDataSize, void * param);

typedef void * SoundRecData;

SoundRecData soundrec_Start(unsigned int device, unsigned int channels, unsigned int sample_rate, unsigned int bitsPerSample, size_t bufferSizeMs,  HaveRecordSoundCallback callback, void * callbackparam,
                            enum BOOLEAN_VALUE debugThread, enum BOOLEAN_VALUE debugRecord);

void soundrec_Stop(SoundRecData sndrec);

void soundrec_PrintDevices();

typedef void * SoundPlaybackData;

SoundPlaybackData soundplay_init(unsigned int device, unsigned int channels, unsigned int sample_rate, unsigned int bitsPerSample, unsigned int bufferSizeMs, 
                                 enum BOOLEAN_VALUE debugThread, enum BOOLEAN_VALUE debugPlayback, const char * playbackWriteAudioFilename, GPIOHandle gpiohandle, 
                                 unsigned int wakeUpLengthMs, unsigned int dataBitLengthMs, unsigned int dataGapLengthMs);

void soundplay_deinit(SoundPlaybackData data);

void soundplay_play(SoundPlaybackData playdata, const double * samples, size_t numSamples, MemoryBuffer * gpioBitSequence);

void soundplay_wait(SoundPlaybackData playdata);

//========================================================
//goertzel - Copyright (C) 2006 Exstrom Laboratories LLC - https://exstrom.com/journal/sigproc/goertzel.c
//========================================================

/*
    Nf = number of frequency  points to calculate FT
    f1 = starting frequency [Hz]
    f2 = ending frequency [Hz]
    sps = samples per second
    q = array where samples are stored
    N = number of samples to read from the input
    
    Returns the frequncy bucket indes of the loudest frequency
*/
double goertzel_process_buffer(int Nf, double f1, double f2, double sps, const double * q, int N, double * retQr);

//======================================================
//Sample handling Utilities
//======================================================


/**
* Seek backwards from 'currentOffset', to the first silent sample value. 
* Any *absolute* sample value that is lower than the specified 'silenceThreshold' is considered as silence
*
*   sampleData                  = Entire sample buffer
*   numSampleValuesInBuffer     = Total number of sample values in the buffer
*   currentOffset               = Current position in sample buffer. 
*                                   This should be somewhere in a noisy section, where this function will then seek back to the end of the silent section.
*   searchEnd                   = Don't seek backwards beyond this point - Prevents crossing into previously processed sample data
*   silenceThreshold            = Used to detect end of silence section. Any absolute sample value lower than this is to be considered as silence
*
* Returns the absolute sample position (from the start of the buffer) of where the silent sample value was found
*/
size_t smputil_seekBackToSilenceEnd(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset, size_t searchEnd, double silenceThreshold);

/**
* Seek forwards from 'currentOffset', to the first non-silent sample.
* Any *absolute* sample value that is lower than the specified 'silenceThreshold' is considered as silence
*
*   sampleData                  = Entire sample buffer
*   numSampleValuesInBuffer     = Total number of sample values in the buffer
*   currentOffset               = Current position in sample buffer. 
*                                       This should be somewhere in a silent section, where this function will then seek forwards to the start of the non-silent section.
*   silenceThreshold            = Used to detect end of silence section. Any absolute sample value lower than this is to be considered as silence
*   retNonSilencePos            = On success, returns the absolute sample position (from the start of the buffer) of where the next non-silent sample value was found
* 
* Returns BOOL_TRUE if successful, BOOL_FALSE if there wasnt' any non-silence to be found. 
*/
enum BOOLEAN_VALUE smputil_findNextNonSilence(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset, double silenceThreshold, size_t * retNonSilencePos );

/**
* Find the zero point of the waveform where the sample frame should end
*
*   sampleData                  = Entire sample buffer
*   numSampleValuesInBuffer     = Total number of sample values in the buffer
*   currentOffset               = Current position in sample buffer. 
*   numValuesInWindow           = Size of the window, in sample values
*   zeroThreshold               = Any absolute sample value below this is considered to be zero, and indicates the frame edge
*
* Returns the absolute sample position (from the start of the buffer) of where the frame edge was found
*/
size_t smputil_findWindowEndEdge(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset,  size_t numValuesInWindow,  double silenceThreshold );

/**
* Find the zero point of the waveform where the sample frame should start
*
*   sampleData                  = Entire sample buffer
*   numSampleValuesInBuffer     = Total number of sample values in the buffer
*   currentOffset               = Current position in sample buffer. 
*   numValuesInWindow           = Size of the window, in sample values
*   zeroThreshold               = Any absolute sample value below this is considered to be zero, and indicates the frame edge
*
* Returns the absolute sample position (from the start of the buffer) of where the frame edge was found
*/
size_t smputil_findWindowStartEdge(const double * sampleData, size_t numSampleValuesInBuffer, size_t currentOffset,  size_t numValuesInWindow,  double zeroThreshold );

/**
* Convert the specified sample buffer into mono doubles
*
* Multiple channels are merged to mono.
*
*   sampleBuffer                = The input sample buffer
*   dataSizeBytes               = Size of data in input buffer
*   channels                    = How many channels does the input buffer represent (stereo/mono)
*   sampleRate                  = Sample rate, in hz
*   bitsPerSample               = Number of bits per single-channel sample value
*   dest                        = Where to write the destination
*   maxDestValues               = Maximum size, in doubles, of destination buffer
*   retNumInputBytesProcessed   = Returns the number of input bytes that have been processed
*
* Returns the number of doubles written to 'dest'
*/

size_t smputil_convertSampleBytesToDoubles(const uint8_t * sampleBuffer, size_t dataSizeBytes, enum WAV_FORMAT format, unsigned int channels, unsigned int sampleRate, unsigned int bitsPerSample, 
                                           double silenceThreshold, MemoryBuffer * destBuffer, enum BOOLEAN_VALUE * retClipping, enum BOOLEAN_VALUE * retSilence);


size_t smputil_convertDoublesToSamples(const double * sampleDataToWrite, size_t numSamplesToWrite, unsigned int bitsPerSample, unsigned char * output, size_t maxOutputBytes);

size_t smputil_convertDoublesToSampleBytes(const double * sampleData, size_t numSamplesToWrite, unsigned int channels, unsigned int numBitsPerSample, double scaleFactor, uint8_t * outputBuffer, size_t maxOutputSizeBytes);

void smputil_generateToneDoubles(unsigned int freq, unsigned int durationMs,  unsigned int samplesPerSec, double scale, MemoryBuffer * output);

//======================================================
//Baudot decoder
//======================================================

//Various options to configure the decoder
typedef struct _BauDot_Decoder_Options
{
    enum BOOLEAN_VALUE baudotDebug;
    enum BOOLEAN_VALUE baudotTediousDebug;

    //Baudot decode options
    unsigned int startFreq;   //Ignore frequencies below this value (hz)
    unsigned int endFreq;     //Ignore frequencies Above this value (hz)
    unsigned int highFreq;    //This is the frequency that represents a 1 (hz)
    unsigned int lowFreq;     //This is the frequency that represents a 0 (hz)
    double silenceThreshold;  //When the loudest frequency is found, and it is less than this value, then we have silence.

    double bitLengthMs; //Length of a bit, in milliseconds
    unsigned int syncSizeBits; //Number of start bits used for sync at start (hint: 1)
    unsigned int valueSizeBits; //Size of a character - in bits (hint: 5)
    unsigned int paddingSizeBits; //Number of bits used for padding at end (hint: 2)
    

    unsigned int bufferSizeMs;   //Block size to read in one go. Increases real-time latency, decreases IO time
    unsigned int windowSizeMs;   //Size of goertzel window
    unsigned int freqBlockSize;  //Number of frequencies to 'bundle' together into a block - the more blocks, the more accurate analysis is, and the more processing time there will be.
    unsigned int freqErrorHz;      //When checking if the high or low (1 or 0) frequency is present, allow this much error (in hz) during the checking.
    
    unsigned int silenceBreakMs;    //Minimum amount of silence to consider as a break in input

    //Playback options
    double outputScale;
    unsigned int playWakeDurationMs;
    unsigned int playWakeFreq;
    unsigned int charGapDurationMs;
    unsigned int charGapFreq;

    const char * decoderOutAudioFile; //If non-null, the full path of where to save audio that has been passed to the baudot decoder
    
} BauDot_Decoder_Options;

/*
* Callback used called by the decoder when it receives and decodes a single character
*
*   value       = The value decoded.  This may be certain special values
*                   0 = control code received (such as switch between letters and numbers)

*   rawValue    = The raw baudot value
*   param       = Callback parameter
*/
typedef enum BOOLEAN_VALUE(BD_CALLBACK *CharacterReceivedCallback)(char value, uint8_t rawvalue, void * param);


typedef void * BaudotHandle;

BaudotHandle baudot_init(const BauDot_Decoder_Options * opts, enum WAV_FORMAT format,  unsigned int sampleRate, unsigned int channels, unsigned int bitsPerSample, 
                         CharacterReceivedCallback receiveCallaback,void * recvCallbackParam,
                         GPIOHandle gpioHandle);

void baudot_free(BaudotHandle handle);

enum BaudotProcessState
{
    BaudotProcessState_Error,           //Something went wrong
    BaudotProcessState_NeedMoreData,    //Need more data
    BaudotProcessState_Break,           //End of audio block
};

/*
 * Use the supplied 'readCallback' to fetch sample data as doubles.  Then detect and decode thost sample values as baudot values
 *
 * This only supports the ITA2 standard
 *
 *  - handle                = The internal state data. Returned from a successful call to baudot_init() 
 *  - sampleData            = Raw sample data bytes to process
 *  - numBytes              = Number of bytes of input data
 *  - writeWavDest          = Handle to wav dest writer. Can be NULL to not write processed audio to disk.
 *
 * Returns BaudotProcessState - based on current processing state
*/
enum BaudotProcessState  baudot_process_samples(BaudotHandle handle, const unsigned char * sampleData, size_t numBytes, /*WAVDESTHND*/ void * writeWavDest);


void baudot_encode_string(const BauDot_Decoder_Options * opts, int sampleRate, double scale, const char * str, enum BOOLEAN_VALUE * currentShiftState, 
                          MemoryBuffer * outputBuffer, MemoryBuffer * outBitSeqBuffer);


//======================================================
//Wav destination writer
//======================================================

typedef void * WAVDESTHND;

WAVDESTHND wavdest_init(const char * filename, int sampleRate, double scaleFactor, enum BOOLEAN_VALUE threadDebug);

void wavdest_write(WAVDESTHND hnd, const double * data, size_t numSampleValues);

void wavdest_wait(WAVDESTHND hnd);

void wavdest_free(WAVDESTHND hnd);

//======================================================
//Main Processor
//======================================================
typedef struct _MegaHALOptions
{   
    //Megehal options
    int megaHALResponseTime;
    enum BOOLEAN_VALUE megahalLearn;
    unsigned int maxBrainSize;
    enum BOOLEAN_VALUE debugMegahal;
} MegaHALOptions;

enum ProcessStatus
{
    ProcessStatus_Error,                //Something went wrong
    ProcessStatus_Success,              //Success (input = ProcessStatus_ReadyToProcess, output = no error)
    ProcessStatus_MoreToCome,           //Data added, and possibly more
    ProcessStatus_ReadyToProcess,       //Break in input - not the end, but process what you have as a complete block of text
    ProcessStatus_NoMoreData,           //Ran out of input - process it, and never call the process step again 
    ProcessStatus_AbortedByOutputCallback //Ouput callback encountered and error and returned FALSE
};


typedef enum ProcessStatus (BD_CALLBACK *InputProcessCallback)(MemoryBuffer * recvBuffer, void * param);
typedef enum BOOLEAN_VALUE (BD_CALLBACK *OutputProcessCallback)(const char * outputStr, void * param);

//--------------------------------------------------------
//Process input from audio capture
typedef void * AudioCaptureInputHandle;

AudioCaptureInputHandle process_initAudioCaptureHandle(const BauDot_Decoder_Options * processOptions, const SoundDevice * device, 
                                                       enum BOOLEAN_VALUE threadDebug, enum BOOLEAN_VALUE captureDebug,  enum BOOLEAN_VALUE tediousCaptureDebug, enum BOOLEAN_VALUE powerDebug, 
                                                       GPIOHandle gpiohandle);

void process_AudioCaptureInputPower(AudioCaptureInputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent);

void process_freeAudioCaptureHandle(AudioCaptureInputHandle hnd);

enum ProcessStatus BD_CALLBACK process_InputAudioCaptureCallback(MemoryBuffer * recvBuffer, void * param);

//--------------------------------------------------------
//Process input from audio file
typedef void * AudioFileInputHandle;

AudioFileInputHandle process_initAudioFileInputHandle(const char * filename, const BauDot_Decoder_Options * processOptions);

void process_AudioFileInputPower(AudioFileInputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent);

void process_freeAudioFileInputHandle(AudioFileInputHandle hnd);

enum ProcessStatus BD_CALLBACK process_InputAudioCallback(MemoryBuffer * recvBuffer,  void * param);

//--------------------------------------------------------
//Process input from string
typedef void * StringInputHandle;

StringInputHandle process_initStringInputHandle(const char * str);
void process_freeStringInputHandle(StringInputHandle hnd);
void process_StringInputPower(StringInputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent);

enum ProcessStatus BD_CALLBACK process_StringCallback(MemoryBuffer * recvBuffer, void * param);


//--------------------------------------------------------
//Output via console

enum BOOLEAN_VALUE BD_CALLBACK process_OutputConsoleCallback(const char * str, void * param);

//--------------------------------------------------------
//Output via baudot audio file

typedef void * BaudotAudioFileOutputHandle;

BaudotAudioFileOutputHandle process_initBaudotAudioFileOutputHandle(const BauDot_Decoder_Options * options, const char * outputFilename, int samplerate,  enum BOOLEAN_VALUE threadDebug);

void process_BaudotAudioFileOutputPower(BaudotAudioFileOutputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent);

void process_freeBaudotAudioFileOutputHandle(BaudotAudioFileOutputHandle hnd);

enum BOOLEAN_VALUE BD_CALLBACK process_OutputBaudotAudioCallback(const char * str, void * param);

void BD_CALLBACK process_WaitForOutputBaudotAudioCallback(void * param);

//--------------------------------------------------------
//Output via baudot audio file to audio device

typedef void * BaudotAudioPlaybackHandle;

enum BOOLEAN_VALUE BD_CALLBACK process_OutputBaudotAudioPlaybackCallback(const char * str, void * param);

BaudotAudioPlaybackHandle process_initBaudotAudioPlaybackOutputHandle(const BauDot_Decoder_Options * options,   const SoundDevice * soundDevice,  
                                                                      enum BOOLEAN_VALUE threadDebug, enum BOOLEAN_VALUE playbackDebug, enum BOOLEAN_VALUE powerDebug,
                                                                      const char ** playbackWriteAudioFilename, GPIOHandle gpiohandle);

void process_BaudotAudioPlaybackPower(BaudotAudioFileOutputHandle hnd, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent);

void process_freeBaudotAudioPlaybackHandle(BaudotAudioPlaybackHandle hnd);

void BD_CALLBACK process_WaitForBaudotAudioPlaybackOutputCallback(void * param);

//--------------------------------------------------------

typedef void * InputProcessStep;
typedef void * OutputProcessStep;

typedef void (BD_CALLBACK * ProcessStepFreeCallback)(void * param);
typedef void (BD_CALLBACK * ProcessWaitForOutputFinishCallback)(void * param);
typedef void (BD_CALLBACK * ProcessStepPowerCallback)(void * param, enum BOOLEAN_VALUE pwrState, ThreadHandle notifyEvent);


InputProcessStep process_add_input(InputProcessStep first, InputProcessCallback inputCallback, void * inputCallbackParam, ProcessStepFreeCallback freeCallback, ProcessStepPowerCallback pwrCallback,
                                   enum BOOLEAN_VALUE isAsync);

void process_free_input(InputProcessStep first);


OutputProcessStep process_add_output(OutputProcessStep first, OutputProcessCallback outputCallback, void * outputCallbackParam, ProcessWaitForOutputFinishCallback waitCallback, 
                                     ProcessStepPowerCallback pwrCallback, ProcessStepFreeCallback freeCallback);

void process_free_output(OutputProcessStep first);

enum BOOLEAN_VALUE process_run(InputProcessStep inputSteps, OutputProcessStep outputSteps, struct _MEGAHAL * megahalHandle, MegaHALOptions * megahalOpts, 
                                enum BOOLEAN_VALUE showInput,  enum BOOLEAN_VALUE showOutput, int pollTimeMs, enum BOOLEAN_VALUE consolemode, enum BOOLEAN_VALUE debugThreads,enum BOOLEAN_VALUE debugPower,
                                GPIOHandle gpiohandle);




#endif
