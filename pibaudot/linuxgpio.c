#ifdef __linux__ 

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <poll.h>

#include "internal.h"


#define MAX_PINS 32

typedef struct _GPIOData
{
    ThreadHandle gpioThread;
    enum BOOLEAN_VALUE runInputThread;
    enum BOOLEAN_VALUE debugInputThread;
    GPIOInputTriggerEventCallback inputCallback;
    void * inputCallbackParam;
    enum BOOLEAN_VALUE haveInputFuncSet;

    int pinFd[MAX_PINS];
    int pinNums[MAX_PINS];
    enum BOOLEAN_VALUE isOutput[MAX_PINS];
    enum BOOLEAN_VALUE currentState[MAX_PINS];
    enum BOOLEAN_VALUE initialised[MAX_PINS];
} GPIOData;

static enum BOOLEAN_VALUE writePinFd(int fd, enum BOOLEAN_VALUE val)
{
    int w;
    if(fd == -1)
        return BOOL_TRUE;

    w = write(fd, val ? "1" : "0", 1);

    return (w == 1);
}

static enum BOOLEAN_VALUE readPinFd(int fd, int pinnum)
{
    int r;
    char value_str[3];
    
    if(fd == -1)
        return BOOL_FALSE;

    lseek(fd, 0, SEEK_SET);

    r = read(fd, value_str, 3);
    if(r == -1)
        return BOOL_FALSE;

    //printf("Input Pin %d is [%s]\r\n", pinnum, value_str);

    return (value_str[0] == '1') ? BOOL_TRUE : BOOL_FALSE;
}

static void  unexportPin(unsigned int pin)
{
    char pinBuf[256];    
    int fd;
    int bytes_written;
    int res;
    

    // Export the desired pin by writing to /sys/class/gpio/export
    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (fd == -1) 
    {
        printf("GPIO: Unable to open /sys/class/gpio/unexport \r\n");
        return;
    }

    snprintf(pinBuf, sizeof(pinBuf) - 1, "%d", pin);
    bytes_written = strlen(pinBuf);
    res = write(fd, pinBuf, bytes_written);
    if(res != bytes_written)
    {
        printf("GPIO: Error writing to /sys/class/gpio/unexport for pin %u - result was %d wanted %d - %s\r\n", pin, res, bytes_written, strerror(errno));
        close(fd);
        return;
    }

    close(fd);
}


static int  exportPin(unsigned int pin, enum BOOLEAN_VALUE output)
{
    char pinBuf[256];    
    int fd;
    int bytes_written;
    int res;
    enum BOOLEAN_VALUE retry = BOOL_FALSE;
    
    // Export the desired pin by writing to /sys/class/gpio/export
    fd = open("/sys/class/gpio/export", O_WRONLY);
    if (fd == -1) 
    {
        if(errno == EBUSY)
        {
            printf("GPIO: Busy when trying to open /sys/class/gpio/export - ASSUME ALREADY EXPORTED\r\n");
        }
        else
        {
            printf("GPIO: Unable to open /sys/class/gpio/export \r\n");
            return -1;
        }
    }
    else
    {
        snprintf(pinBuf, sizeof(pinBuf) - 1, "%d", pin);
        bytes_written = strlen(pinBuf);
        res = write(fd, pinBuf, bytes_written);
        if(res != bytes_written)
        {
            if(errno == EBUSY)
            {
                printf("GPIO: Busy when trying to write to /sys/class/gpio/export - ASSUME ALREADY EXPORTED\r\n");
            }
            else
            {
                printf("GPIO: Error writing to /sys/class/gpio/export for pin %u - result was %d wanted %d - %s\r\n", pin, res, bytes_written, strerror(errno));
                close(fd);
                return -1;
            }
        }

        close(fd);
    }

    //May need to wait for a bit before setting the direction to avoid "permission denied" error
    do
    {
        retry = BOOL_FALSE;

        // Set the pin to be an output/input by writing "out"/"in" to /sys/class/gpio/gpio/direction
        sprintf(pinBuf,"/sys/class/gpio/gpio%d/direction", pin);
        fd = open(pinBuf, O_WRONLY);
        if (fd == -1) 
        {
            const int pinerr = errno;
            if(pinerr == EACCES)
            {
                printf("GPIO: Permission Denied when trying to open %s - %s - RETRY\r\n", pinBuf, strerror(pinerr));
                retry = BOOL_TRUE;
            }
            else
            {
                printf("GPIO: Unable to open %s - %s \r\n", pinBuf, strerror(pinerr));
                unexportPin(pin);
                return -1;
            }
        }
        else
        {
            if(output)
            {
                if (write(fd, "out", 3) != 3) 
                {
                    const int pinerr = errno;
                    close(fd);
                    if(pinerr == EACCES)
                    {
                        printf("GPIO: Permission Denied Error writing OUT to %s - %s - RETRY \r\n", pinBuf, strerror(pinerr));
                        retry = BOOL_TRUE;
                    }
                    else
                    {
                        printf("GPIO: Error writing OUT to %s - %s\r\n", pinBuf, strerror(pinerr) );
                        unexportPin(pin);
                        return -1;
                    }
                }
            }
            else
            {
                if (write(fd, "in", 2) != 2) 
                {
                    const int pinerr = errno;
                    close(fd);
                    if(pinerr == EACCES)
                    {
                        printf("GPIO: Permission Denied Error writing OUT to %s - %s - RETRY \r\n", pinBuf, strerror(pinerr));
                        retry = BOOL_TRUE;
                    }
                    else
                    {
                        printf("GPIO: Error writing IN to %s - %s\r\n", pinBuf, strerror(pinerr));
                        unexportPin(pin);
                        return -1;
                    }
                }
            }
        }

        //If retrying, sleep first.
        if(retry)
            usleep(250000); //250 milliseconds
    }
    while(retry);


    close(fd);
    

    //Open actual pin - write for output, read for input
    sprintf(pinBuf,"/sys/class/gpio/gpio%d/value", pin);
    if(output)
    {
        fd = open(pinBuf, O_WRONLY);
    }
    else
    {
        fd = open(pinBuf, O_RDONLY | O_SYNC);
    }

    if (fd == -1) 
    {
        printf("Unable to open %s \r\n", pinBuf);
        unexportPin(pin);
        return -1;
    }

    return fd;
}

static  void BD_CALLBACK gpioInputPollerThread(void * param)
{
    GPIOData * gpiodata = (GPIOData *)param;
    enum BOOLEAN_VALUE newState = BOOL_FALSE;
        
    cppthrd_LockThread(gpiodata->gpioThread);


    while(gpiodata->runInputThread) 
    {
        
        if(gpiodata->inputCallback != NULL)
        {
            //Poll for events
            for(size_t x=0; x < MAX_PINS; ++x)
            {
                uint32_t curValue = 1 << x;
                if((gpiodata->pinFd[x] != -1) &&   ((curValue & GPIOFunction_All_Input_Functions) != 0))
                {   
                    newState = readPinFd(gpiodata->pinFd[x], gpiodata->pinNums[x]);
                    

                    if((newState != gpiodata->currentState[x]) || (!gpiodata->initialised[x]))
                    {
                        gpiodata->currentState[x] = newState;
                        gpiodata->initialised[x] = BOOL_TRUE;

                            //Call call-back outside the lock
                        const int pinnum = gpiodata->pinNums[x];
                        void * cbparam = gpiodata->inputCallbackParam;
                        GPIOInputTriggerEventCallback cb = gpiodata->inputCallback;
                        if(cb != NULL)
                        {

                            cppthrd_UnlockThread(gpiodata->gpioThread);

                            //Call call-back outside of lock
                            cb( cbparam,  pinnum, (enum GPIOFunction)curValue, newState ? BOOL_TRUE : BOOL_FALSE);
                        
                            cppthrd_LockThread(gpiodata->gpioThread);
                        }
                        
                    }
                }
            }
        }

        //Wait for something, or timeout
        cppthrd_WaitForEvent(gpiodata->gpioThread, gpiodata->haveInputFuncSet ?  1000 : 6000, BOOL_TRUE);
    }

    cppthrd_UnlockThread(gpiodata->gpioThread);
}


GPIOHandle gpio_init(enum BOOLEAN_VALUE debugThread)
{
    GPIOData * gpiodata = (GPIOData *)malloc(sizeof(GPIOData));
    memset(gpiodata,-1,sizeof(GPIOData));

    gpiodata->debugInputThread = debugThread;
    gpiodata->runInputThread = BOOL_TRUE;
    gpiodata->gpioThread = cppthrd_CreateThread( gpiodata, gpioInputPollerThread, gpiodata->debugInputThread);

    return gpiodata;
}

void gpio_free(GPIOHandle gpiohnd)
{
    size_t x;
    GPIOData * gpiodata = (GPIOData *)gpiohnd;
    if(gpiodata == NULL)
        return;

    for(x=0; x < MAX_PINS; ++x)
    {
        if(gpiodata->pinFd[x] != -1)
        {
            //Turn pin off before closing
            if(gpiodata->isOutput[x])
                writePinFd(gpiodata->pinFd[x], BOOL_FALSE);

            close(gpiodata->pinFd[x]);

            gpiodata->pinFd[x] = -1;

            unexportPin(gpiodata->pinNums[x]);
        }
    }

    if(gpiodata->gpioThread != NULL)
    {
        gpiodata->runInputThread = BOOL_FALSE;
        cppthrd_WaitAndDestroyThread(gpiodata->gpioThread);
        gpiodata->gpioThread=NULL;
    }

    free(gpiodata);
}

enum BOOLEAN_VALUE gpio_addConfig(GPIOHandle gpiohnd, enum GPIOFunction func, int pin)
{
    GPIOData * gpiodata = (GPIOData *)gpiohnd;
    uint32_t curValue;
    
    if(pin >= MAX_PINS)
        return BOOL_FALSE;

    for(size_t x=0; x < MAX_PINS; ++x)
    {
        curValue = 1 << x;
        if((curValue & func) == curValue)
        {
            //Export pin for output/input, based on the function 
            gpiodata->isOutput[x] = ((func & GPIOFunction_All_Input_Functions) == 0) ?  BOOL_TRUE : BOOL_FALSE;
            gpiodata->pinFd[x] = exportPin(pin,   gpiodata->isOutput[x]);
            if(gpiodata->pinFd[x] == -1)
            {
                printf("ERROR: Failed to export pin %u for output\r\n",pin);
                return BOOL_FALSE;
            }

            gpiodata->pinNums[x] = pin;
            gpiodata->currentState[x] = BOOL_FALSE;
            gpiodata->initialised[x] = BOOL_FALSE;
            
            //Set pin, but only if it's an output pin
            if((func & GPIOFunction_All_Input_Functions) == 0) 
            {
                if(!writePinFd(gpiodata->pinFd[x], BOOL_FALSE))
                {
                    printf("ERROR: Failed to write to pin %u\r\n",pin);
                    return BOOL_FALSE;
                }
            }
            else
            {
                gpiodata->haveInputFuncSet = BOOL_TRUE;
            }
            

            return BOOL_TRUE;
        }
    }

    return BOOL_FALSE;
}

void gpio_set( GPIOHandle gpiohnd, enum GPIOFunction mask, enum GPIOFunction values )
{
    GPIOData * gpiodata = (GPIOData *)gpiohnd;
    uint32_t curValue;
    enum BOOLEAN_VALUE newstate;

    //Lock
    cppthrd_LockThread(gpiodata->gpioThread);

    //work out which pins to change
    for(size_t x=0; x < MAX_PINS; ++x)
    {
        curValue = 1 << x;
        if(((curValue & mask) == curValue) && (gpiodata->pinFd[x] != -1))
        {
            newstate = ((values & curValue) == curValue) ? BOOL_TRUE : BOOL_FALSE;
            if((gpiodata->currentState[x] != newstate) || (!gpiodata->initialised[x]))
            {
                gpiodata->currentState[x] = newstate;
                gpiodata->initialised[x] = BOOL_TRUE;
                writePinFd(gpiodata->pinFd[x], newstate);
            }
        }
    }

    //Unlock
    cppthrd_UnlockThread(gpiodata->gpioThread);
}

void gpio_setInputTriggerCallback(GPIOHandle gpiohnd, GPIOInputTriggerEventCallback callback, void * param)
{
    GPIOData * gpiodata = (GPIOData *)gpiohnd;

    cppthrd_LockThread(gpiodata->gpioThread);

    gpiodata->inputCallbackParam = param;
    gpiodata->inputCallback = callback;

    cppthrd_StartThread(gpiodata->gpioThread, BOOL_TRUE);

    cppthrd_SignalThreadEvent(gpiodata->gpioThread, BOOL_TRUE);

    cppthrd_UnlockThread(gpiodata->gpioThread);
}

#endif