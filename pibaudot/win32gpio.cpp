#include <malloc.h>
#include <string.h>
#include <Windows.h>
extern "C"
{
    #include "internal.h"
}

static const int s_max_pins = 32;

typedef struct _GPIOConfigEntry
{
    enum GPIOFunction func;
    int pin;
} GPIOConfigEntry;

typedef struct _GPIOData
{
    ThreadHandle gpioThread;
    bool runInputThread;
    BOOLEAN_VALUE debugInputThread;
    GPIOInputTriggerEventCallback inputCallback;
    void * inputCallbackParam;
    bool haveInputFuncSet;

    GPIOConfigEntry configs[32];
    bool pinStates[s_max_pins];
    HANDLE hConsole;
    
}GPIOData;

static  void BD_CALLBACK gpioInputPollerThread(void * param)
{
    GPIOData * gpiodata = (GPIOData *)param;
    
    cppthrd_LockThread(gpiodata->gpioThread);

    while(gpiodata->runInputThread)
    {
        if(gpiodata->inputCallback != NULL)
        {
            for(size_t x=0; x < (sizeof(gpiodata->configs) / sizeof(GPIOConfigEntry)); ++x)
            {
                uint32_t curValue = 1 << x;
                if((curValue & GPIOFunction_All_Input_Functions) != 0)
                {
                    //Check F-Key changing state
                    const int pin = gpiodata->configs[x].pin;
                    SHORT tabKeyState = GetAsyncKeyState( VK_F1 + pin  - 1);

                    // Test high bit - if set, key was down when GetAsyncKeyState was called.
                    bool newState = ((( 1 << 15 ) & tabKeyState ) != 0);
                    if(newState != gpiodata->pinStates[pin])
                    {
                        gpiodata->pinStates[pin] = newState;


                        //Call call-back outside the lock
                        GPIOFunction func =  gpiodata->configs[x].func;
                        void * cbparam = gpiodata->inputCallbackParam;
                        GPIOInputTriggerEventCallback cb = gpiodata->inputCallback;
                        
                        cppthrd_UnlockThread(gpiodata->gpioThread);
                        cb(cbparam , pin, func, newState ? BOOL_TRUE : BOOL_FALSE);
                        cppthrd_LockThread(gpiodata->gpioThread);
                    }
                }
            }
        }

        //Wait for something, or timeout
        cppthrd_WaitForEvent(gpiodata->gpioThread, gpiodata->haveInputFuncSet ?  1000 : 60000, BOOL_TRUE);
    }

    cppthrd_UnlockThread(gpiodata->gpioThread);
}


extern "C" GPIOHandle gpio_init(BOOLEAN_VALUE debugThread)
{
    GPIOData * gpiodata = (GPIOData *)malloc(sizeof(GPIOData));
    memset(gpiodata,0,sizeof(GPIOData));

    //Open and configure console
    gpiodata->hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    if(gpiodata->hConsole ==  INVALID_HANDLE_VALUE)
    {
        free(gpiodata);
        return NULL;
    }

    gpiodata->debugInputThread = debugThread;
    
    gpiodata->runInputThread = true;
    gpiodata->gpioThread = cppthrd_CreateThread( gpiodata, gpioInputPollerThread, gpiodata->debugInputThread);

    return gpiodata;
}

extern "C" void gpio_free(GPIOHandle gpiohnd)
{
    GPIOData * gpiodata = (GPIOData *)gpiohnd;
    if(gpiodata == NULL)
        return;

    if(gpiodata->hConsole != NULL)
    {
        CloseHandle(gpiodata->hConsole);
        gpiodata->hConsole = INVALID_HANDLE_VALUE;
    }

    if(gpiodata->gpioThread != NULL)
    {
        gpiodata->runInputThread = false;
        cppthrd_WaitAndDestroyThread(gpiodata->gpioThread);
        gpiodata->gpioThread=NULL;
    }

    free(gpiodata);
}


extern "C" enum BOOLEAN_VALUE gpio_addConfig(GPIOHandle gpiohnd, enum GPIOFunction func, int pin)
{
    GPIOData * gpiodata = (GPIOData *)gpiohnd;
    
    if(pin >= s_max_pins)
        return BOOL_FALSE;

    //Lock
    cppthrd_LockThread(gpiodata->gpioThread);

    for(size_t x=0; x < (sizeof(gpiodata->configs) / sizeof(GPIOConfigEntry)); ++x)
    {
        uint32_t curValue = 1 << x;
        if((curValue & func) == curValue)
        {
            GPIOConfigEntry * newentry = &gpiodata->configs[x];
            
            if(newentry->func != 0)
            {
                cppthrd_UnlockThread(gpiodata->gpioThread);
                return BOOL_FALSE;
            }

    
            newentry->func = func;
            newentry->pin = pin;

            if((func & GPIOFunction_All_Input_Functions) != 0)
                gpiodata->haveInputFuncSet = true;
            
            cppthrd_UnlockThread(gpiodata->gpioThread);
            return  BOOL_TRUE;
        }
    }

    cppthrd_UnlockThread(gpiodata->gpioThread);
    return BOOL_FALSE;
}


extern "C" void gpio_set( GPIOHandle gpiohnd, enum GPIOFunction mask, enum GPIOFunction values )
{   
    GPIOData * gpiodata = (GPIOData *)gpiohnd;
    bool change = false;
    char tmpbuffer[s_max_pins + 3];
    memset(tmpbuffer,' ',sizeof(tmpbuffer) - 1);
    tmpbuffer[0] = '[';
    tmpbuffer[s_max_pins + 1] = ']';
    tmpbuffer[s_max_pins + 2] = 0;

    if(gpiohnd == NULL)
        return;

    //Lock
    cppthrd_LockThread(gpiodata->gpioThread);


    //work out which pin
    for(size_t x=0; x < (sizeof(gpiodata->configs) / sizeof(gpiodata->configs[0])); ++x)
    {
        uint32_t curValue = 1 << x;

        if(((curValue & mask) == curValue) && (gpiodata->configs[x].func != GPIOFunction_None))
        {
            const int pin = gpiodata->configs[x].pin;
            const bool newState = ((values & curValue) == curValue);
            if(gpiodata->pinStates[pin] != newState)
            {
                change = true;
                gpiodata->pinStates[pin] = newState;
            }
        }
    }

    //If nothing has changed, then don't update
    if(!change)
    {
        cppthrd_UnlockThread(gpiodata->gpioThread);
        return;
    }

    //Blank top row first
    COORD pos = {0, 0};
    DWORD dwBytesWritten = 0;
    WriteConsoleOutputCharacterA(gpiodata->hConsole, tmpbuffer  , sizeof(tmpbuffer) - 1, pos, &dwBytesWritten);

    //Write individual pin values
    for(size_t x=0; x < (sizeof(gpiodata->pinStates) / sizeof(gpiodata->pinStates[0])); ++x)
    {
        COORD pos = {x + 1, 0};
        DWORD dwBytesWritten = 0;
        WriteConsoleOutputCharacterA(gpiodata->hConsole, gpiodata->pinStates[x] ? "1" : "0"  , 1, pos, &dwBytesWritten);
    }
    
    //Unlock
    cppthrd_UnlockThread(gpiodata->gpioThread);
}

extern "C" void gpio_setInputTriggerCallback(GPIOHandle gpiohnd, GPIOInputTriggerEventCallback callback, void * param)
{
    GPIOData * gpiodata = (GPIOData *)gpiohnd;

    cppthrd_LockThread(gpiodata->gpioThread);

    gpiodata->inputCallbackParam = param;
    gpiodata->inputCallback = callback;

    cppthrd_StartThread(gpiodata->gpioThread, BOOL_TRUE);

    cppthrd_SignalThreadEvent(gpiodata->gpioThread, BOOL_TRUE);

    cppthrd_UnlockThread(gpiodata->gpioThread);
}