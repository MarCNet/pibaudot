#ifndef __MEGAHAL__INTERNAL__H__
#define __MEGAHAL__INTERNAL__H__

#include <stdint.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include <stdarg.h>
#include <wchar.h>

#include "internal.h"


/*---------------------------------------------------------------------------*/
#define nmalloc malloc
#define nfree free
#define nrealloc realloc
#define Context
#define MIN(A,B) ((A) < (B) ? (A) : (B))

#define bool enum BOOLEAN_VALUE
#define TRUE BOOL_TRUE
#define FALSE BOOL_FALSE

enum LOG_LEVELS
{
    LOG_MISC,
    LOG_PUBLIC
};

#ifdef __linux__ 
#define SEP "/"
#else
#define SEP "\\"
#endif

#ifdef USE_WCHAR

    #define _T(x) __T(x)
    #define _TEXT(x) __T(x)
    #define __T(x) L ## x

    #define ISALNUM iswalnum
    #define STRLEN  wcslen
    #define STRCPY  wcscpy
    #define STRNCMP wcsncmp
    #define TOLOWER towlower
    #define TOUPPER towupper
    #define STRCHR wcschr
    #define ISSPACE isspace
    #define ISALPHA iswalpha
    #define ISDIGIT iswdigit
#else

    #define _T(x) x
    #define _TEXT(x) x
    #define __T(x) x
    
    #ifdef WIN32
        #define STRDUP _strdup
        #define SNPRINTF _snprintf
    #else
        #define STRDUP strdup
        #define SNPRINTF snprintf
    #endif
    
    #define ISALNUM isalnum
    #define STRLEN  strlen
    #define STRCPY strcpy
    #define STRNCMP strncmp
    #define TOLOWER tolower
    #define TOUPPER toupper
    #define STRCHR strchr
    #define ISSPACE isspace
    #define ISALPHA isalpha
    #define ISDIGIT isdigit
#endif

//=============================================================================
//  MegaHAL Context
//=============================================================================

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Initialize_Context
 *
 *	Purpose:	Set the context of the model to a default value.
 */
void mh_initialize_context(MODEL *model);

/*
 *	Function:	Update_Context
 *
 *	Purpose:	Update the context of the model without adding the symbol.
 */
void mh_update_context(MODEL *model, int symbol);

//=============================================================================
//  MegaHAL Swap
//=============================================================================

/*
 *	Function:	Initialize_Swap
 *
 *	Purpose:	Read a swap structure from a file.
 */
SWAP * mh_initialize_swap(char *filename);

void mh_free_swap(SWAP *swap);

//=============================================================================
//  MegaHAL Dictionary
//=============================================================================

DICTIONARY *mh_realloc_dictionary(DICTIONARY *dictionary);

/*
 *	Function:	Initialize_Dictionary
 *
 *	Purpose:	Add dummy words to the dictionary.
 */
void mh_initialize_dictionary(DICTIONARY *dictionary);

/*
 *	Function:	Initialize_List
 *
 *	Purpose:	Read a dictionary from a file.
 */
DICTIONARY * mh_initialize_list(char *filename);

 /*
 *	Function:	New_Dictionary
 *
 *	Purpose:	Allocate room for a new dictionary.
 */
 DICTIONARY * mh_new_dictionary(void);

 void mh_free_word(STRING word);

 void mh_free_words(DICTIONARY *words);

 /*
 *	Function:	Free_Dictionary
 *
 *	Purpose:	Release the memory consumed by the dictionary.
 */
void mh_free_dictionary(DICTIONARY *dictionary);


/*
 *	Function:	Search_Dictionary
 *
 *	Purpose:	Search the dictionary for the specified word, returning its
 *			position in the index if found, or the position where it
 *			should be inserted otherwise.
 */
int mh_search_dictionary(DICTIONARY *dictionary, STRING word, bool *find);

/*
 *	Function:	Load_Dictionary
 *
 *	Purpose:	Load a dictionary from the specified file.
 */
void mh_load_dictionary(FILE *file, DICTIONARY *dictionary);

/*
 *	Function:	Add_Word
 *
 *	Purpose:	Add a word to a dictionary, and return the identifier
 *			assigned to the word.  If the word already exists in
 *			the dictionary, then return its current identifier
 *			without adding it again.
 */
BYTE2 mh_add_word(DICTIONARY *dictionary, STRING word);

/*
 *	Function:	Find_Word
 *
 *	Purpose:	Return the symbol corresponding to the word specified.
 *			We assume that the word with index zero is equal to a
 *			NULL word, indicating an error condition.
 */
BYTE2 mh_find_word(DICTIONARY *dictionary, STRING word);

/*
 *	Function:	Add_Key
 *
 *	Purpose:	Add a word to the keyword dictionary.
 */
void mh_add_key(MEGAHAL * mh,  DICTIONARY *keys, STRING word);

/*
 *	Function:	Add_Aux
 *
 *	Purpose:	Add an auxilliary keyword to the keyword dictionary.
 */
void mh_add_aux(MEGAHAL * mh, DICTIONARY *keys, STRING word);

/*
 *	Function:	Dissimilar
 *
 *	Purpose:	Return TRUE or FALSE depending on whether the dictionaries
 *			are the same or not.
 */
bool mh_dissimilar(DICTIONARY *words1, DICTIONARY *words2);

bool mh_isrepeating(MODEL * model, DICTIONARY *replywords);

bool mh_dissimilar2(DICTIONARY *words1, DICTIONARY *words2);

/*
 *	Function:	Word_Exists
 *
 *	Purpose:	A silly brute-force searcher for the reply string.
 */
bool mh_word_exists(DICTIONARY *dictionary, STRING word);

/*
 *	Function:	Save_Dictionary
 *
 *	Purpose:	Save a dictionary to the specified file.
 */
void mh_save_dictionary(FILE *file, DICTIONARY *dictionary);

int mh_dictionary_expmem(DICTIONARY *dictionary);

// tries to find words in the main dictionary that arent being used in the model anymore and deletes them and updates everything thats necessary
void mh_trimdictionary(MODEL * model);

//=============================================================================
//  MegaHAL Tree
//=============================================================================

TREE * mh_realloc_tree(TREE *tree);

void mh_free_tree(TREE *tree);

/*
 *	Function:	Load_Tree
 *
 *	Purpose:	Load a tree structure from the specified file.
 */
void mh_load_tree(FILE *file, TREE *node);

/*
 *	Function:	Add_Symbol
 *
 *	Purpose:	Update the statistics of the specified tree with the
 *			specified symbol, which may mean growing the tree if the
 *			symbol hasn't been seen in this context before.
 */
TREE * mh_add_symbol(TREE *tree, BYTE2 symbol);

/*
 *	Function:	Find_Symbol
 *
 *	Purpose:	Return a pointer to the child node, if one exists, which
 *			contains the specified symbol.
 */
TREE * mh_find_symbol(TREE *node, int symbol);

/*
 *	Function:	Save_Tree
 *
 *	Purpose:	Save a tree structure to the specified file.
 */
void mh_save_tree(FILE *file, TREE *node);

// returns the amount of nodes/leaves in a tree by recursing through all its branches
int mh_recurse_tree(TREE *node);

// this decrements the usage and count counters in a branch and deletes branches that arent used anymore
void mh_decrement_tree(TREE *node, TREE *parent);

//=============================================================================
//  MegaHAL Node
//=============================================================================
/*
 *	Function:	New_Node
 *
 *	Purpose:	Allocate a new node for the n-gram tree, and initialise
 *			its contents to sensible values.
 */
TREE * mh_new_node(void);

/*
 *	Function:	Search_Node
 *
 *	Purpose:	Perform a binary search for the specified symbol on the
 *			subtree of the given node.  Return the position of the
 *			child node in the subtree if the symbol was found, or the
 *			position where it should be inserted to keep the subtree
 *			sorted if it wasn't.
 */
int mh_search_node(TREE *node, int symbol, bool *found_symbol);

/*
 *	Function:	Add_Node
 *
 *	Purpose:	Attach a new child node to the sub-tree of the tree
 *			specified.
 */
void mh_add_node(TREE *tree, TREE *node, int position);

//=============================================================================
//  MegaHAL Model
//=============================================================================

/*
 *	Function:	New_Model
 *
 *	Purpose:	Create and initialise a new ngram model.
 */
MODEL * mh_new_model(int order);

void mh_free_model(MODEL *model);

/*
 *	Function:	Load_Model
 *
 *	Purpose:	Load a model into memory.
 */
bool mh_load_model(char *filename, MODEL *model);

void mh_save_model(char *filename, MODEL *model);

BYTE2 ** mh_realloc_phrase(MODEL *model);

/*
 *	Function:	Update_Model
 *
 *	Purpose:	Update the model with the specified symbol.
 */
void mh_update_model(MODEL *model, int symbol);



//=============================================================================
//  MegaHAL Utilities
//=============================================================================
/*
 *	Function:	Error
 *
 *	Purpose:	Print the specified message to the error file.
 */
void mh_error(char *title, char *fmt, ...);

void mh_warn(char *title, char *fmt, ...);

void mh_putlog(int lvl, char *title, char *fmt, ...);

#ifdef USE_WCHAR
wchar_t * mh_locale_to_wchar(char *str);

char * mh_wchar_to_locale(wchar_t *str);

#endif

const CHARTYPE * mh_mystrstr(const CHARTYPE *s, const CHARTYPE *sub);

CHARTYPE * mh_mystrdup(const CHARTYPE *s);


/*
 *	Function:	Capitalize
 *
 *	Purpose:	Convert a string to look nice.
 */
void mh_capitalize(CHARTYPE *string);

/*
 *	Function:	Upper
 *
 *	Purpose:	Convert a string to its uppercase representation.
 */
void mh_upper(CHARTYPE *string);

/*
 *	Function:	Make_Words
 *
 *	Purpose:	Break a string into an array of words.
 */
void mh_make_words(CHARTYPE *pinput, DICTIONARY *words);

/*---------------------------------------------------------------------------*/

/*
 *	Function:	Wordcmp
 *
 *	Purpose:	Compare two words, and return an integer indicating whether
 *			the first word is less than, equal to or greater than the
 *			second word.
 */
int mh_wordcmp(STRING word1, STRING word2);

/*
 *	Function:	Rnd
 *
 *	Purpose:	Return a random integer between 0 and range-1.
 */
int mh_rnd(int range);

#endif //__MEGAHAL__INTERNAL__H__